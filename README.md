# AthenaProject

AthenaProject
# LV Athena Website

Website redesign and re-platform to Sitecore from C5.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Frontend

[Read Frontend specific docs](src/Frontend/README-FRONTEND.md) for notes on setup and, build and deploying.

### Prerequisites

* Visual Studio 2019
* SQL Server Developer Edition or SQL Express (SIM will require you to have an SQL login which uses sql authentication e.g. a username and password)
* Sitecore Instance Manager
* Sitecore 8.2 update 5
* Node LTS version https://nodejs.org/en/download/
* Gulp https://gulpjs.com/

### Installing

This project assumes the following settings:

Git repository location: C:\projects\athena\
Website location: C:\inetpub\wwwroot\athena
Website URL (master) http://athena.local/
Website URL (web) 	 http://athena.web.local/ 

_Note:_ The `athena.web.local` url may need to be configured manually in both IIS and in the hosts file
The hosts file can be edited through `SIM -> Bundled tools -> Hosts Editor` or directly `C:\Windows\System32\drivers\etc\hosts`
127.0.0.1 athena.web.local


To change these settings, see the "Configuring your settings" below

1. Clone this repository into `C:\projects\athena`, most git tools will try and clone it into a folder called website this wont work by default
1. Make sure the Sitecore [nuget feed](https://sitecore.myget.org/F/sc-packages/api/v3/index.json) is added to your Visual Studio [Package Sources](https://docs.microsoft.com/en-us/vsts/package/nuget/consume)
1. Install an instance of Sitecore 8.2 Update 5 using SIM into `C:\inetpub\wwwroot\athena` with a url of `http://athena.local/`.  You can disable xDB when you install the instance.
1. Open a CLI at the root of your repository
1. Install the windows build tools package `npm install --global windows-build-tools` this is a one time action
1. Run `npm install`
1. Run `gulp`
1. Load http://athena.local/ and check that the home pages loads


In Project Source Code Before Publishing do following changes:

In z.Adviser.DevSettings.config change the data folder and source folder 

<sc.variable name="dataFolder" value="D:\websites\Athena\Data" />
        <sc.variable name="sourceFolder" value="D:\projects\athenaproject\src" />
        <sc.variable name="rootHostName" value="local" />