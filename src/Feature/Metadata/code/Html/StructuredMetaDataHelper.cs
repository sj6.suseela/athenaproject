﻿using System.Web;
using System.Web.Mvc;
using Sitecore.Mvc;

namespace Athena.Feature.Metadata.Html
{
	public static class StructuredMetaDataHelper
	{
		public static HtmlString RenderStructuredMetaData(this HtmlHelper helper)
		{
			var currentItem = helper.Sitecore().CurrentItem;
			return new HtmlString(currentItem[Templates.StructuredMetaData.Fields.StructuredMetaData]);
		}
	}
}