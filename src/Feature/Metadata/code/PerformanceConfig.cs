﻿using Athena.Foundation.Common;

namespace Athena.Feature.Metadata
{
	internal static class PerformanceConfig
	{
		internal static bool EnsightenAsyncEnabled = AppSettings.Get<bool>("Performance.EnsightenAsyncEnabled");
	}
}