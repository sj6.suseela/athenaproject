﻿namespace Athena.Feature.Metadata.Models
{
	public class Twitter
	{
		public string Card { get; set; }

		public string Title { get; set; }

		public string Description { get; set; }

		public string Image { get; set; }

		public string Player { get; set; }

		public string PlayerWidth { get; set; }

		public string PlayerHeight { get; set; }
	}
}