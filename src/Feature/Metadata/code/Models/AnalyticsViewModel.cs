﻿namespace Athena.Feature.Metadata.Models
{
	public class AnalyticsHeaderViewModel
	{
		public string GoogleTagManagerContainerID { get; set; }

		public bool GoogleTagManagerEnabled { get; set; }

		public bool MaxymiserDisabled { get; set; }

		public bool EnsightenEnabled { get; set; }

		public bool EnsightenAsyncEnabled { get; set; }

		public string MaxymiserUrl { get; set; }

		public string EnsightenUrl { get; set; }
	}

	public class AnalyticsBodyViewModel
	{
		public string GoogleTagManagerContainerID { get; set; }

		public bool GoogleTagManagerEnabled { get; set; }
	}
}