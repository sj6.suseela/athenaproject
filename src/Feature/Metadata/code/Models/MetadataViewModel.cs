﻿namespace Athena.Feature.Metadata.Models
{
	public class MetadataViewModel
	{
		public string Title { get; set; }

		public string Description { get; set; }

		public string CanonicalUrl { get; set; }
		public string CanonicalQueryString { get; set; }

		public string Next { get; set; }
		public string Previous { get; set; }

		public bool CanIndex { get; set; }

		public bool FollowLinks { get; set; }

		public bool ShowSnippet { get; set; }

		public bool Archive { get; set; }

		public OpenGraph OpenGraph { get; set; } = new OpenGraph();

		public Twitter Twitter { get; set; } = new Twitter();
	}
}
