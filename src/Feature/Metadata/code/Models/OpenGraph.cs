﻿namespace Athena.Feature.Metadata.Models
{
	public class OpenGraph
	{
		public string SiteName { get; set; }

		public string Title { get; set; }

		public string Description { get; set; }

		public string Image { get; set; }

		public string Video { get; set; }

		public string Locale { get; set; }

		public string Type { get; set; }
	}
}