﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Athena.Foundation.Multisite.Utils;
using Athena.Foundation.SitecoreExtensions.Extensions;

namespace Athena.Feature.Metadata.sitecore.admin
{
	public partial class FixAnalyticsRenderings : System.Web.UI.Page
	{
		public readonly string AnalyticsHeadRendering = "{A7DB29F2-22C4-4530-A2DA-18DE88B3F204}";
		public readonly string AnalyticsBodyRendering = "{71EFB5F1-2C0C-4915-A9BD-82D3432396F4}";

		public readonly string AnalyticsSettingsPath = "/sitecore/content/Athena/Settings/Analytics Settings";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Sitecore.Context.IsAdministrator)
			{
				Response.Redirect("~/sitecore/login?returnUrl=%2fsitecore%2fadmin%2fFixAnalyticsRenderings%2Easpx", true);
			}

			if (!Page.IsPostBack)
			{
				if (txtRenderingIds.Text.HasNoValue())
					txtRenderingIds.Text = $"{AnalyticsHeadRendering}|{AnalyticsBodyRendering}";

				if (txtSourcesForReplacement.Text.HasNoValue())
					txtSourcesForReplacement.Text = "site:analyticssettings|{963E5995-630A-4F3D-B960-0E3BD7AA61A5}";

				if (txtDatasource.Text.HasNoValue())
					txtDatasource.Text = AnalyticsSettingsPath;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			//  get all children with layouts
			// find all AnalyticsHead and AnalyticsBody renderings
			// if empty DS, fill with ID for analytics settings item
			if (txtDatasource.Text.HasNoValue() || txtPath.Text.HasNoValue() || txtRenderingIds.Text.HasNoValue())
			{
				lblMessage.Text = "Please fill in all fields";
				return;
			}

			var db = Sitecore.Data.Database.GetDatabase("master");
			var parentPath = db.GetItem(txtPath.Text);
			if (parentPath == null)
			{
				lblMessage.Text = "Parent path is not found!";
				return;
			}

			var ds = db.GetItem(txtDatasource.Text);
			if (ds == null)
			{
				lblMessage.Text = "Target datasource is not found!";
				return;
			}

			var renderingIds = txtRenderingIds.Text.Split(new [] {'|'}, StringSplitOptions.RemoveEmptyEntries);
			var sourcesForReplacement = txtSourcesForReplacement.Text.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

			int pagesChecked = 0;
			int renderingsChanged = 0;
			int errorCount = 0;
			string errors = string.Empty;

			var pages = chkDescendents.Checked ? parentPath.Axes.GetDescendants().Where(i => i.HasLayout()) : parentPath.Children.Where(i => i.HasLayout());
			
			foreach (var page in pages)
			{
				try
				{
					LayoutUtils.ApplyActionToAllSharedRenderings(page, definition =>
					{
						var source = definition.Datasource;
						if (!string.IsNullOrEmpty(source) && !sourcesForReplacement.Contains(source))
							return;

						if (!renderingIds.Contains(definition.ItemID))
							return;

						string datasource = ds.ID.ToString();
						definition.Datasource = datasource;
						renderingsChanged++;
					});
					pagesChecked++;
				}
				catch (Exception exception)
				{
					errorCount++;
					errors = errors.HasNoValue() ? exception.Message : $"{errors}<br/>{exception.Message}";
				}
			}

			lblMessage.Text =
				$"Completed with {pagesChecked} pages checked. {renderingsChanged} renderings updated. {errorCount} errors.";
			lblErrors.Text = errors;
		}
	}
}