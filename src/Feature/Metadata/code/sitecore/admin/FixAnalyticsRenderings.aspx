﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FixAnalyticsRenderings.aspx.cs" Inherits="LV.Feature.Metadata.sitecore.admin.FixAnalyticsRenderings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fix Analytics Renderings</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Parent Path:<br/><asp:TextBox runat="server" ID="txtPath" Text="/sitecore/content/LV" Width="355"/><br/>
            <asp:CheckBox runat="server" ID="chkDescendents" Text="Apply to all Descendants (May be slow)" Checked="True"/><br/>
            Renderings:<br/><asp:TextBox runat="server" ID="txtRenderingIds" TextMode="MultiLine" Width="355"/><br/>
            Sources from replacement:<br/><asp:TextBox runat="server" ID="txtSourcesForReplacement" TextMode="MultiLine" Width="355"/><br/>
            Target Datasource Item:<br/><asp:TextBox runat="server" ID="txtDatasource" Width="355"/><br/>
            <asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click"/><br/>
            <asp:Label runat="server" ID="lblMessage"></asp:Label><br/>
            <asp:Label runat="server" ID="lblErrors"></asp:Label>
        </div>
    </form>
</body>
</html>
