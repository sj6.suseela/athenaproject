﻿using Athena.Feature.Metadata.Services;
using Athena.Foundation.DependencyInjection.Pipelines;
using Microsoft.Extensions.DependencyInjection;

namespace Athena.Feature.Metadata.Pipeline
{
    public class RegisterServices
    {
        public void Process(InitializeDependencyInjectionArgs args)
        {
            args.ServiceCollection.AddScoped<IMetadataService, MetadataService>();
        }
    }
}