﻿using Sitecore.Data;

namespace Athena.Feature.Metadata
{
	internal struct Templates
	{
		internal struct Analytics
		{
			public struct Fields
			{
				public static readonly ID GoogleTagManagerEnabled = new ID("{87F4A0F0-5F48-42F4-B54A-FCCF4E55EDDC}");
				public static readonly ID MaxymiserDisabled = new ID("{7197AFB8-2900-4EDB-8614-CF9BC683EBCF}");
				public static readonly ID EnsightenEnabled = new ID("{581AF35E-45C1-4EE4-B6D9-276A82F75894}");
			}
		}

		internal struct AnalyticsSettings
		{
			public static readonly ID TemplateId = new ID("{CA94FE23-0D00-4CB3-89DA-1FA3CA556B30}");

			public struct Fields
			{
				public static readonly ID GoogleTagManagerContainerID = new ID("{C6B986FF-0264-4EE7-A9B9-94B59229A649}");
				public static readonly ID MaxymiserUrl = new ID("{4895834E-AC86-418B-AA92-2641CC7A7BCD}");
				public static readonly ID EnsightenUrl = new ID("{2C7C0AD3-2307-461B-84B0-AE5EC1B5A480}");
			}
		}

		internal struct PageMetadata
		{
			public static ID ID = new ID("{26E942AF-7487-4F63-A6E5-E9B3E007EFE7}");

			public struct Fields
			{
				public static readonly ID Title = new ID("{5FF0B777-6C2D-46F3-AD8C-A5012BA48798}");
				public static readonly ID Description = new ID("{C1BA23B7-6E33-43BE-8A16-F4BA6B450F92}");
				public static readonly ID CanIndex = new ID("{2D5958D1-A618-4B63-B544-2EB2EFBD8371}");
				public static readonly ID FollowLinks = new ID("{130AA914-7CF3-4B9D-AF10-2E98550E58FC}");
				public static readonly ID ShowSnippet = new ID("{4C3E87AC-C1A7-4705-8C56-61921FAE50F8}");
				public static readonly ID Archive = new ID("{C5553B9D-6613-46DA-9BFC-77D82CB03F6E}");
			}
		}

		internal struct OpenGraphSiteSettings
		{
			public static readonly ID ID = new ID("{04478DA8-8281-4228-B018-87A040D83E09}");

			public struct Fields
			{
				public static readonly ID SiteName = new ID("{31BCE85A-5B22-409A-820F-E7E25B05C8A1}");
				public static readonly ID DefaultType = new ID("{41FC5B85-5559-4B89-8952-DAE01266E503}");
				public static readonly ID DefaultLocale = new ID("{A96F87BE-C81B-4B1A-9711-DB905BC4F957}");
			}
		}

		internal struct OpenGraphSettings
		{
			public struct Fields
			{
				public static readonly ID Title = new ID("{59FA55BA-7C22-47B8-815B-91DDF8F6D3D2}");
				public static readonly ID Description = new ID("{03FF021D-376A-415D-8E34-8A6ACD160C70}");
				public static readonly ID Image = new ID("{7BC8D9E3-38A2-407E-80ED-079280170A2F}");
				public static readonly ID Video = new ID("{B7157C0A-DEC0-4B75-9372-804D99ED6191}");
				public static readonly ID Locale = new ID("{DB9988C1-5B81-4DFF-8F96-6EF34330C898}");
				public static readonly ID Type = new ID("{5702FBD8-360A-4B62-8CE0-C6F97FEC1EF5}");
			}
		}

		internal struct StructuredMetaData
		{
			public static readonly ID Id = new ID("{F29D72AB-C024-47BA-895C-73193D971478}");

			public struct Fields
			{
				public static readonly ID StructuredMetaData = new ID("{6A3CE0E5-EBC6-44E0-BD33-16D57EE6DBC0}");
			}

		}

		internal struct Twitter
		{
			public struct Fields
			{
				public static readonly ID Card = new ID("{4EF5E7D6-B8C5-4414-A98B-2EF0A728C49A}");
				public static readonly ID Title = new ID("{648D8C71-B7A2-46CE-8587-D2765FFDAFD7}");
				public static readonly ID Description = new ID("{25D6EDB3-EDD7-4746-A400-5CFF0B707274}");
				public static readonly ID Image = new ID("{0AAED4ED-825F-458C-B4F5-1400A0FF337C}");
				public static readonly ID Player = new ID("{8F386355-E23D-45C2-B730-E6DEA7C58174}");
				public static readonly ID PlayerWidth = new ID("{D1BACC8A-1AF6-461C-A117-8E9DF8060AB3}");
				public static readonly ID PlayerHeight = new ID("{75805C35-A099-4DE3-9C5C-BB0FCD04191A}");
			}
		}
	}
}