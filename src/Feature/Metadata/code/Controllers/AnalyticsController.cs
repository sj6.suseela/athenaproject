﻿using System.Web.Mvc;
using Athena.Feature.Metadata.Models;
using Athena.Foundation.Common;
using Sitecore;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Metadata.Controllers
{
	public class AnalyticsController : Controller
	{
		public ActionResult Head()
		{
			var settings = RenderingContext.Current?.Rendering?.Item ?? RenderingContext.Current?.ContextItem;
			if (settings == null)
				return null;

			var page  = Context.Item;

			var model = new AnalyticsHeaderViewModel
			{
				GoogleTagManagerContainerID = settings[Templates.AnalyticsSettings.Fields.GoogleTagManagerContainerID],
				MaxymiserUrl = settings[Templates.AnalyticsSettings.Fields.MaxymiserUrl],
				GoogleTagManagerEnabled = MainUtil.GetBool(page[Templates.Analytics.Fields.GoogleTagManagerEnabled], false),
				MaxymiserDisabled = MainUtil.GetBool(page[Templates.Analytics.Fields.MaxymiserDisabled], false),
				EnsightenEnabled = MainUtil.GetBool(page[Templates.Analytics.Fields.EnsightenEnabled], false),
				EnsightenAsyncEnabled = PerformanceConfig.EnsightenAsyncEnabled,
				EnsightenUrl = settings[Templates.AnalyticsSettings.Fields.EnsightenUrl]
			};

			return View(model);
		}

		public ActionResult Body()
		{
			var settings = RenderingContext.Current?.Rendering?.Item ?? RenderingContext.Current?.ContextItem;
			if (settings == null)
				return null;

			var page = Context.Item;

			var model = new AnalyticsBodyViewModel
			{
				GoogleTagManagerContainerID = settings[Templates.AnalyticsSettings.Fields.GoogleTagManagerContainerID],
				GoogleTagManagerEnabled = MainUtil.GetBool(page[Templates.Analytics.Fields.GoogleTagManagerEnabled], false)
			};

			return View(model);
		}
	}
}