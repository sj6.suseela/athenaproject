﻿using System.Web.Mvc;
using Athena.Feature.Metadata.Services;
using Sitecore;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Metadata.Controllers
{
    public class MetadataController : Controller
    {
        private readonly IMetadataService _metaDataService;

        public MetadataController(IMetadataService metadataService)
        {
            _metaDataService = metadataService;
        }

        public ActionResult PageMetaData()
        {
            var item = RenderingContext.Current?.Rendering?.Item ?? RenderingContext.Current?.ContextItem;
			if (item == null)
                return null;

	        var database = Context.ContentDatabase ?? Context.Database;

			var metadata = _metaDataService.GetMetadata(database, item);

            return View(metadata);
        }
    }
}