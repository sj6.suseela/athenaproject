﻿using System.Linq;
using System.Web;
using Athena.Feature.Metadata.Models;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Athena.Feature.Metadata.Services
{
	public class MetadataService : IMetadataService
	{
		public MetadataViewModel GetMetadata(Database database, Item page)
		{
			var viewModel = new MetadataViewModel();

			viewModel.CanonicalUrl = page.CanonicalUrl();
			viewModel.CanonicalQueryString = GetHttpContextItemOrNull("canonical-querystring");
			viewModel.Next = GetHttpContextItemOrNull("nextPageUrl");
			viewModel.Previous = GetHttpContextItemOrNull("previousPageUrl");

			viewModel.Title = page[Templates.PageMetadata.Fields.Title];
			viewModel.Description = page[Templates.PageMetadata.Fields.Description];
			viewModel.CanIndex = MainUtil.GetBool(page[Templates.PageMetadata.Fields.CanIndex], false);
			viewModel.FollowLinks = MainUtil.GetBool(page[Templates.PageMetadata.Fields.FollowLinks], false);
			viewModel.ShowSnippet = MainUtil.GetBool(page[Templates.PageMetadata.Fields.ShowSnippet], false);
			viewModel.Archive = MainUtil.GetBool(page[Templates.PageMetadata.Fields.Archive], false);

			GetOpenGraphFields(database, page, viewModel);

			GetTwitterFields(page, viewModel);

			return viewModel;
		}

		private static string GetHttpContextItemOrNull(string key)
		{
			var result = HttpContext.Current.Items[key] as string;
			return string.IsNullOrWhiteSpace(result) ? null : result;
		}

		private static void GetOpenGraphFields(Database database, Item page, MetadataViewModel viewModel)
		{
			var settings = database.GetItem($"{Context.Site.RootPath}/Settings");

			var siteSettings = settings.ChildrenOfTemplate(Templates.OpenGraphSiteSettings.ID)
				.FirstOrDefault();

			if (siteSettings != null)
			{
				viewModel.OpenGraph.SiteName = siteSettings[Templates.OpenGraphSiteSettings.Fields.SiteName];
				viewModel.OpenGraph.Type = siteSettings[Templates.OpenGraphSiteSettings.Fields.DefaultType];
				viewModel.OpenGraph.Locale = siteSettings[Templates.OpenGraphSiteSettings.Fields.DefaultLocale];
			}

			if (page[Templates.OpenGraphSettings.Fields.Type].HasValue())
				viewModel.OpenGraph.Type = page[Templates.OpenGraphSettings.Fields.Type];

			if (page[Templates.OpenGraphSettings.Fields.Locale].HasValue())
				viewModel.OpenGraph.Locale = page[Templates.OpenGraphSettings.Fields.Locale];

			viewModel.OpenGraph.Title = page[Templates.OpenGraphSettings.Fields.Title];
			viewModel.OpenGraph.Description = page[Templates.OpenGraphSettings.Fields.Description];

			ImageField image = page.Fields[Templates.OpenGraphSettings.Fields.Image];
			viewModel.OpenGraph.Image = image.HasImage() ? image.FullImageUrl() : string.Empty;

			LinkField video = page.Fields[Templates.OpenGraphSettings.Fields.Video];
			viewModel.OpenGraph.Video = video.GetFriendlyUrl();
		}

		private static void GetTwitterFields(Item page, MetadataViewModel viewModel)
		{
			viewModel.Twitter.Card = page[Templates.Twitter.Fields.Card];
			viewModel.Twitter.Title = page[Templates.Twitter.Fields.Title];
			viewModel.Twitter.Description = page[Templates.Twitter.Fields.Description];

			ImageField image = page.Fields[Templates.Twitter.Fields.Image];
			viewModel.Twitter.Image = image.HasImage() ? image.FullImageUrl() : string.Empty;

			LinkField player = page.Fields[Templates.Twitter.Fields.Player];
			viewModel.Twitter.Player = player.GetFriendlyUrl();

			viewModel.Twitter.PlayerWidth = page[Templates.Twitter.Fields.PlayerWidth];
			viewModel.Twitter.PlayerHeight = page[Templates.Twitter.Fields.PlayerHeight];
		}
	}
}