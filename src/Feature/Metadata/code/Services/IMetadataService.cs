﻿using Athena.Feature.Metadata.Models;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Athena.Feature.Metadata.Services
{
	public interface IMetadataService
	{
		MetadataViewModel GetMetadata(Database database, Item page);
	}
}