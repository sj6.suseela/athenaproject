﻿using Athena.Feature.BannerHeader.Services;
using Athena.Foundation.DependencyInjection.Pipelines;
using Microsoft.Extensions.DependencyInjection;

namespace Athena.Feature.BannerHeader.Pipelines
{
    public class RegisterServices
    {
        public void Process(InitializeDependencyInjectionArgs args)
        {
            args.ServiceCollection.AddScoped<IBannerHeaderService, BannerHeaderService>();
        }
    }
}