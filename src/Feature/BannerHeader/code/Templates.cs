﻿






















// ReSharper disable InconsistentNaming
namespace Athena.Feature.BannerHeader
{
    public static partial class Templates
    {

        #region /sitecore/templates/Feature/BannerHeader/_BannerHeader
        /// <summary>
        ///   _BannerHeader
        ///   <para>ID: {545CDDF4-908E-4B28-A3B3-258279C6F224}</para>
        ///   <para>Path: /sitecore/templates/Feature/BannerHeader/_BannerHeader</para>
        /// </summary>
        public static class BannerHeader
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{545CDDF4-908E-4B28-A3B3-258279C6F224}"));
            public const string IdString = "{545CDDF4-908E-4B28-A3B3-258279C6F224}";


            public static class Fields
            {

                /// <summary>
                ///   Background Color
                ///   <para>{3FCD7CAE-0157-4DB3-9893-5FADF625CAB2}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID BackgroundColor = new Sitecore.Data.ID("{3FCD7CAE-0157-4DB3-9893-5FADF625CAB2}");

                /// <summary>
                ///   Background Color
                ///   <para>{3FCD7CAE-0157-4DB3-9893-5FADF625CAB2}</para>
                /// </summary>
                public const string BackgroundColor_FieldName = "Background Color";


                /// <summary>
                ///   Brand Icon
                ///   <para>{FAC2E2A5-48AE-43E4-98B1-2AB3DD4BA210}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID BrandIcon = new Sitecore.Data.ID("{FAC2E2A5-48AE-43E4-98B1-2AB3DD4BA210}");

                /// <summary>
                ///   Brand Icon
                ///   <para>{FAC2E2A5-48AE-43E4-98B1-2AB3DD4BA210}</para>
                /// </summary>
                public const string BrandIcon_FieldName = "Brand Icon";


                /// <summary>
                ///   Content Alignment
                ///   <para>{46B57D53-0BA3-4095-82CF-1DE30970E832}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID ContentAlignment = new Sitecore.Data.ID("{46B57D53-0BA3-4095-82CF-1DE30970E832}");

                /// <summary>
                ///   Content Alignment
                ///   <para>{46B57D53-0BA3-4095-82CF-1DE30970E832}</para>
                /// </summary>
                public const string ContentAlignment_FieldName = "Content Alignment";


                /// <summary>
                ///   Enable Bottom Curve
                ///   <para>{B9C4AC34-4A6A-4689-80A2-4E5540017F94}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID EnableBottomCurve = new Sitecore.Data.ID("{B9C4AC34-4A6A-4689-80A2-4E5540017F94}");

                /// <summary>
                ///   Enable Bottom Curve
                ///   <para>{B9C4AC34-4A6A-4689-80A2-4E5540017F94}</para>
                /// </summary>
                public const string EnableBottomCurve_FieldName = "Enable Bottom Curve";


                /// <summary>
                ///   GIF Image
                ///   <para>{112CEBA4-FDC8-4091-828C-0E51BBFE8A23}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID GIFImage = new Sitecore.Data.ID("{112CEBA4-FDC8-4091-828C-0E51BBFE8A23}");

                /// <summary>
                ///   GIF Image
                ///   <para>{112CEBA4-FDC8-4091-828C-0E51BBFE8A23}</para>
                /// </summary>
                public const string GIFImage_FieldName = "GIF Image";


                /// <summary>
                ///   Image
                ///   <para>{E9CC791E-0933-4B31-A8A9-DA4F5200BAAC}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Image = new Sitecore.Data.ID("{E9CC791E-0933-4B31-A8A9-DA4F5200BAAC}");

                /// <summary>
                ///   Image
                ///   <para>{E9CC791E-0933-4B31-A8A9-DA4F5200BAAC}</para>
                /// </summary>
                public const string Image_FieldName = "Image";


                /// <summary>
                ///   Main Heading Mobile
                ///   <para>{81A1AB9C-4218-4460-843C-8ED9E7194F3E}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MainHeadingMobile = new Sitecore.Data.ID("{81A1AB9C-4218-4460-843C-8ED9E7194F3E}");

                /// <summary>
                ///   Main Heading Mobile
                ///   <para>{81A1AB9C-4218-4460-843C-8ED9E7194F3E}</para>
                /// </summary>
                public const string MainHeadingMobile_FieldName = "Main Heading Mobile";


                /// <summary>
                ///   Main Heading Type
                ///   <para>{9D88919E-8EDF-40D4-A0E7-1BB5AC2C3030}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MainHeadingType = new Sitecore.Data.ID("{9D88919E-8EDF-40D4-A0E7-1BB5AC2C3030}");

                /// <summary>
                ///   Main Heading Type
                ///   <para>{9D88919E-8EDF-40D4-A0E7-1BB5AC2C3030}</para>
                /// </summary>
                public const string MainHeadingType_FieldName = "Main Heading Type";


                /// <summary>
                ///   Main Heading
                ///   <para>{154E1BDB-791F-4BF3-A3FF-47FD92026737}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MainHeading = new Sitecore.Data.ID("{154E1BDB-791F-4BF3-A3FF-47FD92026737}");

                /// <summary>
                ///   Main Heading
                ///   <para>{154E1BDB-791F-4BF3-A3FF-47FD92026737}</para>
                /// </summary>
                public const string MainHeading_FieldName = "Main Heading";


                /// <summary>
                ///   Media Type
                ///   <para>{34AB2943-79BC-459F-980D-13A5EA7E3CA1}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MediaType = new Sitecore.Data.ID("{34AB2943-79BC-459F-980D-13A5EA7E3CA1}");

                /// <summary>
                ///   Media Type
                ///   <para>{34AB2943-79BC-459F-980D-13A5EA7E3CA1}</para>
                /// </summary>
                public const string MediaType_FieldName = "Media Type";


                /// <summary>
                ///   Small Heading Mobile
                ///   <para>{B46DB8F8-6AA7-4941-8D76-081AE6526076}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID SmallHeadingMobile = new Sitecore.Data.ID("{B46DB8F8-6AA7-4941-8D76-081AE6526076}");

                /// <summary>
                ///   Small Heading Mobile
                ///   <para>{B46DB8F8-6AA7-4941-8D76-081AE6526076}</para>
                /// </summary>
                public const string SmallHeadingMobile_FieldName = "Small Heading Mobile";


                /// <summary>
                ///   Small Heading Type
                ///   <para>{A9A3F00C-603D-4BAA-90D7-F6A9986CB5A2}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID SmallHeadingType = new Sitecore.Data.ID("{A9A3F00C-603D-4BAA-90D7-F6A9986CB5A2}");

                /// <summary>
                ///   Small Heading Type
                ///   <para>{A9A3F00C-603D-4BAA-90D7-F6A9986CB5A2}</para>
                /// </summary>
                public const string SmallHeadingType_FieldName = "Small Heading Type";


                /// <summary>
                ///   Small Heading
                ///   <para>{C2E6018D-FE74-4D0A-8B94-56E219486B50}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID SmallHeading = new Sitecore.Data.ID("{C2E6018D-FE74-4D0A-8B94-56E219486B50}");

                /// <summary>
                ///   Small Heading
                ///   <para>{C2E6018D-FE74-4D0A-8B94-56E219486B50}</para>
                /// </summary>
                public const string SmallHeading_FieldName = "Small Heading";


                /// <summary>
                ///   Sub Text Link
                ///   <para>{961DB5FF-A5D6-4B1C-8B12-2A5D289EE969}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID SubTextLink = new Sitecore.Data.ID("{961DB5FF-A5D6-4B1C-8B12-2A5D289EE969}");

                /// <summary>
                ///   Sub Text Link
                ///   <para>{961DB5FF-A5D6-4B1C-8B12-2A5D289EE969}</para>
                /// </summary>
                public const string SubTextLink_FieldName = "Sub Text Link";


                /// <summary>
                ///   Video ID
                ///   <para>{3AE604DB-7C75-40EA-801B-AB6CB71E8469}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID VideoID = new Sitecore.Data.ID("{3AE604DB-7C75-40EA-801B-AB6CB71E8469}");

                /// <summary>
                ///   Video ID
                ///   <para>{3AE604DB-7C75-40EA-801B-AB6CB71E8469}</para>
                /// </summary>
                public const string VideoID_FieldName = "Video ID";


            }

        }
        #endregion

    }
}


