﻿using System.Web.Mvc;
using Athena.Feature.BannerHeader.Services;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.BannerHeader.Controllers
{
    public class BannerHeaderController : Controller
    {
		// GET: BannerHeader
		private readonly IBannerHeaderService _bannerHeaderService;

		public BannerHeaderController(IBannerHeaderService bannerHeaderService)
		{
			_bannerHeaderService = bannerHeaderService;
		}


		public ActionResult BannerHeader()
		{
			var viewModel = _bannerHeaderService.GetBannerHeader(RenderingContext.Current, Context.Item);
			
			return this.GetModuleView(() => View(viewModel));
		}

		public ActionResult BannerHeaderOval()
		{
			var viewModel = _bannerHeaderService.GetBannerHeaderOval(RenderingContext.Current, Context.Item);
			
			return this.GetModuleView(() => View(viewModel));
		}

	}

}