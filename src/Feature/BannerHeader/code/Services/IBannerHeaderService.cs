﻿using Athena.Feature.BannerHeader.Models;
using Athena.Foundation.React.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;


namespace Athena.Feature.BannerHeader.Services
{
	public interface IBannerHeaderService
	{
		AbstractComponentModel GetBannerHeader(RenderingContext renderingContext, Item page);

		AbstractComponentModel GetBannerHeaderOval(RenderingContext renderingContext, Item page);
	}
}
