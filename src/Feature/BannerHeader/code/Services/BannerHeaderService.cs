﻿
using Athena.Feature.BannerHeader.Models;
using Athena.Foundation.Elements.Extensions;
using Athena.Foundation.React.Models;
using Athena.Foundation.ReusableTemplates;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Resources.Media;

namespace Athena.Feature.BannerHeader.Services
{
	public class BannerHeaderService : IBannerHeaderService
	{
		public AbstractComponentModel GetBannerHeader(RenderingContext renderingContext, Item page)
		{
			
			var item = RenderingContext.Current?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.BannerHeader.ID))
			{
				return null;
			}

			AbstractComponentModel componentModel = new AbstractComponentModel();
			var viewModel = new BannerHeaderDataModel();
			SetCommonModelElements(item, viewModel);
			if (viewModel.MediaType.Equals("image"))
			{
				viewModel.AdvancedImage = item.GetAdavncedImageElement(Templates.BannerHeader.Fields.Image, new REND_1536_1152(), new REND_2880_640());
			}
			else if (viewModel.MediaType.Equals("gif"))
			{
				viewModel.GIFImageURL = item.GetAdavncedImageElement(Templates.BannerHeader.Fields.GIFImage, new REND_1536_1152(), new REND_2880_640());
			}
			componentModel.ComponentName = "bannerHeader";
			componentModel.setComponentData(viewModel);
			return componentModel;

		}

		public AbstractComponentModel GetBannerHeaderOval(RenderingContext renderingContext, Item page)
		{

			var item = RenderingContext.Current?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.BannerHeader.ID))
			{
				return null;
			}

			AbstractComponentModel componentModel = new AbstractComponentModel();
			var viewModel = new BannerHeaderDataModel();
			SetCommonModelElements(item, viewModel);
			if (viewModel.MediaType.Equals("image"))
			{
				viewModel.AdvancedImage = item.GetAdavncedImageElement(Templates.BannerHeader.Fields.Image, new REND_1536_1152(), new REND_1620_640());
			}
			else if (viewModel.MediaType.Equals("gif"))
			{
				viewModel.GIFImageURL = item.GetAdavncedImageElement(Templates.BannerHeader.Fields.GIFImage, new REND_1536_1152(), new REND_1620_640());
			}
			componentModel.setComponentData(viewModel);
			componentModel.ComponentName = "bannerHeaderOval";
			return componentModel;

		}

		private BannerHeaderDataModel SetCommonModelElements(Item item, BannerHeaderDataModel viewModel)
		{
			viewModel.SmallHeading = new Heading
			{
				Title = item.RenderField(Templates.BannerHeader.Fields.SmallHeading),
				Type = item.GetLookupValue(Templates.BannerHeader.Fields.SmallHeadingType)
			};
			viewModel.SmallHeadingMobile = item.RenderField(Templates.BannerHeader.Fields.SmallHeadingMobile);
			viewModel.MainHeading = new Heading
			{
				Title = item.RenderRichTextField(Templates.BannerHeader.Fields.MainHeading),
				Type = item.GetLookupValue(Templates.BannerHeader.Fields.MainHeadingType)
			};
			viewModel.MainHeadingMobile = item.RenderRichTextField(Templates.BannerHeader.Fields.MainHeadingMobile);
			viewModel.SubTextLink = item.GetLinkElement(Templates.BannerHeader.Fields.SubTextLink);
			viewModel.ContentAlignment = item.GetLookupValue(Templates.BannerHeader.Fields.ContentAlignment);
			viewModel.MediaType = item.GetLookupValue(Templates.BannerHeader.Fields.MediaType);

			if (viewModel.MediaType.Equals("video"))
			{
				viewModel.VideoDetails = item.GetVideoElement(Templates.BannerHeader.Fields.VideoID);
			};
			viewModel.CTA = item.GetCTAWithType();
			ImageField imageField = item.Fields[Templates.BannerHeader.Fields.BrandIcon];
			if (imageField.MediaItem != null)
			{
				viewModel.BrandIcon = MediaManager.GetMediaUrl(imageField.MediaItem);			
			}
			viewModel.EnableBottomCurve = item.Fields[Templates.BannerHeader.Fields.EnableBottomCurve]?.IsChecked() ?? false;
			
			return viewModel;
		}
		
	}
}