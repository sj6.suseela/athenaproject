﻿using Athena.Foundation.Elements.Models;
using Athena.Foundation.React;
using Athena.Foundation.React.Models;
using Athena.Foundation.ReusableTemplates.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Athena.Feature.BannerHeader.Models
{
	public class BannerHeaderDataModel : AbstractDataModel
	{
		private static readonly string COMPONENT_NAME = "bannerHeader";

		[JsonProperty("smallHeading")]
		public Heading SmallHeading { get; set; }

		[JsonProperty("smallHeadingMobile")]
		public string SmallHeadingMobile { get; set; }

		[JsonProperty("mainHeading")]
		public Heading MainHeading { get; set; }

		[JsonProperty("mainHeadingMobile")]
		public string MainHeadingMobile { get; set; }

		[JsonProperty("subTextLink")]
		public LinkElement SubTextLink { get; set; }

		[JsonProperty("cta")]
		public CTA CTA { get; set; }

		[JsonProperty("mediaType")]
		public string MediaType { get; set; }

		[JsonProperty("image")]
		public AdvancedImageElement AdvancedImage { get; set; }

		[JsonProperty("gif")]
		public AdvancedImageElement GIFImageURL { get; set; }

		[JsonProperty("video")]
		public VideoElement VideoDetails { get; set; }

		[JsonProperty("backgroundColor")]
		public string BackgroudColor { get; set; } = "#007C9E";

		[JsonProperty("contentAlignment")]
		public string ContentAlignment { get; set; }

		[JsonProperty("brandIcon")]
		public string BrandIcon { get; set; }

		[JsonProperty("bottomCurve")]
		public bool EnableBottomCurve { get; set; }

		public override string GetComponentName()
		{
			return COMPONENT_NAME;
		}

	}




	public class Heading
	{
		[JsonProperty("description")]
		public string Title { get; set; } 

		[JsonProperty("type")]
		public string Type { get; set; }
	}

	//public class CTA
	//{
		
	//	[JsonProperty("url")]
	//	public string Url { get; set; }
	//	[JsonProperty("label")]
	//	public string Label { get; set; }
	//	[JsonProperty("target")]
	//	public string Target { get; set; }
	//	[JsonProperty("type")]
	//	public string Type { get; set; }
	//}


}