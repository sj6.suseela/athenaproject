﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Feature.Cards.Models
{
    public class CardPropertiesModel
    {
        [JsonProperty("enableBorder")]
        public bool EnableBorder {get; set;}
    }
}