﻿using Athena.Foundation.React;
using Athena.Foundation.React.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Feature.Cards.Models
{
    public class ContentCardViewModel : AbstractDataModel
	{
		private static readonly string COMPONENT_NAME = "contentCard";

		public override string GetComponentName()
		{
			return COMPONENT_NAME;
		}

		public new string GetRenderType()
		{
			return ReactConstants.RENDER_SERVERSIDE;
		}
		public CardHeadingModel CardHeading { get; set; }
		public CardPropertiesModel cardPropertiesModel { get; set; }
		public List<CardModel> ContentCards { get; set; }

	}
	
	
}