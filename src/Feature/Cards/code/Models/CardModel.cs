﻿using Athena.Foundation.Elements.Models;
using Athena.Foundation.ReusableTemplates.Models;
using Newtonsoft.Json;
using Sitecore.Data.Items;
using System.Collections.Generic;

namespace Athena.Feature.Cards.Models
{
	public class CardModel
	{
		[JsonProperty("image")]
		public AdvancedImageElement Image { get; set; }

		[JsonProperty("titleHeading")]
		public Title Title { get; set; }

		[JsonProperty("link")]
		public Link Link { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }
		
	}
}