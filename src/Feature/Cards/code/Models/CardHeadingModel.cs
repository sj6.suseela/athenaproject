﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Feature.Cards.Models
{
    public class CardHeadingModel
    {
        [JsonProperty("")]
        public string Heading { get; set; }
        [JsonProperty("")]
        public bool IsCenterAligned { get; set; }
        [JsonProperty("")]
        public string HeadingHtmlTag { get; set; }
    }
}