﻿using Athena.Foundation.Elements.Models;
using Athena.Foundation.ReusableTemplates.Models;
using Sitecore.Data.Items;
using System.Collections.Generic;

namespace Athena.Feature.Cards.Models
{
	public class CardsViewModel
	{
		public AdvancedImageElement Image { get; set; }
		public Title Title { get; set; }
		public Link Link { get; set; }
		public string Description { get; set; }
		
	}
}