﻿using Athena.Foundation.React;
using Athena.Foundation.React.Models;
using Athena.Foundation.ReusableTemplates.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Feature.Cards.Models
{
    public class HomePageCardViewModel:AbstractDataModel
    {
		private static readonly string COMPONENT_NAME = "HomePageCard";

		public override string GetComponentName()
		{
			return COMPONENT_NAME;
		}
		public new string GetRenderType()
		{
			return ReactConstants.RENDER_SERVERSIDE;
		}
		[JsonProperty("")]
		public HomePagePropertiesModel HomePagePropertiesModel { get; set; }
		[JsonProperty("cardItem")]
		public List<HomePageCard> HomePageCards { get; set; }

	}
	public class HomePagePropertiesModel :CardPropertiesModel
	{
		//[JsonProperty("variation")]
		//public string Variation { get; set; }
	}
	public class HomePageCard : CardModel
	{
		[JsonProperty("buttonLink")]
		public CTA CTA { get; set; }

	}
}
