﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Athena.Foundation.Elements.Models;
using Athena.Foundation.React;
using Athena.Foundation.React.Models;
using Athena.Foundation.ReusableTemplates.Models;

namespace Athena.Feature.Cards.Models
{
	public class ProductCardViewModel : AbstractDataModel
	{
		private static readonly string COMPONENT_NAME = "productCard";

		public override string GetComponentName()
		{
			return COMPONENT_NAME;
		}

		public new string GetRenderType()
		{
			return ReactConstants.RENDER_SERVERSIDE;
		}
		public CardHeadingModel CardHeading { get; set; }
		public CardPropertiesModel cardPropertiesModel { get; set; }
		public List<ProductCard> ProductCards { get; set; }

	}
	public class ProductCard: CardModel
	{
		public CTA CTA { get; set; }
		
	}
}