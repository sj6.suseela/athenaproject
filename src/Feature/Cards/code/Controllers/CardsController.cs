﻿using Athena.Feature.Cards.Services;
using Sitecore;
using Sitecore.Mvc.Presentation;
using System.Web.Mvc;

namespace Athena.Feature.Cards.Controllers
{
    public class CardsController : Controller
    {
        private readonly ICardsServices _CardsService;
       
        public CardsController(ICardsServices CardsService)
        {
            _CardsService = CardsService;
        }

        public ActionResult ContentCard()
        {
            var viewModel = _CardsService.GetContentCardsModel(RenderingContext.Current, Context.Item);
            return View(viewModel);
        }

        public ActionResult ProductCard()
        {
            var viewModel = _CardsService.GetProductCardsModel(RenderingContext.Current, Context.Item);
            return View(viewModel);
        }
        public ActionResult HomePageCard()
        {
            var viewModel = _CardsService.GetHomePageCardsModel(RenderingContext.Current, Context.Item);
            return View(viewModel);
        }
    }
}