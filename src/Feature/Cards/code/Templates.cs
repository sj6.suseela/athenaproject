﻿






















// ReSharper disable InconsistentNaming
namespace Athena.Feature.Cards
{
    public static partial class Templates
    {

        #region /sitecore/templates/Feature/Cards/_Card
        /// <summary>
        ///   _Card
        ///   <para>ID: {79478CDD-81B4-4B75-8397-042D81CA0AFC}</para>
        ///   <para>Path: /sitecore/templates/Feature/Cards/_Card</para>
        /// </summary>
        public static class Card
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{79478CDD-81B4-4B75-8397-042D81CA0AFC}"));
            public const string IdString = "{79478CDD-81B4-4B75-8397-042D81CA0AFC}";


            public static class Fields
            {

                /// <summary>
                ///   Description
                ///   <para>{AB18DDA7-5188-48B0-8DA4-AF0CF16A9BB6}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Description = new Sitecore.Data.ID("{AB18DDA7-5188-48B0-8DA4-AF0CF16A9BB6}");

                /// <summary>
                ///   Description
                ///   <para>{AB18DDA7-5188-48B0-8DA4-AF0CF16A9BB6}</para>
                /// </summary>
                public const string Description_FieldName = "Description";


                /// <summary>
                ///   Image
                ///   <para>{E2CA62FD-0F85-4496-83EB-F2EB76B1F95E}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Image = new Sitecore.Data.ID("{E2CA62FD-0F85-4496-83EB-F2EB76B1F95E}");

                /// <summary>
                ///   Image
                ///   <para>{E2CA62FD-0F85-4496-83EB-F2EB76B1F95E}</para>
                /// </summary>
                public const string Image_FieldName = "Image";


            }

        }
        #endregion

        #region /sitecore/templates/Feature/Cards/_CardHeading
        /// <summary>
        ///   _CardHeading
        ///   <para>ID: {2EB990C4-77E4-4696-9A3C-C98379FC5CDA}</para>
        ///   <para>Path: /sitecore/templates/Feature/Cards/_CardHeading</para>
        /// </summary>
        public static class CardHeading
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{2EB990C4-77E4-4696-9A3C-C98379FC5CDA}"));
            public const string IdString = "{2EB990C4-77E4-4696-9A3C-C98379FC5CDA}";


            public static class Fields
            {

                /// <summary>
                ///   Heading Size
                ///   <para>{12DDE86B-687D-4B28-A89B-21EC4CB943EB}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID HeadingSize = new Sitecore.Data.ID("{12DDE86B-687D-4B28-A89B-21EC4CB943EB}");

                /// <summary>
                ///   Heading Size
                ///   <para>{12DDE86B-687D-4B28-A89B-21EC4CB943EB}</para>
                /// </summary>
                public const string HeadingSize_FieldName = "Heading Size";


                /// <summary>
                ///   Heading
                ///   <para>{4E0C0287-01FC-4A5F-8324-9DFA0DA915F0}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Heading = new Sitecore.Data.ID("{4E0C0287-01FC-4A5F-8324-9DFA0DA915F0}");

                /// <summary>
                ///   Heading
                ///   <para>{4E0C0287-01FC-4A5F-8324-9DFA0DA915F0}</para>
                /// </summary>
                public const string Heading_FieldName = "Heading";


                /// <summary>
                ///   Is Center Aligned
                ///   <para>{D76E998F-9B56-42C8-B83C-755D55604D32}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID IsCenterAligned = new Sitecore.Data.ID("{D76E998F-9B56-42C8-B83C-755D55604D32}");

                /// <summary>
                ///   Is Center Aligned
                ///   <para>{D76E998F-9B56-42C8-B83C-755D55604D32}</para>
                /// </summary>
                public const string IsCenterAligned_FieldName = "Is Center Aligned";


            }

        }
        #endregion

        #region /sitecore/templates/Feature/Cards/_ContentCard
        /// <summary>
        ///   _ContentCard
        ///   <para>ID: {C3800735-01F0-4E02-A6E6-F6BB167E09A1}</para>
        ///   <para>Path: /sitecore/templates/Feature/Cards/_ContentCard</para>
        /// </summary>
        public static class ContentCard
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{C3800735-01F0-4E02-A6E6-F6BB167E09A1}"));
            public const string IdString = "{C3800735-01F0-4E02-A6E6-F6BB167E09A1}";


        }
        #endregion

        #region /sitecore/templates/Feature/Cards/_ContentCards
        /// <summary>
        ///   _ContentCards
        ///   <para>ID: {2A2C1881-79DD-4CAB-8C63-59770808C139}</para>
        ///   <para>Path: /sitecore/templates/Feature/Cards/_ContentCards</para>
        /// </summary>
        public static class ContentCards
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{2A2C1881-79DD-4CAB-8C63-59770808C139}"));
            public const string IdString = "{2A2C1881-79DD-4CAB-8C63-59770808C139}";


        }
        #endregion

        #region /sitecore/templates/Feature/Cards/_HomePageCard
        /// <summary>
        ///   _HomePageCard
        ///   <para>ID: {BD2B06E4-00B5-46AD-8F1A-36DB5F821922}</para>
        ///   <para>Path: /sitecore/templates/Feature/Cards/_HomePageCard</para>
        /// </summary>
        public static class HomePageCard
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{BD2B06E4-00B5-46AD-8F1A-36DB5F821922}"));
            public const string IdString = "{BD2B06E4-00B5-46AD-8F1A-36DB5F821922}";


        }
        #endregion

        #region /sitecore/templates/Feature/Cards/_HomePageCards
        /// <summary>
        ///   _HomePageCards
        ///   <para>ID: {ADBF0ECF-5DCF-4946-B038-4EEF1E2E12D2}</para>
        ///   <para>Path: /sitecore/templates/Feature/Cards/_HomePageCards</para>
        /// </summary>
        public static class HomePageCards
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{ADBF0ECF-5DCF-4946-B038-4EEF1E2E12D2}"));
            public const string IdString = "{ADBF0ECF-5DCF-4946-B038-4EEF1E2E12D2}";


        }
        #endregion

        #region /sitecore/templates/Feature/Cards/_ProductCard
        /// <summary>
        ///   _ProductCard
        ///   <para>ID: {C5D9927F-1060-4E9F-B762-6715FDD5FAB8}</para>
        ///   <para>Path: /sitecore/templates/Feature/Cards/_ProductCard</para>
        /// </summary>
        public static class ProductCard
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{C5D9927F-1060-4E9F-B762-6715FDD5FAB8}"));
            public const string IdString = "{C5D9927F-1060-4E9F-B762-6715FDD5FAB8}";


        }
        #endregion

        #region /sitecore/templates/Feature/Cards/_ProductCards
        /// <summary>
        ///   _ProductCards
        ///   <para>ID: {12DAD46E-4C6B-42F3-95B7-F297D5E191CC}</para>
        ///   <para>Path: /sitecore/templates/Feature/Cards/_ProductCards</para>
        /// </summary>
        public static class ProductCards
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{12DAD46E-4C6B-42F3-95B7-F297D5E191CC}"));
            public const string IdString = "{12DAD46E-4C6B-42F3-95B7-F297D5E191CC}";


        }
        #endregion

    }
}


