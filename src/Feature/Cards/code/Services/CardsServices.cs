﻿using Athena.Feature.Cards.Models;
using Athena.Feature.Cards;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System.Linq;
using Athena.Foundation.React.Models;
using System.Collections.Generic;
using Athena.Foundation.ReusableTemplates;
using Athena.Foundation.Elements.Extensions;

namespace Athena.Feature.Cards.Services
{
	public class CardsServices : ICardsServices
	{
        #region Product Cards
        public AbstractComponentModel GetProductCardsModel(RenderingContext renderingContext, Item page)
		{
			var item = renderingContext?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.ProductCards.ID))
				return null;
			AbstractComponentModel componentModel = new AbstractComponentModel();
			componentModel.setComponentData(GetProductCardsViewModel(item));

			return componentModel;

		}
		private ProductCardViewModel GetProductCardsViewModel(Item item)
		{

			var productCardViewModel = new ProductCardViewModel()
			{
				CardHeading = new CardHeadingModel
				{
					Heading = item.RenderField(Templates.CardHeading.Fields.Heading),
					IsCenterAligned = item.GetBoolFieldValue(Templates.CardHeading.Fields.IsCenterAligned),
					HeadingHtmlTag = item.GetLookupValue(Templates.CardHeading.Fields.HeadingSize)
				},
				ProductCards = item.ChildrenOfTemplate(Templates.ProductCard.ID).Select(MapProductCard).ToList()
			};
			return productCardViewModel;

		}
		private static ProductCard MapProductCard(Item item)
		{
			return new ProductCard()
			{

				Title = item.GetTitleWithHtmlTag(),
				Image = item.GetAdavncedImageElement(Templates.Card.Fields.Image, new REND_516_516(), new REND_1536_400()),
				Description = item.RenderField(Templates.Card.Fields.Description),
				CTA = item.GetCTAWithType(),
				Link = item.GetLinkWithText()

			};
		}

		#endregion
		#region Content Cards
		public AbstractComponentModel GetContentCardsModel(RenderingContext renderingContext, Item page)
		{
			var item = renderingContext?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.ContentCards.ID))
				return null;
			AbstractComponentModel componentModel = new AbstractComponentModel();
			componentModel.setComponentData(GetContentCardsModel(item));

			return componentModel;

		}
		private ContentCardViewModel GetContentCardsModel(Item item)
		{

			var contentCardViewModel = new ContentCardViewModel()
			{
				CardHeading = new CardHeadingModel
				{
					Heading = item.RenderField(Templates.CardHeading.Fields.Heading),
					IsCenterAligned = item.GetBoolFieldValue(Templates.CardHeading.Fields.IsCenterAligned),
					HeadingHtmlTag = item.GetLookupValue(Templates.CardHeading.Fields.HeadingSize)
				},
				ContentCards = item.ChildrenOfTemplate(Templates.ContentCard.ID).Select(MapContentCard).ToList()
			};
			return contentCardViewModel;

		}
		private static CardModel MapContentCard(Item item)
		{
			return new CardModel()
			{

				Title = item.GetTitleWithHtmlTag(),
				Image = item.GetAdavncedImageElement(Templates.Card.Fields.Image, new REND_516_516(), new REND_1536_400()),
				Description = item.RenderField(Templates.Card.Fields.Description),
				Link = item.GetLinkWithText()

			};
		}

		#endregion
		#region HomePage Card
		public AbstractComponentModel GetHomePageCardsModel(RenderingContext renderingContext, Item page)
		{
			var item = renderingContext?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.HomePageCards.ID))
				return null;
			AbstractComponentModel componentModel = new AbstractComponentModel();
			componentModel.setComponentData(GetHomePageCardViewModel(item));

			return componentModel;

		}
		private HomePageCardViewModel GetHomePageCardViewModel(Item item)
		{

			var HomePageCardViewModel = new HomePageCardViewModel()
			{
				HomePagePropertiesModel = new HomePagePropertiesModel
				{
					//Variation = item.GetLookupValue(Athena.Foundation.ReusableTemplates.Templates.ImageAllignment.Fields.Allignment),
				},
				HomePageCards = item.ChildrenOfTemplate(Templates.HomePageCard.ID).Select(MapHomePageCard).ToList()
			};
			return HomePageCardViewModel;

		}
		private static HomePageCard MapHomePageCard(Item item)
		{
			return new HomePageCard()
			{

				Title = item.GetTitleWithHtmlTag(),
				Image = item.GetAdavncedImageElement(Templates.Card.Fields.Image, new REND_516_516(), new REND_1536_400()),
				Description = item.RenderField(Templates.Card.Fields.Description),
				CTA = item.GetCTAWithType(),
				Link = item.GetLinkWithText()

			};
		}
		#endregion

	}

}
