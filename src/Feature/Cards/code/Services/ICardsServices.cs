﻿using Athena.Feature.Cards.Models;
using Athena.Foundation.React.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Cards.Services
{
    public interface ICardsServices
    {
        AbstractComponentModel GetContentCardsModel(RenderingContext renderingContext, Item page);
        AbstractComponentModel GetHomePageCardsModel(RenderingContext renderingContext, Item page);
        AbstractComponentModel GetProductCardsModel(RenderingContext renderingContext, Item page);
    }
}
