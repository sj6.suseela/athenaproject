﻿using Athena.Feature.Cards.Services;
using Athena.Foundation.DependencyInjection.Pipelines;
using Microsoft.Extensions.DependencyInjection;

namespace Athena.Feature.Cards.Pipelines
{
    public class RegisterServices
    {
        public void Process(InitializeDependencyInjectionArgs args)
        {
            args.ServiceCollection.AddScoped<ICardsServices, CardsServices>();
        }
    }
}