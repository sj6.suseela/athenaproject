﻿using Sitecore.Data;

namespace Athena.Feature.Header
{
	public struct Template
	{
		public class HeaderBanner
		{
			public static ID TemplateId = new ID("{82E9E706-8876-427D-BF51-9182B03630A8}");

			public struct FieldIDs
			{
				public static ID DesktopImage = new ID("{931D0CF3-EE36-4CBF-9102-BC188681A550}");
				public static ID MobileImage = new ID("{7B0C438D-504C-479E-B725-5AA1E426C6F9}");
				public static ID Subheading = new ID("{481C7E5F-7F70-4BB2-AE85-8CC5EB643E3E}");
				
			}
		}
		public static class AlternateHeaderBanner
		{
			public static readonly ID ID = new ID("{1B4749EA-4461-4C9A-9E41-595076A16B35}");

			public static class FieldsIDs
			{

				public static ID HubLink = new ID("{BCC96E15-C9E2-44E3-BA9E-20865DAF1B78}");

				public static ID HubName = new ID("{57E71C8A-B7E4-4FF9-B4E1-4FA6FBB6DF03}");

			}
		}


		public class BetaHeaderBanner
		{
			public static ID TemplateId = new ID("{13C76B75-EAF2-4EA7-9FE7-6C10FDFF75D7}");

			public struct FieldIDs
			{
				public static ID Headline = new ID("{4BDC323B-BC34-4C07-B0C1-54CF58F5F528}");
				public static ID HeaderLabel = new ID("{2867B283-EC02-4EFF-AB5F-9F29D012B820}");
				public static ID BetaTooltip = new ID("{D0046145-98DD-4C4D-A012-66669798CD5D}");
				public static ID FeedbackCta = new ID("{EF1DFBF6-D32F-4807-9778-F5159487075D}");
			}
		}


		public class BasePage
		{
			public struct FieldIDs
			{
				public static ID PageTitle = new ID("{D9FF68B8-F749-4D92-8373-7818B962610E}");
			}
		}

		public static class SecondaryNavigation
		{
			public static readonly ID TemplateId = new ID("{0411C9A9-E2BE-4B84-8575-9746AFEF10DE}");

		}

		public static class Renderings	
		{
			public static readonly ID SecondaryNavigation =new ID("{6646ECDD-20BA-4CEB-82B5-A9E2BE4CCE95}");
		}
	}
}