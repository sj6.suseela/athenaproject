﻿






// ReSharper disable InconsistentNaming
namespace Athena.Feature.Header
{
    public static partial class Templates
    {
        #region /sitecore/templates/Foundation/Header/_Header
        /// <summary>
        ///   _Header
        ///   <para>ID: {C726588D-2AD6-4FBD-A0FD-F07D2021962D}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_Header</para>
        /// </summary>
        public static class Header
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{C726588D-2AD6-4FBD-A0FD-F07D2021962D}"));
            public const string IdString = "{C726588D-2AD6-4FBD-A0FD-F07D2021962D}";

            public static class Fields
            {
                /// <summary>
                ///   Logo Image
                ///   <para>{26D089B0-E29C-49B1-824E-2393F86F336E}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID LogoImage = new Sitecore.Data.ID("{26D089B0-E29C-49B1-824E-2393F86F336E}");

                /// <summary>
                ///   Logo Image
                ///   <para>{26D089B0-E29C-49B1-824E-2393F86F336E}</para>
                /// </summary>
                public const string LogoImage_FieldName = "Logo Image";

                /// <summary>
                ///   Logo Link
                ///   <para>{F380ED73-18F9-4A7E-A005-989D9306D5EC}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID LogoLink = new Sitecore.Data.ID("{F380ED73-18F9-4A7E-A005-989D9306D5EC}");

                /// <summary>
                ///   Logo Link
                ///   <para>{F380ED73-18F9-4A7E-A005-989D9306D5EC}</para>
                /// </summary>
                public const string LogoLink_FieldName = "Logo Link";

                /// <summary>
                ///   Logo Type
                ///   <para>{3359FE82-9D75-41B5-BDF5-0272F1B0064F}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID LogoType = new Sitecore.Data.ID("{3359FE82-9D75-41B5-BDF5-0272F1B0064F}");

                /// <summary>
                ///   Logo Type
                ///   <para>{3359FE82-9D75-41B5-BDF5-0272F1B0064F}</para>
                /// </summary>
                public const string LogoType_FieldName = "Logo Type";

                /// <summary>
                ///   Partner Logo Image
                ///   <para>{1993E301-861E-405F-B8DA-1D91FBB07AE8}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID PartnerLogoImage = new Sitecore.Data.ID("{1993E301-861E-405F-B8DA-1D91FBB07AE8}");

                /// <summary>
                ///   Partner Logo Image
                ///   <para>{1993E301-861E-405F-B8DA-1D91FBB07AE8}</para>
                /// </summary>
                public const string PartnerLogoImage_FieldName = "Partner Logo Image";

                /// <summary>
                ///   Partner Logo Link
                ///   <para>{5AD4470B-AAF5-46D6-ACA2-5F7475E87FD7}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID PartnerLogoLink = new Sitecore.Data.ID("{5AD4470B-AAF5-46D6-ACA2-5F7475E87FD7}");

                /// <summary>
                ///   Partner Logo Link
                ///   <para>{5AD4470B-AAF5-46D6-ACA2-5F7475E87FD7}</para>
                /// </summary>
                public const string PartnerLogoLink_FieldName = "Partner Logo Link";

                /// <summary>
                ///   Optional Title
                ///   <para>{B59D1287-AFB8-4BDF-B2CA-D6D433996F54}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID OptionalTitle = new Sitecore.Data.ID("{B59D1287-AFB8-4BDF-B2CA-D6D433996F54}");

                /// <summary>
                ///   Optional Title
                ///   <para>{B59D1287-AFB8-4BDF-B2CA-D6D433996F54}</para>
                /// </summary>
                public const string OptionalTitle_FieldName = "Optional Title";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/Header/_HeaderContactUs
        /// <summary>
        ///   _HeaderContactUs
        ///   <para>ID: {B16BEE2A-1252-4E82-9840-DDC9C522A186}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_HeaderContactUs</para>
        /// </summary>
        public static class HeaderContactUs
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{B16BEE2A-1252-4E82-9840-DDC9C522A186}"));
            public const string IdString = "{B16BEE2A-1252-4E82-9840-DDC9C522A186}";

            public static class Fields
            {
                /// <summary>
                ///   Link
                ///   <para>{28DC8880-5F44-4DC0-8086-B8EC59C01BE6}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Link = new Sitecore.Data.ID("{28DC8880-5F44-4DC0-8086-B8EC59C01BE6}");

                /// <summary>
                ///   Link
                ///   <para>{28DC8880-5F44-4DC0-8086-B8EC59C01BE6}</para>
                /// </summary>
                public const string Link_FieldName = "Link";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/Header/_HeaderPageFields
        /// <summary>
        ///   _HeaderPageFields
        ///   <para>ID: {99AACC87-9705-410D-BDFB-59D29E3FB20A}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_HeaderPageFields</para>
        /// </summary>
        public static class HeaderPageFields
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{99AACC87-9705-410D-BDFB-59D29E3FB20A}"));
            public const string IdString = "{99AACC87-9705-410D-BDFB-59D29E3FB20A}";

            public static class Fields
            {
                /// <summary>
                ///   Invert Text Colour
                ///   <para>{D8F30793-7A1E-458E-82DD-C151E8B3A865}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID InvertTextColour = new Sitecore.Data.ID("{D8F30793-7A1E-458E-82DD-C151E8B3A865}");

                /// <summary>
                ///   Invert Text Colour
                ///   <para>{D8F30793-7A1E-458E-82DD-C151E8B3A865}</para>
                /// </summary>
                public const string InvertTextColour_FieldName = "Invert Text Colour";

                /// <summary>
                ///   Is Logo H1
                ///   <para>{4F461134-E2D2-407A-9DB5-8A15D25757C9}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID IsLogoH1 = new Sitecore.Data.ID("{4F461134-E2D2-407A-9DB5-8A15D25757C9}");

                /// <summary>
                ///   Is Logo H1
                ///   <para>{4F461134-E2D2-407A-9DB5-8A15D25757C9}</para>
                /// </summary>
                public const string IsLogoH1_FieldName = "Is Logo H1";

                /// <summary>
                ///   Is Mini
                ///   <para>{91572AF2-A1C0-4368-8451-C564E49D2C0B}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID IsMini = new Sitecore.Data.ID("{91572AF2-A1C0-4368-8451-C564E49D2C0B}");

                /// <summary>
                ///   Is Mini
                ///   <para>{91572AF2-A1C0-4368-8451-C564E49D2C0B}</para>
                /// </summary>
                public const string IsMini_FieldName = "Is Mini";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/Header/_HeaderSearch
        /// <summary>
        ///   _HeaderSearch
        ///   <para>ID: {B4B0C66D-FC39-464A-92FA-CE9672E3E404}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_HeaderSearch</para>
        /// </summary>
        public static class HeaderSearch
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{B4B0C66D-FC39-464A-92FA-CE9672E3E404}"));
            public const string IdString = "{B4B0C66D-FC39-464A-92FA-CE9672E3E404}";

            public static class Fields
            {
                /// <summary>
                ///   AjaxUrl
                ///   <para>{B8D40E42-8D79-4A1C-93B2-8271750B5190}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID AjaxUrl = new Sitecore.Data.ID("{B8D40E42-8D79-4A1C-93B2-8271750B5190}");

                /// <summary>
                ///   AjaxUrl
                ///   <para>{B8D40E42-8D79-4A1C-93B2-8271750B5190}</para>
                /// </summary>
                public const string AjaxUrl_FieldName = "AjaxUrl";

                /// <summary>
                ///   CharSearchLimit
                ///   <para>{143B7B2F-1EB0-4DB5-8672-C1C792474391}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CharSearchLimit = new Sitecore.Data.ID("{143B7B2F-1EB0-4DB5-8672-C1C792474391}");

                /// <summary>
                ///   CharSearchLimit
                ///   <para>{143B7B2F-1EB0-4DB5-8672-C1C792474391}</para>
                /// </summary>
                public const string CharSearchLimit_FieldName = "CharSearchLimit";

                /// <summary>
                ///   Label
                ///   <para>{AC242AA7-E126-425A-8BF7-1F4C5786365B}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Label = new Sitecore.Data.ID("{AC242AA7-E126-425A-8BF7-1F4C5786365B}");

                /// <summary>
                ///   Label
                ///   <para>{AC242AA7-E126-425A-8BF7-1F4C5786365B}</para>
                /// </summary>
                public const string Label_FieldName = "Label";

                /// <summary>
                ///   NoResultsText
                ///   <para>{6C4164D3-899B-4345-99B0-E52B34A31E78}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID NoResultsText = new Sitecore.Data.ID("{6C4164D3-899B-4345-99B0-E52B34A31E78}");

                /// <summary>
                ///   NoResultsText
                ///   <para>{6C4164D3-899B-4345-99B0-E52B34A31E78}</para>
                /// </summary>
                public const string NoResultsText_FieldName = "NoResultsText";

                /// <summary>
                ///   Placeholder
                ///   <para>{EF426733-A2E2-44EE-A8EE-169EE9A0908F}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Placeholder = new Sitecore.Data.ID("{EF426733-A2E2-44EE-A8EE-169EE9A0908F}");

                /// <summary>
                ///   Placeholder
                ///   <para>{EF426733-A2E2-44EE-A8EE-169EE9A0908F}</para>
                /// </summary>
                public const string Placeholder_FieldName = "Placeholder";

                /// <summary>
                ///   SearchImage
                ///   <para>{3F183841-6300-4F52-A53D-14831FF8F250}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID SearchImage = new Sitecore.Data.ID("{3F183841-6300-4F52-A53D-14831FF8F250}");

                /// <summary>
                ///   SearchImage
                ///   <para>{3F183841-6300-4F52-A53D-14831FF8F250}</para>
                /// </summary>
                public const string SearchImage_FieldName = "SearchImage";

                /// <summary>
                ///   Url
                ///   <para>{2C7D3C18-89C3-4970-9B16-56B45914154C}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Url = new Sitecore.Data.ID("{2C7D3C18-89C3-4970-9B16-56B45914154C}");

                /// <summary>
                ///   Url
                ///   <para>{2C7D3C18-89C3-4970-9B16-56B45914154C}</para>
                /// </summary>
                public const string Url_FieldName = "Url";

                /// <summary>
                ///   viewAllLabel
                ///   <para>{0520B75A-96E0-4C92-967E-CB1046D3DBD9}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID ViewAllLabel = new Sitecore.Data.ID("{0520B75A-96E0-4C92-967E-CB1046D3DBD9}");

                /// <summary>
                ///   viewAllLabel
                ///   <para>{0520B75A-96E0-4C92-967E-CB1046D3DBD9}</para>
                /// </summary>
                public const string ViewAllLabel_FieldName = "viewAllLabel";

                /// <summary>
                ///   Other Business Heading
                ///   <para>{F8D3A0FE-5209-4967-B5DB-3058066D0D56}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID OtherBusinessHeading = new Sitecore.Data.ID("{F8D3A0FE-5209-4967-B5DB-3058066D0D56}");

                /// <summary>
                ///   Other Business Heading
                ///   <para>{F8D3A0FE-5209-4967-B5DB-3058066D0D56}</para>
                /// </summary>
                public const string OtherBusinessHeading_FieldName = "Other Business Heading";

                /// <summary>
                ///   Other Business Link
                ///   <para>{54272279-02DB-4EEE-BD05-859314FAE8F2}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID OtherBusinessLink = new Sitecore.Data.ID("{54272279-02DB-4EEE-BD05-859314FAE8F2}");

                /// <summary>
                ///   Other Business Link
                ///   <para>{54272279-02DB-4EEE-BD05-859314FAE8F2}</para>
                /// </summary>
                public const string OtherBusinessLink_FieldName = "Other Business Link";

                /// <summary>
                ///   Links
                ///   <para>{52AB7C76-303F-4C27-A16E-5D84A328978C}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Links = new Sitecore.Data.ID("{52AB7C76-303F-4C27-A16E-5D84A328978C}");

                /// <summary>
                ///   Links
                ///   <para>{52AB7C76-303F-4C27-A16E-5D84A328978C}</para>
                /// </summary>
                public const string Links_FieldName = "Links";

                /// <summary>
                ///   NoResults
                ///   <para>{150D4967-E5E8-4FF0-A468-9F7D85708031}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID NoResults = new Sitecore.Data.ID("{150D4967-E5E8-4FF0-A468-9F7D85708031}");

                /// <summary>
                ///   NoResults
                ///   <para>{150D4967-E5E8-4FF0-A468-9F7D85708031}</para>
                /// </summary>
                public const string NoResults_FieldName = "NoResults";

                /// <summary>
                ///   NoSearch
                ///   <para>{9BC674C4-D5A8-4142-BDFC-8146A7204CA1}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID NoSearch = new Sitecore.Data.ID("{9BC674C4-D5A8-4142-BDFC-8146A7204CA1}");

                /// <summary>
                ///   NoSearch
                ///   <para>{9BC674C4-D5A8-4142-BDFC-8146A7204CA1}</para>
                /// </summary>
                public const string NoSearch_FieldName = "NoSearch";

                /// <summary>
                ///   Results
                ///   <para>{0B630859-C96A-4F75-9A89-84284A129051}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Results = new Sitecore.Data.ID("{0B630859-C96A-4F75-9A89-84284A129051}");

                /// <summary>
                ///   Results
                ///   <para>{0B630859-C96A-4F75-9A89-84284A129051}</para>
                /// </summary>
                public const string Results_FieldName = "Results";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/Header/_IsNavigationItem
        /// <summary>
        ///   _IsNavigationItem
        ///   <para>ID: {8A2D3773-130B-4EB9-A378-7D200B411BB9}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_IsNavigationItem</para>
        /// </summary>
        public static class IsNavigationItem
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{8A2D3773-130B-4EB9-A378-7D200B411BB9}"));
            public const string IdString = "{8A2D3773-130B-4EB9-A378-7D200B411BB9}";

            public static class Fields
            {
                /// <summary>
                ///   Label
                ///   <para>{A4F50AA9-83A0-49AC-997E-65561AEDD6D6}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Label = new Sitecore.Data.ID("{A4F50AA9-83A0-49AC-997E-65561AEDD6D6}");

                /// <summary>
                ///   Label
                ///   <para>{A4F50AA9-83A0-49AC-997E-65561AEDD6D6}</para>
                /// </summary>
                public const string Label_FieldName = "Label";

                /// <summary>
                ///   Link
                ///   <para>{3F3D36EF-B5F6-40BA-BEB9-B927CEE65690}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Link = new Sitecore.Data.ID("{3F3D36EF-B5F6-40BA-BEB9-B927CEE65690}");

                /// <summary>
                ///   Link
                ///   <para>{3F3D36EF-B5F6-40BA-BEB9-B927CEE65690}</para>
                /// </summary>
                public const string Link_FieldName = "Link";

                /// <summary>
                ///   Mobile Label
                ///   <para>{034BC9FF-1E68-420E-9563-0B68767483FA}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MobileLabel = new Sitecore.Data.ID("{034BC9FF-1E68-420E-9563-0B68767483FA}");

                /// <summary>
                ///   Mobile Label
                ///   <para>{034BC9FF-1E68-420E-9563-0B68767483FA}</para>
                /// </summary>
                public const string MobileLabel_FieldName = "Mobile Label";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/Header/_MasterHeaderBarPageFields
        /// <summary>
        ///   _MasterHeaderBarPageFields
        ///   <para>ID: {DB3B2CEC-F61A-43FD-AE0A-3244C168CEBB}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_MasterHeaderBarPageFields</para>
        /// </summary>
        public static class MasterHeaderBarPageFields
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{DB3B2CEC-F61A-43FD-AE0A-3244C168CEBB}"));
            public const string IdString = "{DB3B2CEC-F61A-43FD-AE0A-3244C168CEBB}";

        }
        #endregion
        #region /sitecore/templates/Foundation/Header/_MicrositeHeader
        /// <summary>
        ///   _MicrositeHeader
        ///   <para>ID: {0B69ACD9-85FE-448E-9332-BAF5A7ACABA9}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_MicrositeHeader</para>
        /// </summary>
        public static class MicrositeHeader
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{0B69ACD9-85FE-448E-9332-BAF5A7ACABA9}"));
            public const string IdString = "{0B69ACD9-85FE-448E-9332-BAF5A7ACABA9}";

            public static class Fields
            {
                /// <summary>
                ///   Back Cta
                ///   <para>{B73B7664-3F3E-4AA7-B199-79D47EFDE930}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID BackCta = new Sitecore.Data.ID("{B73B7664-3F3E-4AA7-B199-79D47EFDE930}");

                /// <summary>
                ///   Back Cta
                ///   <para>{B73B7664-3F3E-4AA7-B199-79D47EFDE930}</para>
                /// </summary>
                public const string BackCta_FieldName = "Back Cta";

                /// <summary>
                ///   Logo Image
                ///   <para>{46D31BF1-B6EA-4C83-B0D9-77875936281B}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID LogoImage = new Sitecore.Data.ID("{46D31BF1-B6EA-4C83-B0D9-77875936281B}");

                /// <summary>
                ///   Logo Image
                ///   <para>{46D31BF1-B6EA-4C83-B0D9-77875936281B}</para>
                /// </summary>
                public const string LogoImage_FieldName = "Logo Image";

                /// <summary>
                ///   Logo Link
                ///   <para>{867E7C35-CF85-4D2B-BF9E-80B1F8C47D08}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID LogoLink = new Sitecore.Data.ID("{867E7C35-CF85-4D2B-BF9E-80B1F8C47D08}");

                /// <summary>
                ///   Logo Link
                ///   <para>{867E7C35-CF85-4D2B-BF9E-80B1F8C47D08}</para>
                /// </summary>
                public const string LogoLink_FieldName = "Logo Link";

                /// <summary>
                ///   Logo Type
                ///   <para>{8CA7573A-EC87-4F4E-A21C-C38AE5350FEF}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID LogoType = new Sitecore.Data.ID("{8CA7573A-EC87-4F4E-A21C-C38AE5350FEF}");

                /// <summary>
                ///   Logo Type
                ///   <para>{8CA7573A-EC87-4F4E-A21C-C38AE5350FEF}</para>
                /// </summary>
                public const string LogoType_FieldName = "Logo Type";

                /// <summary>
                ///   Partner Logo Image
                ///   <para>{5E66DC1F-3B78-43AF-9FE4-33722D684F5C}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID PartnerLogoImage = new Sitecore.Data.ID("{5E66DC1F-3B78-43AF-9FE4-33722D684F5C}");

                /// <summary>
                ///   Partner Logo Image
                ///   <para>{5E66DC1F-3B78-43AF-9FE4-33722D684F5C}</para>
                /// </summary>
                public const string PartnerLogoImage_FieldName = "Partner Logo Image";

                /// <summary>
                ///   Partner Logo Link
                ///   <para>{4EA8D5F3-3A78-41B6-B29B-26CDA0B09B8D}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID PartnerLogoLink = new Sitecore.Data.ID("{4EA8D5F3-3A78-41B6-B29B-26CDA0B09B8D}");

                /// <summary>
                ///   Partner Logo Link
                ///   <para>{4EA8D5F3-3A78-41B6-B29B-26CDA0B09B8D}</para>
                /// </summary>
                public const string PartnerLogoLink_FieldName = "Partner Logo Link";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/Header/_PrimaryNavigation
        /// <summary>
        ///   _PrimaryNavigation
        ///   <para>ID: {4CA00D8C-FBBD-4FB6-9DC8-03D25D04743F}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_PrimaryNavigation</para>
        /// </summary>
        public static class PrimaryNavigation
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{4CA00D8C-FBBD-4FB6-9DC8-03D25D04743F}"));
            public const string IdString = "{4CA00D8C-FBBD-4FB6-9DC8-03D25D04743F}";

            public static class Fields
            {
                /// <summary>
                ///   More Label
                ///   <para>{8FCC1F35-57C1-4B14-86ED-345CAFC0967D}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MoreLabel = new Sitecore.Data.ID("{8FCC1F35-57C1-4B14-86ED-345CAFC0967D}");

                /// <summary>
                ///   More Label
                ///   <para>{8FCC1F35-57C1-4B14-86ED-345CAFC0967D}</para>
                /// </summary>
                public const string MoreLabel_FieldName = "More Label";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/Header/_SecondaryNavigation
        /// <summary>
        ///   _SecondaryNavigation
        ///   <para>ID: {0411C9A9-E2BE-4B84-8575-9746AFEF10DE}</para>
        ///   <para>Path: /sitecore/templates/Foundation/Header/_SecondaryNavigation</para>
        /// </summary>
        public static class SecondaryNavigation
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{0411C9A9-E2BE-4B84-8575-9746AFEF10DE}"));
            public const string IdString = "{0411C9A9-E2BE-4B84-8575-9746AFEF10DE}";

            public static class Fields
            {
                /// <summary>
                ///   Label
                ///   <para>{1871E1D0-B024-4677-93E0-E440BBBF8FC8}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Label = new Sitecore.Data.ID("{1871E1D0-B024-4677-93E0-E440BBBF8FC8}");

                /// <summary>
                ///   Label
                ///   <para>{1871E1D0-B024-4677-93E0-E440BBBF8FC8}</para>
                /// </summary>
                public const string Label_FieldName = "Label";

                /// <summary>
                ///   Link
                ///   <para>{D408E265-89ED-4AA0-B184-2E8DFD426E8E}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Link = new Sitecore.Data.ID("{D408E265-89ED-4AA0-B184-2E8DFD426E8E}");

                /// <summary>
                ///   Link
                ///   <para>{D408E265-89ED-4AA0-B184-2E8DFD426E8E}</para>
                /// </summary>
                public const string Link_FieldName = "Link";

                /// <summary>
                ///   MobileHeading
                ///   <para>{A98B6C07-A136-4A57-AF97-D7756F11A891}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MobileHeading = new Sitecore.Data.ID("{A98B6C07-A136-4A57-AF97-D7756F11A891}");

                /// <summary>
                ///   MobileHeading
                ///   <para>{A98B6C07-A136-4A57-AF97-D7756F11A891}</para>
                /// </summary>
                public const string MobileHeading_FieldName = "MobileHeading";

                /// <summary>
                ///   MobileLabel
                ///   <para>{74A7A1C8-256D-4CE1-86BE-F247FD5271FA}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MobileLabel = new Sitecore.Data.ID("{74A7A1C8-256D-4CE1-86BE-F247FD5271FA}");

                /// <summary>
                ///   MobileLabel
                ///   <para>{74A7A1C8-256D-4CE1-86BE-F247FD5271FA}</para>
                /// </summary>
                public const string MobileLabel_FieldName = "MobileLabel";

            }
        }
        #endregion
    }
}


