﻿namespace Athena.Feature.Header
{
	public class ParametersTemplates
	{
		public struct AlternateHeaderBannerParameters
		{
			public const string IsLeftAligned = "Is Left Aligned";
			public static string IsMobileContentUnderBanner = "is Mobile Content Under Banner";
			public const string IsAltHeader = "Is Alt Header";
		}
	}
}		