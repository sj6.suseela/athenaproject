﻿using Sitecore.Data;

namespace Athena.Feature.Header
{
	public partial class Templates
	{
		public struct LinkItem
		{
			public static ID TemplateId = new ID("{6ECF334F-337F-4E21-B46E-D8B1998D7DEE}");

			public struct Fields
			{
				public static ID Link = new ID("{B02B3D52-D1C3-4112-B4F4-959043FB552A}");
			}
		}
	}
}