﻿namespace Athena.Feature.Header.Pipelines
{
	using Services;
	using Foundation.DependencyInjection.Pipelines;
	using Microsoft.Extensions.DependencyInjection;

	public class RegisterServices
	{
		public void Process(InitializeDependencyInjectionArgs args)
		{
			args.ServiceCollection.AddScoped<IUtilityBarService, UtilityBarService>();
			args.ServiceCollection.AddScoped<IMasterHeaderService, MasterHeaderService>();
			args.ServiceCollection.AddScoped<IHeaderService, HeaderService>();
			args.ServiceCollection.AddScoped<IHeaderBannerService, HeaderBannerService>();
		}
	}
}