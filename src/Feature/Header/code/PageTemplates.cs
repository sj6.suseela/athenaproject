﻿namespace Athena.Feature.Header
{
	using Sitecore.Data;

	internal static class PageTemplates
	{
		public struct PageProperties
		{
			public struct Fields
			{
				public static ID HighlightActiveSite = new ID("{C3CB6B8D-1B8B-4F29-B0A5-55735D843A6F}");
			}
		}
	}
}