﻿using System.Collections.Generic;

namespace Athena.Feature.Header.Models.UtilityBar
{
	public class UtilityBarViewModel
	{
		public List<UtilityNavigationItem> Items { get; set; }
	}

	public class UtilityNavigationItem
	{
		public string Link { get; set; }
	}
}