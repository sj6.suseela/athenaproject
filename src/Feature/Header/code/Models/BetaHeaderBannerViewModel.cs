﻿using Athena.Foundation.Elements.Models;

namespace Athena.Feature.Header.Models
{
	public class BetaHeaderBannerViewModel : AlternateHeaderBannerViewModel
	{
		public BetaContentModel Beta {get; set;}	

	}

}