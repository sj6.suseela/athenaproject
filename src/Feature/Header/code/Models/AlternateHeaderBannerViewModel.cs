﻿//using Athena.Foundation.Theme;

namespace Athena.Feature.Header.Models
{
	public class AlternateHeaderBannerViewModel: HeaderBannerViewModel
	{
		public bool IsAltHeader { get; set; } = false;

		public bool IsLeftAligned { get; set; } = false;

		public bool IsMobileContentUnderBanner { get; set; } = false;

	}
}	