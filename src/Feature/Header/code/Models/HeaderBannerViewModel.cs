﻿using Athena.Foundation.Elements.Models;

namespace Athena.Feature.Header.Models
{
	public class HeaderBannerViewModel
	{
		public string Height = "mini";

		public ImageElement DesktopImage { get; set; }

		public ImageElement MobImage { get; set; }

		public string Headline { get; set; }

		public string Subheading { get; set; }
		
		public string Label { get; set; }

		public string LabelLink { get; set; }
		
		public string ColourOverlay { get; set; }

		//public HeaderBannerTheme  Theme { get; set; }

		public bool HasSecNav { get; set; }

		

	}

}