﻿using Athena.Feature.Header.Models.PrimaryNav;
using Athena.Foundation.Elements.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Athena.Feature.Header.Models
{
	public class HeaderViewModel
	{
		public List<Header> Header { get; set; }

		public string Logo { get; set; }

		public string LogoTag { get; set; }

		public LinkElement LogoLink { get; set; }

		public Partner Partner { get; set; } = new Partner();

		[JsonProperty("primarynav")]
		public PrimaryNavViewModel PrimaryNav { get; set; } = new PrimaryNavViewModel();

		public HeaderSearchViewModel Search { get; set; }

		public bool IsInvertedColour { get; set; }

		public bool IsMini { get; set; }
		public ContactUsViewModel ContactUs { get; set; }
		public string LogoType { get; set; }
		public string OptionalTitle { get; set; }
	}
	public class Header
	{
		public string Logo { get; set; }

		public string LogoTag { get; set; }

		public LinkElement LogoLink { get; set; }

		public Partner Partner { get; set; } = new Partner();

		[JsonProperty("primarynav")]
		public PrimaryNavViewModel PrimaryNav { get; set; } = new PrimaryNavViewModel();

		public HeaderSearchViewModel Search { get; set; }

		public bool IsInvertedColour { get; set; }

		public bool IsMini { get; set; }
		public ContactUsViewModel ContactUs { get; set; }
		public string LogoType { get; set; }
		public string OptionalTitle { get; set; }
	}
}