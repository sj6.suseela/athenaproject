﻿using Athena.Foundation.Elements.Models;

namespace Athena.Feature.Header.Models
{
	public class ContactUsViewModel
	{
		public string Label { get; set; }
		public LinkElement Link { get; set; }
		public bool Selected { get; set; }
	}
}