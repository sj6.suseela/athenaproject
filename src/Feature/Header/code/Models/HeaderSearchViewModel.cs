﻿using System.Collections.Generic;
using Athena.Foundation.Elements.Models;

namespace Athena.Feature.Header.Models
{
	public class HeaderSearchViewModel
	{
		public string Label { get; set; }

		public string PlaceholderText { get; set; }

		public LinkElement Link { get; set; }

		public int CharSearchLimit { get; set; }

		public string AjaxUrl { get; set; }

		public SearchLabels SearchLabels { get; set; }

		public string SearchImage { get; set; }

		public string NoReultsText { get; set; }

		public OtherBusinessText OtherBusinessText { get; set; }

		public List<string> Items { get; set; } = new List<string>();

		public string ViewAllLabel { get; set; }	
	}

	public class SearchLabels
	{
		public string NoSearch { get; set; }

		public string Results { get; set; }

		public string NoResults { get; set; }
	}

	public class OtherBusinessText
	{
		public string Heading { get; set; }
		public LinkElement Link { get; set; }
	}
}