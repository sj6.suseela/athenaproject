﻿using Athena.Feature.Header.Models.PrimaryNav;
using Athena.Foundation.Elements.Models;
using Newtonsoft.Json;

namespace Athena.Feature.Header.Models
{
	public class MicrositeHeaderNavModel
	{
		public string Logo { get; set; }
		public string LogoTag { get; set; }
		public LinkElement LogoLink { get; set; }
		public Partner Partner { get; set; } = new Partner();
		public bool IsInvertedColour { get; set; }		
		public string LogoType { get; set; }
		public string BackCta { get; set; }
	}
}