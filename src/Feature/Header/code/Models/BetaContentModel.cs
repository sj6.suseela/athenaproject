﻿namespace Athena.Feature.Header.Models
{
	public class BetaContentModel
	{
		public string BetaTxt { get; set; }
		public string FeedbackCta { get; set; }
	}
}