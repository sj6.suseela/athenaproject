﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using Athena.Foundation.Elements.Models;

namespace Athena.Feature.Header.Models
{
	public class MasterHeaderViewModel
	{
		public string MobileLabel { get; set; }
		public List<MasterHeaderLinkItem> Items { get; set; }
	}

	public class MasterHeaderLinkItem
	{
		public string Label { get; set; }
		public string MobileLabel { get; set; }
		
		public LinkElement Link { get; set; }
		public bool IsSelected { get; set; }
	}
}