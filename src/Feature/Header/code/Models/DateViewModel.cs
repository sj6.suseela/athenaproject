﻿using System;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;

namespace Athena.Feature.Header.Models
{
	public class DateViewModel
	{
		public static readonly DateViewModel Empty =
			new DateViewModel
			{
				Day = string.Empty,
				Month = new MonthViewModel
				{
					Label = string.Empty,
					Number = string.Empty
				},
				Year = string.Empty,
				Time = string.Empty
			};

		public DateViewModel(DateTime dateTime)
		{
			Day = dateTime.Day.ToString();
			Month = new MonthViewModel
			{
				Label = dateTime.ToString("MMMM"),
				Number = dateTime.ToString("MM")
			};
			Year = dateTime.Year.ToString();
			Time = dateTime.ToString("HH:mm");

		}

		private DateViewModel()
		{
		}

		public string Day { get; private set; }

		public MonthViewModel Month { get; private set; }

		public string Year { get; private set; }

		public string Time { get; private set; }


		public static DateViewModel FromDateField(params DateField[] dateField)
		{
			Assert.ArgumentNotNull(dateField, nameof(dateField));
			Assert.ArgumentCondition(dateField.Length > 0, nameof(dateField), "dateField required");

			for (var i = 0; i < dateField.Length; i++)
			{
				if (dateField[i].DateTime > DateTime.MinValue)
				{
					return new DateViewModel(dateField[i].DateTime);
				}
			}

			return new DateViewModel(dateField[0].DateTime);
		}

		public class MonthViewModel
		{
			public string Label { get; set; }

			public string Number { get; set; }
		}
	}
}