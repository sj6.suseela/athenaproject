﻿using Athena.Foundation.Elements.Models;

namespace Athena.Feature.Header.Models
{
	public class Partner
	{
		public string Logo { get; set; }

		public LinkElement LogoLink { get; set; }
	}
}