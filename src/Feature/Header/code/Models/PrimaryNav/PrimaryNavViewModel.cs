﻿using Athena.Foundation.Elements.Models;
using System.Collections.Generic;

namespace Athena.Feature.Header.Models.PrimaryNav
{
	public class PrimaryNavViewModel
	{
		public List<NavigationGroup> Groups { get; set; } = new List<NavigationGroup>();
	}

	public class NavigationGroup : NavigationItemBase
	{
		public int Position { get; set; }
		public bool Visible { get; set; } = true;
		public bool GroupOnMobile { get; set; }
		public List<NavigationItem> Items { get; set; }
		public bool IsOverflow { get; set; }
		
	}

	public class NavigationItem : NavigationItemBase
	{
		public List<NavigationItemBase> Children { get; set; }		
	}

	public class SubNavigationItem : NavigationItemBase
	{
	}

	public abstract class NavigationItemBase
	{
		public string Label { get; set; }
		public LinkElement Link { get; set; }

		public bool Selected { get; set; }
		public string MobileLabel { get; set; }
	}
}