﻿namespace Athena.Feature.Header.Models
{
	public class Cta
	{
		public const string Primary = "primary";
		public const string Secondary = "secondary";

		public string Type { get; set; }
		public string Link { get; set; }

		public Cta(string type, string link)
		{
			Type = type;
			Link = link;
		}
	}
}