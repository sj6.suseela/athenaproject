﻿using System.Web.Mvc;
using Athena.Feature.Header.Services;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Header.Controller
{
	public class MasterHeaderController : System.Web.Mvc.Controller
	{
		private readonly IMasterHeaderService _masterHeaderService;

		public MasterHeaderController(IMasterHeaderService masterHeaderService)
		{
			_masterHeaderService = masterHeaderService;
		}

		public ActionResult Index()
		{
			var viewModel = _masterHeaderService.GetMasterHeaderViewModel(RenderingContext.Current);

			return this.GetModuleView(() => View(viewModel));
		}
	}
}