﻿using System.Web.Mvc;
using Athena.Feature.Header.Services;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Header.Controller
{
	public class HeaderController : System.Web.Mvc.Controller
	{
		private readonly IHeaderService _headerService;
		private readonly IHeaderBannerService _bannerService;
				
		public HeaderController(IHeaderService headerService, IHeaderBannerService bannerService)
		{
			_headerService = headerService;
			_bannerService = bannerService;
		}

		// GET: Header
		public ActionResult Index()
		{
			var viewModel = _headerService.GetHeader(RenderingContext.Current, Context.Item, Request?.Url?.Scheme);

			return this.GetModuleView(() => View(viewModel));
		}

		public ActionResult MicrositeNav()
		{
			var viewModel = _headerService.GetMicrositeNav(RenderingContext.Current, Context.Item, Request?.Url?.Scheme);

			return this.GetModuleView(() => View(viewModel));
		}

		public ActionResult HeaderBanner()
		{
			var viewModel = _bannerService.GetHeaderBanner(RenderingContext.Current, Context.Item);

			return this.GetModuleView(() => View(viewModel));
		}

		public ActionResult BetaHeaderBanner()
		{
			var viewModel = _bannerService.GetBetaHeaderBanner(RenderingContext.Current, Context.Item);

			return this.GetModuleView(() => View(viewModel));
		}

		public ActionResult AlternateHeaderBanner()
		{
			var viewModel = _bannerService.GetAlternateHeaderBanner(RenderingContext.Current, Context.Item);

			return this.GetModuleView(() => View(viewModel));
		}
	}
}