﻿using System.Web.Mvc;
using Athena.Feature.Header.Services;
using Athena.Foundation.Common.Attributes;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Header.Controller
{
	public class UtilityBarController : System.Web.Mvc.Controller
	{
		private readonly IUtilityBarService _utilityBarService;

		public UtilityBarController(IUtilityBarService utilityBarService)
		{
			_utilityBarService = utilityBarService;
		}

		[DeprecatedRendering]
		public ActionResult Index()
		{
			var viewModel = _utilityBarService.GetUtilityBar(RenderingContext.Current);

			return this.GetModuleView(() => View(viewModel));
		}
	}
}