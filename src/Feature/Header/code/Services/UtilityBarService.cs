﻿using System.Linq;
using Athena.Feature.Header.Models.UtilityBar;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace Athena.Feature.Header.Services
{
	public class UtilityBarService : IUtilityBarService
	{
		public UtilityBarViewModel GetUtilityBar(RenderingContext renderingContext)
		{
			var item = renderingContext?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.UtilityBar.ID))
				return null;
			var model = new UtilityBarViewModel();
			var list = item.ChildrenOfTemplate(Templates.UtilityNavigationItem.ID)
				.Select(navItem => new UtilityNavigationItem()
				{
					Link = navItem.RenderField(Templates.UtilityNavigationItem.Fields.NavigationLink)
				})
				.ToList();
			model.Items = list;
			return (list.Any() && !Context.PageMode.IsExperienceEditor) ? model : null;
		}
	}
}