﻿using Athena.Feature.Header.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Header.Services
{
	public interface IHeaderBannerService
	{
		HeaderBannerViewModel GetHeaderBanner(RenderingContext renderingContext, Item page);
		HeaderBannerViewModel GetBetaHeaderBanner(RenderingContext renderingContext, Item page);
		HeaderBannerViewModel GetAlternateHeaderBanner(RenderingContext renderingContext, Item page);
		void AddBaseHeader(HeaderBannerViewModel headerBanner, RenderingContext renderingContext, Item page);
		void AddBetaHeader(BetaHeaderBannerViewModel headerBanner, RenderingContext renderingContext, Item page);
		void AddAlternateHeader(AlternateHeaderBannerViewModel headerBanner, RenderingContext renderingContext, Item page);
		void AddHeaderImages(HeaderBannerViewModel headerBanner, Item item);
	}
}