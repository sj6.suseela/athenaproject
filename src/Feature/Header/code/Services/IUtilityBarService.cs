﻿using Athena.Feature.Header.Models.UtilityBar;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Header.Services
{
	public interface IUtilityBarService
	{
		UtilityBarViewModel GetUtilityBar(RenderingContext renderingContext);
	}
}