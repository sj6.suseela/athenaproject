﻿using System;
using System.Collections.Generic;
using Athena.Feature.Header.Models;
using Athena.Foundation.Elements.Extensions;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Mvc.Presentation;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Sites;

namespace Athena.Feature.Header.Services
{
	public class MasterHeaderService : IMasterHeaderService
	{
		public MasterHeaderViewModel GetMasterHeaderViewModel(RenderingContext renderingContext)
		{
			var item = renderingContext?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.MasterHeader.ID))
				return null;

			var page = renderingContext.PageContext.Item;
			var highlightSelected = MainUtil.GetBool(page[PageTemplates.PageProperties.Fields.HighlightActiveSite], false);

			var linkItems = new List<MasterHeaderLinkItem>();

			BuildLinkItem(item, linkItems, highlightSelected, Templates.MasterHeader.Fields.CTA1Link, Templates.MasterHeader.Fields.CTA1Label, Templates.MasterHeader.Fields.CTA1Highlight);
			BuildLinkItem(item, linkItems, highlightSelected, Templates.MasterHeader.Fields.CTA2Link, Templates.MasterHeader.Fields.CTA2Label, Templates.MasterHeader.Fields.CTA2Highlight);

			var model = new MasterHeaderViewModel()
			{
				MobileLabel = item.RenderField(Templates.MasterHeader.Fields.MobileLabel),
				Items = linkItems
			};
			
			return model.Items.Any() ? model : null;
		}

		private static void BuildLinkItem(Item item, List<MasterHeaderLinkItem> linkItems, bool highlightSelected,
			ID link,
			ID label, ID highlight)
		{
			var link1 = item.GetLinkElement(link);
			var link1Label = item.RenderField(label);

			if (Context.PageMode.IsExperienceEditor || (link1.Html.HasValue() && link1Label.HasValue()))
			{
				linkItems.Add(new MasterHeaderLinkItem()
				{
					Label = link1Label,
					MobileLabel = item.RenderField(Templates.MasterHeader.Fields.CTA1MobileLabel),
					Link = link1,
					IsSelected = highlightSelected && IsSelected(item, highlight)
				});
			}
		}

		private static bool IsSelected(Item cta, ID cta1Highlight)
		{
			CheckboxField field = cta.Fields[cta1Highlight];
			return field != null && field.Checked;
		}
	}
}