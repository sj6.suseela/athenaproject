﻿using System.Collections.Generic;
using Athena.Feature.Header.Models;
using Athena.Feature.Header.Models.PrimaryNav;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Header.Services
{
	public interface IHeaderService
	{
		HeaderViewModel GetHeader(RenderingContext renderingContext, Item page, string urlScheme);

		MicrositeHeaderNavModel GetMicrositeNav(RenderingContext renderingContext, Item page, string urlScheme);

		void AddBaseHeader(HeaderViewModel headerViewModel, RenderingContext renderingContext, Item page, string urlScheme);

	}
}	