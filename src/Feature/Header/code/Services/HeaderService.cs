﻿using System;
using System.Collections.Generic;
using System.Linq;
using Athena.Feature.Header.Models;
using Athena.Feature.Header.Models.PrimaryNav;
using Athena.Foundation.Elements.Extensions;
using Athena.Foundation.Elements.Models;
using Athena.Foundation.ReusableTemplates;
using Athena.Foundation.NoFollowLink.Extensions;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.StringExtensions;
using Convert = System.Convert;

namespace Athena.Feature.Header.Services
{
	public class HeaderService : IHeaderService
	{
		public const string DefaultLogoTag = "h1";

		public HeaderViewModel GetHeader(RenderingContext renderingContext, Item page, string urlScheme)
		{
			var headerViewModel = new HeaderViewModel();

			AddBaseHeader(headerViewModel, renderingContext, page, urlScheme);

			return headerViewModel;
		}

		public void AddBaseHeader(HeaderViewModel headerViewModel, RenderingContext renderingContext, Item page, string urlScheme)
		{
			var item = renderingContext?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.Header.ID))
				return;

			headerViewModel.Logo = item.RenderField(Templates.Header.Fields.LogoImage);
			headerViewModel.LogoLink = item.GetLinkElement(Templates.Header.Fields.LogoLink, defaultHref: "/");
			headerViewModel.LogoType = item.GetLookupValue(Templates.Header.Fields.LogoType);
			headerViewModel.LogoTag = MainUtil.GetBool(page[Templates.HeaderPageFields.Fields.IsLogoH1], false) ? DefaultLogoTag : null; 
			headerViewModel.IsInvertedColour = page.Fields[Templates.HeaderPageFields.Fields.InvertTextColour]?.IsChecked() ?? false;
			headerViewModel.IsMini = page.Fields[Templates.HeaderPageFields.Fields.IsMini]?.IsChecked() ?? false;

			//headerViewModel.OptionalTitle = item.RenderField(Templates.Header.Fields.OptionalTitle);

			//headerViewModel.Partner.Logo = item.RenderField(Templates.Header.Fields.PartnerLogoImage);
			//headerViewModel.Partner.LogoLink = item.GetLinkElement(Templates.Header.Fields.PartnerLogoLink, defaultHref: "/");

			var primaryNav = item.ChildrenOfTemplate(Templates.PrimaryNavigation.ID).FirstOrDefault();

			if (primaryNav != null)
			{
				var pos = 0;

				headerViewModel.PrimaryNav.Groups.AddRange(BuildNavigationGroup(renderingContext, primaryNav));

				// Add the 'More' label item
				var moreGroup = new NavigationGroup
				{
					Position = pos,
					Visible = false,
					Link = new LinkElement() { Url = "#" },
					Label = primaryNav.RenderField(Templates.PrimaryNavigation.Fields.MoreLabel),
					MobileLabel = primaryNav.RenderField(Templates.PrimaryNavigation.Fields.MoreLabel),
					IsOverflow = true
				};

				headerViewModel.PrimaryNav.Groups.Add(moreGroup);
			}

			// Add Search item

			var search = item.ChildrenOfTemplate(Templates.HeaderSearch.ID).FirstOrDefault();

			if (search != null)
			{
				headerViewModel.Search = new HeaderSearchViewModel
				{
					Label = search.RenderField(Templates.HeaderSearch.Fields.Label),
					PlaceholderText = search[Templates.HeaderSearch.Fields.Placeholder],
					CharSearchLimit = Convert.ToInt32(search[Templates.HeaderSearch.Fields.CharSearchLimit]),
					NoReultsText = search[Templates.HeaderSearch.Fields.NoResultsText],
					SearchImage = search.RenderField(Templates.HeaderSearch.Fields.SearchImage),
					ViewAllLabel = search.RenderField(Templates.HeaderSearch.Fields.ViewAllLabel),
					SearchLabels = new SearchLabels
					{
						NoResults = search[Templates.HeaderSearch.Fields.NoResults],
						NoSearch = search[Templates.HeaderSearch.Fields.NoSearch],
						Results = search[Templates.HeaderSearch.Fields.Results]
					},
					OtherBusinessText = new OtherBusinessText()
					{
						Heading = search[Templates.HeaderSearch.Fields.OtherBusinessHeading],
						Link = search.GetLinkElement(Templates.HeaderSearch.Fields.OtherBusinessLink, true)
					}
				};

				headerViewModel.Search.AjaxUrl = new UriBuilder(urlScheme ?? Uri.UriSchemeHttps, $"{Context.Site.TargetHostName}{search[Templates.HeaderSearch.Fields.AjaxUrl]}").Uri.AbsoluteUri;
				headerViewModel.Search.Link = search.GetLinkElement(Templates.HeaderSearch.Fields.Url, defaultHref: "#");
				MultilistField links = search.Fields[Templates.HeaderSearch.Fields.Links];

				//if (links.TargetIDs.Length > 0)
				//{
				//	headerViewModel.Search.Items.AddRange(links.TargetIDs.Select(i => item.Database.GetItem(i)?.RenderField(Templates.LinkItem.Fields.Link)));
				//}
			}

			 // add Contact Us item
			LinkField contactUs = item.Fields[Templates.HeaderContactUs.Fields.Link];
			if (!contactUs.Value.IsNullOrEmpty())
			{
				var link = item.GetLinkElement(Templates.HeaderContactUs.Fields.Link, true);
				headerViewModel.ContactUs = new ContactUsViewModel()
				{
					Link = link,
					Label = link.Label,
					Selected = IsSelectedPageOrAncestorOf(contactUs, renderingContext)
				};
			}
		}

		private List<NavigationGroup> BuildNavigationGroup(RenderingContext renderingContext, Item navigationItem)
		{
			var pos = 0;
			var navGroups = new List<NavigationGroup>();
			foreach (var navGroup in navigationItem.ChildrenOfTemplate(Templates.IsNavigationItem.ID))
			{
				LinkField href = navGroup.Fields[Templates.IsNavigationItem.Fields.Link];
				var group = new NavigationGroup
				{
					Position = pos,
					Link = navGroup.GetLinkElement(Templates.IsNavigationItem.Fields.Link),
					Label = navGroup.RenderField(Templates.IsNavigationItem.Fields.Label),
					MobileLabel = navGroup.RenderField(Templates.IsNavigationItem.Fields.MobileLabel),
					Selected = IsSelectedPageOrAncestorOf(href, renderingContext)
				};

				/* Changed this from only replacing null with a # to replacing empty string as well,
				 * so that Google Tag Manager doesn't cause the browser to follow the link (to the current page)
				 * https://live.wearethink.com/browse/LVFS-145 
				*/
				if (string.IsNullOrEmpty(group.Link.Url))
				{
					group.Link.Url = "#";
				}

				if (navGroup.HasChildren)
				{
					group.Items = new List<NavigationItem>();
					group.GroupOnMobile = true;

					foreach (var navItem in navGroup.ChildrenOfTemplate(Templates.IsNavigationItem.ID))
					{
						LinkField niHref = navItem.Fields[Templates.IsNavigationItem.Fields.Link];
						var ni = new NavigationItem
						{
							Link = navItem.GetLinkElement(Templates.IsNavigationItem.Fields.Link),
							Label = navItem.RenderField(Templates.IsNavigationItem.Fields.Label),
							MobileLabel = navItem.RenderField(Templates.IsNavigationItem.Fields.MobileLabel),
							Selected = IsSelectedPageOrAncestorOf(niHref, renderingContext)
						};

						if (navItem.HasChildren)
						{
							ni.Children = new List<NavigationItemBase>();
							foreach (var subnavItem in navItem.ChildrenOfTemplate(Templates.IsNavigationItem.ID))
							{
								LinkField sniHref = subnavItem.Fields[Templates.IsNavigationItem.Fields.Link];
								var sni = new SubNavigationItem
								{
									Link = subnavItem.GetLinkElement(Templates.IsNavigationItem.Fields.Link),
									Label = subnavItem.RenderField(Templates.IsNavigationItem.Fields.Label),
									MobileLabel = subnavItem.RenderField(Templates.IsNavigationItem.Fields.MobileLabel),
									Selected = IsSelectedPageOrAncestorOf(sniHref, renderingContext)
								};

								ni.Children.Add(sni);
							}
						}

						group.Items.Add(ni);
					}
				}

				navGroups.Add(group);
				pos++;
			}
			return navGroups;
		}
		private static bool IsSelectedPageOrAncestorOf(LinkField linkField, RenderingContext context)
		{
			if (linkField == null)
				return false;

			if (linkField.IsInternal)
			{
				if (ID.IsNullOrEmpty(linkField.TargetID))
				{
					return false;
				}

				var item = context.PageContext.Item;
				while (item != null)
				{
					if (linkField.TargetID == item.ID)
					{
						return true;
					}
					item = item.Parent;
				}
				return false;
			}
			var linkUrl = linkField.Url;
			if (Uri.IsWellFormedUriString(linkUrl, UriKind.Relative))
			{
				return string.Equals(linkUrl.Split('?', '#')[0], context.PageContext.Item.Url(), StringComparison.CurrentCultureIgnoreCase);
			}

			return false;
		}

		public MicrositeHeaderNavModel GetMicrositeNav(RenderingContext renderingContext, Item page, string urlScheme)
		{
			var headerViewModel = new MicrositeHeaderNavModel();

			AddMicrositeNav(headerViewModel, renderingContext, page, urlScheme);

			return headerViewModel;
		}

		public void AddMicrositeNav(MicrositeHeaderNavModel headerViewModel, RenderingContext renderingContext, Item page, string urlScheme)
		{
			var item = renderingContext?.Rendering?.Item;

			if (item == null || !item.IsDerived(Templates.MicrositeHeader.ID))
				return;

			headerViewModel.Logo = item.RenderField(Templates.MicrositeHeader.Fields.LogoImage);
			headerViewModel.LogoLink = item.GetLinkElement(Templates.MicrositeHeader.Fields.LogoLink, defaultHref: "/");
			headerViewModel.LogoType = item.GetLookupValue(Templates.MicrositeHeader.Fields.LogoType);
			headerViewModel.LogoTag = MainUtil.GetBool(page[Templates.HeaderPageFields.Fields.IsLogoH1], false) ? DefaultLogoTag : null;
			headerViewModel.IsInvertedColour = page.Fields[Templates.HeaderPageFields.Fields.InvertTextColour]?.IsChecked() ?? false;			

			headerViewModel.Partner.Logo = item.RenderField(Templates.MicrositeHeader.Fields.PartnerLogoImage);
			headerViewModel.Partner.LogoLink = item.GetLinkElement(Templates.MicrositeHeader.Fields.PartnerLogoLink, defaultHref: "/");

			headerViewModel.BackCta = item.RenderField(Templates.MicrositeHeader.Fields.BackCta);

			
		}
	}
}