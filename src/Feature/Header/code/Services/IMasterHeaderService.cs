﻿using Athena.Feature.Header.Models;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Header.Services
{
	public interface IMasterHeaderService
	{
		MasterHeaderViewModel GetMasterHeaderViewModel(RenderingContext renderingContext);
	}
}