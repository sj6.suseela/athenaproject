﻿using Athena.Foundation.Elements.Extensions;
using Athena.Feature.Header.Models;
using Athena.Foundation.Multisite.Utils;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Athena.Foundation.WebAbstractions.Services;
using Sitecore.Data.Items;
using Sitecore.Layouts;
using Sitecore.Mvc.Presentation;
using Sitecore.StringExtensions;
using System.Linq;

namespace Athena.Feature.Header.Services
{
	/// <remarks>
	/// Registered as Singleton - do not store state
	/// </remarks>
	public class HeaderBannerService : BaseSitecoreControllerService, IHeaderBannerService
	{
		public HeaderBannerService(ISitecoreContextService sitecoreContextService) : base(sitecoreContextService)
		{
		}

		public HeaderBannerViewModel GetHeaderBanner(RenderingContext renderingContext, Item page)
		{
			var headerViewModel = new HeaderBannerViewModel();

			AddBaseHeader(headerViewModel, renderingContext, page);

			return headerViewModel;
		}
		public HeaderBannerViewModel GetAlternateHeaderBanner(RenderingContext renderingContext, Item page)
		{
			var headerViewModel = new AlternateHeaderBannerViewModel();

			AddAlternateHeader(headerViewModel, renderingContext, page);

			return headerViewModel;
		}

		public HeaderBannerViewModel GetBetaHeaderBanner(RenderingContext renderingContext, Item page)
		{
			var headerViewModel = new BetaHeaderBannerViewModel();

			AddBetaHeader(headerViewModel, renderingContext, page);

			return headerViewModel;
		}

		

		public void AddBaseHeader(HeaderBannerViewModel headerBanner, RenderingContext renderingContext, Item page)
		{
			var item = GetDatasourceItem();

			if (item == null || !item.IsDerived(Template.HeaderBanner.TemplateId))
			{
				return;
			}			

			AddHeaderImages(headerBanner, item);

			headerBanner.Subheading = item.RenderField(Template.HeaderBanner.FieldIDs.Subheading);
			headerBanner.Headline = page.RenderField(Template.BasePage.FieldIDs.PageTitle);
			//headerBanner.Theme = renderingContext.GetTheme<HeaderBannerTheme>(item.Database);
			headerBanner.HasSecNav = HasSecondaryNavRendering(renderingContext);
			
		}

		public void AddAlternateHeader(AlternateHeaderBannerViewModel headerBanner, RenderingContext renderingContext, Item page)
		{
			var item = GetDatasourceItem();

			if (item == null || !item.IsDerived(Template.AlternateHeaderBanner.ID))
			{
				return;
			}

			AddBaseHeader(headerBanner, renderingContext, page);

			headerBanner.IsAltHeader = RenderingContext.Current.GetRenderingParameterValue(ParametersTemplates.AlternateHeaderBannerParameters.IsAltHeader) == "1";
			headerBanner.IsLeftAligned = RenderingContext.Current.GetRenderingParameterValue(ParametersTemplates.AlternateHeaderBannerParameters.IsLeftAligned) == "1";
			headerBanner.IsMobileContentUnderBanner = RenderingContext.Current.GetRenderingParameterValue(ParametersTemplates.AlternateHeaderBannerParameters.IsMobileContentUnderBanner) == "1";
			headerBanner.Label = item.RenderField(Template.AlternateHeaderBanner.FieldsIDs.HubName);
			headerBanner.LabelLink = item.RenderField(Template.AlternateHeaderBanner.FieldsIDs.HubLink, Template.AlternateHeaderBanner.FieldsIDs.HubName.ToString());

		}

		public void AddBetaHeader(BetaHeaderBannerViewModel headerBanner, RenderingContext renderingContext, Item page)
		{
			var item = GetDatasourceItem();

			if (item == null || !item.IsDerived(Template.BetaHeaderBanner.TemplateId))
			{
				return;
			}

			AddAlternateHeader(headerBanner, renderingContext, page);

			headerBanner.Headline = item.RenderField(Template.BetaHeaderBanner.FieldIDs.Headline);
			headerBanner.Label = item.RenderField(Template.BetaHeaderBanner.FieldIDs.HeaderLabel);
			headerBanner.LabelLink = null;
			headerBanner.Beta = new BetaContentModel()
			{
				BetaTxt = item[Template.BetaHeaderBanner.FieldIDs.BetaTooltip],
				FeedbackCta = item[Template.BetaHeaderBanner.FieldIDs.FeedbackCta]
			};

		}

		private static bool HasSecondaryNavRendering(RenderingContext renderingContext)
		{
			var hasMatchedRendering = false;
			var db = renderingContext?.PageContext.Item.Database;

			if (renderingContext != null)
			{
				LayoutUtils.ApplyRenderingCollectionActionToAllFinalRenderings(renderingContext.PageContext.Item, renderings =>
				{
					hasMatchedRendering = renderings.OfType<RenderingDefinition>()
						.Where(r => !r.Datasource.IsNullOrEmpty())
						.Where(r => r.ItemID == Template.Renderings.SecondaryNavigation.ToString())
						.Select(r => db.GetItem(r.Datasource))
						.Any(x => x.IsDerived(Template.SecondaryNavigation.TemplateId));
				});
			}

			return hasMatchedRendering;
		}

		public void AddHeaderImages(HeaderBannerViewModel headerBanner, Item item)
		{
			DesktopImage(headerBanner, item);

			MobileImage(headerBanner, item);
		}

		private static void DesktopImage(HeaderBannerViewModel headerBanner, Item item)
		{
			headerBanner.DesktopImage = item.GetImageElement(Template.HeaderBanner.FieldIDs.DesktopImage);
		}

		private static void MobileImage(HeaderBannerViewModel headerBanner, Item item)
		{
			headerBanner.MobImage = item.GetImageElement(Template.HeaderBanner.FieldIDs.MobileImage);
		}
	}
}