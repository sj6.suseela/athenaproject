﻿






// ReSharper disable InconsistentNaming
namespace Athena.Feature.Header
{
    public static partial class Templates
    {
        #region /sitecore/templates/Feature/Header/_AlternateHeaderBanner
        /// <summary>
        ///   _AlternateHeaderBanner
        ///   <para>ID: {1B4749EA-4461-4C9A-9E41-595076A16B35}</para>
        ///   <para>Path: /sitecore/templates/Feature/Header/_AlternateHeaderBanner</para>
        /// </summary>
        public static class AlternateHeaderBanner
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{1B4749EA-4461-4C9A-9E41-595076A16B35}"));
            public const string IdString = "{1B4749EA-4461-4C9A-9E41-595076A16B35}";

            public static class Fields
            {
                /// <summary>
                ///   Hub Link
                ///   <para>{BCC96E15-C9E2-44E3-BA9E-20865DAF1B78}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID HubLink = new Sitecore.Data.ID("{BCC96E15-C9E2-44E3-BA9E-20865DAF1B78}");

                /// <summary>
                ///   Hub Link
                ///   <para>{BCC96E15-C9E2-44E3-BA9E-20865DAF1B78}</para>
                /// </summary>
                public const string HubLink_FieldName = "Hub Link";

                /// <summary>
                ///   Hub Name
                ///   <para>{57E71C8A-B7E4-4FF9-B4E1-4FA6FBB6DF03}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID HubName = new Sitecore.Data.ID("{57E71C8A-B7E4-4FF9-B4E1-4FA6FBB6DF03}");

                /// <summary>
                ///   Hub Name
                ///   <para>{57E71C8A-B7E4-4FF9-B4E1-4FA6FBB6DF03}</para>
                /// </summary>
                public const string HubName_FieldName = "Hub Name";

            }
        }
        #endregion
        #region /sitecore/templates/Feature/Header/_MasterHeader
        /// <summary>
        ///   _MasterHeader
        ///   <para>ID: {C08CED6F-62D4-4F8C-938B-A139B9371976}</para>
        ///   <para>Path: /sitecore/templates/Feature/Header/_MasterHeader</para>
        /// </summary>
        public static class MasterHeader
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{C08CED6F-62D4-4F8C-938B-A139B9371976}"));
            public const string IdString = "{C08CED6F-62D4-4F8C-938B-A139B9371976}";

            public static class Fields
            {
                /// <summary>
                ///   CTA 1 Highlight
                ///   <para>{0AA28300-2AD4-4972-BACF-34B888BCAEBD}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTA1Highlight = new Sitecore.Data.ID("{0AA28300-2AD4-4972-BACF-34B888BCAEBD}");

                /// <summary>
                ///   CTA 1 Highlight
                ///   <para>{0AA28300-2AD4-4972-BACF-34B888BCAEBD}</para>
                /// </summary>
                public const string CTA1Highlight_FieldName = "CTA 1 Highlight";

                /// <summary>
                ///   CTA 1 Label
                ///   <para>{0FBC2E12-EEED-4C8A-A49D-7FD07F9F14C9}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTA1Label = new Sitecore.Data.ID("{0FBC2E12-EEED-4C8A-A49D-7FD07F9F14C9}");

                /// <summary>
                ///   CTA 1 Label
                ///   <para>{0FBC2E12-EEED-4C8A-A49D-7FD07F9F14C9}</para>
                /// </summary>
                public const string CTA1Label_FieldName = "CTA 1 Label";

                /// <summary>
                ///   CTA 1 Link
                ///   <para>{C96B1C43-AE39-4FD3-B2B6-0BA420791334}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTA1Link = new Sitecore.Data.ID("{C96B1C43-AE39-4FD3-B2B6-0BA420791334}");

                /// <summary>
                ///   CTA 1 Link
                ///   <para>{C96B1C43-AE39-4FD3-B2B6-0BA420791334}</para>
                /// </summary>
                public const string CTA1Link_FieldName = "CTA 1 Link";

                /// <summary>
                ///   CTA 1 Mobile Label
                ///   <para>{682340A7-8D08-487F-BF61-AFB7A5C3A9D0}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTA1MobileLabel = new Sitecore.Data.ID("{682340A7-8D08-487F-BF61-AFB7A5C3A9D0}");

                /// <summary>
                ///   CTA 1 Mobile Label
                ///   <para>{682340A7-8D08-487F-BF61-AFB7A5C3A9D0}</para>
                /// </summary>
                public const string CTA1MobileLabel_FieldName = "CTA 1 Mobile Label";

                /// <summary>
                ///   CTA 2 Highlight
                ///   <para>{B732BD37-760C-4BF3-B1D2-8723412CF98F}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTA2Highlight = new Sitecore.Data.ID("{B732BD37-760C-4BF3-B1D2-8723412CF98F}");

                /// <summary>
                ///   CTA 2 Highlight
                ///   <para>{B732BD37-760C-4BF3-B1D2-8723412CF98F}</para>
                /// </summary>
                public const string CTA2Highlight_FieldName = "CTA 2 Highlight";

                /// <summary>
                ///   CTA 2 Label
                ///   <para>{41534A4F-80E2-4BF4-B53F-694A366F8CD0}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTA2Label = new Sitecore.Data.ID("{41534A4F-80E2-4BF4-B53F-694A366F8CD0}");

                /// <summary>
                ///   CTA 2 Label
                ///   <para>{41534A4F-80E2-4BF4-B53F-694A366F8CD0}</para>
                /// </summary>
                public const string CTA2Label_FieldName = "CTA 2 Label";

                /// <summary>
                ///   CTA 2 Link
                ///   <para>{4E1FC372-20EE-464D-9354-B9B9EEB8B7CE}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTA2Link = new Sitecore.Data.ID("{4E1FC372-20EE-464D-9354-B9B9EEB8B7CE}");

                /// <summary>
                ///   CTA 2 Link
                ///   <para>{4E1FC372-20EE-464D-9354-B9B9EEB8B7CE}</para>
                /// </summary>
                public const string CTA2Link_FieldName = "CTA 2 Link";

                /// <summary>
                ///   CTA 2 Mobile Label
                ///   <para>{54E55171-454D-4684-A0AF-76DF528AF118}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTA2MobileLabel = new Sitecore.Data.ID("{54E55171-454D-4684-A0AF-76DF528AF118}");

                /// <summary>
                ///   CTA 2 Mobile Label
                ///   <para>{54E55171-454D-4684-A0AF-76DF528AF118}</para>
                /// </summary>
                public const string CTA2MobileLabel_FieldName = "CTA 2 Mobile Label";

                /// <summary>
                ///   Mobile Label
                ///   <para>{2D8344B8-05D3-4A86-AAB9-CE2618ACF830}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID MobileLabel = new Sitecore.Data.ID("{2D8344B8-05D3-4A86-AAB9-CE2618ACF830}");

                /// <summary>
                ///   Mobile Label
                ///   <para>{2D8344B8-05D3-4A86-AAB9-CE2618ACF830}</para>
                /// </summary>
                public const string MobileLabel_FieldName = "Mobile Label";

            }
        }
        #endregion
        #region /sitecore/templates/Feature/Header/_UtilityBar
        /// <summary>
        ///   _UtilityBar
        ///   <para>ID: {7702816E-9FA4-4E32-AE21-045B306A7C11}</para>
        ///   <para>Path: /sitecore/templates/Feature/Header/_UtilityBar</para>
        /// </summary>
        public static class UtilityBar
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{7702816E-9FA4-4E32-AE21-045B306A7C11}"));
            public const string IdString = "{7702816E-9FA4-4E32-AE21-045B306A7C11}";

        }
        #endregion
        #region /sitecore/templates/Feature/Header/_UtilityNavigationItem
        /// <summary>
        ///   _UtilityNavigationItem
        ///   <para>ID: {9DE6B7DB-CE49-4C25-9275-8FCC2E15221E}</para>
        ///   <para>Path: /sitecore/templates/Feature/Header/_UtilityNavigationItem</para>
        /// </summary>
        public static class UtilityNavigationItem
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{9DE6B7DB-CE49-4C25-9275-8FCC2E15221E}"));
            public const string IdString = "{9DE6B7DB-CE49-4C25-9275-8FCC2E15221E}";

            public static class Fields
            {
                /// <summary>
                ///   NavigationLink
                ///   <para>{90975E49-6B1A-4074-BC05-1DBA2E952C14}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID NavigationLink = new Sitecore.Data.ID("{90975E49-6B1A-4074-BC05-1DBA2E952C14}");

                /// <summary>
                ///   NavigationLink
                ///   <para>{90975E49-6B1A-4074-BC05-1DBA2E952C14}</para>
                /// </summary>
                public const string NavigationLink_FieldName = "NavigationLink";

            }
        }
        #endregion
    }
}


