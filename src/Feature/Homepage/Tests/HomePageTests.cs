﻿using FluentAssertions;
using Sitecore.FakeDb;
using Sitecore;
using NUnit.Framework;
using NUnit;
using Sitecore.Data;
using System.Diagnostics.CodeAnalysis;
using Sitecore.SecurityModel;

namespace Athena.Feature.Homepage.Tests
{
    public class HomePageTests
    {
        [TestCase("/sitecore")]
        [TestCase("/sitecore/content")]
        [TestCase("/sitecore/media library")]
        [TestCase("/sitecore/system")]
        [TestCase("/sitecore/templates")]
        public void CheckIfDefaultItemsExist(string path)
        {
            using (var db = new Db())
            {

                db.GetItem(path).Should().NotBeNull();
            }

        }

        [Test]
        public void CreateHomeTemplateUsingBaseTemplate()
        {
            //Arrange
            var metaDataTemplateId = Sitecore.Data.ID.NewID;
            var analyticsTemplateId = Sitecore.Data.ID.NewID;
            var homeTemplateId = Sitecore.Data.ID.NewID;

            using (Db db = new Db
                {
                    new DbTemplate("base one", metaDataTemplateId),
                    new DbTemplate("base two", analyticsTemplateId),
                    new DbTemplate("Home", homeTemplateId)
                        {
                            BaseIDs = new[] { metaDataTemplateId, analyticsTemplateId }
                        }
                })
            {
                //Act
                var template = Sitecore.Data.Managers.TemplateManager.GetTemplate(homeTemplateId, db.Database);

                // Assert
                Assert.Contains(metaDataTemplateId, template.BaseIDs);
                Assert.Contains(analyticsTemplateId, template.BaseIDs);

                Assert.True(template.InheritsFrom(metaDataTemplateId));
                Assert.True(template.InheritsFrom(analyticsTemplateId));
            }
        }

        [Test]
        public void CheckIfHomeItemCreated()
        {
            //Arrange

            var folderTemplateId = new TemplateID(Sitecore.Data.ID.NewID);
            var homeTemplateId = new TemplateID(Sitecore.Data.ID.NewID);


            using (var db = new Db
                {

                    new DbTemplate("AthenaFolderTemplate", folderTemplateId){ },
                    new DbTemplate("HomePageTemplate", homeTemplateId)
                        {
                            {"Title", "$name"},
                            {"Meta Description" },
                            {new DbField("Can Index") {Type = "checkbox"} }
                        },

                })
            {

                //Act
                Sitecore.Data.Items.Item contentRoot = db.GetItem(Sitecore.ItemIDs.ContentRoot);

                Sitecore.Data.Items.Item athena = contentRoot.Add("Athena", folderTemplateId);

                Sitecore.Data.Items.Item home = athena.Add("Home", homeTemplateId);


                using (new SecurityDisabler())
                {

                    home.Editing.BeginEdit();
                    home["Meta Description"] = "It's Athena Home Page";
                    home["Can Index"] = "1"; //True

                    home.Editing.EndEdit();
                }
                //Assert
                athena.Should().NotBeNull();

                home.ParentID.Should().BeSameAs(athena.ID);

                home["Title"].Should().NotBeNullOrEmpty();
                home["Title"].Should().Be("Home");

                home["Meta Description"].Should().NotBeNullOrEmpty();
                home["Meta Description"].Should().Be("It's Athena Home Page");

                home["Can Index"].Should().NotBeNullOrEmpty();
                home["Can Index"].Should().Be("1");

            }

        }
    }
}
