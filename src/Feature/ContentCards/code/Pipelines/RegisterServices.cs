﻿using Athena.Feature.ContentCards.Services;
using Athena.Foundation.DependencyInjection.Pipelines;
using Microsoft.Extensions.DependencyInjection;

namespace Athena.Feature.ContentCards.Pipelines
{
    public class RegisterServices
    {
        public void Process(InitializeDependencyInjectionArgs args)
        {
            args.ServiceCollection.AddScoped<IContentCardsServices, ContentCardsServices>();
        }
    }
}