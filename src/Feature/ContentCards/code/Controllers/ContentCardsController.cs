﻿using Athena.Feature.ContentCards.Services;
using Sitecore;
using Sitecore.Mvc.Presentation;
using System.Web.Mvc;

namespace Athena.Feature.ContentCards.Controllers
{
    public class ContentCardsController : Controller
    {
        private readonly IContentCardsServices _contentCardsService;

        public ContentCardsController(IContentCardsServices contentCardsService)
        {
            _contentCardsService = contentCardsService;
        }
        
        public ActionResult Index()
        {
            var viewModel = _contentCardsService.GetContentCardsModel(RenderingContext.Current, Context.Item);
            return View(viewModel);
        }
    }
}