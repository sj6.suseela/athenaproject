﻿





// ReSharper disable InconsistentNaming
namespace Athena.Feature.ContentCards
{
    public static partial class Templates
    {
        #region /sitecore/templates/Feature/ContentCards/_ContentCard
        /// <summary>
        ///   _ContentCard
        ///   <para>ID: {79478CDD-81B4-4B75-8397-042D81CA0AFC}</para>
        ///   <para>Path: /sitecore/templates/Feature/ContentCards/_ContentCard</para>
        /// </summary>
        public static class ContentCard
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{79478CDD-81B4-4B75-8397-042D81CA0AFC}"));
            public const string IdString = "{79478CDD-81B4-4B75-8397-042D81CA0AFC}";

            public static class Fields
            {
                /// <summary>
                ///   Description
                ///   <para>{E29409D4-D691-42EC-AFC5-C8593E114062}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Description = new Sitecore.Data.ID("{E29409D4-D691-42EC-AFC5-C8593E114062}");

                /// <summary>
                ///   Description
                ///   <para>{E29409D4-D691-42EC-AFC5-C8593E114062}</para>
                /// </summary>
                public const string Description_FieldName = "Description";

                /// <summary>
                ///   image
                ///   <para>{E2CA62FD-0F85-4496-83EB-F2EB76B1F95E}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Image = new Sitecore.Data.ID("{E2CA62FD-0F85-4496-83EB-F2EB76B1F95E}");

                /// <summary>
                ///   image
                ///   <para>{E2CA62FD-0F85-4496-83EB-F2EB76B1F95E}</para>
                /// </summary>
                public const string Image_FieldName = "image";

            }
        }
        #endregion
        #region /sitecore/templates/Feature/ContentCards/_ContentCards
        /// <summary>
        ///   _ContentCards
        ///   <para>ID: {12DAD46E-4C6B-42F3-95B7-F297D5E191CC}</para>
        ///   <para>Path: /sitecore/templates/Feature/ContentCards/_ContentCards</para>
        /// </summary>
        public static class ContentCards
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{12DAD46E-4C6B-42F3-95B7-F297D5E191CC}"));
            public const string IdString = "{12DAD46E-4C6B-42F3-95B7-F297D5E191CC}";

        }
        #endregion
    }
}


