﻿using Sitecore.Data.Items;
using System.Collections.Generic;
using Athena.Foundation.ReusableTemplates.Models;
using Athena.Foundation.Elements.Models;
using Newtonsoft.Json;
using Athena.Foundation.React.Models;

namespace Athena.Feature.ContentCards.Models
{
	public class ContentCardsViewModel : AbstractDataModel
	{
		private static readonly string COMPONENT_NAME = "contentcards";

		public List<ContentCard> ContentCards { get; set; }

		public override string GetComponentName()
		{
			return COMPONENT_NAME;
		}
	}

	public class ContentCard
	{
		[JsonProperty("description")]
		public string Description { get; set; }

		[JsonProperty("titleHeading")]
		public Title Title { get; set; }

		[JsonProperty("buttonLink")]
		public CTA CTA { get; set; }

		[JsonProperty("link")]
		public Link Link { get; set; }
	}
}