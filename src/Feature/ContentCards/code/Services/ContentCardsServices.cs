﻿using Athena.Feature.ContentCards.Models;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System.Linq;
using Athena.Foundation.ReusableTemplates;

namespace Athena.Feature.ContentCards.Services
{
    public class ContentCardsServices : IContentCardsServices
    {
        public ContentCardsViewModel GetContentCardsModel(RenderingContext renderingContext, Item page)
        {
            var item = RenderingContext.Current?.Rendering?.Item;
            if (item == null || !item.IsDerived(Templates.ContentCards.ID))
                return null;

            var model = new ContentCardsViewModel()
            {
                ContentCards = item.ChildrenOfTemplate(Templates.ContentCard.ID).Select(MapContentCard).ToList(),
            };
            return model;
        }

        private static ContentCard MapContentCard(Item item)
        {
            return new ContentCard()
            {
                Description = item.RenderField(Templates.ContentCard.Fields.Description),
                Title = item.GetTitleWithHtmlTag(),
                CTA = item.GetCTAWithType(),
                Link = item.GetLinkWithText()
            };
        }
    }
}