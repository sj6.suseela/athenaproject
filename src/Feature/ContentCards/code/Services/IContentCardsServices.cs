﻿using Athena.Feature.ContentCards.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.ContentCards.Services
{
    public interface IContentCardsServices
    {
        ContentCardsViewModel GetContentCardsModel(RenderingContext renderingContext, Item page);
    }
}
