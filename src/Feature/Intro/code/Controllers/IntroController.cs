﻿using Sitecore;
using System.Web.Mvc;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Athena.Feature.Intro.Models;
using Athena.Feature.Intro.Services;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.Intro.Controllers
{
    public class IntroController : Controller
    {
        private readonly IIntroServices _introService;

        public IntroController(IIntroServices introService)
        {
            _introService = introService;
        }
        // GET: Intro
        public ActionResult Index()
        {
            var viewModel = _introService.GetIntroModel(RenderingContext.Current, Context.Item);
            return View(viewModel);
        }
    }
}