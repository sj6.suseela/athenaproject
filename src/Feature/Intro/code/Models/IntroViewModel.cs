﻿using Sitecore.Data.Items;

namespace Athena.Feature.Intro.Models
{
	public class IntroViewModel
	{
		public string Description { get; set; }

		public string Title { get; set; }
	}
}