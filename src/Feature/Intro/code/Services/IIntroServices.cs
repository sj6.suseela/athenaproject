﻿using Athena.Feature.Intro.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Athena.Feature.Intro.Services
{
    public interface IIntroServices
    {
        IntroViewModel GetIntroModel(RenderingContext renderingContext, Item page);
    }
}
