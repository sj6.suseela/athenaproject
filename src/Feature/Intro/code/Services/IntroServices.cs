﻿using Athena.Feature.Intro.Models;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Feature.Intro.Services
{
    public class IntroServices : IIntroServices
    {
        public IntroViewModel GetIntroModel(RenderingContext renderingContext, Item page)
        {
            var item = RenderingContext.Current?.Rendering?.Item;
            if (item == null || !item.IsDerived(Templates.WelcomeContent.ID))
                return null;
            var model = new IntroViewModel()
            {
                Title = item.RenderField(Templates.WelcomeContent.Fields.Title),
                Description=item.RenderField(Templates.WelcomeContent.Fields.Description)

            };
            return model;
        }
    }
}