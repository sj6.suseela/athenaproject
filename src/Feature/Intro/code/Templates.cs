﻿






// ReSharper disable InconsistentNaming
namespace Athena.Feature.Intro
{
    public static partial class Templates
    {
        #region /sitecore/templates/Feature/Intro/_WelcomeContent
        /// <summary>
        ///   _WelcomeContent
        ///   <para>ID: {E532102C-FC1D-49A8-98A6-89630DE0CB95}</para>
        ///   <para>Path: /sitecore/templates/Feature/Intro/_WelcomeContent</para>
        /// </summary>
        public static class WelcomeContent
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{E532102C-FC1D-49A8-98A6-89630DE0CB95}"));
            public const string IdString = "{E532102C-FC1D-49A8-98A6-89630DE0CB95}";

            public static class Fields
            {
                /// <summary>
                ///   Description
                ///   <para>{0B665EF3-0EFB-410B-80F0-7C51034B1AB7}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Description = new Sitecore.Data.ID("{0B665EF3-0EFB-410B-80F0-7C51034B1AB7}");

                /// <summary>
                ///   Description
                ///   <para>{0B665EF3-0EFB-410B-80F0-7C51034B1AB7}</para>
                /// </summary>
                public const string Description_FieldName = "Description";

                /// <summary>
                ///   Title
                ///   <para>{91921ED6-1E13-4293-B805-8798695576F7}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Title = new Sitecore.Data.ID("{91921ED6-1E13-4293-B805-8798695576F7}");

                /// <summary>
                ///   Title
                ///   <para>{91921ED6-1E13-4293-B805-8798695576F7}</para>
                /// </summary>
                public const string Title_FieldName = "Title";

            }
        }
        #endregion
    }
}


