﻿using Athena.Feature.Intro.Services;
using Athena.Foundation.DependencyInjection.Pipelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Extensions.DependencyInjection;

namespace Athena.Feature.Intro.Pipelines
{
    public class RegisterServices
    {
        public void Process(InitializeDependencyInjectionArgs args)
        {
            args.ServiceCollection.AddScoped<IIntroServices, IntroServices>();
        }
    }
}