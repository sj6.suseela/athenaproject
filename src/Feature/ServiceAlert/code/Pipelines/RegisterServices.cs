﻿using Athena.Feature.ServiceAlert.Services;
using Athena.Foundation.DependencyInjection.Pipelines;
using Microsoft.Extensions.DependencyInjection;

namespace Athena.Feature.ServiceAlert.Pipelines
{
    public class RegisterServices
    {
        public void Process(InitializeDependencyInjectionArgs args)
        {
            args.ServiceCollection.AddScoped<IAlertService, AlertService>();
        }
    }
}