﻿using Athena.Foundation.Elements.Models;
using Athena.Foundation.React;
using Athena.Foundation.React.Models;
using Athena.Foundation.ReusableTemplates.Models;
using Sitecore.Data.Items;
using System.Collections.Generic;

namespace Athena.Feature.ServiceAlert.Models
{
	public class ServiceAlertDataModel : AbstractDataModel
	{
		/** The Constant COMPONENT_NAME. */
		private static readonly string COMPONENT_NAME = "service";

		public bool enableBorder { get; set; } = false;

		public bool enableBackground { get; set; } = false;

		public string variation { get; set; }

		public ImageElement image { get; set; }

		public string imageAltText { get; set; }

		public Title titleLink { get; set; }

		public string date { get; set; }

		public string description { get; set; }

		public override string GetComponentName()
		{
			return COMPONENT_NAME;
		}
		public override string GetRenderType()
		{
			return ReactConstants.RENDER_SERVERSIDE;
		}
	}

	
}