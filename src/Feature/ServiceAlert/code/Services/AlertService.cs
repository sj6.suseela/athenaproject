﻿using Athena.Feature.ServiceAlert.Models;
using Athena.Foundation.React.Models;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Mvc.Presentation;
using System.Linq;
using Athena.Foundation.Elements.Extensions;
using Athena.Foundation.ReusableTemplates;
using System.Globalization;

namespace Athena.Feature.ServiceAlert.Services
{
    public class AlertService : IAlertService
    {
        public AbstractComponentModel GetServiceAlertModel(RenderingContext renderingContext, Item page)
        {
            var item = RenderingContext.Current?.Rendering?.Item;

            AbstractComponentModel acm = new AbstractComponentModel();

            acm.setComponentData(FetchServiceDataModel(item));

            return acm;
        }

		public ServiceAlertDataModel FetchServiceDataModel(Item item)
		{

			var serviceDataModel = new ServiceAlertDataModel()
			{

				enableBorder = item.GetBoolFieldValue(Templates.ServiceAlert.Fields.EnableBorder),
				enableBackground = item.Fields[Templates.ServiceAlert.Fields.EnableBackgorund]?.IsChecked() ?? false,
				//variation = item.RenderFieldNonEditable(Templates.ServiceAlert.Fields.Variation),
				variation = item.GetLookupValue(Templates.ServiceAlert.Fields.Variation),
				image = item.GetImageElement(Templates.ServiceAlert.Fields.Image),
				//imageAltText = item.RenderField(Templates.ServiceComponent.FieldIDs.),
				date = FetchServerDateTime(item),
				titleLink = item.GetTitleWithHtmlTag(),
				description = item.RenderRichTextField(Templates.ServiceAlert.Fields.Description)
			};
			return serviceDataModel;
		}


		public string FetchServerDateTime(Item item)
		{

			var dateField = (DateField)item.Fields[Templates.ServiceAlert.Fields.Date];

			var dateValue = Sitecore.DateUtil.FormatDateTime(Sitecore.DateUtil.ToServerTime(dateField.DateTime), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

			return dateValue;
		}

	}
}