﻿using Athena.Feature.ServiceAlert.Models;
using Athena.Foundation.React.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace Athena.Feature.ServiceAlert.Services
{
    public interface IAlertService
    {
        AbstractComponentModel GetServiceAlertModel(RenderingContext renderingContext, Item page);
    }
}
