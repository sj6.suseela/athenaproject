﻿using Athena.Feature.ServiceAlert.Services;
using Sitecore;
using Sitecore.Mvc.Presentation;
using System.Web.Mvc;

namespace Athena.Feature.ServiceAlert.Controllers
{
    public class ServiceAlertController : Controller
    {
        private readonly IAlertService _AlertService;

        public ServiceAlertController(IAlertService alertService)
        {
            _AlertService = alertService;
        }
        
        public ActionResult Index()
        {
            //var viewModel = _AlertService.GetServiceAlertModel(RenderingContext.Current, Context.Item);
            //return View(viewModel);
            return null;
        }

        public ActionResult loadServiceAlert()
        {
            var viewModel = _AlertService.GetServiceAlertModel(RenderingContext.Current, Context.Item);

            return View(viewModel);
        }
    }
}