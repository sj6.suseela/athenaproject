﻿






















// ReSharper disable InconsistentNaming
namespace Athena.Feature.ServiceAlert
{
    public static partial class Templates
    {

        #region /sitecore/templates/Feature/ServiceAlert/_ServiceAlert
        /// <summary>
        ///   _ServiceAlert
        ///   <para>ID: {97717A5C-0716-4871-9702-55A5BA417C29}</para>
        ///   <para>Path: /sitecore/templates/Feature/ServiceAlert/_ServiceAlert</para>
        /// </summary>
        public static class ServiceAlert
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{97717A5C-0716-4871-9702-55A5BA417C29}"));
            public const string IdString = "{97717A5C-0716-4871-9702-55A5BA417C29}";


            public static class Fields
            {

                /// <summary>
                ///   Date
                ///   <para>{1FE81042-F893-4909-B6D7-6FDF327D1A98}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Date = new Sitecore.Data.ID("{1FE81042-F893-4909-B6D7-6FDF327D1A98}");

                /// <summary>
                ///   Date
                ///   <para>{1FE81042-F893-4909-B6D7-6FDF327D1A98}</para>
                /// </summary>
                public const string Date_FieldName = "Date";


                /// <summary>
                ///   Description
                ///   <para>{C4CCFF68-4AFB-44DA-B11D-BB6D310228E3}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Description = new Sitecore.Data.ID("{C4CCFF68-4AFB-44DA-B11D-BB6D310228E3}");

                /// <summary>
                ///   Description
                ///   <para>{C4CCFF68-4AFB-44DA-B11D-BB6D310228E3}</para>
                /// </summary>
                public const string Description_FieldName = "Description";


                /// <summary>
                ///   Enable backgorund
                ///   <para>{FDB39BE8-7E3B-43FB-8F01-E30B5F700585}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID EnableBackgorund = new Sitecore.Data.ID("{FDB39BE8-7E3B-43FB-8F01-E30B5F700585}");

                /// <summary>
                ///   Enable backgorund
                ///   <para>{FDB39BE8-7E3B-43FB-8F01-E30B5F700585}</para>
                /// </summary>
                public const string EnableBackgorund_FieldName = "Enable backgorund";


                /// <summary>
                ///   Enable Border
                ///   <para>{1BE0A491-80FA-4C74-8C53-24032FD9D98F}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID EnableBorder = new Sitecore.Data.ID("{1BE0A491-80FA-4C74-8C53-24032FD9D98F}");

                /// <summary>
                ///   Enable Border
                ///   <para>{1BE0A491-80FA-4C74-8C53-24032FD9D98F}</para>
                /// </summary>
                public const string EnableBorder_FieldName = "Enable Border";


                /// <summary>
                ///   Image
                ///   <para>{0A69EB5A-4A0C-45A9-9076-D000E1D2B02E}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Image = new Sitecore.Data.ID("{0A69EB5A-4A0C-45A9-9076-D000E1D2B02E}");

                /// <summary>
                ///   Image
                ///   <para>{0A69EB5A-4A0C-45A9-9076-D000E1D2B02E}</para>
                /// </summary>
                public const string Image_FieldName = "Image";


                /// <summary>
                ///   Variation
                ///   <para>{807D2F34-108D-4654-AE12-C039937D80E7}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Variation = new Sitecore.Data.ID("{807D2F34-108D-4654-AE12-C039937D80E7}");

                /// <summary>
                ///   Variation
                ///   <para>{807D2F34-108D-4654-AE12-C039937D80E7}</para>
                /// </summary>
                public const string Variation_FieldName = "Variation";


            }

        }
        #endregion

        public static class TitleLink {

            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{45A6C5C4-FB68-4A67-B667-9B3DA81AF7AB}"));
            public const string IdString = "{45A6C5C4-FB68-4A67-B667-9B3DA81AF7AB}";

            public static class Fields
            {

                /// <summary>
                ///   Date
                ///   <para>{1FE81042-F893-4909-B6D7-6FDF327D1A98}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Title = new Sitecore.Data.ID("{F6AB6A47-906D-4B3A-BFC6-C376D763C0B7}");

                /// <summary>
                ///   Date
                ///   <para>{1FE81042-F893-4909-B6D7-6FDF327D1A98}</para>
                /// </summary>
                public const string Title_FieldName = "Title";


                /// <summary>
                ///   Description
                ///   <para>{C4CCFF68-4AFB-44DA-B11D-BB6D310228E3}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Link = new Sitecore.Data.ID("{7B555707-97F7-4944-8B66-50C279B622A7}");

                /// <summary>
                ///   Description
                ///   <para>{C4CCFF68-4AFB-44DA-B11D-BB6D310228E3}</para>
                /// </summary>
                public const string Link_FieldName = "Link";
            }

        }

    }
}


