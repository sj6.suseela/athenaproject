﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Athena.Foundation.Logging
{
   public static class AthenaLogger
    {
        private static ILog log;
        public static ILog Log
        {
            get
            {
                return log ?? (log = Sitecore.Diagnostics.LoggerFactory.GetLogger("AthenaLogger"));
            }
        }

       
    }
   
}
