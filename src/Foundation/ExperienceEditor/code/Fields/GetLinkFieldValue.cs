﻿using Sitecore.Pipelines.RenderField;

namespace Athena.Foundation.ExperienceEditor.Fields
{
	public class GetLinkFieldValue : Sitecore.Pipelines.RenderField.GetLinkFieldValue
	{
		protected override bool SkipProcessor(RenderFieldArgs args)
		{
			if (args == null) return true;
			string fieldTypeKey = args.FieldTypeKey;
			return fieldTypeKey != "link" && fieldTypeKey != "general link" && fieldTypeKey != "media link";
		}
	}
}