﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.ExperienceEditor
{
	public struct EditFrameButtons
	{
		public struct ButtonPaths
		{
			public const string Default = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Default";
			public const string CoverItem = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Cover Item";
			public const string ReevooCustomerReviewItem = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Reevoo Customer Review";
			public const string AuthorableCustomerReviewItem = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Authorable Customer Review";
			public const string ReevooProductReviewItem = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Reevoo Product Review";
			public const string Form = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Form";
			public const string FormControlGroup = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Form Control Group";
			public const string FormControl = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Form Control";
			public const string ContactUs = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Contact Us";
			public const string ContactUsSection = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Contact Us Section";
			public const string ContactUsContentPanel = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Contact Us Content Panel";
			public const string ContactUsContactInfo = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Contact Us Contact Info";
			public const string ContactUsCopy = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Contact Us Copy";
			public const string ContactUsLinks = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Contact Us Links";
			public const string ContactUsLinkItem = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Contact Us Link Item";
			public const string SortDelete = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Sort Delete";
			public const string Insert = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Insert";
			public const string HomepageProductTile = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Homepage Product Tile";
			public const string HomepageStoryCard = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Homepage Story Card";
			public const string NextStepsCta = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Nextsteps Cta";
			public static string CustomerMomentsItem = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Customer Moments";
			public const string ImageGalleryItem = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Image Gallery Item";
			public static string ReevooReviewsItem = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Reevoo Reviews";
			public static string HomepageHeaderBanner ="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Homepage Header Banner";
			public static string Anchor = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Anchor";
		}
	}
}