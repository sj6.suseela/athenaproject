﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Athena.Foundation.ExperienceEditor.ContentEditor
{
	// based on https://kamsar.net/index.php/2015/05/Extending-Sitecore-Rich-Text-Preview-CSS/
	public class RichTextEditorPreview : Sitecore.Shell.Controls.RADEditor.Preview
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			RichTextEditorStylesheetHelper.ApplyStylesheets(SitecoreConstants.Stylesheet.Fields.UseInRichTextPreview, (path) =>
			{
				Stylesheets.Controls.Add(new LiteralControl(string.Format("<link href=\"{0}\" rel=\"stylesheet\">", path)));
			});
		}
	}
}