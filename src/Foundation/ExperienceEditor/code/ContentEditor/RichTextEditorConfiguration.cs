﻿using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.ExperienceEditor.ContentEditor
{
	// based on https://jammykam.wordpress.com/2015/03/11/user-specific-or-multi-site-specific-css-styles-in-sitecore-rich-text-editor/
	public class RichTextEditorConfiguration : Sitecore.Shell.Controls.RichTextEditor.EditorConfiguration
	{
		public RichTextEditorConfiguration(Item profile) : base(profile)
		{
		}

		protected override void SetupStylesheets()
		{
			RichTextEditorStylesheetHelper.ApplyStylesheets(SitecoreConstants.Stylesheet.Fields.UseInRichTextEditor, this.Editor.CssFiles.Add);

			base.SetupStylesheets();
		}
	}
}