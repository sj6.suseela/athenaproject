﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.ExperienceEditor.ContentEditor
{
	public class SitecoreConstants
	{
		public const string StylesheetsPath = "/sitecore/system/Settings/Foundation/ContentEditor/Stylesheets";

		public class Stylesheet
		{
			public static readonly ID TemplateID = new ID("{C986AD38-E2EF-409A-8FC0-77A5E09ACB2F}");

			public class Fields
			{
				public static readonly ID Path = new ID("{6AF82972-D404-4E7F-88D9-443F32464367}");
				public static readonly ID UseInRichTextEditor = new ID("{5B616333-C8AA-4764-B05E-73704C28B1C0}");
				public static readonly ID UseInRichTextPreview = new ID("{3944518F-C838-4D4A-A269-B9C7B91496B5}");
			}
		}
	}
}