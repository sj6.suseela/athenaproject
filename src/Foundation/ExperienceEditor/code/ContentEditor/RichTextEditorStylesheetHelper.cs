﻿using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.ExperienceEditor.ContentEditor
{
	public class RichTextEditorStylesheetHelper
	{
		public static void ApplyStylesheets(ID FilterOnFieldID, Action<string> applyStylesheet)
		{
			var coredb = Sitecore.Configuration.Factory.GetDatabase("core");

			var stylesheetsFolder = coredb.GetItem(SitecoreConstants.StylesheetsPath);
			if (stylesheetsFolder == null)
			{
				return;
			}

			foreach (Item item in stylesheetsFolder.Children)
			{
				if (item?.TemplateID == SitecoreConstants.Stylesheet.TemplateID)
				{
					if (item.Fields[FilterOnFieldID]?.Value == "1")
					{
						var path = item.Fields[SitecoreConstants.Stylesheet.Fields.Path]?.Value;
						if (!string.IsNullOrWhiteSpace(path))
						{
							applyStylesheet?.Invoke(path);
						}
					}
				}
			}
		}
	}
}