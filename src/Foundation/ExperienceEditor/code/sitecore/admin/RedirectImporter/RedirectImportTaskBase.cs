﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Athena.Foundation.ExperienceEditor.RedirectImporter
{
	public abstract class RedirectImportTaskBase
	{
		protected readonly Item ImportFolder;
		private readonly bool _disableWorkflow;
		public const int ItemsPerFolder = 100;

		protected RedirectImportTaskBase(Item importFolder, bool disableWorkflow)
		{
			ImportFolder = importFolder;
			_disableWorkflow = disableWorkflow;
		}

		/// <summary>
		/// Main method for executing the task
		/// </summary>
		/// <param name="redirects"></param>
		public void RunImport(IEnumerable<Redirect> redirects)
		{
			BeforeImport();
			AddRedirects(redirects);
		}

		/// <summary>
		/// Run before the redirects are added
		/// </summary>
		protected virtual void BeforeImport() { }


		/// <summary>
		/// Add the specified redirect into the provided folder
		/// </summary>
		/// <param name="redirect">The specifications of the redirect</param>
		/// <param name="name">Name of the item to be created</param>
		/// <param name="folderItem">The folder that the redirect will be placed in</param>
		protected virtual void AddRedirect(Redirect redirect, string name, Item folderItem)
		{
			var redirectItem = folderItem.Add(name, RedirectTemplates.SimpleRedirect);
			using (new EditContext(redirectItem))
			{
				// incoming path
				redirectItem.Fields["Path"].Value = redirect.From;

				// redirect url
				LinkField linkField = redirectItem.Fields["Target"];
				linkField.Clear();
				linkField.LinkType = "external";
				linkField.Url = redirect.ToUrl;

				// enabled
				redirectItem.Fields["enabled"].Value = "1"; // 1 = true/checked

				if (_disableWorkflow)
				{
					redirectItem.Fields[FieldIDs.DefaultWorkflow].Value = null;
					redirectItem.Fields[FieldIDs.Workflow].Value = null;
					redirectItem.Fields[FieldIDs.WorkflowState].Value = null;
				}
			}
		}

		/// <summary>
		/// Find a name that matches the current restrictions
		/// </summary>
		/// <param name="redirect"></param>
		/// <returns></returns>
		private static string GetRedirectName(Redirect redirect)
		{
			return ItemUtil.ProposeValidItemName(redirect.Name);
		}

		/// <summary>
		/// Add the specified redirects to the import folder
		/// </summary>
		/// <param name="redirects"></param>
		private void AddRedirects(IEnumerable<Redirect> redirects)
		{
			int index = 1;
			foreach (var redirect in redirects.OrderBy(redirect => redirect.Name))
			{
				var name = GetRedirectName(redirect);
				Item folderItem = FindAppropriateSubFolder(ref index);
				AddRedirect(redirect, name, folderItem);
			}
		}

		/// <summary>
		/// Look for a folder/ container to put the redirects in to prevent over crowding
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		private Item FindAppropriateSubFolder(ref int index)
		{
			while (true)
			{
				string name = "import_" + index;
				var item = GetSubFolder(name) ?? AddSubFolder(name);
				if (!item.HasChildren || item.Children.Count < ItemsPerFolder)
				{
					return item;
				}
				index++;
			}
		}

		/// <summary>
		/// Find a subfolder with the specified name. 
		/// </summary>
		/// <param name="name">Name of the subfolder</param>
		/// <returns>Null if not found</returns>
		private Item GetSubFolder(string name)
		{
			return ImportFolder.Children.FirstOrDefault(item => item.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
		}

		/// <summary>
		/// Add a subfolder to the Import Folder 
		/// </summary>
		/// <param name="name">Name of the subfolder</param>
		/// <returns></returns>
		private Item AddSubFolder(string name)
		{
			return ImportFolder.Add(name, RedirectTemplates.RedirectSubfolder);
		}
	}
}