using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using Microsoft.VisualBasic.FileIO;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Web;
using FieldType = Microsoft.VisualBasic.FileIO.FieldType;

namespace Athena.Foundation.ExperienceEditor.RedirectImporter
{
	public partial class ImportRedirects : Page
	{
		private const string ImportSubfolder = "Imports";
		private const int IndexFrom = 0;
		private const int IndexTo = 1;
		private const int IndexName = 2;

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void btnUpload_Click(object sender, EventArgs e)
		{
			if (FileUpload1.HasFile)
			{
				var extension = Path.GetExtension(FileUpload1.FileName);
				if (extension != ".csv")
				{
					SetStatus($"'{extension}' is an incorrect file type. Please choose a CSV ");
					return;
				}

				var relativePath = "~/temp/" + FileUpload1.FileName;
				var mapPath = Server.MapPath(relativePath);
				FileUpload1.SaveAs(mapPath);
				try
				{
					RunCsvImport(mapPath);
				}
				catch (Exception ex)
				{
					SetStatus(ex.ToString());
					return;
				}

				SetStatus("Success");
			}
			else
			{
				SetStatus("Error: File not uploaded");
			}
		}

		private void SetStatus(string status)
		{
			litStatus.Text = status;
		}

		private void RunCsvImport(string csvPath)
		{
			// should be the importer item, which should be under the Redirect settings folder
			var item = GetEditorContextItem();
			Assert.IsNotNull(item, "Where is the context item? Seems to be missing");
			var rewriteSettingsFolder = item.Parent;


			// make sure the importer item is in the right place
			Assert.AreEqual(rewriteSettingsFolder.TemplateID.ToString(), RedirectTemplates.RedirectFolder.ToString(),
				"Parent item is not a Redirect settings folder");

			var redirects = GetRedirectsFromFile(csvPath);
			var importFolder = GetOrCreateImportSubfolder(rewriteSettingsFolder);

			var disableWorkflow = cbxDisableWorkflow.Checked;

			RedirectImportTaskBase taskBase = GetImportMode(importFolder, disableWorkflow);

			taskBase.RunImport(redirects);
		}

		private RedirectImportTaskBase GetImportMode(Item importFolder, bool disableWorkflow)
		{
			string key = ddlImportMode.SelectedValue;
			switch (key)
			{
				case "overwrite":
					return new OverwritePreviousRedirectImport(importFolder, disableWorkflow);
				case "replace":
					return new ReplaceOnMatchingName(importFolder, disableWorkflow);
				//case "append":
				default:
					return new AppendAll(importFolder, disableWorkflow);
			}
		}

		private static Item GetEditorContextItem()
		{
			return Database.GetDatabase("master").GetItem(new ID(WebUtil.GetQueryString("id")));
		}

		private Item GetOrCreateImportSubfolder(Item rewriteFolder)
		{
			return GetImportSubfolder(rewriteFolder) ?? rewriteFolder.Add(ImportSubfolder, RedirectTemplates.RedirectSubfolder);
		}

		private static Item GetImportSubfolder(Item rewriteFolder)
		{
			return rewriteFolder.HasChildren
				? rewriteFolder.Children.FirstOrDefault(item =>
					item.TemplateID == RedirectTemplates.RedirectSubfolder && item.Name == ImportSubfolder)
				: null;
		}



		private IEnumerable<Redirect> GetRedirectsFromFile(string csvPath)
		{
			using (var parser = new TextFieldParser(csvPath))
			{
				parser.TextFieldType = FieldType.Delimited;
				parser.SetDelimiters(",");
				while (!parser.EndOfData)
				{
					//Processing row
					var fields = parser.ReadFields();
					if (fields.Length != 3)
					{
						throw new InvalidDataException($"Line {parser.LineNumber} - Incorrect number of columns: {fields.Length}");
					}

					yield return new Redirect
					{
						From = fields[IndexFrom],
						ToUrl = fields[IndexTo],
						Name = fields[IndexName]
					};
				}
			}
		}
	}
	public static class RedirectTemplates
	{
		public static readonly ID RedirectFolder = new ID("{CBE995D0-FCE0-4061-B807-B4BBC89962A7}");
		public static readonly TemplateID SimpleRedirect = new TemplateID(new ID("{E30B15B9-34CD-419C-8671-60FEAAAD5A46}"));
		public static readonly TemplateID RedirectSubfolder = new TemplateID(new ID("{9461E537-8E89-4B91-896A-1F2C3AF4A3D5}"));
	}

	public class Redirect
	{
		public string From { get; set; }
		public string ToUrl { get; set; }
		public string Name { get; set; }
	}
}