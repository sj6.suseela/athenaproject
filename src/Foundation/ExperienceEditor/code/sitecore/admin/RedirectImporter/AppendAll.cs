﻿using Sitecore.Data.Items;

namespace Athena.Foundation.ExperienceEditor.RedirectImporter
{
	/// <summary>
	/// Import all redirects without checking for conflicts in name 
	/// </summary>
	public class AppendAll : RedirectImportTaskBase
	{
		public AppendAll(Item importFolder, bool disableWorkflow) : base(importFolder, disableWorkflow)
		{
		}
	}
}