﻿using Sitecore.Data.Items;

namespace Athena.Foundation.ExperienceEditor.RedirectImporter
{
	/// <summary>
	/// Deletes all previous imports
	/// </summary>
	public class OverwritePreviousRedirectImport : RedirectImportTaskBase
	{
		protected override void BeforeImport()
		{
			foreach (Item item in ImportFolder.Children)
			{
				item.Delete();
			}
		}

		public OverwritePreviousRedirectImport(Item importFolder, bool disableWorkflow) : base(importFolder, disableWorkflow)
		{
		}
	}
}