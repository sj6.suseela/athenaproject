﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.Data.Items;

namespace Athena.Foundation.ExperienceEditor.RedirectImporter
{
	/// <summary>
	/// Where a new imported redirect has the same name as old import, the old import is removed
	/// </summary>
	public class ReplaceOnMatchingName : RedirectImportTaskBase
	{
		ILookup<string, Item> _childLookUp;

		public ReplaceOnMatchingName(Item importFolder, bool disableWorkflow) : base(importFolder, disableWorkflow)
		{ }

		protected override void AddRedirect(Redirect redirect, string name, Item folderItem)
		{
			// if an item already exists with that name, then delete it.
			if (_childLookUp.Contains(name))
			{
				foreach (var item in _childLookUp[name])
				{
					item.Delete();
				}
			}

			base.AddRedirect(redirect, name, folderItem);
		}

		protected override void BeforeImport()
		{
			// Use Lookup rather than dictionary as there may be multiple matches
			_childLookUp = ImportFolder.Axes.GetDescendants().Where(IsSimpleRedirectTemplate).ToLookup(item => item.Name);
		}

		private bool IsSimpleRedirectTemplate(Item item)
		{
			return item.TemplateID == RedirectTemplates.SimpleRedirect;
		}
	}
}