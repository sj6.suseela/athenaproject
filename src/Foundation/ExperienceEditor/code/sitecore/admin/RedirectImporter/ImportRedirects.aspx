﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportRedirects.aspx.cs" Inherits="Athena.Foundation.ExperienceEditor.RedirectImporter.ImportRedirects" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,400italic,600italic,700italic,300,600,700,800" rel="stylesheet">

    <link href="/sitecore/shell/themes/standard/default/Default.css" rel="stylesheet">

    <link href="/sitecore/shell/controls/Lib/Flexie/flex.css" rel="stylesheet">

    <link href="/sitecore/shell/themes/standard/default/Ribbon.css" rel="stylesheet">

    <link href="/sitecore/shell/themes/standard/default/Content Manager.css" rel="stylesheet">

    <script type="text/javascript">
        //<![CDATA[
        var __jsnlog_configure = function (JL) {
            JL.setOptions({ "clientIP": "127.0.0.1", "requestId": "g4l3owl12nc5fw0nxnv2d2ih-08c1a0e7f6dc4804b573a70f7eedb702", "defaultAjaxUrl": "/jsnlog.logger", "enabled": true, "maxMessages": 20 });
            var a0 = JL.createAjaxAppender('ajaxAppender');
            a0.setOptions({ "level": 5000, "batchSize": 10, "sendWithBufferLevel": 5000, "storeInBufferLevel": 2000, "bufferSize": 20 });
            var a1 = JL.createConsoleAppender('consoleAppender');
            a1.setOptions({});
            var logger = JL("");
            logger.setOptions({ "appenders": [a0, a1] });
            setInterval(function () {
                JL('').appenders.forEach(function (appender) { appender.sendBatch(); });
            }, 1000);
        }; try { __jsnlog_configure(JL); } catch (e) { };
//]]>
    </script>
    <script type="text/javascript" src="/sitecore/shell/controls/lib/jsnlog/jsnlog.min.js"></script>
    <script src="/sitecore/shell/controls/lib/prototype/prototype.js" type="text/javascript"></script>
    <script src="/sitecore/shell/controls/Browser.js" type="text/javascript"></script>
    <script src="/sitecore/shell/controls/Sitecore.js" type="text/javascript"></script>
    <script type="text/JavaScript" language="javascript">
        if (!window.scSitecore) {
            scSitecore = function () { };
        }
        scSitecore.prototype.Settings = {};
        scSitecore.prototype.Settings.SessionTimeout = 1200000;
        scSitecore.prototype.Settings.Icons = {};
        scSitecore.prototype.Settings.Icons.CacheFolder = '/temp/IconCache';
    </script>
    <script type="text/JavaScript" src="/sitecore/shell/Controls/Lib/jQuery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">if (!window.$sc) $sc = jQuery.noConflict();</script>

    <script type="text/JavaScript" src="/sitecore/shell/controls/SitecoreObjects.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/controls/SitecoreKeyboard.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/controls/SitecoreVSplitter.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/controls/SitecoreWindow.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/Applications/Content Manager/Content Editor.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/Applications/Content Manager/Content Editor.Search.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/controls/TreeviewEx/TreeviewEx.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/Controls/Lib/Scriptaculous/Scriptaculous.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/Controls/Lib/Scriptaculous/Effects.js"></script>
    <script type="text/JavaScript" src="/sitecore/shell/Controls/Lib/Scriptaculous/DragDrop.js"></script>
    <script type="text/javascript" src="/sitecore/shell/Controls/Lib/jQuery/jquery-splitter/jquery-splitter.js"></script>

    <script type="text/JavaScript" src="/sitecore/shell/Applications/Analytics/Personalization/Carousel/jquery.jcarousel.min.js"></script>
    <link href="/sitecore/shell/Applications/Analytics/Personalization/Carousel/skin.css" rel="stylesheet">
    <script type="text/JavaScript" src="/sitecore/shell/Applications/Analytics/Personalization/Tooltip.js"></script>
    <link href="/WebResource.axd?d=QKX1AO1vX8ebWKfbb4eOTGDIQz9j6x4q6ioTgcxBSDebSeBmbX6XQI98MDFtAOVyCDaSew-cmvP8EYHfji7DNym0EZwPeH0sZXpVLiqx9bVJnV__z1nnb3xccgL7Arn60&amp;t=636330475860000000" type="text/css" rel="stylesheet" class="Telerik_stylesheet">
    <link href="/WebResource.axd?d=L0_AKNDhmYFLmFvOBCSI4TuoaxTHQYJckR6bl_QxB7Y936hetiQwQxJX_jEnMyeOrE-4Ua85DqQ8TmWig70Kgh_GORs2xm6joS-XbPEOKHMc_P4N-c8dluId9lQvmuZUrXjDPWxe1CHTeEKMY7ZGZ8GJ6ppr1iayR5b9PxSOFUY1&amp;t=636330475880000000" type="text/css" rel="stylesheet" class="Telerik_stylesheet">
    <link href="/WebResource.axd?d=EFjvvw03I-qWJ1M2kiKipw25wYOt4SMPBj-pBfjXlLqVhx39j8e2xh8WvnZZYWNRb2kTHSyVGNTsXNyZHbXzjmN-2xAlMyc0n6Muy2LlAa87AR6P4DZCZ2dwiCZD7StJw4bBBIpGRfn07t14NJ8t_g2&amp;t=636330475860000000" type="text/css" rel="stylesheet" class="Telerik_stylesheet">
    <link href="/WebResource.axd?d=XVDFWeLoj9Fx03-VAorLEIw_9oEzl7gXl0vJ9zCFmyG2BOZUj9w8rDWFIlAWKTPMclDfbS0W2MtY99VpJ6I_00La3wTm1S3QBDK2CuDO0DnCbSLiEWK2R4pWuiI9MJTrmOp4smpyymxlgd9RjwacEMY02z8Sx4CcXTBPV0eM-eE1&amp;t=636330475880000000" type="text/css" rel="stylesheet" class="Telerik_stylesheet">
    <script type="text/javascript" src="/sitecore/shell/Controls/BucketList/BucketList.js" id="BucketListJs"></script>
    <link rel="stylesheet" href="/sitecore/shell/Controls/BucketList/BucketList.css">
</head>
<body>
    <form id="form1" runat="server">
        <div id="EditorPanel3CEE1D4DC3DE4C1590A93C1B8850E062" class="scEditorPanel">
            <div class="scEditorHeader">
                <a href="#" class="scEditorHeaderIcon" onclick="javascript:return scForm.invoke('item:selecticon')" title="Change the icon for this item">
                    <img src="/temp/iconcache/apps/32x32/routes.png?rev=bf659618-593b-4916-bc58-067e1df08dbf&amp;la=en" class="scEditorHeaderIcon" alt="" border="0"></a><div class="scEditorHeaderTitlePanel">
                        <a href="#" class="scEditorHeaderTitle" onclick="javascript:return scForm.invoke('item:rename')" title="Rename this item">Url Redirect Settings</a>
                    </div>
            </div>
            <div id="Reset" class="scEditorSectionCaptionExpanded">
                Upload Url Redirects
            </div>
            <table width="100%" class="scEditorSectionPanel">
                <tbody>
                    <tr>
                        <td class="scEditorSectionPanelCell">
                            <table class="scEditorQuickInfo">
                                <colgroup> 
                                    <col style="white-space: nowrap" valign="top">
                                    <col style="white-space: nowrap" valign="top">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td>Select file:</td>
                                        <td>
                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Select Mode:</td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlImportMode" Width="40em">
                                                <asp:ListItem Text="Append to current" Value="append" Selected="true"></asp:ListItem>
                                                <asp:ListItem Text="Replace on matching name" Value="replace"></asp:ListItem>
                                                <asp:ListItem Text="Overwrite previous import" Value="overwrite"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr> 
                                <tr>
                                        <td>Disable Workflow?</td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="cbxDisableWorkflow" Width="3em" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:Button runat="server" ID="btnUpload" OnClick="btnUpload_Click" Text="Import" Width="200" /></td>
                                    </tr>
                                    <tr style="margin-top: 20px;">
                                        <td></td>
                                        <td style="color: red">
                                            <asp:Literal runat="server" ID="litStatus"></asp:Literal></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
