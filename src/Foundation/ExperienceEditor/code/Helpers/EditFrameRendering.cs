﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using Sitecore.Mvc.Common;

namespace Athena.Foundation.ExperienceEditor.Helpers
{
	public class EditFrameRendering : IDisposable
	{
		private readonly EditFrame _editFrame;
		private readonly HtmlTextWriter _htmlWriter;

		public EditFrameRendering(TextWriter writer, string datasource, string buttonsPath = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Default", string title = "Edit this item", string tooltip = null, string cssClass = null, object parameters = null)
		{
			this._htmlWriter = new HtmlTextWriter(writer);
			this._editFrame = new EditFrame(datasource, buttonsPath, title, tooltip, cssClass, parameters);
			this._editFrame.RenderFirstPart(this._htmlWriter);
		}

		public void Write(string htmlText)
		{
			this._htmlWriter.Write(htmlText);
		}

		public void DoAction(Action<HtmlTextWriter> action)
		{
			action(_htmlWriter);
		}


		public void Dispose()
		{
			this._editFrame.RenderLastPart(this._htmlWriter);
			this._htmlWriter.Dispose();
		}
	}
}