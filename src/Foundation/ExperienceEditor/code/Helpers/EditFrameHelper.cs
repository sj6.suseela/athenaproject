﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using Sitecore.Mvc.Common;

namespace Athena.Foundation.ExperienceEditor.Helpers
{
	public static class EditFrameHelper
	{
		public const string DefaultButton = "<div class='cta cta--edit'><span>EDIT</span></div>"; // "<img src=\"/dist/images/cms/Edit_Rect_x2.png\" />";
		public static string GetEditFrameString(string datasource, string htmlText, string buttonsPath = null, string title = "Edit this item", string tooltip = null, string cssClass = null, object parameters = null)
		{
			if (!Sitecore.Context.PageMode.IsExperienceEditor)
				return "";

			buttonsPath = buttonsPath ?? EditFrameButtons.ButtonPaths.Default;

			var frameString = string.Empty;
			using (var stringWriter = new StringWriter())
			{
				using (var editFrameRendering = new EditFrameRendering(stringWriter, datasource, buttonsPath, title, tooltip, cssClass, parameters))
				{
					editFrameRendering.Write(htmlText);
				}

				frameString = stringWriter.ToString();
			}

			return frameString;
		}

		public static string GetEditFrameString(string datasource, Action<HtmlTextWriter> action, string buttonsPath = null, string title = "Edit this item", string tooltip = null, string cssClass = null, object parameters = null)
		{
			if (!Sitecore.Context.PageMode.IsExperienceEditor)
				return "";

			buttonsPath = buttonsPath ?? EditFrameButtons.ButtonPaths.Default;

			var frameString = string.Empty;
			using (var stringWriter = new StringWriter())
			{
				using (var editFrameRendering = new EditFrameRendering(stringWriter, datasource, buttonsPath, title, tooltip, cssClass, parameters))
				{
					editFrameRendering.DoAction(action);
				}

				frameString = stringWriter.ToString();
			}

			return frameString;
		}

		public static string GetEditFrameButton(string datasource, string buttonsPath = null, string title = "Edit this item", string tooltip = null, string cssClass = null, object parameters = null)
		{
			return GetEditFrameString(datasource, DefaultButton, buttonsPath, title, tooltip, cssClass, parameters);
		}
	}
}