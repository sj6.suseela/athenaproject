﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Foundation.SitecoreExtensions.Helpers
{
	public static class SitecoreItem
	{

		/// <summary>
		/// Gets the current sitecore database.
		/// </summary>
		public static Database Db
		{
			get { return Context.Database; }
		}

		/// <summary>
		/// Gets the current item.
		/// </summary>
		public static Item CurrentItem
		{
			get { return Context.Item; }
		}

		/// <summary>
		/// Gets the current language.
		/// </summary>
		public static Language CurrentLanguage
		{
			get { return Context.Language; }
		}

		/// <summary>
		/// Gets the name of the current language.
		/// </summary>
		/// <value>
		/// The name of the current language.
		/// </value>
		public static string CurrentLanguageName
		{
			get { return CurrentLanguage.Name.ToLower(); }
		}

		// <summary>
		// Gets the home item.
		// </summary>
		public static Item HomeItem
		{
			get { return Db.GetItem(Context.Site.StartPath); }
		}

		/// <summary>
		/// Gets the home item path.
		/// </summary>
		public static string HomeItemPath
		{
			get { return Context.Site.StartPath; }
		}

		/// <summary>
		/// Gets the website item path.
		/// </summary>
		public static string WebsiteItemPath
		{
			get { return Context.Site.ContentStartPath; }
		}

		public static bool IsPageEditorEditing
		{
			get { return Context.PageMode.IsExperienceEditorEditing; }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is page preview.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is page preview; otherwise, <c>false</c>.
		/// </value>
		public static bool IsPagePreview
		{
			get { return Context.PageMode.IsPreview; }
		}

		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public static Item GetItem(string id, Language language = null)
		{
			using (new SecurityDisabler())
			{
				if (language == null)
				{
					return Db.GetItem(id);
				}
				return Db.GetItem(id, language);
			}
		}

		/// <summary>
		/// Get translated text from dictionary item
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static string GetTranslateText(string key)
		{
			string itemValue = Translate.Text(key);
			if (!String.IsNullOrEmpty(itemValue))
				return itemValue;

			return String.Empty;
		}

		public static string RawUrl => Context.RawUrl;
	}
}
