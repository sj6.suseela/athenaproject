﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Sitecore.Mvc.Extensions;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
    public static class StringExtensions
    {
	    public static bool HasValue(this string value)
	    {
		    return !String.IsNullOrWhiteSpace(value);
	    }

	    public static bool HasNoValue(this string value)
	    {
		    return String.IsNullOrWhiteSpace(value);
	    }

		public static string Humanize(this string input)
        {
            return Regex.Replace(input, "(\\B[A-Z])", " $1");
        }

        public static string ToCssUrlValue(this string url)
        {
            return String.IsNullOrWhiteSpace(url) ? "none" : $"url('{url}')";
        }

        // modified from https://stackoverflow.com/a/28373431/109870
        public static string ToInitials(this string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                return String.Empty;
            }

            // first remove all: punctuation, separator chars, control chars, and numbers (unicode style regexes)
            var initials = Regex.Replace(name, @"[\p{P}\p{S}\p{C}\p{N}]+", "");

            // Replacing all possible whitespace/separator characters (unicode style), with a single, regular ascii space.
            initials = Regex.Replace(initials, @"\p{Z}+", " ");

            // Remove all Sr, Jr, I, II, III, IV, V, VI, VII, VIII, IX at the end of names
            initials = Regex.Replace(initials.Trim(), @"\s+(?:[JS]R|I{1,3}|I[VX]|VI{0,3})$", "", RegexOptions.IgnoreCase);

            var split = initials.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            var result = String.Join("", split.Select(s => s.First()));

            return result;
        }

        public static HtmlString ToInitials(this HtmlString name)
        {
            return new HtmlString(name.ToStringOrEmpty().ToInitials());
        }

	    public static bool IsSameAs(this string stringA, string stringB, bool ignoreCase = true)
	    {
		    var comparison = ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture;
		    return String.Compare(stringA, stringB, comparison ) == 0;
	    }

	    /// <summary>
	    ///     Trims the sentence down to the closest word
	    /// </summary>
	    /// <param name="text"></param>
	    /// <param name="maxLength"></param>
	    /// <returns></returns>
	    public static string SubstringToNearestWord(this string text, int maxLength)
	    {
		    if (maxLength < 1)
		    {
			    throw new ArgumentException("maxLength requires a value more than 0", nameof(maxLength));
		    }

		    if (text.Length <= maxLength)
		    {
			    return text;
		    }

		    var position = text.IndexOf(" ", maxLength - 1 , StringComparison.InvariantCultureIgnoreCase);
		    if (position >= 0)
		    {
			    return text.Substring(0, position) + "…";
		    }

		    return text;
	    }
    }
}