﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Mvc.Presentation;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
	public static class RenderingContextExtensions
	{
		public static string GetRenderingParameterValue(this RenderingContext renderingContext, string parameterFieldName, string lookupField = null)
		{
			var parameters = renderingContext.Rendering.Parameters;

			if (ID.TryParse(lookupField, out var fieldId) && ID.TryParse(parameters?[parameterFieldName], out var lookUpItemId))
			{
				var lookupItem = renderingContext.Rendering?.Item.Database.GetItem(lookUpItemId);
				return lookupItem?[fieldId];
			}

			return parameters?[parameterFieldName];
		}
	}
}