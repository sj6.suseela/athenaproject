﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
	public static class DatabaseExtensions
	{
		public static string GetLookUpValue(this Database database, string targetItemId, string fieldName = "Value")
		{
			if (database == null)
				throw new ArgumentNullException(nameof(database));

			if (fieldName == null)
				throw new ArgumentNullException(nameof(fieldName));

			if (string.IsNullOrEmpty(targetItemId))
				return null;

			var item = database.GetItem(targetItemId);
			return item?[fieldName];
		}
	}
}