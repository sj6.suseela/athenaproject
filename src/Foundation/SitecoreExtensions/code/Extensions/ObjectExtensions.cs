﻿namespace Athena.Foundation.SitecoreExtensions.Extensions
{
	public static class ObjectExtensions
	{
		public static bool IsSomething(this object item)
		{
			return item != null;
		}

		public static bool IsNothing(this object item)
		{
			return item == null;
		}
	}
}