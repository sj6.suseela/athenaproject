﻿using System.Reflection;
using System.Web.Mvc;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
	public static class UrlHelperExtensions
	{
		public static string ContentVersioned(this UrlHelper self, string contentPath)
		{
			var versionedContentPath = $"{contentPath}?v={Assembly.GetAssembly(typeof(UrlHelperExtensions)).GetName().Version}";
			return self.Content(versionedContentPath);
		}
	}
}