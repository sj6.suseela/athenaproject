﻿//using Athena.Foundation.LinkTracking.Extensions;
//using Athena.Foundation.NoFollowLink.Extensions;
using Athena.Foundation.LinkTracking.Extensions;
using Athena.Foundation.NoFollowLink.Extensions;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;
using Sitecore.Foundation.SitecoreExtensions.Helpers;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.StringExtensions;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
    public static class ItemExtensions
    {
        private const decimal BytesInAMegaByte = 1048576;
        private const decimal BytesInAKiloByte = 1024;

        public static Item GetAncestorOrSelfOfTemplate(this Item item, ID templateID)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return item.IsDerived(templateID) ? item : item.Axes.GetAncestors().LastOrDefault(i => i.IsDerived(templateID));
        }

        public static IList<Item> GetAncestorsAndSelfOfTemplate(this Item item, ID templateID)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            var returnValue = new List<Item>();
            if (item.IsDerived(templateID))
            {
                returnValue.Add(item);
            }

            returnValue.AddRange(item.Axes.GetAncestors().Reverse().Where(i => i.IsDerived(templateID)));
            return returnValue;
        }

        public static IEnumerable<Item> ChildrenOfTemplate(this Item item, ID templateId)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return item.Children.Where(i => i.IsDerived(templateId));
        }

        public static bool HasChildrenOfTemplate(this Item item, ID templateId)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return item.Children.Any(i => i.IsDerived(templateId));
        }

        public static bool HasLayout(this Item item)
        {
            return item?.Visualization?.Layout != null;
        }

        public static bool IsDerived(this Item item, ID templateId)
        {
            if (item == null)
            {
                return false;
            }
            return !templateId.IsNull && item.IsDerived(item.Database.Templates[templateId]);
        }

        private static bool IsDerived(this Item item, Item templateItem)
        {
            if (item == null)
            {
                return false;
            }
            if (templateItem == null)
            {
                return false;
            }
            var itemTemplate = TemplateManager.GetTemplate(item);
            return itemTemplate != null && (itemTemplate.ID == templateItem.ID || itemTemplate.DescendsFrom(templateItem.ID));
        }

        public static string Url(this Item item, UrlOptions options = null)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            if (options != null)
                return LinkManager.GetItemUrl(item, options);
            return !item.Paths.IsMediaItem ? LinkManager.GetItemUrl(item) : MediaManager.GetMediaUrl(item);
        }

        public static string CanonicalUrl(this Item item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            string canonicalUrl = null;
            if (string.IsNullOrWhiteSpace(canonicalUrl) && item == Context.Item)
            {
                canonicalUrl = HttpContext.Current?.Items?["canonical"] as string;
            }

            if (string.IsNullOrWhiteSpace(canonicalUrl))
            {
                var canonicalField = item != null ? item["Canonical Url"] : null;
                if (!string.IsNullOrEmpty(canonicalField))
                {
                    canonicalUrl = canonicalField;
                }
            }

            if (string.IsNullOrWhiteSpace(canonicalUrl))
            {
                var urlOptions = LinkManager.GetDefaultUrlOptions();
                urlOptions.AlwaysIncludeServerUrl = true;
                urlOptions.LanguageEmbedding = LanguageEmbedding.Never;
                canonicalUrl = item.Url(urlOptions);
            }

            canonicalUrl = canonicalUrl.Replace(":443", "");

            return canonicalUrl;
        }

        /// <summary>
        /// Used in conjuction with the BackgroundImage custom experience button. REQUIRES the field name to be "BackgroundImage"
        /// </summary>
        /// <param name="item">Datasource item</param>
        /// <param name="backgroundImage"></param>
        /// <param name="imageField">Background Image Field ID</param>s
        /// <returns>A non-editable image</returns>
        public static string RenderBackgroundImage(this Item item, ID backgroundImage)
        {
            return RenderFieldNonEditable(item, backgroundImage);
        }

        /// <summary>
        /// Renders a field without it being editable
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public static string RenderFieldNonEditable(this Item item, ID fieldId)
        {
            var render = RenderField(item, fieldId, null, "disable-web-editing=true");
            return Context.PageMode.IsExperienceEditor && render.Contains("scEmptyImage") ? String.Empty : render;
        }

        /// <summary>
        /// Renders a richtext field removing or adding paragraph tags as required
        /// </summary>
        /// <param name="item"></param>
        /// <param name="linkFieldId"></param>
        /// <param name="enforceParagraph">Add an enclosing paragraph tag if one does not exist</param>
        /// <param name="stripParagraphs">Strip all paragraph tags</param>
        /// <returns></returns>
        public static string RenderRichTextField(this Item item, ID linkFieldId, bool enforceParagraph = false, bool stripParagraphs = false)
        {
            var render = RenderField(item, linkFieldId);

            if (Sitecore.Context.PageMode.IsExperienceEditor || (!enforceParagraph && !stripParagraphs))
                return render;

            if (stripParagraphs)
            {
                render = render.Replace("<p>", "").Replace("</p>", "</p>");
            }

            if (enforceParagraph)
            {
                if (!render.StartsWith("<p") && !render.EndsWith("</p>"))
                {
                    render = $"<p>{render}</p>";
                }
            }

            return render;
        }

        public static string RenderField(this Item item, ID linkFieldId, string labelFieldId = null, string additionalParameters = null, string url = null)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var field = item.Fields[linkFieldId];

            Field labelField = null;
            if (labelFieldId != null)
            {
                labelField = item.Fields[ID.Parse(labelFieldId)];
            }

            if (field?.Type == "General Link" && !Sitecore.Context.PageMode.IsExperienceEditor)
            {
                return RenderLinkField(item, linkFieldId, labelField, url: url);
            }

            var fieldName = field?.Name;
            if (string.IsNullOrEmpty(fieldName))
                throw new Exception($"Error in {nameof(RenderField)} method. Field of ID '{linkFieldId}' cannot be found in {item.Name} ({item.TemplateName})");

            var labelFieldName = labelField?.Name;

            if (labelField != null && string.IsNullOrEmpty(labelFieldName))
                throw new Exception($"Error in {nameof(RenderField)} method. Field of ID '{labelFieldId}' cannot be found in {item.Name} ({item.TemplateName})");
            if (additionalParameters != null)
                return labelField != null ? string.Concat(FieldRenderer.Render(item, fieldName, additionalParameters), FieldRenderer.Render(item, labelFieldName)) : FieldRenderer.Render(item, fieldName, additionalParameters);
            return labelField != null ? string.Concat(FieldRenderer.Render(item, fieldName), FieldRenderer.Render(item, labelFieldName)) : FieldRenderer.Render(item, fieldName);

        }

        public static string RenderMediaLinkField(this Item item, ID linkFieldId)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var field = item.Fields[linkFieldId];
            if (field?.Type == "General Link" && !Sitecore.Context.PageMode.IsExperienceEditor)
            {
                return RenderLinkField(item, linkFieldId, appendMediaDetails: true);
            }

            return RenderField(item, linkFieldId);
        }


        private static string RenderLinkField(Item item, ID linkFieldId, Field labelField = null, bool appendMediaDetails = false, string url = null)
        {
            if (string.IsNullOrEmpty(item[linkFieldId]))
                return "";

            LinkField linkField = item.Fields[linkFieldId];
            var linkUrl = url ?? (linkField.IsInternal
                ? (linkField.TargetItem != null ? linkField.TargetItem.Url() : "#")
                : linkField.GetFriendlyUrl());
            var linkCss = !string.IsNullOrEmpty(linkField.Class) ? $" class=\"{linkField.Class}\"" : "";
            var linkTarget = !string.IsNullOrEmpty(linkField.Target) ? $" target=\"{linkField.Target}\"" : "";
            var linkText = labelField != null ? labelField.Value : linkField.Text;
            //var noFollowTag = linkField.NoFollowTag();
            var noFollowTag = string.Empty;
            var linkRel = noFollowTag.IsNullOrEmpty() ? "" : $" rel=\"{noFollowTag}\"";
            if (linkField.IsMediaLink && appendMediaDetails)
            {
                MediaItem media = linkField.TargetItem;
                if (media != null)
                {
                    linkText = $"{linkText} ({media.Extension?.ToUpper()}, {ConvertToFileSizeString(media.Size)})";
                }
            }

            var id = item.GetTrackingId(linkField);

            return RenderAnchorTag(id, linkUrl, linkText, $"{linkCss}{linkTarget}{linkRel}");
        }

        public static string RenderAnchorTag(string trackingId, string url, string linkText, string additionalAttributes = null)
        {
            return $"<a id=\"{trackingId}\" href=\"{url}\"{additionalAttributes}>{linkText}</a>";
        }

        private static string ConvertToFileSizeString(long size)
        {
            var suffix = "KB";
            var value = System.Convert.ToDecimal(size);
            if (size >= BytesInAMegaByte)
            {
                suffix = "MB";
                value = Math.Round(value / BytesInAMegaByte, 1);
            }
            else if (size >= BytesInAKiloByte)
            {
                value = Math.Round(value / BytesInAKiloByte);
            }

            return $"{value.ToString().Replace(".0", "")}{suffix}";
        }

        public static string RenderEmptyUrlLink(this Item item, ID moreLinkLabelId)
        {
            var field = item.Fields[moreLinkLabelId];
            if (Context.PageMode.IsExperienceEditorEditing)
                return RenderField(item, moreLinkLabelId);

            var linkUrl = "#";
            var linkText = field != null ? field.Value : string.Empty;

            return $"<a href=\"{linkUrl}\">{linkText}</a>";
        }

        public static double RenderNumberField(this Item item, ID numberFieldId)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var field = item.Fields[numberFieldId];
            var d = field != null && field.Value != String.Empty ? Double.Parse(field.Value) : 0;
            return d;
        }

        public static string GetLinkText(this Item item, ID fieldId)
        {
            LinkField linkField = item.Fields[fieldId];
            return linkField?.Text;
        }

        public static string GetHref(this Item item, ID fieldId)
        {
            LinkField linkField = item.Fields[fieldId];

            return linkField?.GetFriendlyUrl();
        }

        public static string GetFileExt(this Item item, ID fieldId)
        {
            var ext = string.Empty;

            if (item == null)
                throw new ArgumentNullException(nameof(item));

            LinkField linkField = item.Fields[fieldId];

            if (linkField == null || linkField.Value.HasNoValue())
                return ext;

            if (linkField.IsMediaLink)
            {
                MediaItem media = linkField.TargetItem;
                if (media != null)
                {
                    ext = media.Extension.ToLowerInvariant();
                }
            }
            else if (linkField.Url.Contains('.'))
            {
                var url = linkField.Url;
                if (url.Contains('?'))
                    url = url.Substring(0, linkField.Url.IndexOf('?'));
                ext = url.Substring(url.LastIndexOf('.') + 1).ToLowerInvariant();
            }

            return ext;
        }

        public static string DownLoadMediaLink(this Item item, ID mediaFieldId, ID labelFieldId, string downloadApiUrl)
        {
            ImageField field = item.Fields[mediaFieldId];
            var downloadUrl = string.Format(downloadApiUrl, field.MediaItem.ID);
            return field.RenderMediaLinkTag(downloadUrl, item.Fields[labelFieldId]?.Value, item.GetTrackingId(mediaFieldId));
        }

        public static bool HasFinalWorkflowState(this Item item)
        {
            foreach (var itemVersion in item.Versions.GetVersions())
            {
                var workflow = item.Database.WorkflowProvider.GetWorkflow(itemVersion);
                var state = workflow?.GetState(itemVersion);
                if (state != null && state.FinalState)
                    return true;
            }
            return false;
        }

        public static string GetFieldValue(this Item item, ID fieldId, string defaultValue = "")
        {
            try
            {
                if (item != null && !string.IsNullOrEmpty(item[fieldId]))
                    return item[fieldId];
                return defaultValue;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return defaultValue;
            }

        }

        public static string GetFieldValue(this Item item, string fieldName, string defaultValue = "")
        {
            try
            {
                if (item != null && !string.IsNullOrEmpty(item[fieldName]))
                    return item[fieldName];
                return defaultValue;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return defaultValue;
            }

        }

        public static Item GetFieldValueAsItem(this Item item, ID fieldId)
        {
            try
            {
                string fieldValue = item.GetFieldValue(fieldId, string.Empty);
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    return SitecoreItem.Db.GetItem(fieldValue);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return null;
            }
            return null;
        }

        public static Item GetFieldValueAsItem(this Item item, string fieldName)
        {
            try
            {
                string fieldValue = item.GetFieldValue(fieldName, string.Empty);
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    return SitecoreItem.Db.GetItem(fieldValue);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return null;
            }
            return null;
        }

        public static bool GetBoolFieldValue(this Item item, ID fieldID)
        {
            try
            {
                CheckboxField fieldValue = (CheckboxField)item.Fields[fieldID];
                if (fieldValue != null)
                    return fieldValue.Checked;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return false;
            }
        }

        public static bool GetBoolFieldValue(this Item item, string fieldName)
        {
            try
            {
                CheckboxField fieldValue = (CheckboxField)item.Fields[fieldName];
                if (fieldValue != null)
                    return fieldValue.Checked;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return false;
            }
        }

        public static string RenderLookupField(this Item item, ID lookupFieldId, ID targetField)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            LookupField lookupField = item.Fields[lookupFieldId];
            if (lookupField != null && !Context.PageMode.IsExperienceEditor)
            {
                return lookupField.TargetItem != null ? lookupField.TargetItem[targetField] : string.Empty;
            }

            var fieldName = item.Fields[lookupFieldId].Name;
            if (string.IsNullOrEmpty(fieldName))
            {
                var methodName = nameof(RenderLookupField);
                throw new Exception($"Error in {methodName} method. Field of ID '{lookupFieldId}' cannot be found in {item.Name} ({item.TemplateName})");
            }

            return FieldRenderer.Render(item, fieldName);
        }

        public static string GetLookupValue(this Item item, string referencingFieldName, string lookupFieldName)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            if (referencingFieldName == null)
                throw new ArgumentNullException(nameof(referencingFieldName));

            if (lookupFieldName == null)
                throw new ArgumentNullException(nameof(lookupFieldName));

            LookupField lookupField = item.Fields[referencingFieldName];

            return lookupField?.TargetItem?[lookupFieldName];
        }

        public static string GetLookupValue(this Item item, ID referencingFieldId, string lookupFieldName)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var referencingFieldName = item.Fields[referencingFieldId]?.Name;

            return GetLookupValue(item, referencingFieldName, lookupFieldName);
        }

        public static string GetLookupValue(this Item item, ID referencingFieldId, ID lookupFieldId)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var lookupFieldName = item.Fields[lookupFieldId]?.Name;

            return GetLookupValue(item, referencingFieldId, lookupFieldName);
        }
    }
}
