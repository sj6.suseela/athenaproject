﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
	public static class HtmlHelperExtensions
	{
		public static IHtmlString AttributeIf(this HtmlHelper htmlHelper, bool condition, string attributeName,
			string value = null)
		{
			if (string.IsNullOrEmpty(attributeName))
				throw new ArgumentNullException(nameof(attributeName));

			var returnValue = condition ? value != null ? $"{attributeName}=\"{value}\"" : attributeName : string.Empty;

			return htmlHelper.Raw(returnValue);
		}

		public static IHtmlString AttributeIfNotEmpty(this HtmlHelper htmlHelper, string attributeName, string value)
		{
			if (string.IsNullOrEmpty(attributeName))
				throw new ArgumentNullException(nameof(attributeName));

			var returnValue = !string.IsNullOrEmpty(value) ? $"{attributeName}=\"{value}\""  : string.Empty;

			return htmlHelper.Raw(returnValue);
		}
	}
}