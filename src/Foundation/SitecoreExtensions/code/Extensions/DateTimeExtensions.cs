﻿using System;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
	public static class DateTimeExtensions
	{
		public static string ToISO8601String(this DateTime value)
		{
			return value.ToString("yyyy-MM-ddTHH:mm:ssZ");
		}
	}
}