﻿using System;
using System.Xml;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
	public static class FieldExtensions
	{
		public static string FullImageUrl(this ImageField imageField)
		{
			return imageField.ImageUrl(true);
		}

		public static string ImageUrl(this ImageField imageField)
		{
			return imageField.ImageUrl(false);
		}

		public static string ImageUrl(this ImageField imageField, bool fullImagePath)
		{
			if (imageField?.MediaItem == null) throw new ArgumentNullException(nameof(imageField.MediaItem));

			var options = MediaUrlOptions.Empty;
			options.AbsolutePath = fullImagePath;
			options.AlwaysIncludeServerUrl = fullImagePath;
			int width, height;

			if (int.TryParse(imageField.Width, out width)) options.Width = width;

			if (int.TryParse(imageField.Height, out height)) options.Height = height;

			return imageField.ImageUrl(options);
		}

		public static string ImageUrl(this ImageField imageField, MediaUrlOptions options)
		{
			if (imageField?.MediaItem == null) throw new ArgumentNullException(nameof(imageField.MediaItem));

			return options == null
				? imageField.ImageUrl()
				: HashingUtils.ProtectAssetUrl(MediaManager.GetMediaUrl(imageField.MediaItem, options));
		}

		public static bool HasImage(this ImageField imageField)
		{
			return imageField?.MediaItem != null;
		}

		public static bool IsChecked(this Field checkboxField)
		{
			if (checkboxField == null) throw new ArgumentNullException(nameof(checkboxField));
			return MainUtil.GetBool(checkboxField.Value, false);
		}
		
		public static bool IsFieldChecked(this Item item, ID field)
		{
			return item.Fields[field].IsChecked();
		}

		public static bool TryParseDateTime(this Field dateTimeField, out DateTime datetime)
		{
			return DateTime.TryParseExact(dateTimeField?.Value, "yyyyMMdd'T'HHmmss'Z'", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeUniversal, out datetime);
		}

		public static string GetAdvancedImageUrl(this ImageField imageField, int width = 100, int height = 100)
		{
			if (string.IsNullOrWhiteSpace(imageField.Value))
				return string.Empty;

			if (imageField.MediaItem == null)
				return string.Empty;

			var xml = new XmlDocument();
			xml.LoadXml(imageField.Value);

			if (xml.DocumentElement == null) return string.Empty;

			var cropx = xml.DocumentElement.HasAttribute("cropx") ? xml.DocumentElement.GetAttribute("cropx") : string.Empty;
			var cropy = xml.DocumentElement.HasAttribute("cropy") ? xml.DocumentElement.GetAttribute("cropy") : string.Empty;
			var focusx = xml.DocumentElement.HasAttribute("focusx") ? xml.DocumentElement.GetAttribute("focusx") : string.Empty;
			var focusy = xml.DocumentElement.HasAttribute("focusy") ? xml.DocumentElement.GetAttribute("focusy") : string.Empty;

			float.TryParse(cropx, out float cx);
			float.TryParse(cropy, out float cy);
			float.TryParse(focusx, out float fx);
			float.TryParse(focusy, out float fy);

			var imageSrc = MediaManager.GetMediaUrl(imageField.MediaItem);

			var src = $"{imageSrc}?cx={cx}&amp;cy={cy}&amp;cw={width}&amp;ch={height}";

			var hash = HashingUtils.GetAssetUrlHash(src);

			return $"{src}&amp;hash={hash}";
		}
	}
}