﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Mvc.Presentation;

namespace Athena.Foundation.SitecoreExtensions.Extensions
{
	public static class ControllerExtensions
	{
		/// <summary>
		/// An extension to wrap the return of a View, to confirm that the rendering in question has a datasource before attempting to render the View. 
		/// Use when the View has logic that depends on an associated sitecore item.
		/// </summary>
		/// <param name="controller"></param>
		/// <param name="getModuleView"></param>
		/// <param name="wrapperElement"></param>
		/// <returns></returns>
		public static ActionResult GetModuleView(
			this Controller controller,
			Func<ActionResult> getModuleView,
			string wrapperElement = null)
		{
			var result = new ContentResult {ContentType = "text/HTML"};

			if (RenderingHasContent())
			{
				return getModuleView.Invoke();
			}

			if (Sitecore.Context.PageMode.IsExperienceEditor && !RenderingHasContent())
			{
				var html = "<p class=\"sc-edit-tooltip\">Please select the content for this module</p>";
				if (!string.IsNullOrEmpty(wrapperElement))
				{
					html = $"<{wrapperElement}>{html}</{wrapperElement}>";
				}

				result.Content = html;
			}

			return result;
		}

		private static bool RenderingHasContent()
		{
			return RenderingContext.Current?.Rendering?.Item != null &&
			       !string.IsNullOrEmpty(RenderingContext.Current.Rendering.DataSource);
		}
	}
}