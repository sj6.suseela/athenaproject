﻿using React;
using React.TinyIoC;

namespace Athena.Foundation.React
{
	/// <summary>
	/// Register the customreact environment used to support loading react components seperatly on page render and page load
	/// </summary>
	public class RegisterReactDependencies : IAssemblyRegistration
	{
		public void Register(TinyIoCContainer container)
		{
			container.Register<OnPageRenderEnvironment, OnPageRenderEnvironment>().AsPerRequestSingleton();
		}
	}
}