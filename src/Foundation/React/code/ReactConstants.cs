﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.React
{
	public class ReactConstants
	{
		/** The render server side. */
		public static string RENDER_SERVERSIDE = "static";

		/** The render client side. */
		public static string RENDER_CLIENTSIDE = "dynamic";
	}
}