﻿using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using React;
using React.Web.Mvc;
using IHtmlHelper = System.Web.Mvc.HtmlHelper;

namespace Athena.Foundation.React
{
    public static class HtmlExtensions
    {
        /// <summary>
        /// Render a react components.
        /// </summary>
        public static IHtmlString ReactComponent<T>(this IHtmlHelper htmlHelper, string componentName, T props, string htmlTag = null, string containerId = null, bool clientOnly = false, bool serverOnly = false, bool onPageRender = false, string containerClass = null)
        {
            try
            {
                var modelJson = JsonConvert.SerializeObject(props);
                JObject jObject = JObject.Parse(modelJson);
                JObject properties = (JObject)jObject["props"];

                containerId = (string)jObject["id"];
                string type = (string)jObject["type"];
                var component = Environment.CreateComponent(componentName, properties, containerId, clientOnly);
                if (!string.IsNullOrEmpty(htmlTag))
                    component.ContainerTag = htmlTag;
                if (!string.IsNullOrEmpty(containerClass))
                    component.ContainerClass = containerClass;
                var htmlString = component.RenderHtml(false);
                if (type.Equals("static"))
                {
                    return new HtmlString(htmlString);
                }
                else
                {
                    var innerHtml = $"LV.components.data.push({modelJson});";
                    var tagBuilder = new TagBuilder("script")
                    {
                        InnerHtml = innerHtml
                    };
                    var newLine = System.Environment.NewLine;
                    var str2 = tagBuilder.ToString();
                    return new HtmlString(htmlString + newLine + str2);
                }
            }
            finally
            {
                Environment.ReturnEngineToPool();
            }
        }

        /// <summary>
        /// Ouptut compnents you have elected to be loaded on page render
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="clientOnly"></param>
        /// <returns></returns>
        public static IHtmlString ReactInitJavaScriptOnPageRender(this IHtmlHelper htmlHelper, bool clientOnly = false)
        {
            try
            {
                var initJavaScript = OnPageRenderEnvironment.GetOnRenderInitJavaScript(clientOnly);
                return new HtmlString(new TagBuilder("script")
                {
                    InnerHtml = initJavaScript
                }.ToString());
            }
            finally
            {
                Environment.ReturnEngineToPool();
            }
        }

        /// <summary>
        /// Output components wrapped in an event listener so that they are loaded on page load
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="clientOnly"></param>
        /// <returns></returns>
        public static IHtmlString ReactInitJavaScriptOnPageLoad(this IHtmlHelper htmlHelper, bool clientOnly = false)
        {
            try
            {
                var initJavaScript = Environment.GetInitJavaScript(clientOnly);
                var script = BuildOnLoadScript(initJavaScript);

                return GetScriptTag(script);
            }
            finally
            {
                Environment.ReturnEngineToPool();
            }
        }

        private static string BuildOnLoadScript(string initJavaScript)
        {
            return $"window.addEventListener(\'load\', function(){{{initJavaScript}}});";
        }

        private static IHtmlString GetScriptTag(string script)
        {
            var tag = new TagBuilder("script")
            {
                InnerHtml = script
            };
            return new HtmlString(tag.ToString());
        }

        private static IReactEnvironment Environment => ReactEnvironment.GetCurrentOrThrow;

        private static OnPageRenderEnvironment OnPageRenderEnvironment => AssemblyRegistration.Container.Resolve<OnPageRenderEnvironment>();

        private static string AddToArray(bool onPageRender)
        {
            return onPageRender ? "hydrateOnReader" : "hydrateOnLoad";
        }
    }
}