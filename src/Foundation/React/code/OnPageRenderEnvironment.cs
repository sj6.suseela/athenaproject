﻿using System.Collections.Generic;
using System.Text;
using React;

namespace Athena.Foundation.React
{
	/// <summary>
	/// A custom implementation of the ReactEnvironment from ReactJs.net this supports us adding components to another list that is then output at page rendered.  
	/// This means other react components can be wrapped in an event listener that allows them to be loaded on page load. 
	/// </summary>
	public class OnPageRenderEnvironment : ReactEnvironment
	{
		protected readonly IList<IReactComponent> OnRenderComponents = new List<IReactComponent>();

		public OnPageRenderEnvironment(IJavaScriptEngineFactory engineFactory, IReactSiteConfiguration config, ICache cache, IFileSystem fileSystem, IFileCacheHash fileCacheHash) : base(engineFactory, config, cache, fileSystem, fileCacheHash)
		{
		}

		public IReactComponent CreateOnRenderComponent<T>(string componentName, T props, string containerId = null, bool clientOnly = false)
		{
			if (!clientOnly)
				EnsureUserScriptsLoaded();

			var reactComponent = new ReactComponent(this, _config, componentName, containerId)
			{
				Props = props
			};
			OnRenderComponents.Add(reactComponent);
			return reactComponent;
		}

		public virtual string GetOnRenderInitJavaScript(bool clientOnly = false)
		{
			var stringBuilder = new StringBuilder();
			if (!clientOnly)
			{
				var str = Execute<string>("console.getCalls()");
				stringBuilder.Append(str);
			}
			foreach (var component in OnRenderComponents)
			{
				stringBuilder.Append(component.RenderJavaScript());
				stringBuilder.AppendLine(";");
			}
			return stringBuilder.ToString();
		}
	}
}