﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.React.Models
{
	public class AbstractComponentModel
	{
		[JsonProperty("name")]
		public string ComponentName { get; set; }
		[JsonProperty("id")]
		public string ComponentId { get; set; } = Guid.NewGuid().ToString();
		[JsonProperty("type")]
		public string ComponentType { get; set; }
		[JsonProperty("props")]
		public AbstractDataModel ComponentProperties { get; set; }

		public void setComponentData(AbstractDataModel dataModel)
		{
			ComponentName = dataModel.GetComponentName();
			ComponentType = dataModel.GetRenderType();
			ComponentProperties = dataModel;
		}
	}
}
