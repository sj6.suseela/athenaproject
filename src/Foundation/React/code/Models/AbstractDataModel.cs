﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.React.Models
{
	public abstract class AbstractDataModel
	{
		public string GetRenderType()
		{
			return ReactConstants.RENDER_CLIENTSIDE;
		}

		public abstract string GetComponentName();
	}
}