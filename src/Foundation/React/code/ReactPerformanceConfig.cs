﻿using Athena.Foundation.Common;

namespace Athena.Foundation.React
{
	public class ReactPerformanceConfig
	{
		/// <summary>
		/// Components will be loaded on page load and not automatically.  
		/// </summary>
		public static bool LazyLoadComponents => AppSettings.Get<bool>("Performance.LazyLoadComponents");

		/// <summary>
		/// Images are loaded after page load
		/// </summary>
		public static bool LazyLoadImages => AppSettings.Get<bool>("Performance.LazyLoadImages");
		
		/// <summary>
		/// Allows components to be hyrdrated inline.  This mean that each component will get its own React.Hydrate call.  This is required to output cache components
		/// </summary>
		public static bool InlineComponentHydrateEnabled => AppSettings.Get<bool>("Performance.InlineComponentHydrateEnabled");

		/// <summary>
		/// Wrap hyrdate of inline components in an on load event handler
		/// </summary>
		public static bool LazyLoadInlineComponents => AppSettings.Get<bool>("Performance.LazyLoadInlineComponents");
	}
}