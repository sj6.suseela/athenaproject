﻿using System;
using System.Collections.Generic;
using Sitecore.Data;

namespace Athena.Foundation.NoFollowLink.Interfaces
{
	public interface INoFollowCache
	{
		List<ID> GetOrAddNoFollowMediaItems(string siteName, Func<List<ID>> getAction);

		List<ID> GetOrAddNoFollowInternalItems(string siteName, Func<List<ID>> getAction);

		List<string> GetOrAddNoFollowDomains(string siteName, Func<List<string>> getAction);
	}
}