﻿using System.Collections.Generic;
using Sitecore.Data;

namespace Athena.Foundation.NoFollowLink.Interfaces
{
	public interface INoFollowRepository
	{
		IEnumerable<ID> GetNoFollowMediaItems();

		IEnumerable<ID> GetNoFollowInternalItem();

		IEnumerable<string> GetNoFollowDomains();
	}
}