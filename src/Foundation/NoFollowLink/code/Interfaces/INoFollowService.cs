﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Athena.Foundation.NoFollowLink.Interfaces
{
	public interface INoFollowService
	{
		string NoFollowTag { get; }

		string AddNoFollowTag(LinkField linkField);

		bool IsANoFollowItem(Item item);

		bool IsANoFollowMediaItem(Item item);
	}
}