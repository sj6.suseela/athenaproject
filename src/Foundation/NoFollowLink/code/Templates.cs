﻿using Sitecore.Data;

namespace Athena.Foundation.NoFollowLink
{
	public static class Templates
	{
		public static class NoFollowLink
		{
			public static readonly ID TemplateId = new ID("{402C7F33-463D-4C9D-AACA-0CB0B5284228}");

			public static class Fields
			{
				public static readonly ID ExcludedInternalPages = new ID("{A9008AA7-267E-4420-8EDE-D183BE49232D}");
				public static readonly ID ExcludedMediaItems = new ID("{A4E0BCEF-D3DD-47EB-94BE-28152FCC9C9C}");
				public static readonly ID ExcludedDomainItems = new ID("{4DA11D3D-01A0-4367-9F70-F3240DFFA358}");
			}
		}

		public static class ExternalDomain
		{
			public static class Fields
			{
				public static readonly ID DomainUrl=new ID("{E805CCDF-C236-4F36-93D4-2641AA3131B3}");
			}
		}
	}
}