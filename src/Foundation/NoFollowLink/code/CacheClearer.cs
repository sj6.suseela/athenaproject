﻿using System;
using System.Collections;
using Sitecore.Caching;
using Sitecore.Diagnostics;

namespace Athena.Foundation.NoFollowLink
{
	public class CacheClearer
	{
		public void ClearCaches(object sender, EventArgs args)
		{
			if (sender == null)
			{
				throw new ArgumentNullException(nameof(sender));
			}
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}

			try
			{
				DoClear();
			}
			catch (Exception ex)
			{
				Log.Error(this + ": " + ex, ex, this);
			}
		}

		private void DoClear()
		{
			foreach (string cacheName in Caches)
			{
				var cache = CacheManager.FindCacheByName<string>(cacheName);
				if (cache == null)
					continue;

				Log.Info($"Clearing {cache.Count} items from {cacheName}", this);

				cache.Clear();
			}
		}

		public ArrayList Caches { get; } = new ArrayList();
	}
}