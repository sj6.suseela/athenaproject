﻿using System;
using Sitecore.Caching;

namespace Athena.Foundation.NoFollowLink
{
	public static class CacheExtensions
	{
		private static readonly object Lock = new object();

		/// <summary>
		/// Handles getting items from cache with locking on adding items to the cache.  This pattern has a single lock item shared across the whole cache. 
		/// </summary>
		/// <typeparam name="TResult"></typeparam>
		/// <param name="cache"></param>
		/// <param name="key"></param>
		/// <param name="getItem"></param>
		/// <param name="duration"></param>
		/// <returns></returns>
		public static TResult GetOrAdd<TResult>(this Cache cache, string key, Func<TResult> getItem, TimeSpan duration)
		{
			TResult result;
			var data = cache[key]; // Can't cast using as operator as TResult may be an int or bool

			if (data == null)
			{
				lock (Lock)
				{
					data = cache[key];

					if (data == null)
					{
						result = getItem();

						if (result == null)
							return result;

						cache.Add(key, result, DateTime.UtcNow.Add(duration));
					}
					else
						result = (TResult)data;
				}
			}
			else
				result = (TResult)data;

			return result;
		}
	}
}