﻿using Athena.Foundation.NoFollowLink.Interfaces;

namespace Athena.Foundation.NoFollowLink
{
	public static class NoFollowServiceFactory
	{
		public static INoFollowService Create()
		{
			return new NoFollowService(new NoFollowRepository(NoFollowCacheManager.GetNoFollowCache()));
		}
	}
}