﻿using Athena.Foundation.NoFollowLink.Interfaces;
using Sitecore.Data.Items;

namespace Athena.Foundation.NoFollowLink.Extensions
{
	public static class NoFollowLinkItemExtensions
	{
		private static readonly INoFollowService NoFollowService = NoFollowServiceFactory.Create();

		public static string AddNoFollowTag(this Item item)
		{
			if (item == null)
			{
				return string.Empty;
			}

			return NoFollowService.IsANoFollowItem(item) ? NoFollowService.NoFollowTag : string.Empty;
		}
	}
}