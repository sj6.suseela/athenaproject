﻿using Athena.Foundation.NoFollowLink.Interfaces;
using Sitecore.Data.Fields;

namespace Athena.Foundation.NoFollowLink.Extensions
{
	public static class NoFollowLinkFieldExtensions
	{
		private static readonly INoFollowService NoFollowService = NoFollowServiceFactory.Create();

		public static string NoFollowTag(this LinkField linkField)
		{
			return NoFollowService.AddNoFollowTag(linkField);
		}

		public static string RenderLinkTag(this LinkField linkField,string linkText, string id)
		{
			return NoFollowLinkHelper.RenderNoFollowLinkTag(linkField?.NoFollowTag(), linkField?.GetFriendlyUrl(), linkText, id);
		}
	}
}