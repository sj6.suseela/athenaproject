﻿using Athena.Foundation.NoFollowLink.Interfaces;
using Sitecore.Data.Fields;

namespace Athena.Foundation.NoFollowLink.Extensions
{
	public static class ImageFieldExtensions
	{
		private static readonly INoFollowService NoFollowService = NoFollowServiceFactory.Create();

		public static string AddNoFollowTag(this ImageField imageField)
		{
			if (imageField.MediaItem == null)
			{
				return string.Empty;
			}
			return NoFollowService.IsANoFollowMediaItem(imageField.MediaItem) ?
				NoFollowService.NoFollowTag :
				string.Empty;
		}

		public static string RenderMediaLinkTag(this ImageField field, string url,string linkText, string id)
		{
			return NoFollowLinkHelper.RenderNoFollowLinkTag(field.AddNoFollowTag(), url, linkText, id);
		}
	}
}