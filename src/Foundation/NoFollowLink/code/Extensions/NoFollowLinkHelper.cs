﻿namespace Athena.Foundation.NoFollowLink.Extensions
{
	public static class NoFollowLinkHelper
	{
		public static string RenderNoFollowLinkTag(string noFollowTag, string url, string linkText, string id)
		{
			var linkRel = string.IsNullOrEmpty(noFollowTag) ? "" : $" rel=\"{noFollowTag}\"";
			return $"<a id='{id}'{linkRel} href='{url}'>{linkText}</a>";
		}
	}
}