﻿using System;
using System.Collections.Generic;
using Athena.Foundation.NoFollowLink.Interfaces;
using Sitecore.Caching;
using Sitecore.Data;

namespace Athena.Foundation.NoFollowLink
{
	public class NoFollowCache : Cache, INoFollowCache
	{
		private readonly TimeSpan _defaultExpiration = TimeSpan.Parse(Sitecore.Configuration.Settings.GetSetting("NoFollowCacheExpiration"));

		public NoFollowCache(string name, long maxSize) : base(name, maxSize) { }

		public List<ID> GetOrAddNoFollowMediaItems(string siteName, Func<List<ID>> action)
		{
			return this.GetOrAdd(Keys.NoFollowMediaItems(siteName), action, _defaultExpiration);
		}

		public List<ID> GetOrAddNoFollowInternalItems(string siteName, Func<List<ID>> action)
		{
			return this.GetOrAdd(Keys.NoFollowInternalItems(siteName), action, _defaultExpiration);
		}

		public List<string> GetOrAddNoFollowDomains(string siteName, Func<List<string>> action)
		{
			return this.GetOrAdd(Keys.NoFollowDomains(siteName), action, _defaultExpiration);
		}

		private static class Keys
		{
			public static string NoFollowMediaItems(string siteName) => BuildCacheKey(siteName, "NoFollowMediaItems");
			public static string NoFollowInternalItems(string siteName) => BuildCacheKey(siteName, "NoFollowInternalItems");
			public static string NoFollowDomains(string siteName) => BuildCacheKey(siteName, "NoFollowDomains");

			private static string BuildCacheKey(string siteName, string key) => $"{siteName}.{key}";
		}
	}
}