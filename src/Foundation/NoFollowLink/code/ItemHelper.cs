﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;

namespace Athena.Foundation.NoFollowLink
{
	/// <summary>
	/// This class only exists to avoid a circular reference back to the Athena.Foundation.SitecoreExtensions project
	/// </summary>
	public static class ItemHelper
	{
		public static bool IsDerived(Item item, ID templateId)
		{
			if (item == null)
			{
				return false;
			}
			return !templateId.IsNull && IsDerived(item, item.Database.Templates[templateId]);
		}

		public static bool IsDerived(Item item, Item templateItem)
		{
			if (item == null)
			{
				return false;
			}
			if (templateItem == null)
			{
				return false;
			}
			var itemTemplate = TemplateManager.GetTemplate(item);
			return itemTemplate != null && (itemTemplate.ID == templateItem.ID || itemTemplate.DescendsFrom(templateItem.ID));
		}
	}
}