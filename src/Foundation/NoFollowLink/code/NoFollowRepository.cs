﻿using System;
using System.Collections.Generic;
using System.Linq;
using Athena.Foundation.NoFollowLink.Interfaces;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Athena.Foundation.NoFollowLink
{
	public class NoFollowRepository : INoFollowRepository
	{
		private readonly INoFollowCache _cache;

		public NoFollowRepository(INoFollowCache cache)
		{
			_cache = cache ?? throw new ArgumentNullException(nameof(cache));
		}

		public IEnumerable<ID> GetNoFollowMediaItems()
		{
			var noFollowMediaItems = _cache.GetOrAddNoFollowMediaItems(GetSiteName(),
				() => GetNoFollowMediaItemFromStorage()
				.ToList());

			return noFollowMediaItems;
		}

		public IEnumerable<ID> GetNoFollowInternalItem()
		{
			var noFollowInternalItems = _cache.GetOrAddNoFollowInternalItems(GetSiteName(),
				() => GetNoFollowInternalItemsFromStorage()
				.ToList());

			return noFollowInternalItems;
		}

		public IEnumerable<string> GetNoFollowDomains()
		{
			var noFollowDomains = _cache.GetOrAddNoFollowDomains(GetSiteName(),
				() => GetNoFollowDomainsFromStorage()
				.ToList());

			return noFollowDomains;
		}

		private IEnumerable<ID> GetNoFollowMediaItemFromStorage()
		{
			return GetNoFollowItemsFromStorage(Templates.NoFollowLink.Fields.ExcludedMediaItems);
		}

		private IEnumerable<ID> GetNoFollowInternalItemsFromStorage()
		{
			return GetNoFollowItemsFromStorage(Templates.NoFollowLink.Fields.ExcludedInternalPages);
		}

		private IEnumerable<ID> GetNoFollowItemsFromStorage(ID noFollowTemplateField)
		{
			var siteRootItem = GetSiteRootItem();

			var allNoFollowInternalItems = new List<ID>();

			if (!ItemHelper.IsDerived(siteRootItem, Templates.NoFollowLink.TemplateId))
				return allNoFollowInternalItems;

			MultilistField excludedInternalLinks = siteRootItem.Fields[noFollowTemplateField];
			var internalLinkIdList = excludedInternalLinks?.TargetIDs?.ToList();
			if (internalLinkIdList != null && internalLinkIdList.Any())
			{
				allNoFollowInternalItems = GetAllItemsAndChildren(siteRootItem, internalLinkIdList).Select(x => x.ItemId)
					.ToList();
			}
			return allNoFollowInternalItems;
		}

		private IEnumerable<SearchResultItem> GetAllItemsAndChildren(Item siteRootItem, IEnumerable<ID> idsToMatch)
		{
			var indexableItem = new SitecoreIndexableItem(siteRootItem);
			var index = ContentSearchManager.GetIndex(indexableItem);
			var query = PredicateBuilder.False<SearchResultItem>();

			foreach (var id in idsToMatch)
			{
				query = query.Or(i => i.ItemId == id || i.Paths.Contains(id));
			}

			using (var context = index.CreateSearchContext())
			{
				var result = context.GetQueryable<SearchResultItem>().Where(query);
				return result;
			}
		}

		private IEnumerable<string> GetNoFollowDomainsFromStorage()
		{
			var siteRootItem = GetSiteRootItem();

			if (!ItemHelper.IsDerived(siteRootItem, Templates.NoFollowLink.TemplateId))
			{
				return new List<string>();
			}

			var domains = new List<string>();
			MultilistField excludedDomainItems = siteRootItem.Fields[Templates.NoFollowLink.Fields.ExcludedDomainItems];
			var domainList = excludedDomainItems?.TargetIDs?.ToList();
			if (domainList != null)
			{
				domains.AddRange(domainList.Select(domainId => siteRootItem.Database.GetItem(domainId))
					.Where(domainItem => domainItem != null)
					.Select(domainItem => domainItem[Templates.ExternalDomain.Fields.DomainUrl]));
			}
			return domains;
		}

		private Item GetSiteRootItem() => Context.Database.GetItem(Context.Site.RootPath);

		private static string GetSiteName() => Context.Site.Name;
	}
}