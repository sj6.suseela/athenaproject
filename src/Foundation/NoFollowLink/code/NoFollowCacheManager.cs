﻿using Athena.Foundation.NoFollowLink.Interfaces;
using Sitecore.Caching;

namespace Athena.Foundation.NoFollowLink
{
	public class NoFollowCacheManager
	{
		private static INoFollowCache _noFollowCache;

		public static INoFollowCache GetNoFollowCache()
		{
			if (_noFollowCache != null)
			{
				return _noFollowCache;
			}

			const string name = "NoFollowCache";

			var cache = CacheManager.FindCacheByName<string>(name);

			if (cache != null)
				return (INoFollowCache)cache;

			var size = Sitecore.Configuration.Settings.Caching.MediumCacheSize;
			_noFollowCache = new NoFollowCache(name, size);
			return _noFollowCache;
		}
	}
}