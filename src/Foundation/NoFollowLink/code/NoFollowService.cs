﻿using System;
using System.Linq;
using Athena.Foundation.NoFollowLink.Interfaces;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.StringExtensions;

namespace Athena.Foundation.NoFollowLink
{
	/// <summary>
	/// At a site level content editors can configure internal pages, media items or external domains that should have "nofollow" added to any inbound links.  This service tells consumers if items are no follow items. 
	/// </summary>
	public class NoFollowService : INoFollowService
	{
		private readonly NoFollowRepository _repository;

		public string NoFollowTag => "nofollow";

		public NoFollowService(NoFollowRepository repository)
		{
			_repository = repository;
		}

		public string AddNoFollowTag(LinkField linkField)
		{
			if (linkField == null)
				return string.Empty;

			if (linkField.IsInternal)
			{
				if (IsANoFollowInternalItem(linkField.TargetItem))
					return NoFollowTag;
			}
			if (linkField.IsMediaLink)
			{
				if (IsANoFollowMediaItem(linkField.TargetItem))
					return NoFollowTag;
			}
			if (linkField.TargetItem == null && !linkField.GetFriendlyUrl().IsNullOrEmpty())
			{
				if (IsANoFollowExternalDomain(linkField.GetFriendlyUrl()))
					return NoFollowTag;
			}
			return string.Empty;
		}

		public bool IsANoFollowItem(Item item)
		{
			if (item == null)
				return false;

			return IsANoFollowInternalItem(item) || IsANoFollowMediaItem(item);
		}

		public bool IsANoFollowMediaItem(Item item)
		{
			if (item == null)
				return false;

			var noFollowMediaItems = _repository.GetNoFollowMediaItems();

			return noFollowMediaItems != null && noFollowMediaItems.Any(x => x == item.ID);
		}

		private bool IsANoFollowInternalItem(Item item)
		{
			if (item == null)
				return false;

			var noFollowInternalItem = _repository.GetNoFollowInternalItem();

			return noFollowInternalItem != null && noFollowInternalItem.Any(x => x == item.ID);
		}

		private bool IsANoFollowExternalDomain(string url)
		{
			if (url.IsNullOrEmpty() && url.Trim('/').IsNullOrEmpty())
				return false;

			var noFollowDomains = _repository.GetNoFollowDomains();

			return noFollowDomains != null &&
			       noFollowDomains.Any(x => url.Trim('/').ToLower().Contains(x.Trim('/').ToLower()));
		}
	}
}