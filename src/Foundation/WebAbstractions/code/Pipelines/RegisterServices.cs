﻿using System.Web;
using Athena.Foundation.DependencyInjection.Pipelines;
using Athena.Foundation.WebAbstractions.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Athena.Foundation.WebAbstractions.Pipelines
{
	public class RegisterServices
	{
		public void Process(InitializeDependencyInjectionArgs args)
		{
			args.ServiceCollection.AddSingleton<ISitecoreContextService, SitecoreContextService>();
		}
	}
}