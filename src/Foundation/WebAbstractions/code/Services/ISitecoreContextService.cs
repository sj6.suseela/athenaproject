﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Mvc.Presentation;

namespace Athena.Foundation.WebAbstractions.Services
{
	public interface ISitecoreContextService
	{
		RenderingContext GetCurrentRenderingContext();
		string GetSiteName();
		Database GetContextDatabase();

		/// <summary>
		///     Gets the Item based on the current context database
		/// </summary>
		/// <returns></returns>
		Item GetItem(ID itemId);

		Item GetItem(ID itemId, Version version, Language language = null);

		bool IsPageEditing();
		Item GetSiteItem();
		Item GetItem(string path);
	}
}