﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Mvc.Presentation;

namespace Athena.Foundation.WebAbstractions.Services
{
	/// <remarks>
	/// Registered as Singleton - do not store state
	/// </remarks>
	public class SitecoreContextService : ISitecoreContextService
	{
		public RenderingContext GetCurrentRenderingContext()
		{
			return RenderingContext.Current;
		}

		public string GetSiteName()
		{
			return Context.Site.Name;
		}

		public Item GetSiteItem()
		{
			return GetContextDatabase().GetItem(Context.Site.RootPath);
		}

		public Item GetItem(string path)
		{
			return GetContextDatabase().GetItem(path);
		}

		public Database GetContextDatabase()
		{
			
			return Context.ContentDatabase ?? Context.Database;
		}

		public Item GetItem(ID itemId)
		{
			return GetContextDatabase().GetItem(itemId);
		}

		public Item GetItem(ID itemId,  Version version, Language language = null)
		{
			return GetContextDatabase().GetItem(itemId, language ?? Language.Current, version);
		}

		public bool IsPageEditing()
		{
			return Context.PageMode.IsExperienceEditor;
		}
	}
}