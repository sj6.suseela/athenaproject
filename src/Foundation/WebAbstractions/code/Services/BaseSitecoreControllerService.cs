﻿//using Athena.Foundation.Theme;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace Athena.Foundation.WebAbstractions.Services
{
	public abstract class BaseSitecoreControllerService
	{
		protected readonly ISitecoreContextService SitecoreContextService;

		protected BaseSitecoreControllerService(ISitecoreContextService sitecoreContextService)
		{
			SitecoreContextService = sitecoreContextService;
		}

		protected Item GetDatasourceItem()
		{
			return SitecoreContextService.GetCurrentRenderingContext().Rendering?.Item;
		}

		protected Item GetPageItem()
		{
			return SitecoreContextService.GetCurrentRenderingContext().PageContext.Item;
		}

		//protected Theme.Theme GetTheme()
		//{
		//	return null;  //RenderingContext.Current.GetTheme<Theme.Theme>(SitecoreContextService.GetContextDatabase());
		//}

	}
}