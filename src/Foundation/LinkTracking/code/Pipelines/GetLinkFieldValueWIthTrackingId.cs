﻿using Athena.Foundation.LinkTracking.Extensions;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Pipelines.RenderField;

namespace Athena.Foundation.LinkTracking.Pipelines
{
	public class GetLinkFieldValueWIthTrackingId : Foundation.ExperienceEditor.Fields.GetLinkFieldValue
	{
		public new void Process(RenderFieldArgs args)
		{
			base.Process(args);

			if (args.FieldTypeKey != "general link" || string.IsNullOrEmpty(args.FieldValue) ||
			    Context.PageMode.IsExperienceEditor)
			{
				return;
			}

			LinkField linkField = args.GetField();
			var id = args.Item.GetTrackingId(linkField);

			if (args.Result.FirstPart.StartsWith("<a"))
			{
				args.Result.FirstPart = args.Result.FirstPart.Insert(2, $" id=\"{id}\"");
			}
		}
	}
}