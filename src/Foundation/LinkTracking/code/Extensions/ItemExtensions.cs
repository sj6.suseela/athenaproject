﻿
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;

namespace Athena.Foundation.LinkTracking.Extensions
{
	public static class ItemExtensions
	{
		public static string GetTrackingId(this Item item, string url = null)
		{
			return item == null ? null : $"{item.ID.ToShortID()}{url ?? item.Uri.Path}";
		}

		public static string GetTrackingId(this Item item, ID fieldId)
		{
			LinkField linkField = item?.Fields[fieldId];
			return GetTrackingId(item, linkField);
		}

		public static string GetTrackingId(this Item item, LinkField linkField)
		{
			return item == null || linkField == null ? null : GetTrackingId(item.ID.ToShortID().ToString(), linkField.InnerField.ID.ToShortID().ToString(), linkField.GetFriendlyUrl());
		}

		public static string GetTrackingId(string itemShortId, string innerFieldShortId, string friendlyUrl)
		{
			return $"{itemShortId}-{innerFieldShortId}{friendlyUrl}";
		}
	}
}