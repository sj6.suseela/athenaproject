﻿using System;

namespace Athena.Foundation.DependencyInjection
{
    public static class Container
    {
        private static SimpleInjector.Container _config = new SimpleInjector.Container();

        public static void SetContainer(SimpleInjector.Container container)
        {
            _config = container;
        }

        public static SimpleInjector.Container CreateContainer()
        {
            _config = new SimpleInjector.Container();
            return _config;
        }

        public static T Resolve<T>() where T : class
        {
            return _config.GetInstance<T>();
        }

        public static object Resolve(Type type)
        {
            return _config.GetInstance(type);
        }
    }
}