﻿using Microsoft.Extensions.DependencyInjection;
using Sitecore.Pipelines;

namespace Athena.Foundation.DependencyInjection.Pipelines
{
    public class InitializeDependencyInjectionArgs : PipelineArgs
    {
        public IServiceCollection ServiceCollection { get; set; }

        public InitializeDependencyInjectionArgs(IServiceCollection serviceCollection)
        {
            this.ServiceCollection = serviceCollection;
        }
    }
}