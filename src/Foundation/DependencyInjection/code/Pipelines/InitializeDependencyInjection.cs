﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;

namespace Athena.Foundation.DependencyInjection.Pipelines
{
    public class InitializeDependencyInjection
    {
        public void Process(PipelineArgs args)
        {
            Log.Info("Start dependency injection initialization", this);

            var serviceCollection = new ServiceCollection();

            var container = new SimpleInjector.Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            // start the pipeline to register all dependencies
            var dependencyInjectionArgs = new InitializeDependencyInjectionArgs(serviceCollection);
            CorePipeline.Run("initializeDependencyInjection", dependencyInjectionArgs);
            var containerCache = new List<Type>();

            foreach (var serviceDescriptor in dependencyInjectionArgs.ServiceCollection)
            {
                // Safety check so we don't try to register the same type twice
                if (containerCache.Contains(serviceDescriptor.ServiceType))
                    continue;
                Lifestyle siScope = Lifestyle.Scoped;

                switch (serviceDescriptor.Lifetime)
                {
                    case ServiceLifetime.Singleton:
                        siScope = Lifestyle.Singleton;
                        break;
                    case ServiceLifetime.Transient:
                        siScope = Lifestyle.Transient;
                        break;
                    case ServiceLifetime.Scoped:
                        break;
                    default:
                        siScope = Lifestyle.Scoped;
                        break;
                }

	            if (serviceDescriptor.ImplementationInstance != null)
	            {
					container.Register(serviceDescriptor.ServiceType, () => serviceDescriptor.ImplementationInstance, siScope);
				}
	            else
	            {
					container.Register(serviceDescriptor.ServiceType, serviceDescriptor.ImplementationType, siScope);
				}

                containerCache.Add(serviceDescriptor.ServiceType);
            }

            //Register Mvc controllers
            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => (a.FullName.StartsWith("Athena.Feature.") || a.FullName.StartsWith("Athena.Foundation.")) && !a.FullName.EndsWith("Testing"));

            container.RegisterMvcControllers(assemblies.ToArray());

            // Register Mvc filter providers
            container.RegisterMvcIntegratedFilterProvider();

            // Verify our registrations
            container.Verify();

            // Set the ASP.NET dependency resolver
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            Container.SetContainer(container);
        }
    }
}