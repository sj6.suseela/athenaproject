﻿using System;
using System.Web.Mvc;

namespace Athena.Foundation.DependencyInjection
{
	public class ResolveAttribute : CustomModelBinderAttribute, IModelBinder
	{
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			Type modelType = bindingContext.ModelType;
			return Container.Resolve(modelType);
		}

		public override IModelBinder GetBinder()
		{
			return this;
		}
	}
}