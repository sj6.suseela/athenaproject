﻿namespace Athena.Foundation.Multisite
{
	using Sitecore;
	using Sitecore.Data.Items;
	using Sitecore.Diagnostics;
	using Athena.Foundation.Multisite.Providers;
	using Athena.Foundation.Multisite;

	public class SiteContext
	{
		private readonly ISiteDefinitionsProvider siteDefinitionsProvider;

		public SiteContext() : this(new SiteDefinitionsProvider())
		{
		}

		public SiteContext(ISiteDefinitionsProvider siteDefinitionsProvider)
		{
			this.siteDefinitionsProvider = siteDefinitionsProvider;
		}

		public virtual SiteDefinition GetSiteDefinition([NotNull] Item item)
		{
			Assert.ArgumentNotNull(item, nameof(item));

			return this.siteDefinitionsProvider.GetContextSiteDefinition(item);
		}
	}
}