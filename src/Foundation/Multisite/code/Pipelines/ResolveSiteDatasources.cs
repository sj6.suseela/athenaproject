﻿using Athena.Foundation.Multisite.Providers;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Mvc.Pipelines.Response.GetPageRendering;
using Sitecore.Pipelines.Save;
using Sitecore.Publishing.Pipelines.PublishItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.Multisite.Pipelines
{
	public class ResolveSiteDatasources : GetPageRenderingProcessor
	{
		private readonly IDatasourceProvider _datasourceProvider;

		public ResolveSiteDatasources() : this(new DatasourceProvider())
		{
		}

		public ResolveSiteDatasources(IDatasourceProvider datasourceProvider)
		{
			this._datasourceProvider = datasourceProvider;
		}

		public override void Process(GetPageRenderingArgs args)
		{
			foreach (var rendering in args.PageDefinition.Renderings
				.Where(r => !string.IsNullOrEmpty(r.DataSource)))
			{
				var name = DatasourceConfigurationService.GetSiteDatasourceConfigurationName(rendering.DataSource);
				if (string.IsNullOrEmpty(name))
				{
					continue;
				}

				var setter = new DatasourceSetterService(_datasourceProvider);
				var datasourceItem = setter.GetResolvedSiteDatasourceItem(args.PageContext.Item, name);
				if (datasourceItem == null)
				{
					continue;
				}
				rendering.DataSource = datasourceItem.Paths.FullPath;
			}
		}
	}
}