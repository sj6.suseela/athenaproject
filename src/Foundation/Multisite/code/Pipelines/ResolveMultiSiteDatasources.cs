﻿using System;
using System.Linq;
using Athena.Foundation.Multisite.Commands;
using Athena.Foundation.Multisite.Providers;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Mvc.Pipelines.Response.GetPageRendering;
using static Athena.Foundation.Multisite.Providers.DatasourceConfigurationService;

namespace Athena.Foundation.Multisite.Pipelines
{
	public class ResolveMultiSiteDatasources : GetPageRenderingProcessor
	{
		private readonly IDatasourceProvider _datasourceProvider;

		public ResolveMultiSiteDatasources() : this(new DatasourceProvider())
		{

		}

		public ResolveMultiSiteDatasources(IDatasourceProvider datasourceProvider)
		{
			_datasourceProvider = datasourceProvider;
		}

		public override void Process(GetPageRenderingArgs args)
		{
			foreach (var rendering in args.PageDefinition.Renderings
				.Where(r => string.IsNullOrEmpty(r.DataSource)))
			{
				var datasourceItem = GetDefaultDatasourceForRendering(rendering.RenderingItem, args.PageContext.Item);
				if (datasourceItem == null)
				{
					continue;
				}
				rendering.DataSource = datasourceItem.Paths.FullPath;
			}
		}

		private Item GetDefaultDatasourceForRendering(RenderingItem renderingItem, Item pageContextItem)
		{
			var settingName = GetDatasourceSettingName(renderingItem);
			if (settingName == null)
			{
				return null;
			}

			var sourceSettingItem = SiteSettingsProvider().GetSetting(pageContextItem, DatasourceProvider.DatasourceSettingsName, settingName);

			if (sourceSettingItem == null)
			{
				return null;
			}

			var itemIdOrPath = sourceSettingItem[Templates.DatasourceConfiguration.Fields.DatasourceDefault];

			var item = _datasourceProvider.GetDatasourceLocationsFromPath(pageContextItem, itemIdOrPath).FirstOrDefault();

			return item;
		}

		private static ISiteSettingsProvider SiteSettingsProvider()
		{
			return new SiteSettingsProvider();
		}

		private string GetDatasourceSettingName(RenderingItem renderingItem)
		{
			if (renderingItem == null)
			{
				return null;
			}

			var locationValue = renderingItem.InnerItem[CreateRenderingSettings.DatasourceLocationFieldName];
			return IsSiteDatasourceLocation(locationValue) ? GetSiteDatasourceConfigurationName(locationValue) : null;
		}

	}
}