﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Athena.Foundation.Multisite.Pipelines
{
using System;
using System.Linq;
using Athena.Foundation.Multisite.Providers;
using Athena.Foundation.Multisite.Utils;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Diagnostics;
using Sitecore.Layouts;
using Sitecore.Pipelines.ItemProvider.AddFromTemplate;

	public class SetSiteDatasourcesOnAddFromTemplate : AddFromTemplateProcessor
	{
		private readonly IDatasourceProvider provider;

		public SetSiteDatasourcesOnAddFromTemplate() : this(new DatasourceProvider())
		{
		}

		public SetSiteDatasourcesOnAddFromTemplate(IDatasourceProvider provider)
		{
			this.provider = provider;
		}


		public override void Process(AddFromTemplateArgs args)
		{
			// this is managed in configuration (runIfAborted=true would have to be set to override the value)
			if (args.Aborted)
			{
				return;
			}

			Assert.IsNotNull(args.FallbackProvider, $"{nameof(args.FallbackProvider)} is null");

			try
			{
				var item = args.FallbackProvider.AddFromTemplate(args.ItemName, args.TemplateId, args.Destination, args.NewId);
				if (item == null)
				{
					return;
				}

				// skip if item is being added into branches folder
				if (!item.Paths.IsContentItem)
					return;

				var setter = new DatasourceSetterService(provider);
				setter.SetSiteDatasources(item);

				args.ProcessorItem = args.Result = item;
			}
			catch (Exception ex)
			{
				Log.Error($"{nameof(SetSiteDatasourcesOnAddFromTemplate)} failed. Removing partially created item if it exists.",
					ex, this);

				var item = args.Destination.Database.GetItem(args.NewId);
				item?.Delete();

				throw;
			}
		}
	}
}