﻿using System;
using Sitecore;
using Sitecore.Data.Items;

namespace Athena.Foundation.Multisite.Providers
{
	public class DatasourceProvider : IDatasourceProvider
	{
		private readonly ISiteSettingsProvider _siteSettingsProvider;
		public const string DatasourceSettingsName = "datasources";
		private const string QueryPrefix = "query:";

		public DatasourceProvider() : this(new SiteSettingsProvider())
		{
		}

		public DatasourceProvider(ISiteSettingsProvider siteSettingsProvider)
		{
			_siteSettingsProvider = siteSettingsProvider;
		}

		public Item[] GetDatasourceLocations(Item contextItem, string datasourceName)
		{
			var sourceSettingItem = _siteSettingsProvider.GetSetting(contextItem, DatasourceSettingsName, datasourceName);
			var datasourceRoot = sourceSettingItem?[Templates.DatasourceConfiguration.Fields.DatasourceLocation];

			return GetDatasourceLocationsFromPath(contextItem, datasourceRoot);
		}

		public Item[] GetDatasourceLocationsFromPath(Item contextItem, string path)
		{
			if (string.IsNullOrEmpty(path))
				return new Item[] { };

			if (path.StartsWith(QueryPrefix, StringComparison.InvariantCulture))
			{
				return GetRootsFromQuery(contextItem, path.Substring(QueryPrefix.Length));
			}
			if (path.StartsWith("./", StringComparison.InvariantCulture))
			{
				return GetRelativeRoots(contextItem, path);
			}

			var sourceRootItem = contextItem.Database.GetItem(path);
			return sourceRootItem != null ? new[] { sourceRootItem } : new Item[] { };
		}

		private static Item[] GetRelativeRoots(Item contextItem, string relativePath)
		{
			var path = contextItem.Paths.FullPath + relativePath.Remove(0, 1);
			var root = contextItem.Database.GetItem(path);
			return root != null ? new[] { root } : new Item[] { };
		}

		private Item[] GetRootsFromQuery([NotNull] Item contextItem, string query)
		{
			if (contextItem == null)
				throw new ArgumentNullException(nameof(contextItem));

			var roots = query.StartsWith("./", StringComparison.InvariantCulture)
				? contextItem.Axes.SelectItems(query)
				: contextItem.Database.SelectItems(query);
			return roots ?? new Item[0];
		}

		public Item GetDatasourceTemplate(Item contextItem, string settingName)
		{
			var settingItem = _siteSettingsProvider.GetSetting(contextItem, DatasourceSettingsName, settingName);
			var templateId = settingItem?[Templates.DatasourceConfiguration.Fields.DatasourceTemplate];

			return string.IsNullOrEmpty(templateId) ? null : contextItem.Database.GetItem(templateId);
		}
	}
}