﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Athena.Foundation.Multisite.Utils;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Data.Items;

namespace Athena.Foundation.Multisite.Providers
{
	public class DatasourceSetterService
	{
		private readonly IDatasourceProvider provider;

		public DatasourceSetterService() : this(new DatasourceProvider())
		{
			
		}

		public DatasourceSetterService(IDatasourceProvider provider)
		{
			this.provider = provider;
		}

		public void SetSiteDatasources(Item item)
		{
			// check the item's layout renderings for datasources that match the multisite filter rules e.g. "site:header"
			LayoutUtils.ApplyActionToAllSharedRenderings(item, definition =>
			{
				var source = definition.Datasource;
				if (string.IsNullOrEmpty(source))
					return;

				var name = DatasourceConfigurationService.GetSiteDatasourceConfigurationName(source);
				if (string.IsNullOrEmpty(name))
				{
					return;
				}

				var datasourceItem = GetResolvedSiteDatasourceItem(item, name);
				string datasource = datasourceItem?.ID?.ToString();
				
				// always set this, to clear out the 'site:[source]' string, as this will break displaying the rendering.
				// if proper datasource cannot be resolved it should be set to null
				definition.Datasource = datasource;
			});
		}

		public Item GetResolvedSiteDatasourceItem(Item item, string name)
		{
			Item result = null;

			var datasourceLocations = this.provider.GetDatasourceLocations(item, name);
			if (datasourceLocations.Any())
			{
				var datasourceItem = datasourceLocations[0];

				var datasourceTemplate = this.provider.GetDatasourceTemplate(item, name);
				if (datasourceTemplate != null)
				{
					if (IsMatchingTemplate(datasourceItem, datasourceTemplate))
					{
						result = datasourceItem;
					}
				}
				else
				{
					result = datasourceItem;
				}
			}

			return result;
		}

		private static bool IsMatchingTemplate(Item datasourceItem, Item datasourceTemplate)
		{
			if (datasourceItem.IsDerived(datasourceTemplate.ID))
				return true;

			if (!datasourceTemplate.IsDerived(TemplateIDs.BranchTemplate) || !datasourceTemplate.HasChildren)
				return false;

			return datasourceItem.IsDerived(datasourceTemplate.Children[0].TemplateID);
		}
	}
}