﻿namespace Athena.Foundation.Multisite.Providers
{
	using Sitecore.Data;
	using Sitecore.Data.Items;

	public interface IDatasourceProvider
	{
		Item[] GetDatasourceLocations(Item contextItem, string name);
		Item[] GetDatasourceLocationsFromPath(Item contextItem, string path);

		Item GetDatasourceTemplate(Item contextItem, string name);
	}
}