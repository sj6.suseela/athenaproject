﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using Sitecore.Layouts;

namespace Athena.Foundation.LocalDatasource.Validators
{
	[Serializable]
	public class ErrantLocalDatasourceValidator : StandardValidator
	{
		public override string Name => "Local Datasources";

		public ErrantLocalDatasourceValidator()
		{
		}

		public ErrantLocalDatasourceValidator(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}


		protected override ValidatorResult Evaluate()
		{
			var item = GetItem();
			if (item == null)
			{
				return ValidatorResult.Valid;
			}

			var dataSourceItems = GetDataSourceItems(item);

			var errantLocalDatasources = ErrantLocalDatasources(dataSourceItems, item).ToArray();

			if (!errantLocalDatasources.Any())
			{
				return ValidatorResult.Valid;
			}

			return SetFailed(errantLocalDatasources);
		}

		private ValidatorResult SetFailed(Item[] errantLocalDatasources)
		{
			Text = GetText("This item contains foreign local datasources");
			foreach (var ds in errantLocalDatasources)
			{
				Errors.Add($"Datasource: {ds.Name} - {ds.ID} {ds.Paths.ContentPath}");
			}
			return GetFailedResult(ValidatorResult.Error);
		}

		private static IEnumerable<Item> ErrantLocalDatasources(List<Item> dataSourceItems, Item item)
		{
			return dataSourceItems.Where(datasource => datasource.IsLocalDatasourceItem())
				.Where(datasource => item.ID != datasource.Parent?.Parent?.ID);
		}

		public static RenderingReference[] GetRenderingReferences(Item i)
		{
			if (i == null)
			{
				return new RenderingReference[0];
			}

			return i.Visualization.GetRenderings(Context.Device, false);
		}

		public static List<Item> GetDataSourceItems(Item i)
		{
			var list = new List<Item>();
			foreach (var reference in GetRenderingReferences(i))
			{
				var dataSourceItem = GetDataSourceItem(reference);
				if (dataSourceItem != null)
				{
					list.Add(dataSourceItem);
				}
			}

			return list;
		}

		public static Item GetDataSourceItem(RenderingReference reference)
		{
			if (reference == null)
			{
				return null;
			}

			var id = reference.Settings.DataSource;
			var db = reference.Database;

			Guid itemId;
			if (Guid.TryParse(id, out itemId))
			{
				return db.GetItem(new ID(itemId));
			}

			return db.GetItem(id);
		}

		protected override ValidatorResult GetMaxValidatorResult()
		{
			return GetFailedResult(ValidatorResult.Error);
		}
	}
}