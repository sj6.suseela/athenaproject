﻿using Sitecore.Data;

namespace Athena.Foundation.LocalDatasource
{
	public struct Templates
	{
		public struct LocalContent
		{
			public static readonly ID TemplateID = new ID("{FFF5F245-FFC0-4022-A998-9B07AA5E761F}");
		}

		public struct RenderingOptions
		{
			public static ID ID = new ID("{D1592226-3898-4CE2-B190-090FD5F84A4C}");

			public struct Fields
			{
				public static readonly ID SupportsLocalDatasource = new ID("{1C307764-806C-42F0-B7CE-FC173AC8372B}");
			}
		}
	}
}