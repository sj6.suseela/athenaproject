﻿using System;
using System.Linq;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace Athena.Foundation.LocalDatasource
{
	public static class ItemExtensions
	{
		public static bool HasLocalDatasourceFolder([NotNull] this Item item)
		{
			if (item == null)
			{
				throw new ArgumentNullException(nameof(item));
			}

			return item.Children.Any(x => x.TemplateID == Templates.LocalContent.TemplateID);
		}

		public static Item GetLocalDatasourceFolder([NotNull] this Item item)
		{
			if (item == null)
			{
				throw new ArgumentNullException(nameof(item));
			}

			return item.Children.SingleOrDefault(x => x.TemplateID == Templates.LocalContent.TemplateID);
		}

		public static Item[] GetLocalDatasourceDependencies(this Item item)
		{
			if (!item.HasLocalDatasourceFolder())
			{
				return new Item[]
				{
				};
			}

			var itemLinks = Globals.LinkDatabase.GetReferences(item).Where(r =>
				(r.SourceFieldID == FieldIDs.LayoutField || r.SourceFieldID == FieldIDs.FinalLayoutField) &&
				r.TargetDatabaseName == item.Database.Name);
			return itemLinks.Select(l => l.GetTargetItem()).Where(i => i != null && i.IsLocalDatasourceItem(item)).Distinct()
				.ToArray();
		}

		public static bool IsLocalDatasourceItem([NotNull] this Item dataSourceItem, Item ofItem)
		{
			if (dataSourceItem == null)
			{
				throw new ArgumentNullException(nameof(dataSourceItem));
			}

			var datasourceFolder = ofItem.GetLocalDatasourceFolder();
			return datasourceFolder != null && dataSourceItem.Axes.IsDescendantOf(datasourceFolder);
		}

		public static bool IsLocalDatasourceItem([NotNull] this Item dataSourceItem)
		{
			if (dataSourceItem == null)
			{
				throw new ArgumentNullException(nameof(dataSourceItem));
			}

			return dataSourceItem.Parent?.TemplateID.Equals(ID.Parse(Templates.LocalContent.TemplateID)) ?? false;
		}

		public static Item GetParentLocalDatasourceFolder([NotNull] this Item dataSourceItem)
		{
			if (dataSourceItem == null)
			{
				throw new ArgumentNullException(nameof(dataSourceItem));
			}

			var template = dataSourceItem.Database.GetTemplate(Templates.LocalContent.TemplateID);
			if (template == null)
			{
				Log.Warn($"Cannot find the local datasource folder template '{Templates.LocalContent.TemplateID}'", dataSourceItem);
				return null;
			}

			return dataSourceItem.Axes.GetAncestors().LastOrDefault(i => i.IsDerived(template.ID));
		}
	}
}