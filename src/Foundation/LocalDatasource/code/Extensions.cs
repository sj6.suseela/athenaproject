﻿using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Athena.Foundation.LocalDatasource
{
	public static class Extensions
	{
		public static Item LocalContent(this Item item)
		{
			return item.GetChildren().SingleOrDefault(x => x.TemplateID == Templates.LocalContent.TemplateID);
		}

		public static bool ShouldIndexField(this Field field)
		{
			var result = !field.Name.StartsWith("__") && IsRichTextField(field) && !string.IsNullOrEmpty(field.Value);

			if (result)
			{
				var indexable = new SitecoreIndexableItem(field.Item);
				var index = ContentSearchManager.GetIndex(indexable);

				if (index?.Configuration?.DocumentOptions?.ExcludedFields != null)
				{
					result = !index.Configuration.DocumentOptions.ExcludedFields.Contains(field.ID.ToString());
				}
			}

			return result;
		}

		public static bool IsTextField(this Field field)
		{
			return IndexOperationsHelper.IsTextField((SitecoreItemDataField)field);
		}

		public static bool IsRichTextField(this Field field)
		{
			return field != null && field.TypeKey == "rich text";
		}
	}
}