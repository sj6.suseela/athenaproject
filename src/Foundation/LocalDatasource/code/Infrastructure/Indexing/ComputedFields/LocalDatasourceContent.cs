﻿using System.Linq;
using System.Text;
using HtmlAgilityPack;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;

namespace Athena.Foundation.LocalDatasource.Infrastructure.Indexing.ComputedFields
{
	public class LocalDatasourceContent : IComputedIndexField
	{
		public virtual string FieldName { get; set; }
		public virtual string ReturnType { get; set; }

		public virtual object ComputeFieldValue(IIndexable indexable)
		{
			var item = (Item) (indexable as SitecoreIndexableItem);
			if (item == null)
			{
				return null;
			}

			//if (item.ShouldIgnoreItem())
			//{
			//	return null;
			//}

			var dataSources = item.GetLocalDatasourceDependencies();

			var result = new StringBuilder();
			foreach (var dataSource in dataSources)
			{
				dataSource.Fields.ReadAll();
				foreach (var field in dataSource.Fields.Where(field => field.ShouldIndexField()))
				{
					result.AppendLine(field.Value);
				}
			}

			var html = result.ToString();
			if (html.HasNoValue())
			{
				return string.Empty;
			}

			return StripHtml(html);
		}

		private static object StripHtml(string html)
		{
			var doc = new HtmlDocument();

			doc.LoadHtml(html);
			return doc.DocumentNode.InnerText;
		}
	}
}