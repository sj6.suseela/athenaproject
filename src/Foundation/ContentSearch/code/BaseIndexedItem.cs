﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Converters;
using Sitecore.Data;
using System.Collections.Generic;
using System.ComponentModel;

namespace Athena.Foundation.ContentSearch.Extensions
{
	public abstract class BaseIndexedItem
	{
		[IndexField("_latestversion")]
		public bool IsLatestVersion { get; set; }

		[IndexField("_uniqueid")]
		public virtual string UniqueId { get; set; }

		[IndexField("_group")]
		[TypeConverter(typeof(IndexFieldIDValueConverter))]
		public virtual ID ItemId { get; set; }

		[IndexField("_language")]
		public virtual string Language { get; set; }

		[IndexField("_path")]
		[TypeConverter(typeof(IndexFieldEnumerableConverter))]
		public virtual IEnumerable<ID> Paths { get; set; }

		[IndexField("all_templates")]
		[TypeConverter(typeof(IndexFieldEnumerableConverter))]
		public virtual IEnumerable<ID> Templates { get; set; }

		[IndexField("search_heading")]
		public virtual string Heading { get; set; }

		[IndexField(SearchFields.LocalDatasourceContent)]
		public string LocalDatasourceContent { get; set; }

		[IndexField("show_in_search")]
		public bool ShowInSearch { get; set; }

	}
}