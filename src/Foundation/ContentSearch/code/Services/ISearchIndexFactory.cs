﻿using Sitecore.ContentSearch;
using Sitecore.Data.Items;

namespace Athena.Foundation.ContentSearch.Services
{
	public interface ISearchIndexFactory
	{
		IProviderSearchContext GetSearchContext(Item item = null);
		IProviderSearchContext GetSearchContext(string indexName, Item item = null);
	}
}