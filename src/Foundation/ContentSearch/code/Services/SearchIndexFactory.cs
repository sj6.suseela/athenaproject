﻿using System;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Athena.Foundation.WebAbstractions.Services;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.Data.Items;

namespace Athena.Foundation.ContentSearch.Services
{
	/// <remarks>
	/// Registered as Singleton - do not store state
	/// </remarks>
	public class SearchIndexFactory : ISearchIndexFactory
	{
		private readonly ISitecoreContextService _contextService;

		public SearchIndexFactory(ISitecoreContextService contextService)
		{
			_contextService = contextService;
		}

		public IProviderSearchContext GetSearchContext(Item item = null)
		{
			item = item ?? Context.Item ?? _contextService.GetContextDatabase().GetItem(Context.Site.StartPath) ?? _contextService.GetContextDatabase().GetItem(Context.Site.RootPath);
			var indexable = new SitecoreIndexableItem(item);
			return ContentSearchManager.GetIndex(indexable).CreateSearchContext();
		}

		public IProviderSearchContext GetSearchContext(string indexName, Item item = null)
		{
			if (indexName.HasNoValue())
				throw new ArgumentException($"{nameof(indexName)} cannot be null or empty");

			if (!indexName.Contains("{0}"))
				return ContentSearchManager.GetIndex(indexName).CreateSearchContext();
			

			var database = item?.Database ?? Context.Item?.Database ?? _contextService.GetContextDatabase();
			indexName = string.Format(indexName, database.Name.ToLower());

			return ContentSearchManager.GetIndex(indexName).CreateSearchContext();
		}
	}
}