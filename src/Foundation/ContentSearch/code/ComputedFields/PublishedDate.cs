﻿using System;
using System.Collections.Generic;
using Athena.Foundation.Common;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Athena.Foundation.ContentSearch.ComputedFields
{
	/// <summary>
    /// The all templates computed field.
    /// </summary>
    public class PublishedDate : IComputedIndexField
    {
        /// <summary>
        /// Gets or sets the field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the return type.
        /// </summary>
        public string ReturnType { get; set; }

        /// <summary>
        /// The compute field value.
        /// </summary>
        /// <param name="indexable">
        /// The indexable.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
	        if (!(indexable is SitecoreIndexableItem indexItem))
            {
                return null;
            }

            var item = indexItem.Item;
	        if (item.IsDerived(Common.Templates.PublishingStatistics.TemplateId))
	        {
				DateField itemField = item.Fields[Common.Templates.PublishingStatistics.Fields.FixedPublishedDateOverride];
				if (itemField.DateTime == DateTime.MinValue)
				{
					itemField = item.Fields[Common.Templates.PublishingStatistics.Fields.Published];
				}
		        return itemField.DateTime;
	        }

	        return null;
        }


    }
}