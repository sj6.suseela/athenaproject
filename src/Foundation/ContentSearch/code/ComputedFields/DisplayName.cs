﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
//using Template = Athena.Foundation.HeaderBanner.Template;

namespace Athena.Foundation.ContentSearch.ComputedFields
{
	public class DisplayName : IComputedIndexField
	{
		public virtual string FieldName { get; set; }
		public virtual string ReturnType { get; set; }

		public virtual object ComputeFieldValue(IIndexable indexable)
		{
			var item = (Item) (indexable as SitecoreIndexableItem);
			if (item == null)
			{
				return null;
			}

			// Get the heading if there is one
			var itemField = item.Fields["sample page"]; //item.Fields[Template.BasePage.FieldIDs.PageTitle];
			if (itemField != null && itemField.HasValue)
			{
				return itemField.Value;
			}

			return item.DisplayName;
		}
	}
}