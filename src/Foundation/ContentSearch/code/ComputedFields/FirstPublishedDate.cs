﻿using Athena.Foundation.Common;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Fields;

namespace Athena.Foundation.ContentSearch.ComputedFields
{
	public class FirstPublishedDate : IComputedIndexField
	{
		/// <summary>
		/// Gets or sets the field name.
		/// </summary>
		public string FieldName { get; set; }

		/// <summary>
		/// Gets or sets the return type.
		/// </summary>
		public string ReturnType { get; set; }

		/// <summary>
		/// The compute field value.
		/// </summary>
		/// <param name="indexable">
		/// The indexable.
		/// </param>
		/// <returns>
		/// The <see cref="object"/>.
		/// </returns>
		public object ComputeFieldValue(IIndexable indexable)
		{
			if (!(indexable is SitecoreIndexableItem indexItem))
			{
				return null;
			}

			var item = indexItem.Item;
			if (item.IsDerived(Templates.FirstPublishingStatistics.TemplateId))
			{
				DateField itemField = item.Fields[Templates.FirstPublishingStatistics.Fields.FirstPublished];
					return itemField.DateTime;
			}
			return null;
		}
	}
}