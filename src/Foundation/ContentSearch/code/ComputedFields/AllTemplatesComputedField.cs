﻿using System.Collections.Generic;
using Athena.Foundation.ContentSearch.Extensions;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data.Items;

namespace Athena.Foundation.ContentSearch.ComputedFields
{
	/// <summary>
    /// The all templates computed field.
    /// </summary>
    public class AllTemplatesComputedField : IComputedIndexField
    {
        /// <summary>
        /// Gets or sets the field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the return type.
        /// </summary>
        public string ReturnType { get; set; }

        /// <summary>
        /// The compute field value.
        /// </summary>
        /// <param name="indexable">
        /// The indexable.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
	        if (!(indexable is SitecoreIndexableItem indexItem))
            {
                return null;
            }

			var item = indexItem.Item;
	        if (item.ShouldIgnoreItem())
	        {
		        return null;
	        }

            var templates = new List<string>(10);
            GetAllTemplates(item.Template, templates);
            return templates;
        }

        /// <summary>
        /// The get all templates.
        /// </summary>
        /// <param name="baseTemplate">
        /// The base template.
        /// </param>
        /// <param name="list">
        /// The list.
        /// </param>
        private void GetAllTemplates(TemplateItem baseTemplate, List<string> list)
        {
            var str = IdHelper.NormalizeGuid(baseTemplate.ID);

            if (list.Contains(str))
            {
                return;
            }

            list.Add(str);

            if (baseTemplate.ID == TemplateIDs.StandardTemplate)
            {
                return;
            }

            foreach (var item in baseTemplate.BaseTemplates)
            {
                GetAllTemplates(item, list);
            }
        }
    }
}