﻿using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Data.Items;

namespace Athena.Foundation.ContentSearch.Extensions
{
	public static class ItemExtensions
	{
		/// <summary>
		/// Ignore item based whether it has a layout or is under the template folder
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public static bool ShouldIgnoreItem(this Item item)
		{
			return !item.HasLayout() || item.Paths.LongID.Contains(ItemIDs.TemplateRoot.ToString());
		}
	}
}