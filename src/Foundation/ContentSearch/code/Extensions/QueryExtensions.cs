﻿using Athena.Foundation.ContentSearch.Highlight;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System.Linq;
using Sitecore.ContentSearch.Linq;

namespace Athena.Foundation.ContentSearch.Extensions
{
	/// <summary>
	///     The query extensions.
	/// </summary>
	public static class QueryExtensions
	{
		public static IQueryable<T> IsCurrentLanguage<T>(this IQueryable<T> queryable) where T : BaseIndexedItem
		{
			return queryable.IsLanguage(Context.Language);
		}

		public static IQueryable<T> IsLanguage<T>(this IQueryable<T> queryable, Language language) where T : BaseIndexedItem
		{
			return queryable.Where(item => item.Language == language.Name);
		}
		public static IQueryable<T> IsTemplate<T>(this IQueryable<T> queryable, ID templateId) where T : BaseIndexedItem
		{
			return queryable.Where(item => item.Templates.Contains(templateId));
		}

		public static IQueryable<T> IsLatestVersion<T>(this IQueryable<T> queryable) where T : BaseIndexedItem
		{
			return queryable.Where(item => item.IsLatestVersion);
		}

		public static IQueryable<T> SetRootItem<T>(this IQueryable<T> queryable, ID startItemId) where T : BaseIndexedItem
		{
			return queryable.Where(arg => arg.Paths.Contains(startItemId));
		}

		public static IQueryable<T> InitialiseQuery<T>(this IProviderSearchContext providerSearchContext)
			where T : BaseIndexedItem
		{
			return
				providerSearchContext
					.GetExtendedQueryable<T>()
					.IsCurrentLanguage()
					.IsLatestVersion();
		}

		/// <summary>
		///     ShowInSearch is set to true <see cref="I_SearchPresentation" />
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="queryable"></param>
		/// <returns></returns>
		public static IQueryable<T> WhereShowInSearch<T>(this IQueryable<T> queryable) where T : BaseIndexedItem
		{
			return queryable.Where(arg => arg.ShowInSearch);
		}

		public static IQueryable<T> WhereNotBlacklisted<T>(this IQueryable<T> queryable, Item[] blacklistedPages) where T : BaseIndexedItem
		{
			if (blacklistedPages == null)
				return queryable;

			var idList = blacklistedPages.Select(x => x.ID);

			foreach(ID id in idList)
			{
				queryable = queryable.Where(arg => arg.ItemId != id);
			}

			return queryable;
		}
	}
}