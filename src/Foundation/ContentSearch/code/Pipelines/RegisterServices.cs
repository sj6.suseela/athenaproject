﻿using Athena.Foundation.ContentSearch.Services;
using Athena.Foundation.DependencyInjection.Pipelines;
using Microsoft.Extensions.DependencyInjection;

namespace Athena.Foundation.ContentSearch.Pipelines
{
	public class RegisterServices
	{
		public void Process(InitializeDependencyInjectionArgs args)
		{
			args.ServiceCollection.AddSingleton<ISearchIndexFactory, SearchIndexFactory>();
			

		}

	}
}