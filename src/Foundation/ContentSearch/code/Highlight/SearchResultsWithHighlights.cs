﻿using System.Collections.Generic;
using Sitecore.ContentSearch.Linq;
using Sitecore.Diagnostics;
using SolrNet.Impl;

namespace Athena.Foundation.ContentSearch.Highlight
{
	/// <summary>
	///	Allows Highlighting on the Sitecore ContentSearch  using Solr search
	/// https://github.com/vasiliyfomichev/Sitecore-Solr-Search-Term-Highlight
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class SearchResultsWithHighlights<T>
	{
		public SearchResultsWithHighlights(SearchResults<T> result, IDictionary<string, HighlightedSnippets> highlights)
		{
			Assert.ArgumentNotNull(result, "result");
			Results = result;
			Highlights = highlights ?? new Dictionary<string, HighlightedSnippets>();
		}

		public SearchResults<T> Results { get; }
		public IDictionary<string, HighlightedSnippets> Highlights { get; }
	}
}