﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Abstractions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.ContentSearch.Linq.Methods;
using Sitecore.ContentSearch.Pipelines.IndexingFilters;
using Sitecore.ContentSearch.Security;
using Sitecore.ContentSearch.SolrProvider;
using SolrNet;

namespace Athena.Foundation.ContentSearch.Highlight
{
	/// <summary>
	///     Allows Highlighting on the Sitecore ContentSearch  using Solr search
	///     https://github.com/vasiliyfomichev/Sitecore-Solr-Search-Term-Highlight
	/// </summary>
	public struct SolrSearchResults<TElement>
	{
		private readonly SolrSearchContext _context;

		private readonly IEnumerable<IExecutionContext> _executionContexts;

		private readonly IIndexDocumentPropertyMapper<Dictionary<string, object>> _mapper;

		private readonly int _numberFound;

		private readonly SolrQueryResults<Dictionary<string, object>> _searchResults;

		private readonly SelectMethod _selectMethod;

		private readonly SolrIndexConfiguration _solrIndexConfiguration;

		private readonly IEnumerable<IFieldQueryTranslator> _virtualFieldProcessors;

		public SolrSearchResults(SolrSearchContext context, SolrQueryResults<Dictionary<string, object>> searchResults,
			SelectMethod selectMethod, IEnumerable<IExecutionContext> executionContexts,
			IEnumerable<IFieldQueryTranslator> virtualFieldProcessors)
		{
			_context = context;
			_solrIndexConfiguration = (SolrIndexConfiguration) _context.Index.Configuration;
			_selectMethod = selectMethod;
			_virtualFieldProcessors = virtualFieldProcessors;
			_executionContexts = executionContexts;
			_numberFound = searchResults.NumFound;
			_searchResults = ApplySecurity(searchResults, context.SecurityOptions,
				context.Index.Locator.GetInstance<ICorePipeline>(), context.Index.Locator.GetInstance<IAccessRight>(),
				ref _numberFound);

			var executionContext = _executionContexts != null
				? _executionContexts.FirstOrDefault(c =>
						c is OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>>) as
					OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>>
				: null;
			_mapper = (executionContext != null ? executionContext.OverrideObject : null) ??
			          _solrIndexConfiguration.IndexDocumentPropertyMapper;
		}

		public int NumberFound => _numberFound;

		private static SolrQueryResults<Dictionary<string, object>> ApplySecurity(
			SolrQueryResults<Dictionary<string, object>> solrQueryResults, SearchSecurityOptions options,
			ICorePipeline pipeline,
			IAccessRight accessRight, ref int numberFound)
		{
			if (!options.HasFlag(SearchSecurityOptions.DisableSecurityCheck))
			{
				var removalList = new HashSet<Dictionary<string, object>>();

				foreach (var searchResult in solrQueryResults.Where(searchResult => searchResult != null))
				{
					object secToken;
					object dataSource;

					if (!searchResult.TryGetValue(BuiltinFields.UniqueId, out secToken))
					{
						continue;
					}

					searchResult.TryGetValue(BuiltinFields.DataSource, out dataSource);

					var isExcluded = OutboundIndexFilterPipeline.CheckItemSecurity(pipeline, accessRight,
						new OutboundIndexFilterArgs((string) secToken, (string) dataSource));

					if (isExcluded)
					{
						removalList.Add(searchResult);
						numberFound = numberFound - 1;
					}
				}

				foreach (var item in removalList)
				{
					solrQueryResults.Remove(item);
				}
			}

			return solrQueryResults;
		}

		public TElement ElementAt(int index)
		{
			if (index < 0 || index > _searchResults.Count)
			{
				throw new IndexOutOfRangeException();
			}

			return _mapper.MapToType<TElement>(_searchResults[index], _selectMethod, _virtualFieldProcessors,
				_executionContexts,
				_context.SecurityOptions);
		}

		public TElement ElementAtOrDefault(int index)
		{
			if (index < 0 || index > _searchResults.Count)
			{
				return default(TElement);
			}

			return _mapper.MapToType<TElement>(_searchResults[index], _selectMethod, _virtualFieldProcessors,
				_executionContexts,
				_context.SecurityOptions);
		}

		public bool Any()
		{
			return _numberFound > 0;
		}

		public long Count()
		{
			return _numberFound;
		}

		public TElement First()
		{
			if (_searchResults.Count < 1)
			{
				throw new InvalidOperationException("Sequence contains no elements");
			}

			return ElementAt(0);
		}

		public TElement FirstOrDefault()
		{
			if (_searchResults.Count < 1)
			{
				return default(TElement);
			}

			return ElementAt(0);
		}

		public TElement Last()
		{
			if (_searchResults.Count < 1)
			{
				throw new InvalidOperationException("Sequence contains no elements");
			}

			return ElementAt(_searchResults.Count - 1);
		}

		public TElement LastOrDefault()
		{
			if (_searchResults.Count < 1)
			{
				return default(TElement);
			}

			return ElementAt(_searchResults.Count - 1);
		}

		public TElement Single()
		{
			if (Count() < 1)
			{
				throw new InvalidOperationException("Sequence contains no elements");
			}

			if (Count() > 1)
			{
				throw new InvalidOperationException("Sequence contains more than one element");
			}

			return _mapper.MapToType<TElement>(_searchResults[0], _selectMethod, _virtualFieldProcessors,
				_executionContexts,
				_context.SecurityOptions);
		}

		public TElement SingleOrDefault()
		{
			if (Count() == 0)
			{
				return default(TElement);
			}

			if (Count() == 1)
			{
				return _mapper.MapToType<TElement>(_searchResults[0], _selectMethod, _virtualFieldProcessors,
					_context.SecurityOptions);
			}

			throw new InvalidOperationException("Sequence contains more than one element");
		}

		public IEnumerable<SearchHit<TElement>> GetSearchHits()
		{
			foreach (var searchResult in _searchResults)
			{
				float score = -1;

				object scoreObj;

				if (searchResult.TryGetValue("score", out scoreObj))
				{
					if (scoreObj is float)
					{
						score = (float) scoreObj;
					}
				}

				yield return new SearchHit<TElement>(score,
					_mapper.MapToType<TElement>(searchResult, _selectMethod, _virtualFieldProcessors,
						_executionContexts,
						_context.SecurityOptions));
			}
		}

		public IEnumerable<TElement> GetSearchResults()
		{
			foreach (var searchResult in _searchResults)
			{
				yield return _mapper.MapToType<TElement>(searchResult, _selectMethod, _virtualFieldProcessors,
					_executionContexts,
					_context.SecurityOptions);
			}
		}

		public Dictionary<string, ICollection<KeyValuePair<string, int>>> GetFacets()
		{
			var facetFields = _searchResults.FacetFields;
			var pivotFacets = _searchResults.FacetPivots;

			var finalresults = facetFields.ToDictionary(x => x.Key, x => x.Value);

			if (pivotFacets.Count > 0)
			{
				foreach (var pivotFacet in pivotFacets)
				{
					finalresults[pivotFacet.Key] = Flatten(pivotFacet.Value, string.Empty);
				}
			}

			return finalresults;
		}


		private ICollection<KeyValuePair<string, int>> Flatten(IEnumerable<Pivot> pivots, string parentName)
		{
			var keys = new HashSet<KeyValuePair<string, int>>();

			foreach (var pivot in pivots)
			{
				if (parentName != string.Empty)
				{
					keys.Add(new KeyValuePair<string, int>(parentName + "/" + pivot.Value, pivot.Count));
				}

				if (pivot.HasChildPivots)
				{
					keys.UnionWith(Flatten(pivot.ChildPivots, pivot.Value));
				}
			}

			return keys;
		}
	}
}