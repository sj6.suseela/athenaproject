﻿using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Methods;
using Sitecore.ContentSearch.Linq.Solr;

namespace Athena.Foundation.ContentSearch.Highlight
{
	/// <summary>
	///	Allows Highlighting on the Sitecore ContentSearch  using Solr search
	/// https://github.com/vasiliyfomichev/Sitecore-Solr-Search-Term-Highlight
	/// </summary>
	public class SolrCompositeQueryWithHighlights : SolrCompositeQuery
	{
		public SolrCompositeQueryWithHighlights(SolrCompositeQuery query, string[] translatedFieldNames,
			GetResultsOptions options = GetResultsOptions.Default)
			: base(query.Query, query.Filter, query.Methods, query.VirtualFieldProcessors, query.FacetQueries,
				query.ExecutionContexts)
		{
			HighlightFields = translatedFieldNames;
			Methods.Insert(0, new GetResultsMethod(options));
		}

		// Can contain extra parameters. For the example, field names are stored only
		public string[] HighlightFields { get; }
		public string BeforeTerm { get; set; }
		public string AfterTerm { get; set; }
		public bool? HighlightMultiTerm { get; set; }

		/// <summary>
		///     Fragment size to be highlighted
		/// </summary>
		public int? FragSize { get; set; }
	}
}