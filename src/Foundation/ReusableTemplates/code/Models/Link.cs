﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.ReusableTemplates.Models
{
    public class Link
    {
		public string Url { get; set; }
		public string Label { get; set; }
		public string Target { get; set; }
	}
}