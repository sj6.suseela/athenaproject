﻿using System;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Athena.Foundation.ReusableTemplates.Models;
using Athena.Foundation.Elements.Models;
using Athena.Foundation.Elements.Extensions;

namespace Athena.Foundation.ReusableTemplates
{
    public static class ItemExtensions
    {
        public static readonly ID ValueField = Templates.TextLookupEntry.Fields.Value;

        /// <summary>
        /// Use with "Text ReusableTemplates Entry" - {63332D49-B05C-4D27-820E-2BE79D0FA5FF}
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public static string GetLookupValue(this Item item, ID fieldId)
        {
            var lookupId = item.Fields[fieldId]?.Value;
            if (!ID.IsID(lookupId))
            {
                return String.Empty;
            }
            var lookupItem = item.Database.GetItem(new ID(lookupId));
            var lookupItemField = lookupItem?.Fields[ValueField];
            return lookupItemField == null ? string.Empty : lookupItemField.Value;
        }

        public static Title GetTitleWithHtmlTag(this Item item)
        {
            var model = new Title();
            if (item == null)
                return model;

            model.Label = item.RenderField(Templates.Title.Fields.Title);
            model.HtmlTag = item.GetLookupValue(Templates.TitleHTMLTag.Fields.TitleHTMLTag);

            LinkField linkField = item.Fields[Templates.Title.Fields.TitleLink];
            model.Url = item.GetLinkElement(Templates.Title.Fields.TitleLink).Url;
            model.Target = item.GetLinkElement(Templates.Title.Fields.TitleLink).Target;

            return model;
        }

        public static CTA GetCTAWithType(this Item item)
        {
            var model = new CTA();
            if (item == null)
                return model;

            model.Label = item.RenderField(Templates.CTA.Fields.CTAText);
            model.Type = item.GetLookupValue(Templates.CTAType.Fields.CTAType);
            model.Url = item.GetLinkElement(Templates.CTA.Fields.CTALink).Url;
            model.Target = item.GetLinkElement(Templates.CTA.Fields.CTALink).Target;

            return model;
        }

        public static Link GetLinkWithText(this Item item)
        {
            var model = new Link();
            if (item == null)
                return model;

            model.Label = item.RenderField(Templates.Link.Fields.LinkText);
            model.Url = item.GetLinkElement(Templates.Link.Fields.Link).Url;
            model.Target = item.GetLinkElement(Templates.Link.Fields.Link).Target;

            return model;
        }
    }
}