﻿









// ReSharper disable InconsistentNaming
namespace Athena.Foundation.ReusableTemplates
{
    public static partial class Templates
    {
        #region /sitecore/templates/Foundation/ReusableTemplates/_CTA
        /// <summary>
        ///   _CTA
        ///   <para>ID: {37938E22-A4A5-4C84-80D8-9E5D0EC683ED}</para>
        ///   <para>Path: /sitecore/templates/Foundation/ReusableTemplates/_CTA</para>
        /// </summary>
        public static class CTA
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{37938E22-A4A5-4C84-80D8-9E5D0EC683ED}"));
            public const string IdString = "{37938E22-A4A5-4C84-80D8-9E5D0EC683ED}";

            public static class Fields
            {
                /// <summary>
                ///   CTA Link
                ///   <para>{B258070D-2FE3-4E29-B6AF-B85732221793}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTALink = new Sitecore.Data.ID("{B258070D-2FE3-4E29-B6AF-B85732221793}");

                /// <summary>
                ///   CTA Link
                ///   <para>{B258070D-2FE3-4E29-B6AF-B85732221793}</para>
                /// </summary>
                public const string CTALink_FieldName = "CTA Link";

                /// <summary>
                ///   CTA Text
                ///   <para>{1FDD9A47-0B6A-4ABD-9311-EF13EE34D559}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTAText = new Sitecore.Data.ID("{1FDD9A47-0B6A-4ABD-9311-EF13EE34D559}");

                /// <summary>
                ///   CTA Text
                ///   <para>{1FDD9A47-0B6A-4ABD-9311-EF13EE34D559}</para>
                /// </summary>
                public const string CTAText_FieldName = "CTA Text";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/ReusableTemplates/_Link
        /// <summary>
        ///   _Link
        ///   <para>ID: {A6F76933-9863-415B-B837-5128257D2D2B}</para>
        ///   <para>Path: /sitecore/templates/Foundation/ReusableTemplates/_Link</para>
        /// </summary>
        public static class Link
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{A6F76933-9863-415B-B837-5128257D2D2B}"));
            public const string IdString = "{A6F76933-9863-415B-B837-5128257D2D2B}";

            public static class Fields
            {
                /// <summary>
                ///   Link Text
                ///   <para>{6AA43BF3-9954-49F1-92B7-0F01F7BECF1B}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID LinkText = new Sitecore.Data.ID("{6AA43BF3-9954-49F1-92B7-0F01F7BECF1B}");

                /// <summary>
                ///   Link Text
                ///   <para>{6AA43BF3-9954-49F1-92B7-0F01F7BECF1B}</para>
                /// </summary>
                public const string LinkText_FieldName = "Link Text";

                /// <summary>
                ///   Link
                ///   <para>{F3CAE8D0-E9D1-49B7-A397-6DFBA413B316}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Link = new Sitecore.Data.ID("{F3CAE8D0-E9D1-49B7-A397-6DFBA413B316}");

                /// <summary>
                ///   Link
                ///   <para>{F3CAE8D0-E9D1-49B7-A397-6DFBA413B316}</para>
                /// </summary>
                public const string Link_FieldName = "Link";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/ReusableTemplates/_TextLookupEntry
        /// <summary>
        ///   _TextLookupEntry
        ///   <para>ID: {408C5101-F5EC-4ED2-B445-E403FAEA7919}</para>
        ///   <para>Path: /sitecore/templates/Foundation/ReusableTemplates/_TextLookupEntry</para>
        /// </summary>
        public static class TextLookupEntry
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{408C5101-F5EC-4ED2-B445-E403FAEA7919}"));
            public const string IdString = "{408C5101-F5EC-4ED2-B445-E403FAEA7919}";

            public static class Fields
            {
                /// <summary>
                ///   Value
                ///   <para>{100AA20B-5FE2-4638-803D-813DEC3B0137}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Value = new Sitecore.Data.ID("{100AA20B-5FE2-4638-803D-813DEC3B0137}");

                /// <summary>
                ///   Value
                ///   <para>{100AA20B-5FE2-4638-803D-813DEC3B0137}</para>
                /// </summary>
                public const string Value_FieldName = "Value";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/ReusableTemplates/_Title
        /// <summary>
        ///   _Title
        ///   <para>ID: {6B5E41C6-65AF-4A65-94A2-0FA8F4C2F741}</para>
        ///   <para>Path: /sitecore/templates/Foundation/ReusableTemplates/_Title</para>
        /// </summary>
        public static class Title
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{6B5E41C6-65AF-4A65-94A2-0FA8F4C2F741}"));
            public const string IdString = "{6B5E41C6-65AF-4A65-94A2-0FA8F4C2F741}";

            public static class Fields
            {
                /// <summary>
                ///   Title Link
                ///   <para>{03809667-F098-41E1-8077-9BC604E73790}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID TitleLink = new Sitecore.Data.ID("{03809667-F098-41E1-8077-9BC604E73790}");

                /// <summary>
                ///   Title Link
                ///   <para>{03809667-F098-41E1-8077-9BC604E73790}</para>
                /// </summary>
                public const string TitleLink_FieldName = "Title Link";

                /// <summary>
                ///   Title
                ///   <para>{17D111A6-9AC7-4036-A8C5-B0E56E740B11}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Title = new Sitecore.Data.ID("{17D111A6-9AC7-4036-A8C5-B0E56E740B11}");

                /// <summary>
                ///   Title
                ///   <para>{17D111A6-9AC7-4036-A8C5-B0E56E740B11}</para>
                /// </summary>
                public const string Title_FieldName = "Title";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/ReusableTemplates/DropLinks/_CTAType
        /// <summary>
        ///   _CTAType
        ///   <para>ID: {0920BD3C-7EAE-4ECA-AEB1-BE500DD07734}</para>
        ///   <para>Path: /sitecore/templates/Foundation/ReusableTemplates/DropLinks/_CTAType</para>
        /// </summary>
        public static class CTAType
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{0920BD3C-7EAE-4ECA-AEB1-BE500DD07734}"));
            public const string IdString = "{0920BD3C-7EAE-4ECA-AEB1-BE500DD07734}";

            public static class Fields
            {
                /// <summary>
                ///   CTA Type
                ///   <para>{E1BBBE49-509B-4D09-8017-28C19A630E37}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID CTAType = new Sitecore.Data.ID("{E1BBBE49-509B-4D09-8017-28C19A630E37}");

                /// <summary>
                ///   CTA Type
                ///   <para>{E1BBBE49-509B-4D09-8017-28C19A630E37}</para>
                /// </summary>
                public const string CTAType_FieldName = "CTA Type";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/ReusableTemplates/DropLinks/_ImageAllignment
        /// <summary>
        ///   _ImageAllignment
        ///   <para>ID: {275DE08E-FA00-416F-8041-6F19C4A8EF13}</para>
        ///   <para>Path: /sitecore/templates/Foundation/ReusableTemplates/DropLinks/_ImageAllignment</para>
        /// </summary>
        public static class ImageAllignment
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{275DE08E-FA00-416F-8041-6F19C4A8EF13}"));
            public const string IdString = "{275DE08E-FA00-416F-8041-6F19C4A8EF13}";

            public static class Fields
            {
                /// <summary>
                ///   Allignment
                ///   <para>{5CE69147-EED1-4B3E-AAFE-F3E333B76169}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Allignment = new Sitecore.Data.ID("{5CE69147-EED1-4B3E-AAFE-F3E333B76169}");

                /// <summary>
                ///   Allignment
                ///   <para>{5CE69147-EED1-4B3E-AAFE-F3E333B76169}</para>
                /// </summary>
                public const string Allignment_FieldName = "Allignment";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/ReusableTemplates/DropLinks/_TextAllignment
        /// <summary>
        ///   _TextAllignment
        ///   <para>ID: {7D7E568D-917F-4AD9-BF02-DC2CF13D8261}</para>
        ///   <para>Path: /sitecore/templates/Foundation/ReusableTemplates/DropLinks/_TextAllignment</para>
        /// </summary>
        public static class TextAllignment
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{7D7E568D-917F-4AD9-BF02-DC2CF13D8261}"));
            public const string IdString = "{7D7E568D-917F-4AD9-BF02-DC2CF13D8261}";

            public static class Fields
            {
                /// <summary>
                ///   Allignment
                ///   <para>{C07B95D4-7F0D-48E3-A9D3-6F32B3C1E9A6}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID Allignment = new Sitecore.Data.ID("{C07B95D4-7F0D-48E3-A9D3-6F32B3C1E9A6}");

                /// <summary>
                ///   Allignment
                ///   <para>{C07B95D4-7F0D-48E3-A9D3-6F32B3C1E9A6}</para>
                /// </summary>
                public const string Allignment_FieldName = "Allignment";

            }
        }
        #endregion
        #region /sitecore/templates/Foundation/ReusableTemplates/DropLinks/_TitleHTMLTag
        /// <summary>
        ///   _TitleHTMLTag
        ///   <para>ID: {3FC86EF8-BD0E-46A3-BAB0-014491505E9C}</para>
        ///   <para>Path: /sitecore/templates/Foundation/ReusableTemplates/DropLinks/_TitleHTMLTag</para>
        /// </summary>
        public static class TitleHTMLTag
        {
            public static readonly Sitecore.Data.TemplateID ID = new Sitecore.Data.TemplateID(new Sitecore.Data.ID("{3FC86EF8-BD0E-46A3-BAB0-014491505E9C}"));
            public const string IdString = "{3FC86EF8-BD0E-46A3-BAB0-014491505E9C}";

            public static class Fields
            {
                /// <summary>
                ///   Title HTML Tag
                ///   <para>{764BEF05-9F15-4922-92A7-DCF236774531}</para>
                /// </summary>
                public static readonly Sitecore.Data.ID TitleHTMLTag = new Sitecore.Data.ID("{764BEF05-9F15-4922-92A7-DCF236774531}");

                /// <summary>
                ///   Title HTML Tag
                ///   <para>{764BEF05-9F15-4922-92A7-DCF236774531}</para>
                /// </summary>
                public const string TitleHTMLTag_FieldName = "Title HTML Tag";

            }
        }
        #endregion
    }
}


