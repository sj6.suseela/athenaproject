﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;

namespace Athena.Foundation.Common
{
	public static class AppSettings
	{
		public static string Get(string key)
		{
			var appSetting = ConfigurationManager.AppSettings[key];
			return appSetting;
		}

		public static T Get<T>(string key)
		{
			var appSetting = ConfigurationManager.AppSettings[key];
			if (string.IsNullOrWhiteSpace(appSetting)) throw new AppSettingNotFoundException(key);

			var converter = TypeDescriptor.GetConverter(typeof(T));
			return (T)converter.ConvertFromInvariantString(appSetting);
		}

		public static T Get<T>(string key, T defaultValue)
		{
			var appSetting = ConfigurationManager.AppSettings[key];
			if (string.IsNullOrWhiteSpace(appSetting))
				return defaultValue;

			var converter = TypeDescriptor.GetConverter(typeof(T));
			return (T)converter.ConvertFromInvariantString(appSetting);
		}

		public static IEnumerable<T> GetAll<T>(string keyStartsWith)
		{
			var converter = TypeDescriptor.GetConverter(typeof(T));

			return GetAll(keyStartsWith)
				.Select(setting => (T)converter.ConvertFromInvariantString(setting));
		}

		public static IEnumerable<string> GetAll(string keyStartsWith)
		{
			return ConfigurationManager.AppSettings.AllKeys
				.Where(key => key.StartsWith(keyStartsWith))
				.Select(key => ConfigurationManager.AppSettings[key]);
		}
	}
}