﻿using System;
using System.Configuration;

namespace Athena.Foundation.Common
{
	public static class EnvironmentHelper
	{
		static EnvironmentHelper()
		{
		}

		private static Environment GetEnvironment()
		{
			var environmentName = ConfigurationManager.AppSettings["Environment"];
			if (!string.IsNullOrEmpty(environmentName))
			{
				foreach (Environment env in Enum.GetValues(typeof(Environment)))
				{
					var environmentMatches = environmentName.ToLowerInvariant()
						.Contains(env.ToString().ToLowerInvariant());

					if (environmentMatches)
					{
						return env;
					}
				}
			}

			return Environment.Unknown;
		}

		public static Environment Current { get; } = GetEnvironment();

		public static bool IsLocal()
		{
			return Current == Environment.Local;
		} 

		public static bool IsProduction()
		{
			return Current == Environment.Prod;
		}
	}
}