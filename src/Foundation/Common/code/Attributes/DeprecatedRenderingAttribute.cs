﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Athena.Foundation.Common.Attributes
{
	public class DeprecatedRenderingAttribute : ActionFilterAttribute
	{
		public DeprecatedRenderingAttribute()
		{
		}

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			if (Sitecore.Context.PageMode.IsExperienceEditor)
			{
				filterContext.Result = new ContentResult()
				{
					Content = "<div>Warning: This rendering has been deprecated.Please remove this rendering from the page.</div>"
				};
			}
			else
			{
				filterContext.Result = new EmptyResult();
			}


			base.OnActionExecuting(filterContext);
		}
	}
}