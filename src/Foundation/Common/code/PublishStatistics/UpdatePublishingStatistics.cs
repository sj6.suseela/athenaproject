﻿using System;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Publishing.Pipelines.PublishItem;
using Sitecore.SecurityModel;

namespace Athena.Foundation.Common.PublishStatistics
{
	public class UpdatePublishingStatistics : PublishItemProcessor
	{
		public override void Process(PublishItemContext context)
		{
			SetPublishingStatisticsFields(context);
		}

		private void SetPublishingStatisticsFields(PublishItemContext context)
		{
			Assert.ArgumentNotNull(context, "context");
			Assert.ArgumentNotNull(context.PublishOptions, "context.PublishOptions");
			Assert.ArgumentNotNull(context.PublishOptions.SourceDatabase, "context.PublishOptions.SourceDatabase");
			Assert.ArgumentNotNull(context.PublishOptions.TargetDatabase, "context.PublishOptions.TargetDatabase");
			Assert.ArgumentCondition(!ID.IsNullOrEmpty(context.ItemId), "context.ItemId", "context.ItemId must be set!");
			Assert.ArgumentNotNull(context.User, "context.User");

			SetPublishingStatisticsFields(context.PublishOptions.SourceDatabase, context.ItemId, context.User.Name);
			SetPublishingStatisticsFields(context.PublishOptions.TargetDatabase, context.ItemId, context.User.Name);
		}

		private void SetPublishingStatisticsFields(Database database, ID itemId, string userName)
		{
			Assert.ArgumentNotNull(database, "database");
			Item item = TryGetItem(database, itemId);

			if (item != null && HasPublishingStatisticsFields(item))
			{
				SetPublishingStatisticsFields(item, userName);
			}
		}

		private void SetPublishingStatisticsFields(Item item, string userName)
		{
			Assert.ArgumentNotNull(item, "item");
			Assert.ArgumentNotNullOrEmpty(userName, "userName");

			if (item.Paths.FullPath.StartsWith("/sitecore/templates/"))
			{
				return;
			}

			using (new SecurityDisabler())
			{
				item.Editing.BeginEdit();
				item.Fields[Templates.PublishingStatistics.Fields.PublishedFieldName].Value = DateUtil.IsoNow;
				item.Fields[Templates.PublishingStatistics.Fields.PublishedByFieldName].Value = userName;
				item.Editing.EndEdit();
			}
		}

		private Item TryGetItem(Database database, ID itemId)
		{
			try
			{
				return database.Items[itemId];
			}
			catch (Exception ex)
			{
				Log.Error(ToString(), ex, this);
			}

			return null;
		}

		private static bool HasPublishingStatisticsFields(Item item)
		{
			Assert.ArgumentNotNull(item, "item");
			return item.Fields[Templates.PublishingStatistics.Fields.PublishedFieldName] != null
					&& item.Fields[Templates.PublishingStatistics.Fields.PublishedByFieldName] != null;
		}
	}
}