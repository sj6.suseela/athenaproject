﻿namespace Athena.Foundation.Common
{
	public static class Api
	{
		public static string Route(string route)
		{
			return $"{ApiSettings.GlobalApiPrefix.Trim('/')}/{route}";
		}

		public static string Path(string route)
		{
			return $"/{Route(route)}";
		}
	}
}