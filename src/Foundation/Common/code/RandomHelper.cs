﻿using System;
using System.Linq;

namespace Athena.Foundation.Common
{
	public class RandomHelper
	{
		private static readonly Random Random = new Random();

		public static string RandomString(int length = 40)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			return new string(Enumerable.Repeat(chars, length)
				.Select(s => s[Random.Next(s.Length)]).ToArray());
		}
	}
}