﻿namespace Athena.Foundation.Common
{
	public enum Role
	{
		Standalone,
		ContentManagement,
		ContentDelivery
	}
}