﻿namespace Athena.Foundation.Common
{
	public enum Environment
	{
		Local,
		Int,
		QA,
		UAT,
		Prod,
		Unknown
	}
}