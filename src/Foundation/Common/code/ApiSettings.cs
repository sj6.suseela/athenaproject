﻿namespace Athena.Foundation.Common
{
	public static class ApiSettings
	{
		public static string GlobalApiPrefix = AppSettings.Get("GlobalAPIPrefix");
	}
}