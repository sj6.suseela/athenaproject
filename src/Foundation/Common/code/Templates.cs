﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;

namespace Athena.Foundation.Common
{
	public class Templates
	{
		public struct Theme
		{
			public struct FieldNames
			{
				public const string ThemeName = "Theme Name";
			}
		}

		public struct Curve
		{
			public struct FieldNames
			{
				public const string Type = "Type";
			}
		}

		public static class PublishingStatistics
		{
			public static readonly ID TemplateId = new ID("{A2DB0770-A1CA-4FEB-8618-040084113853}");

			public static class Fields
			{
				public static readonly ID Published = new ID("{3F35F8C6-9197-44F1-A4E7-719558C7FA90}");
				public static readonly ID FixedPublishedDateOverride = new ID("{46910679-8E12-460F-935E-952F6AA4B826}");
				public static readonly ID PublishedBy = new ID("{3EF7B585-830D-464E-92CE-091964663BF5}");

				public const string PublishedFieldName = "Published";
				public const string PublishedByFieldName = "Published By";
			}
		}
		public static class FirstPublishingStatistics
		{
			public static readonly ID TemplateId = new ID("{EF03650D-91C3-4222-9C03-813D9B785D78}");

			public static class Fields
			{
				public static readonly ID FirstPublished = new ID("{A1BA0C59-2061-4C0F-9F72-8917F42D0AE5}");
				public const string PublishedFieldName = "First Published";
			}
		}



		public struct TextColour
		{
			public struct FieldNames
			{
				public const string ColourName = "Colour Name";
			}
		}

		public struct ImageAlignment
		{
			public struct Fields
			{
				public static ID Alignment = new ID("{BC9145CE-90D1-47C8-9F66-0F7A6E8C7D93}");
			}

			public struct FieldNames
			{
				public const string Alignment = "Alignment";
			}
		}

		public struct LookupItem
		{
			public struct FieldNames
			{
				public const string Value = "Value";
			}
		}

		public struct FileSettings
		{
			public struct FieldIds
			{
				public static ID AcceptedFileTypes = new ID("{EE7FD2D5-C3CE-45D0-8EE5-583E283DF842}");
				public static ID MaximumFileSize = new ID("{769808E0-CCA8-4622-9092-E67E1E62B1D0}");
			}
		}

		public struct FormValidationSettings
		{
			public struct FieldIds
			{
				public static ID DefaultErrorText = new ID("{8795E94D-3D4F-4402-BCBF-CDAC7E169953}");
			}
		}

		public struct Folder
		{
			public static readonly ID TemplateId = new ID("{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}");
		}

		public struct Homepage
		{
			public static readonly ID TemplateId = new ID("{63A49596-14D5-4C17-914E-61980DEEB9A2}");
		}

		public struct SiteRoot
		{
			public static readonly ID TemplateId = new ID("{EAE882EB-1B1E-465A-A7C3-178943C0880D}");
		}

	}
}