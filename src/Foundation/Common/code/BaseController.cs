﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Athena.Foundation.Common
{
	public class BaseController : Controller
	{
		private readonly JsonSerializerSettings _serializerSettings;

		public BaseController()
		{
			_serializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
		}

		protected JsonResult JsonResponse(object value)
		{
			return Json(JsonConvert.SerializeObject(value, _serializerSettings), "application/json", JsonRequestBehavior.AllowGet);
		}

	}
}