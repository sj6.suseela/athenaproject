﻿using System;
using System.Configuration;

namespace Athena.Foundation.Common
{
	public static class RoleHelper
	{
		private static Role GetRole()
		{
			Enum.TryParse<Role>(ConfigurationManager.AppSettings["Role"], out var role);
			return role;
		}

		public static Role Current { get; } = GetRole();

		public static bool IsCd()
		{
			return Current == Role.ContentDelivery;
		}

		public static bool IsCm()
		{
			return Current == Role.ContentManagement;
		}

		public static bool IsStandalone()
		{
			return Current == Role.Standalone;
		}

	}
}