﻿using System;

namespace Athena.Foundation.Common
{
	public class AppSettingNotFoundException : Exception
	{
		public AppSettingNotFoundException(string key):base($"App setting with a key of {key} not found")
		{
			
		}
	}
}