﻿namespace Athena.Foundation.Elements.Models
{
	public class LinkElement
	{
		public string Id { get; set; }
		public string Rel { get; set; }
		public string Url { get; set; }
		public string Label { get; set; }
		public string Html { get; set; }
		public string Target { get; set; }
	}
}