﻿using Newtonsoft.Json;


namespace Athena.Foundation.Elements.Models
{
	public class AdvancedImageElement
	{
	
		[JsonProperty("alt")]
		public string AltText { get; set; }
		[JsonProperty("renditions")]
		public ImageRenditions ImageRenditions { get; set; }
		
	}

	public class ImageRenditions
	{

		[JsonProperty("0")]
		public SmallRendition SmallRendition { get; set; }
		[JsonProperty("769")]
		public LargeRendition LargeRendition { get; set; }
	}

	public class SmallRendition
	{

		[JsonProperty("1x")]
		public string regular { get; set; }

		[JsonProperty("2x")]
		public string retina { get; set; }

	}
		
	public class LargeRendition
	{
		[JsonProperty("1x")]
		public string regular { get; set; }

		[JsonProperty("2x")]
		public string retina { get; set; }

	}
}