﻿
using Sitecore.Data.Fields;

namespace Athena.Foundation.Elements.Models
{
	public class ImageElement
	{
		public string Src { get; set; }
		public string Html { get; set; }
		public ImageSize Size { get; set; }
		public string Extension { get; set; }
		public string AltText { get; set; }
	}

	public class ImageSize
	{
		protected ImageSize()
		{
		}

		public static ImageSize CreateImageSize(ImageField imageField)
		{
			if (imageField != null && int.TryParse(imageField.Width, out int width) && int.TryParse(imageField.Height, out int height))
			{
				return new ImageSize()
				{
					Height = height,
					Width = width
				};
			}
			else
			{
				return new ImageSize()
				{
					Height = 0,
					Width = 0
				};
			}
		}

		public int Height { get; set; }
		public int Width { get; set; }
	}
}