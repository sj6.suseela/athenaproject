﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Athena.Foundation.Elements.Models
{
    public class TitleElement
    {
        public string Label { get; set; }
        [JsonProperty("type")]
        public string HtmlTag { get; set; }
        public string Url { get; set; }
        public string Target { get; set; }
    }
}