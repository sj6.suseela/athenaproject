﻿using Athena.Foundation.Elements.Models;
using Athena.Foundation.LinkTracking.Extensions;
using Athena.Foundation.NoFollowLink.Extensions;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Sites;
using System;

namespace Athena.Foundation.Elements.Extensions
{
	public static class ItemExtensions
	{
		public static LinkElement GetLinkElement(this Item item, ID fieldId, bool includeRawHtml = true, string defaultHref = null, Dictionary<string, string> queryStringParameters = null)
		{
			var model = new LinkElement();
			if (item == null)
				return model;

			LinkField linkField = item.Fields[fieldId];
			if (linkField == null)
				return model;

			model.Id = item.GetTrackingId(linkField);
			var url = linkField.GetFriendlyUrl();
			model.Url = !url.HasValue() && defaultHref != null ? defaultHref : url;

			if (queryStringParameters != null && queryStringParameters.Any())
			{
				foreach(var kv in queryStringParameters)
				{
					model.Url = WebUtil.AddQueryString(model.Url, kv.Key, kv.Value);
				}
			}

			model.Rel = linkField.NoFollowTag();
			model.Label = linkField.Text;
			model.Target = !string.IsNullOrEmpty(linkField.Target) ? linkField.Target : "_self";

			if (includeRawHtml)
			{
				model.Html = item.RenderField(fieldId, url: queryStringParameters != null ? model.Url : null);
			}

			return model;
		}

		public static LinkElement GetItemLinkElement(this Item item, string hrefText, SiteContext site)
		{
			return GetLinkElement(item, true, hrefText);
		}

		public static LinkElement GetLinkElement(this Item item, bool includeRawHtml = false,  string hrefText = null)
		{
			var model = new LinkElement();
			if (item == null)
				return model;

			model.Id = item.GetTrackingId();
			model.Url = item.Url();
			model.Rel = item.AddNoFollowTag();
			if (includeRawHtml && hrefText != null)
			{
				model.Html = $"<a id=\"{model.Id}\" href=\"{model.Url}\" rel=\"{model.Rel}\">{hrefText}</a>";
			}

			return model;
		}

		public static ImageElement GetImageElement(this Item item, ID fieldId)
		{
			var model = new ImageElement();
			if (item == null)
				return model;

			ImageField imageField = item.Fields[fieldId];
			MediaItem media = imageField.MediaItem;

			if (media != null)
			{
				model.Html = item.RenderField(fieldId);
				model.Size = ImageSize.CreateImageSize(imageField);
				model.Src = imageField.ImageUrl();
				model.Extension = media?.Extension;
				model.AltText = imageField.Alt;
			}

			return model;
		}

		public static AdvancedImageElement GetAdavncedImageElement(this Item item, ID fieldId, Enum Rendition1, Enum Rendition2)
		{
			var model = new AdvancedImageElement();
			if (item == null)
			{
				return model;
			}
				
			ImageField imageField = item.Fields[fieldId];
			
			if (imageField != null)
			{
				model.AltText = imageField.Alt;
				model.ImageRenditions = new ImageRenditions
				{
					SmallRendition = new SmallRendition
					{
						regular = imageField.GetAdvancedImageUrl((int)Enum.Parse(Rendition1.GetType(), "width") / 2, (int)Enum.Parse(Rendition1.GetType(), "height") / 2),
						retina = imageField.GetAdvancedImageUrl((int)Enum.Parse(Rendition1.GetType(), "width"), (int)Enum.Parse(Rendition1.GetType(), "height"))
					},
					LargeRendition = new LargeRendition
					{
						regular = imageField.GetAdvancedImageUrl((int)Enum.Parse(Rendition2.GetType(), "width") / 2, (int)Enum.Parse(Rendition2.GetType(), "height") / 2),
						retina = imageField.GetAdvancedImageUrl((int)Enum.Parse(Rendition2.GetType(), "width"), (int)Enum.Parse(Rendition2.GetType(), "height"))
					}
				};
			};

		    return model;
		}
	}
}