﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Athena.Project.Website.Bundler;

namespace Athena.Project.Website
{
	public static class HtmlExtensions
	{
		public static HtmlString StyleSheet(this HtmlHelper helper, string styleSheetName)
		{
			return StyleSheet(helper, new[] {styleSheetName});
		}
		public static HtmlString StyleSheet(this HtmlHelper helper, IEnumerable<string> styleSheetNames)
		{
			return new HtmlString(string.Join("\n",styleSheetNames.Select(SiteBundleConfiguration.StyleSheet)));
		}

		public static HtmlString Script(this HtmlHelper helper, string scriptName, bool async = false)
		{
			return new HtmlString(SiteBundleConfiguration.Script(scriptName, async));
		}

		public static string StyleSheetHref(this HtmlHelper helper, string styleSheetName)
		{
			return SiteBundleConfiguration.StyeSheetHref(styleSheetName);
		}

		public static string LazyLoadStyleSheet(this HtmlHelper helper, string styleSheetName)
		{
			return SiteBundleConfiguration.LazyLoadStyleSheet(styleSheetName);
		}
	}
}