using Newtonsoft.Json;
using React;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Athena.Project.Website.ReactConfig), "Configure")]

namespace Athena.Project.Website
{
    public static class ReactConfig
    {
        public static void Configure()
        {
			//TODO: get the page specific js file based on template.
			ReactSiteConfiguration.Configuration
				.SetJsonSerializerSettings(new JsonSerializerSettings
				{
					ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
				})
				.AddScriptWithoutTransform($"~/dist/js/mediators/homePage.js")
				.SetUseDebugReact(true);
		}
	}
}
