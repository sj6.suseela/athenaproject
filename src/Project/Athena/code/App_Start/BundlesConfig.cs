﻿using Athena.Project.Website.Bundler;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Athena.Project.Website.BundlesConfig), "Configure")]
namespace Athena.Project.Website
{
    /// <summary>
    /// Configure front end asset bundles.  Associate front end assets with template IDs
    /// </summary>
    public static class BundlesConfig
    {
        /// <summary>
        /// References JS and CSS files based on the Template or Base Templates
        /// </summary>
        /// <remarks>
        /// <see cref="SiteBundleConfiguration.GetScriptBundles"/>
        /// </remarks>
        public static void Configure()
        {
            SiteBundleConfiguration.Configuration
                .AddGlobalBundle(new GlobalBundleItem("bundle", isPriority: true))
                .AddGlobalBundle(new GlobalBundleItem("components"));
        }
    }
}