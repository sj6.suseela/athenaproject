﻿namespace Athena.Project.Website.Bundler
{
	public class FontItem
	{
		public FontItem(string href, string site = "")
		{
			Site = site;
			Href = href;
		}

		public string Site { get; set; }
		public string Href { get; set; }
	}
}