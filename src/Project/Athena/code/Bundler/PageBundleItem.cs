﻿using System;
using System.Collections.Generic;
using Sitecore.Data;

namespace Athena.Project.Website.Bundler
{
	public class PageBundleItem : BundleItem
	{
		public ID TemplateId { get; }

		public string TemplateName { get; }

		/// <summary>
		/// Create a bundle defintions
		/// </summary>
		/// <param name="templateId">The template id is the ID of the template that the bundles should be applied to.</param>
		/// <param name="templateName">This is just a helpful name to let another dev know what template they are dealing with it is not used for any logic.</param>
		/// <param name="bundles">The js and css bundles used on the page.</param>
		public PageBundleItem(ID templateId, string templateName, List<string> bundles) : base(bundles)
		{
			TemplateId = templateId ?? throw new ArgumentNullException(nameof(templateId));
			TemplateName = templateName ?? throw new ArgumentNullException(nameof(templateName));
		}

		/// <summary>
		/// Create a bundle defintions
		/// </summary>
		/// <param name="templateId">The template id is the ID of the template that the bundles should be applied to.</param>
		/// <param name="templateName">This is just a helpful name to let another dev know what template they are dealing with it is not used for any logic.</param>
		/// <param name="bundle">The js and css bundles used on the page.</param>
		public PageBundleItem(ID templateId, string templateName, string bundle) : this(templateId, templateName, new List<string> { bundle })
		{
		}
	}
}