﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;

namespace Athena.Project.Website.Bundler
{
	public class GlobalBundleItem : BundleItem
	{
		public IEnumerable<ID> PagesToExclude { get; set; }

		public GlobalBundleItem(string bundle, string site = "", IEnumerable<ID> pagesToExclude = null, bool isPriority = false)
			: this(new List<string> { bundle }, site, pagesToExclude, isPriority)
		{
		}

		public GlobalBundleItem(List<string> bundles, string site = "", IEnumerable<ID> pagesToExclude = null, bool isPriority = false)
			: base(bundles, site, isPriority)
		{
			PagesToExclude = pagesToExclude;
		}
	}
}