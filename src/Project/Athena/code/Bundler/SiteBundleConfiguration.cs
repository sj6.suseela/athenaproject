﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using Athena.Foundation.SitecoreExtensions.Extensions;
using Athena.Foundation.Common;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace Athena.Project.Website.Bundler
{
	public class SiteBundleConfiguration : ISiteBundleConfiguration
	{
		private static readonly ConcurrentDictionary<string, List<string>> Cache = new ConcurrentDictionary<string, List<string>>();

		private readonly List<GlobalBundleItem> _globalBundles = new List<GlobalBundleItem>();
		private readonly List<FontItem> _fontItems = new List<FontItem>();
		private readonly List<PageBundleItem> _pageSpecificBundles = new List<PageBundleItem>();

		public static ISiteBundleConfiguration Configuration { get; set; }

		static SiteBundleConfiguration()
		{
			Configuration = new SiteBundleConfiguration();
		}

		public ISiteBundleConfiguration AddGlobalBundle(GlobalBundleItem bundleItem)
		{
			_globalBundles.Add(bundleItem);
			return this;
		}

		public ISiteBundleConfiguration AddPageBundle(PageBundleItem pageBundleItem)
		{
			_pageSpecificBundles.Add(pageBundleItem);
			return this;
		}

		public ISiteBundleConfiguration AddFont(FontItem fontItem)
		{
			_fontItems.Add(fontItem);
			return this;
		}

		public ISiteBundleConfiguration DisableFallbackFonts(string site)
		{
			if (site.HasNoValue())
				throw new ArgumentNullException(nameof(site));

			_fontItems.Add(new FontItem("", site));
			return this;
		}

		public List<string> GetNonPriorityBundles(Item item)
		{
			var siteName = GetSiteName();
			return GetOrAddToCache("NonPriorityBundles", siteName, item, () =>
			{
				var bundles = GetBundles(item, siteName);

				return bundles.Where(x => !x.IsPriority).SelectMany(x => x.Bundles)
					.ToList();
			});
		}

		public List<string> GetPriorityBundles(Item item)
		{
			var siteName = GetSiteName();
			return GetOrAddToCache("PriorityBundles", siteName, item, () =>
			{
				var bundles = GetBundles(item, siteName);

				return bundles.Where(x => x.IsPriority).SelectMany(x => x.Bundles)
					.ToList();
			});
		}

		public static string Script(string scriptName, bool async = false)
		{
			return $"<script src='{ScriptSrc(scriptName)}' {IsAsync(async)}></script>";
		}

		public static string ScriptSrc(string scriptName)
		{
			return $"{AssetPrefix()}/dist/{scriptName}.bundle{Minified()}.js{Version()}";
		}

		public static string StyleSheet(string styleSheetName)
		{
			return $"<link rel='stylesheet' href='{StyeSheetHref(styleSheetName)}' />";
		}

		public static string PreloadStyleSheet(string styleSheetNameHref)
		{
			return $"<link rel='preload' href='{styleSheetNameHref}' as='style' />";
		}

		public static string StyeSheetHref(string styleSheetName)
		{
			return $"{AssetPrefix()}/dist/{styleSheetName}.bundle{Minified()}.css{Version()}";
		}

		public static string StyeSheetServerHref(string styleSheetName)
		{
			return $"../dist/{styleSheetName}.bundle{Minified()}.css";
		}

		public static string LazyLoadStyleSheet(string styleSheetHref)
		{
			return $"<script>(function(){{var componentsCss = document.createElement('link'); componentsCss.type='text/css'; " +
			         $"componentsCss.rel='stylesheet'; componentsCss.href='{styleSheetHref}';" +
			         $" var s=document.getElementsByTagName('head')[0]; s.appendChild(componentsCss);}})();</script>";
		}

		public List<string> GetScriptBundles(Item item)
		{
			var siteName = GetSiteName();
			return GetOrAddToCache("Scripts", siteName, item, () =>
			{
				var bundles = GetBundles(item, siteName);

				return bundles.SelectMany(x => AssetName(x, BundleType.Javascript))
					.ToList();
			});
		}

		public List<string> GetFonts(Item item)
		{
			var siteName = GetSiteName();
			return GetOrAddToCache("Fonts", siteName, item, () =>
			{
				var fontItems = GetSiteFonts(siteName);

				return fontItems.Select(x => x.Href).ToList();
			});
		}

		private string GetSiteName()
		{
			return Sitecore.Context.GetSiteName();
		}

		public List<string> GetCssBundles(Item item)
		{
			var siteName = GetSiteName();
			return GetOrAddToCache("Styles", siteName, item, () =>
			 {
				 var bundles = GetBundles(item, siteName);

				 return bundles.SelectMany(x => AssetName(x, BundleType.CSS))
					 .ToList();
			 });
		}

		private static List<string> GetOrAddToCache(string cachePrefix, string siteName, Item item, Func<List<string>> get)
		{
			var cacheKey = CreateCacheKey(cachePrefix, siteName, item);
			List<string> temp;
			if (Cache.TryGetValue(cacheKey, out temp))
			{
				Log.Debug($"Bundles found in Cache for template with an ID of {item.TemplateID}");
				return temp;
			}

			Log.Debug($"No bundles found in cache for template with an ID of {item.TemplateID}.  Getting bundles...");

			var bundles = get();

			if (Cache.TryAdd(cacheKey, bundles))
				Log.Debug($"Added bundles to cache for template with an ID of {item.TemplateID}.");
			else
				Log.Debug($"Could not add bundles to cache for template with an ID of {item.TemplateID}.");

			return bundles;
		}

		private static string CreateCacheKey(string cachePrefix, string siteName, Item item)
		{
			return siteName.HasValue() ? $"{cachePrefix}-{siteName.ToLowerInvariant()}-{item.TemplateID}" : $"{cachePrefix}-{item.TemplateID}";
		}

		private IEnumerable<BundleItem> GetBundles(Item item, string siteName)
		{
			var pageBundles = GetPageBundles(item, siteName);
			var globalBundles = GetGlobalBundles(siteName);

			var bundles = globalBundles.Concat(pageBundles);

			return bundles;
		}

		private IEnumerable<BundleItem> GetPageBundles(Item item, string siteName)
		{
			var sitePageBundles = _pageSpecificBundles
									.Where(x => string.Equals(x.Site, siteName, StringComparison.InvariantCultureIgnoreCase))
									.Where(x => x.TemplateId == item.TemplateID || item.IsDerived(x.TemplateId))
									.ToList();

			if (!sitePageBundles.Any())
			{
				sitePageBundles = _pageSpecificBundles
										.Where(x => string.IsNullOrEmpty(x.Site))
										.Where(x => x.TemplateId == item.TemplateID || item.IsDerived(x.TemplateId)).ToList();
			}

			return sitePageBundles;
		}

		private IEnumerable<GlobalBundleItem> GetGlobalBundles(string siteName)
		{
			var siteBundles = _globalBundles.Where(x => string.Equals(x.Site, siteName, StringComparison.InvariantCultureIgnoreCase)).ToList();
			if (!siteBundles.Any())
			{
				siteBundles = _globalBundles.Where(x => string.IsNullOrEmpty(x.Site)).ToList();
			}

			return siteBundles;
		}

		private IEnumerable<FontItem> GetSiteFonts(string siteName)
		{
			var siteBundles = _fontItems.Where(x => string.Equals(x.Site, siteName, StringComparison.InvariantCultureIgnoreCase)).ToList();
			if (!siteBundles.Any())
			{
				siteBundles = _fontItems.Where(x => string.IsNullOrEmpty(x.Site)).ToList();
			}

			return siteBundles.Where(x => x.Href.HasValue()).ToList();
		}

		private static IEnumerable<string> AssetName(BundleItem bundleItem, BundleType bundleType)
		{
			if (bundleType == BundleType.Javascript)
			{
				return bundleItem.Bundles
					.Select(x => Script(x));
			}

			return bundleItem.Bundles
					.Select(StyleSheet);
		}

		private static string AssetPrefix()
		{
			return ConfigurationManager.AppSettings["StaticAssets.Prefix"];
		}

		private static string Minified()
		{
			return UseMinifiedAssets() ? ".min" : string.Empty;
		}

		private static bool UseMinifiedAssets()
		{
			var environmentsToUseMinifiedAssets = AppSettings.Get("Performance.UseMinifiedAssetsInEnvironments")
				.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries)
				.Select(x => x.ToLowerInvariant());

			var currentEnvironmentName = EnvironmentHelper.Current.ToString().ToLowerInvariant();

			return environmentsToUseMinifiedAssets.Contains(currentEnvironmentName);
		}

		private static string Version()
		{
			var versionedContentPath = $"?v={Assembly.GetAssembly(typeof(SiteBundleConfiguration)).GetName().Version}";
			return versionedContentPath;
		}

		private static string IsAsync(bool async)
		{
			return async ? "defer" : string.Empty;
		}
	}
}