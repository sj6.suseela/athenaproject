﻿namespace Athena.Project.Website.Bundler
{
	public enum BundleType
	{
		Javascript,
		CSS
	}
}