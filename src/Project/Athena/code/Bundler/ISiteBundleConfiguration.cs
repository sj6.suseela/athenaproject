﻿using System.Collections.Generic;
using Sitecore.Data.Items;

namespace Athena.Project.Website.Bundler
{
	public interface ISiteBundleConfiguration
	{
		ISiteBundleConfiguration AddGlobalBundle(GlobalBundleItem bundleItem);

		ISiteBundleConfiguration AddPageBundle(PageBundleItem pageBundleItem);

		ISiteBundleConfiguration AddFont(FontItem bundleItem);

		ISiteBundleConfiguration DisableFallbackFonts(string site);

		List<string> GetNonPriorityBundles(Item item);

		List<string> GetPriorityBundles(Item item);

		List<string> GetFonts(Item item);
	}
}