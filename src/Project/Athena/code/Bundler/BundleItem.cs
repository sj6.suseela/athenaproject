﻿using System;
using System.Collections.Generic;

namespace Athena.Project.Website.Bundler
{
	public abstract class BundleItem
	{
		public string Site { get; set; }

		public List<string> Bundles { get; }

		public bool IsPriority { get; }

		protected BundleItem(string bundle, string site = "", bool isPriority = false) : this(new List<string> { bundle }, site, isPriority)
		{ }
		
		protected BundleItem(List<string> bundles, string site = "", bool isPriority = false)
		{
			if (bundles.Count == 0)
			{
				throw new ArgumentException("Value cannot be an empty collection.", nameof(bundles));
			}

			Site = site;
			Bundles = bundles;
			IsPriority = isPriority;
		}
	}
}