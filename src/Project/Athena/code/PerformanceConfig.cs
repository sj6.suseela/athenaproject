﻿
using Athena.Foundation.Common;
using Athena.Foundation.React;

namespace Athena.Project.Website
{
	public static class PerformanceConfig
	{
		public static bool LazyLoadFonts = Athena.Foundation.Common.AppSettings.Get<bool>("Performance.LazyLoadFonts");
		public static bool LazyLoadStyles = Athena.Foundation.Common.AppSettings.Get<bool>("Performance.LazyLoadStyles");
		public static bool InlinePriorityStyles = Athena.Foundation.Common.AppSettings.Get<bool>("Performance.InlinePriorityStyles");
		public static bool LazyLoadComponents = ReactPerformanceConfig.LazyLoadComponents;
		public static bool LazyLoadImages = ReactPerformanceConfig.LazyLoadImages;
		public static bool InlineComponentHydrateEnabled = ReactPerformanceConfig.InlineComponentHydrateEnabled;
	}
}
