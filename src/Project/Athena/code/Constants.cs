﻿namespace Athena.Project.Website
{
	public struct Constants
	{
		public struct Settings
		{
			public static string ApplicationInsightsKey = "ApplicationInsights.Instrumentationkey";
		}

		public struct PageProperties
		{
			public struct Fields
			{
				public static string BodyClass = "Body Class";
			}
		}
	}
}