import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import Contact from '../Contact/Contact';
import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditButton from '../CMS/EditButton';

import './ContactNumber.scss';

/** ContactNumber
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ContactNumber, {
    id : 'id',
    theme : {},
    heading : 'Our 24 hour emergency helpline for medical emergencies and assistance',
    leftContact : {
      tel : '0800 032 2844',
      telAlt : '+44 1243 621 537',
      telAltIntro : 'From abroad:',
      opening : 'Lines open <b>24 hours</b>, 7 days a week',
      info : 'Calls may be recorded and/or monitored for training and audit purposes.'
    },
    imageRight : '<img src="dist/images/cms/flourish-insurance.svg" />'
  }), document.getElementById("element"));

  * @example
  ReactDOM.render(React.createElement(Components.ContactNumber, {
    id : 'id',
    theme : {
      themeName : 'mint',
      themeBefore : 'white',
      topCurve : { type: 'convex' },
      bottomCurve : { type: 'convex' }
    },
    leftContact : {
      title : `Medical Emergencies`,
      tel : '01234 621 537',
      telAlt : '+44 1243 621 537',
      telAltIntro : 'From abroad:',
      opening : 'Lines open <b>24 hours</b>, 7 days a week',
      info : 'Calls may be recorded and/or monitored for training and audit purposes.'
    },
    rightContact : {
      title : `All your other travel claims`,
      tel : '01234 621 537',
      telAlt : '+44 1243 621 537',
      telAltIntro : 'From abroad:',
      opening : 'Lines open <b>24 hours</b>, 7 days a week',
      info : 'Calls may be recorded and/or monitored for training and audit purposes.'
    }
  }), document.getElementById("element"));
*/
class ContactNumber extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading, content, contactLeft, imageLeft, contactRight, imageRight, theme, imageLeftEdit, imageRightEdit} = this.props;
    const baseClass = 'contact-number';

    const isAlt = (theme && theme.themeName === 'white') || (theme && theme.themeName === null);
    const hasHeader = heading || content;
    const hasTwoContactsClass = (contactLeft && contactRight) ? 'equal-heights' : '';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        {hasHeader && (
          <div className={`${baseClass}__header`}>
            <Editable tag="h2" className={`${baseClass}__title`} html={heading} />
            <EditableDiv className={`${baseClass}__content`} html={content} />
          </div>
        )}
        <div className={`columns@md ${hasTwoContactsClass} spaced`}>
          <div>
            {contactLeft && (
              <div className={`box box--${isAlt ? 'alt' : 'default'} box--sm`}>
                <Contact {...contactLeft} />
              </div>
            )}
            <EditButton isSmall={true} html={imageLeftEdit} align="right" />
            {imageLeft !== null && (
              <EditableImage className={`${baseClass}__image`} image={imageLeft.html} />
            )}
            
          </div>
          <div>
            {contactRight && (
              <div className={`box box--${isAlt ? 'alt' : 'default'} box--sm`}>
                <Contact {...contactRight} />
              </div>
            )}
            <EditButton isSmall={true} html={imageRightEdit} align="right" />
            {imageRight !== null && (
              <EditableImage className={`${baseClass}__image`} image={imageRight.html} />
            )}
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

ContactNumber.defaultProps = {
  theme : {
    themeName : 'white'
  },
  heading : '',
  content : '',
  contactLeft : null,
  imageLeft : {},
  imageLeftEdit : '',
  contactRight : null,
  imageRight : {},
  imageRightEdit : ''
};

ContactNumber.propTypes = {
  theme : PropTypes.object,
  heading : PropTypes.string,
  content : PropTypes.string,
  contactLeft : PropTypes.object,
  imageLeft : PropTypes.object,
  imageLeftEdit : PropTypes.string,
  contactRight : PropTypes.object,
  imageRight : PropTypes.object,
  imageRightEdit : PropTypes.string
};

module.exports = ContactNumber;
