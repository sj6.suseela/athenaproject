import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvChangeMobileNumber.scss';

class OpvChangeMobileNumber extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {
      title,
      description,
      back,
      phoneNumberLable,
      phoneNumber,
      formArea
    } = this.props;
    const baseClass = 'mobile-number';

    return (
      <div>
        <EditableDiv className={`${baseClass}__backLink`} html={back} />
        <Editable tag="h2" className={`${baseClass}__title`} html={title} />
        <EditableDiv className={`${baseClass}__content`} html={description} />
        <EditableDiv html={phoneNumberLable} />
        <EditableDiv html={phoneNumber} />
        <EditableDiv html={formArea}></EditableDiv>
      </div>
    );
  }
}

OpvChangeMobileNumber.defaultProps = {
  title: '',
  description: '',
  back: '',
  phoneNumberLable: '',
  phoneNumber: '',
  formArea: ''
};

OpvChangeMobileNumber.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  back: PropTypes.string,
  phoneNumberLable: PropTypes.string,
  phoneNumber: PropTypes.string,
  formArea: PropTypes.string
};

module.exports = OpvChangeMobileNumber;
