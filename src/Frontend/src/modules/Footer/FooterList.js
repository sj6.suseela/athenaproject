import React from 'react';
import PropTypes from 'prop-types';

import ArrowLink from '../ArrowLink/ArrowLink';
import Editable from '../Editable/Editable';
import SocialIcons from '../SocialIcons/SocialIcons';
import Icon from '../Icon/Icon';

class FooterList extends React.Component {
  constructor (props) {
    super(props);

    this.links = this.props.children.map((item,i) => {
      let link = {};
      if (item.link) {
        link = item.link;
      }
      let className = 'footer__listItem';
      if (item.hasSeparator) {
        className += ' footer__listItem--bottom-line';
      }
      return (
        <li className={className} key={`${item.label}:${i}`}>
          {!item.isArrowLink && (
            <Editable html={link.html} />
          )}
          {item.isArrowLink && (
            <ArrowLink isExternal={item.isExternal} link={link.html} subText={item.copy} />
          )}
          {item.isExternal && !item.isArrowLink && (
            <Icon icon='externalLink' />
          )}
        </li>
      );
    });
  }
  render () {
    let cssClass = 'footer__list';
    if (this.props.isLegal) {
      cssClass += ' footer__list--legal';
    }
    return (
      <div className={cssClass}>
        <ul>
          {this.links}
        </ul>
        {!!this.props.socialIcons && (
          <SocialIcons items={this.props.socialIcons}/>
        )}
      </div>
    );
  }
}

FooterList.propTypes = {
  children: PropTypes.array,
  isLegal: PropTypes.bool,
  socialIcons: PropTypes.array
};

module.exports = FooterList;
