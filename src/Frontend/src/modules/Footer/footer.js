import React from 'react';
import PropTypes from 'prop-types';

import './footer.scss';

import Logo from '../Logo/Logo';
import SocialIcons from '../SocialIcons/SocialIcons';
import Accordion from '../Accordion/Accordion';

import FooterList from './FooterList';

import EditableDiv from '../Editable/EditableDiv';

/** Footer
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Footer, {
    copyright:`<p>&copy; 2018 Liverpool Victoria</p>
          <p>All our products are covered by lorem ipsum Duis congue posuere iaculis. Proin pellentes. </p>`,
    logo:'<img src="dist/images/lv-logo.png"></img>',
    logoLink:{href:"index.html"},
    items:[
        {
            heading: "Insurance",
            children: [
              {
                link: {
                  html:'<a href="#">Car Insurance</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Breakdown cover</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Home Insurance</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Travel Insurance</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Pet Insurance</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Landlord Insurance</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">More Insurance...</a>',
                },
              }
            ],
            open:false
        },
        {
            heading: "Life Insurance, Investments & Pensions",
            children: [
              {
                link: {
                  html:'<a href="#">Life Insurance, Investments & Pensions</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Life Insurance</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Life Insurance & Critical Illness</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Income Protection</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Investments</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Pensions & Retirement</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Advisor Centre</a>',
                },
                isExternal: true
              },
              {
                link: {
                  html:'<a href="#">Corporate Solutions</a>',
                },
                isExternal: true
              }
            ],
            open:false
        },
        {
            heading: "About LV=",
            children: [
              {
                link: {
                  html:'<a href="#">About Use</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Company Information</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Corporate Responsibility</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Awards and recognition</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Newsroom</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Members</a>',
                },
              },
              {
                link: {
                  html:'<a href="#">Careers</a>',
                },
                isExternal: true
              },
              {
                link: {
                  html:'<a href="#">Feedback</a>',
                },
              }
            ],
            open:false
        },
        {
          heading: "Contact LV=",
          open: false,
          children:[
            {
              link: {
                html: '<a href="#">Insurance</a>',
              },
              isArrowLink: true,
              copy: 'For all your car, home, travel, pet and other insurance enquiries',
              hasSeparator: true
            },
            {
              link: {
                html: '<a href="#">Life Insurance, Investments & Pensions</a>',
              },
              isArrowLink: true,
              copy: 'For all life insurance, investments retirmeent support',
              hasSeparator: true
            }
          ]
        },

      ],
      securityItems:
      {
        heading: "legal",
        open: false,
        children: [
          {
            link: {
              html:'<a href="#">Terms of use</a>',
            },
          },
          {
            link: {
              html:'<a href="#">Online Security</a>',
            },
          },
          {
            link: {
              html:'<a href="#">Accessibility</a>',
            },
          },
          {
            link: {
              html:'<a href="#">Sitemap</a>',
            },
          },
          {
            link: {
              html:'<a href="#">Modern Slavery Statement</a>',
            },
          },
          {
            link: {
              html:'<a href="#">Cookie Policy</a>',
            },
          },
          {
            link: {
              html:'<a href="#">Marketing Consent</a>',
            },
          },
          {
            link: {
              html:'<a href="#">Data Protection</a>',
            },
          }
        ]
      },
      socialIcons:[
        {
          link:{
            id: '1',
            href:"http://www.facebook.com",
            html:'<a href="http://www.twitter.com">twitter</a>',
          },
          logo:'<img src="dist/images/cms/Facebook.svg" alt="twitter"/>'
        },
        {
          link:{
            id: '2',
            href:"http://www.facebook.com",
            html:'<a href="http://www.twitter.com">twitter</a>',
          },
          logo:'<img src="dist/images/cms/Twitter.svg" alt="twitter"/>'
        },
        {
          link: {
            id: '3',
            href:"http://www.facebook.com",
            html:'<a href="http://www.twitter.com">twitter</a>',
          },
          logo:'<img src="dist/images/cms/LinkedIn.svg" alt="twitter"/>'
        },
        {
          link: {
            id: '4',
            href:"http://www.facebook.com",
            html:'<a href="http://www.twitter.com">twitter</a>',
          },
          logo:'<img src="dist/images/cms/google.svg" alt="twitter"/>'
        }
      ]
  }), document.getElementById("element"));
*/
class Footer extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };

    this.hasItems = !!this.props.items && !!this.props.items.length > 0;
    this.hasSecurityItems = !!this.props.securityItems && !!this.props.securityItems.children && this.props.securityItems.children.length > 0;

    if(this.hasItems) {
      this.footerData = this.props.items.map((item,i) => {
        let socialIcons;
        if (i === this.props.items.length - 1) {
          socialIcons = this.props.socialIcons;
        }
        item.children = <FooterList key={item.heading} heading={item.heading} isLegal={false} socialIcons={socialIcons}>{item.children}</FooterList>;
        return item;
      });
    }
    if(this.hasSecurityItems) {
      this.footerSecurityData = [this.props.securityItems].map((item) => {
        item.children = <FooterList key={item.heading} heading={item.heading} isLegal={true}>{item.children}</FooterList>;
        return item;
      });
    }
  }

  render () {
    const {isMini} = this.props;

    const cssClass = isMini ? 'footer footer--mini' : 'footer';

    let hasSocialIcons = !!this.props.socialIcons;

    if(isMini) {
      this.hasItems = false;
      this.hasSecurityItems = false;
      hasSocialIcons = false;
    }

    return (
      <div className={cssClass}>
        <div className="container">
          <div className="footer__content">
            {this.hasItems && (
              <Accordion className='footer__items' mobileOnly={true} closeSiblings={false} items={this.footerData} openIcon="plus" modifier="footer" />
            )}
            {this.hasSecurityItems && (
              <Accordion className='footer__security' mobileOnly={true} closeSiblings={false} items={this.footerSecurityData} openIcon="plus" modifier="footer" />
            )}
          </div>
          <div className="footer__foot">
            {hasSocialIcons && (
              <SocialIcons items={this.props.socialIcons}/>
            )}
            <div>
              <Logo img={this.props.logo.html} link={this.props.logoLink}/>
            </div>
            <EditableDiv className="footer__copyright" html={this.props.copyright} />
          </div>
        </div>
      </div>
    );
  }
}

Footer.propTypes = {
  socialIcons: PropTypes.any,
  logo: PropTypes.object,
  logoLink: PropTypes.object,
  copyright: PropTypes.string,
  isMini: PropTypes.bool,
  items: PropTypes.array,
  securityItems: PropTypes.object
};

module.exports = Footer;
