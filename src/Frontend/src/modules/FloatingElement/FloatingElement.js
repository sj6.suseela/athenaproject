import React from 'react';
import PropTypes from 'prop-types';

import './FloatingElement.scss';

class FloatingElement extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };
  }

  render () {
    const style = {
      marginTop:this.props.topPos
    };

    return (
        <div className="container">
      <div className="floatingElement" style={style}>
        {this.props.children}
      </div>
        </div>
    );
  }
}

FloatingElement.propTypes = {
  topPos: PropTypes.any,
  children: PropTypes.any
};

module.exports = FloatingElement;
