import React from 'react';
import PropTypes from 'prop-types';

import EditableLink from '../Editable/EditableLink';

import './PartnerLogo.scss';

class PartnerLogo extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      className: 'partnerLogo'
    };
  }

  render () {
    const {img,href} = this.props;

    return (
		<div className={this.state.className}>
			<EditableLink href={href} html={img} />
		</div>
    );
  }
}

PartnerLogo.propTypes = {
  img: PropTypes.string,
  href: PropTypes.string
};

module.exports = PartnerLogo;
