import React from 'react';
import PropTypes from 'prop-types';

import './AnchorNav.scss';
import Editable from '../Editable/Editable';
import ScrollToY from '../scrolltoy';

/** AnchorNav
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.AnchorNav, {
    id : 'id',
    items : [
      {
        label : "Anchor link one",
        href : "#anchorLinkOne"
      },
      {
        label : "Anchor link two",
        href : "#anchorLinkTwo"
      },
      {
        label : "Anchor link three",
        href : "#anchorLinkThree"
      },
      {
        label : "Anchor link four",
        href : "#anchorLinkFour"
      },
      {
        label : "Anchor link five",
        href : "#anchorLinkFive"
      },
      {
        label:"Anchor link six",
        href:"#anchorLinkSix"
      }
    ],
  }), document.getElementById("element"));
*/
class AnchorNav extends React.Component {
  constructor (props) {
    super(props);

    this._onClick = this._onClick.bind(this);
  }

  _onClick (e) {
    e.preventDefault();

    const elementId = e.target.getAttribute('href').split('#')[1];

    const offset = 30;

    const yPos = document.getElementById(elementId).getBoundingClientRect().top + window.pageYOffset - offset;

    new ScrollToY(yPos, 500, 'easeInOutQuint');
  }

  render () {
    const {heading} = this.props;

    this.links = this.props.items.map((item,i) => {
      return <li key={item.label + i}><h3><a onClick={this._onClick} href={item.href}>{item.label}</a></h3></li>;
    });

    return (
      <div className="module anchorNav">
        <div className="container">
          <Editable tag="h2" html={heading} />
          <ol>
            {this.links}
          </ol>
        </div>
      </div>
    );
  }
}

AnchorNav.defaultProps = {
  items: [],
  heading: ''
};

AnchorNav.propTypes = {
  items: PropTypes.array,
  heading: PropTypes.string
};

module.exports = AnchorNav;
