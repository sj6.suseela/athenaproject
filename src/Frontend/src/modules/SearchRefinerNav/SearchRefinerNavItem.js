import React from 'react';
import PropTypes from 'prop-types';

import './SearchRefinerNav.scss';

class SearchRefinerNavItem extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      markerClass: ''
    };
    this._onMouseOver = this._onMouseOver.bind(this);
    this._onMouseOut = this._onMouseOut.bind(this);
  }

  componentDidMount () {
    if (this.props.isSelected) {
      this.setState({
        markerClass: 'selected'
      });
    }
  }

  _onMouseOver () {
    if (!this.props.isSelected) {
      this.setState({
        markerClass: 'hover'
      });
    }
  }

  _onMouseOut () {
    if (!this.props.isSelected) {
      this.setState({
        markerClass: ''
      });
    }
  }

  render () {
    const {label, href, isSelected} = this.props;
    const {markerClass} = this.state;

    const cssClass = isSelected ? 'selected' : '';

    return (
      <li key={label} className={cssClass}><a href={href} title={label} onMouseOver={this._onMouseOver} onMouseOut={this._onMouseOut} >{label}</a>
        <span className={`marker ${markerClass}`}></span>
      </li>
    );
  }
}

SearchRefinerNavItem.propTypes = {
  href: PropTypes.string,
  label: PropTypes.array,
  isSelected: PropTypes.bool
};

module.exports = SearchRefinerNavItem;
