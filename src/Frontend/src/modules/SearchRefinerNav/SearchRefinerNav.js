import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import MediaQuery from 'react-responsive';
import SearchRefinerNavItem from './SearchRefinerNavItem';
import ContentExpander from '../ContentExpander/ContentExpander';
import Icon from '../Icon/Icon';

import './SearchRefinerNav.scss';

/** SearchRefinerNav
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.SearchRefinerNav, {
    id : 'id',
    mobileNavTitle: 'Currently showing',
    searchResultsCount: '123',
    searchResultsText: 'results found',
    items:[
      {
        count:500,
        label:'All',
        href:'/insurance-search?term=*&filter',
        isSelected:true,
        sortOrder:0
      },
      {
        count:0,
        label:'Articles',
        href:'/insurance-search?term=*&filter=Articles',
        isSelected:false,
        sortOrder:1
      },
      {
        count:2,
        label:'Guides',
        href:'/insurance-search?term=*&filter=Guides',
        isSelected:false,
        sortOrder:2
      },
      {
        count:1,
        label:'Press releases',
        href:'/insurance-search?term=*&filter=Press-releases',
        isSelected:false,
        sortOrder:3
      }
    ]
  }), document.getElementById("element"));
*/

class SearchRefinerNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      markerClass: '',
      navOpen: false
    };

    this.navItems = props.items.map((item) => {
      return <SearchRefinerNavItem key={item.label} href={item.href} label={item.label} isSelected={item.isSelected} />;
    });

    this._toggleNav = this._toggleNav.bind(this);
  }

  _closeNav () {
    this.setState({
      navOpen: false
    });
  }

  _toggleNav (e) {
    e.preventDefault();
    this.setState({
      navOpen: !this.state.navOpen
    });
  }

  render () {
    const {mobileNavTitle, searchResultsCount, searchResultsText, items} = this.props;

    const baseClass = 'nav--search-refiner';
    let triggerClass = 'mobileNav__trigger';

    const menuArrow = this.state.navOpen ? 'uparrow' : 'downarrow';

    if (this.state.navOpen) triggerClass += ' active';

    const mobileLabelActive = items.map((item) => {
      if (item.isSelected) {
        return JSON.stringify(item.label).replace(/"/g, '');
      }
    });

    const currentlyActiveTab = items.filter((item) => { return item.isSelected; }).map((item) => {
      return item.label;
    });

    const hideRefinerNav = !!(currentlyActiveTab.toString().toLowerCase() === 'all' && searchResultsCount < 1);

    return (
      <div className={`${baseClass}`}>
        {!hideRefinerNav && (
          <div className="container">
            <nav>
              <MediaQuery query="(max-width: 1023px)">
                <span className="mobileNavTitle">{mobileNavTitle}</span>
                <div className={triggerClass} ref={(c) => { this.trigger = c; }}>
                  <a href="#" onClick={this._toggleNav} >
                    {mobileLabelActive}
                    <Icon icon={menuArrow} fillColor="#04589b" />
                  </a>
                </div>
                <ContentExpander isOpen={this.state.navOpen} isFillScreen={false} hasClose={false} onClose={this._closeNav} trigger={this.trigger}>
                  <nav>
                    <ul>
                      {items.map((item) => {
                        if (!item.isSelected) {
                          return <li><a href={item.href} title={item.label}>{item.label}</a></li>;
                        }
                      })}
                    </ul>
                  </nav>
                </ContentExpander>
              </MediaQuery>

              <MediaQuery query="(min-width: 1024px)">
                <ul>
                  {this.navItems}
                </ul>
              </MediaQuery>
            </nav>
            <p className="search__results">{searchResultsCount.toString()} <Editable html={searchResultsText} /></p>
          </div>)}
        </div>
    );
  }
  }
  
SearchRefinerNav.defaultProps = {
  mobileNavTitle: 'Currently showing:',
  isSelected: false,
  searchResultsText: 'results found'
};

SearchRefinerNav.propTypes = {
  mobileNavTitle: PropTypes.string,
  items: PropTypes.array,
  searchResultsCount: PropTypes.number,
  searchResultsText: PropTypes.string
};


module.exports = SearchRefinerNav;
