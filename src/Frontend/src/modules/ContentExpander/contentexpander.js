import React from 'react';
import PropTypes from 'prop-types';

import './ContentExpander.scss';

import Icon from '../Icon/Icon';

class ContentExpander extends React.Component {
  constructor (props) {
    super(props);

    this.componentDidMount = this.componentDidMount.bind(this);
    this._updateWindowDimensions = this._updateWindowDimensions.bind(this);
    this._hide = this._hide.bind(this);
    this._show = this._show.bind(this);
    this._onClose = this._onClose.bind(this);
    this._handleClickOutside = this._handleClickOutside.bind(this);

    this.width = 0;

    this.state = {
      open:this.props.isOpen,
      visible:false,
      maxHeight:null,
      currentHeight:0,
      mounted:false,
      pageHeight:null,
      hasClose:props.hasClose,
      fillScreen:props.isFillScreen,
      closingAnim:false,
      contentHeight:0,
      screenWidth:0,
      screenHeight:0
    };
  }

  _updateWindowDimensions (event) {
    if(event !== undefined) {
      if(this.container) {
        if(this.state.screenWidth !== window.innerWidth) {
          this.setState({screenWidth:window.innerWidth},this._setMaxHeight);
        }
        else if(this.state.screenHeight !== window.innerHeight) {
          this.setState({screenHeight:window.innerHeight},this._setMaxHeight);
        }
      }
    }
  }

  _setMaxHeight () {
    if(this.state.fillScreen && this.container) {
      if(!SERVER) {
        if(typeof window === 'object') {
          let height = window.innerHeight - this.container.getBoundingClientRect().top;
          if(this.state.contentHeight > height) {
            height = this.state.contentHeight + 20;
          }
          this.setState({maxHeight:height});
        }
      }
    }
    else{
      if(this.container) {
        this.container.style.height = 'auto';
        const clientHeight = this.container.offsetHeight;
        if(this.state.open) {
          this.container.style.height = `${clientHeight}px`;
        }
        else{
          this.container.style.height = '0px';
        }
        this.setState({maxHeight:clientHeight});
      }
    }

    if(this.container) {
      this.setState({mounted:true});
    }
  }

  componentDidMount () {
    this.setState({
      contentHeight:this.container.offsetHeight,
      screenWidth:window.innerWidth,
      screenHeight:window.innerHeight
    },function () {
      this._setMaxHeight();
    });

    if(!SERVER) {
      window.addEventListener('resize', this._updateWindowDimensions);
    }
  }

  _handleClickOutside (e) {
    if(this.container && this.props.trigger) {
      if(!this.container.contains(e.target) && !this.props.trigger.contains(e.target)) {
        this._onClose(e);
        this.removeEventListener('touchstart');
      }
    }
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps (props) {
    // this._updateWindowDimensions();

    if(props.isOpen) {
      this._show();
    }
    else{
      if(this.state.open) {
        this._hide();
      }
    }
  }

  _show () {
    if(!this.state.fillScreen) {
      this._setMaxHeight();
      setTimeout(() => {
        this.setState({open:true});
      },50);
    }
    else{
      this._setMaxHeight();
      setTimeout(() => {
        this.setState({open:true});
      },50);
    }

    if(!SERVER) {
      document.addEventListener('touchstart', this._handleClickOutside);
    }
  }

  _hide () {
    if(!SERVER) {
      window.removeEventListener('resize', this._updateWindowDimensions);
    }
    this.setState({open:false,closingAnim:true});
  }

  _onClose (e) {
    this.props.onClose(e);
  }

  render () {
    const className = `contentExpander ${this.props.className}`;
    let style = {};

    if(this.state.mounted) {
      if(this.state.open) {
        style = {
          height:this.state.maxHeight,
          position:'relative',
          left:'auto',
          transition:'height 400ms ease',
          visibility: 'visible'
        };
      }
      else{
        style = {
          height:0,
          position:'relative',
          left:'auto',
          transition:'height 400ms ease',
          visibility:'visible'
        };
      }
    }
    else{
      style = {
        position:'absolute',
        left:'-2000em',
        height:'auto',
        width:'100%',
        visibility:'hidden'
      };
    }

    const hasClose = this.props.hasClose;

    return (
      <div ref={(c) => {
        this.container = c;
      }} className={`${className} ${this.state.mounted ? 'mounted' : ''} ${this.state.open ? 'open' : ''}`} style={style}>
          {hasClose && (
            <span className="close" onClick={this._onClose}><Icon icon="close" /></span>
          )}
          {this.props.children}
      </div>
    );
  }
}

ContentExpander.defaultProps = {
  hasClose:false,
  isFillScreen:false,
  className:'',
  trigger:null
};

ContentExpander.propTypes = {
  isOpen: PropTypes.bool,
  hasClose: PropTypes.bool,
  onClose: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.any,
  isFillScreen: PropTypes.bool,
  trigger:PropTypes.any
};

module.exports = ContentExpander;
