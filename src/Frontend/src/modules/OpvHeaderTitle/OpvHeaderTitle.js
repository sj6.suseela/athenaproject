import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';

import './OpvHeaderTitle.scss';

class OpvHeaderTitle extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {greetTxt, name, home, settings, logout} = this.props;
    const baseClass = 'header-title';

    return (
      <div className="header-title">
        <div className={`${baseClass}__title`}>
        <Editable  html={greetTxt} />&nbsp;<Editable  html={name} />
        </div>
        <div className={`${baseClass}__link`}>
          <ul>
            <li>
              <Editable className={`${baseClass}__linkHome`} html={home.html} />
            </li>
            <li>
              <Editable className={`${baseClass}__linkSettings`} html={settings.html} />
            </li>
            <li>
              <Editable className={`${baseClass}__linkLogout`} html={logout.html} />
            </li>
          </ul>
        </div>

      </div>
    );
  }
}

OpvHeaderTitle.defaultProps = {
  greetTxt: '',
  name: '',
  home: '',
  settings: '',
  logout: ''
};

OpvHeaderTitle.propTypes = {
  greetTxt: PropTypes.string,
  name: PropTypes.string,
  home: PropTypes.object,
  settings: PropTypes.object,
  logout: PropTypes.object
};

module.exports = OpvHeaderTitle;
