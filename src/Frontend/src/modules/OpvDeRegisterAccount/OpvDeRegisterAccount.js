import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Cta from '../Cta/cta';
import './OpvDeRegisterAccount.scss';

class OpvDeRegisterAccount extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {title, description, back, deleteaccounttext, keepaccounttext} = this.props;
    const baseClass = 'de-register-account';

    return (
      <div className={baseClass}>
        <EditableDiv className={`${baseClass}__backLink`} html={back} />
        <Editable tag="h2" className={`${baseClass}__title`} html={title} />
        <EditableDiv className={`${baseClass}__content`} html={description} />
        <div className={`${baseClass}__footer`}>
          <Cta type="primary" isSmall={true} rawCta={deleteaccounttext.html} />
          <Cta type="secondary" isSmall={true} rawCta={keepaccounttext.html} />
        </div>
      </div>
    );
  }
}

OpvDeRegisterAccount.defaultProps = {
  submit: '',
  title: '',
  description: '',
  back: ''
};

OpvDeRegisterAccount.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  submit: PropTypes.string,
  back: PropTypes.string,
  deleteaccounttext: PropTypes.object,
  keepaccounttext: PropTypes.object
};

module.exports = OpvDeRegisterAccount;
