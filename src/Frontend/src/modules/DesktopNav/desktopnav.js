import React from 'react';
import PropTypes from 'prop-types';

import './desktopnav.scss';

import DesktopNavItem from './DesktopNavItem';

class DesktopNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isSubNavOpen: false,
      markerPos:null,
      markerWidth:null,
      markerVisible:false,
      defaultMarkerPos:null,
      defaultMarkerWidth:null,
      markerClass:'',
      hasSelected:null,
      navMouseOver:false
    };

    this._onMouseOver = this._onMouseOver.bind(this);
    this._onMouseOut = this._onMouseOut.bind(this);
    this._onSelected = this._onSelected.bind(this);

    this._onMouseOverNav = this._onMouseOverNav.bind(this);
    this._onMouseOutNav = this._onMouseOutNav.bind(this);

    this.navItems = props.items.map((item) => {
      let itemWrapper = null;

      if(!item.visible) {
        return null;
      }

      if(item.items) {
        itemWrapper = item.items.map((item) => {
          return <DesktopNavItem key={item.label} label={item.label} onMouseOver={this._onMouseOver} onMouseOut={this._onMouseOut} link={item.link} selected={item.selected}>{item.children}</DesktopNavItem>;
        });

        return itemWrapper;
      }
      else{
        return <DesktopNavItem key={item.label} label={item.label} onMouseOver={this._onMouseOver} onMouseOut={this._onMouseOut} link={item.link} selected={item.selected}>{item.children}</DesktopNavItem>;
      }
    });
  }

  _onSelected (e) {
    if(e) {
      this.setState({
        markerPos:e.posX,
        markerWidth:e.width,
        markerClass:'visible',
        defaultMarkerPos:e.posX,
        defaultMarkerWidth:e.width,
        hasSelected:true
      });
    }
  }

  _onMouseOverNav () {
    const _this = this;
    setTimeout(() => {
      _this.setState({
        navMouseOver:true
      });
    },100);
  }

  _onMouseOutNav () {
    this.setState({
      navMouseOver:false,
      markerClass:''
    });
  }

  _onMouseOver (e) {
    const _this = this;
    if(e) {
      if(this.state.navMouseOver) {
        this.setState({
          markerPos:e.posX,
          markerWidth:e.width,
          markerClass:'hover visible animate'
        });
      }
      else{
        this.setState({
          markerPos:e.posX,
          markerWidth:e.width,
          markerClass:'hover invisible'
        });

        setTimeout(() => {
          _this.setState({
            markerClass:'hover visible'
          });
        },10);
      }
    }
  }

  _onMouseOut () {
    if(this.state.defaultMarkerPos !== null) {
      this.setState({
        markerPos:this.state.defaultMarkerPos,
        markerWidth:this.state.defaultMarkerWidth,
        markerClass:''
      });
    }
    else{
      this.setState({
        markerClass:''
      });
    }
  }

  render () {
    const {markerPos,markerWidth} = this.state;

    const markerClass = `nav__marker ${this.state.markerClass}`;

    const style = {
      left:markerPos || 0,
      width:markerWidth
    };

    let cssClass = 'nav nav--desktop';

    if(this.props.isInvertedColour) cssClass += ' invertColours';

    return (
      <div className={cssClass} onMouseEnter={this._onMouseOverNav} onMouseLeave={this._onMouseOutNav}>
        <nav>
          <span style={style} className={markerClass}></span>
          <ul>
            {this.navItems}
          </ul>
        </nav>
      </div>
    );
  }
}

DesktopNav.propTypes = {
  items: PropTypes.array,
  isInvertedColour:PropTypes.bool
};

module.exports = DesktopNav;
