import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/Icon';

import SubnavPanel from '../SubnavPanel/SubnavPanel';

import {isBrowser} from 'react-device-detect';

class DesktopNavItem extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isSubNavVisible:false,
      isSubNavOpen:false,
      isForcedOpen:false
    };

    this._onClick = this._onClick.bind(this);
    this._onMouseOver = this._onMouseOver.bind(this);
    this._onMouseLeave = this._onMouseLeave.bind(this);
    this._open = this._open.bind(this);
    this._close = this._close.bind(this);
    this._handleClickOutside = this._handleClickOutside.bind(this);

    this.hasMouseOverFired = false;
  }

  componentDidMount () {
    /* if(this.props.selected) {
      const posX = this.text.offsetLeft + this.text.parentNode.parentNode.offsetLeft;
      const width = this.text.offsetWidth;

      this.props.onSelected({
        posX,
        width
      });
    }*/

    if(this.props.children) {
      document.addEventListener('touchstart', this._handleClickOutside);
    }
  }

  _handleClickOutside (e) {
    if (this.navitemref && !this.navitemref.contains(e.target)) {
      this.setState({isSubNavVisible:false,isSubNavOpen:false,isForcedOpen:false});
    }
  }

  _onClick (e) {
    const hasChildren = this.props.children;
    const isSublink = e.nativeEvent.target.className === 'subnav__sublink';

    if(hasChildren && !isSublink) {
      e.preventDefault();

      if(this.state.isSubNavOpen) {
        this.setState({isSubNavVisible:false,isSubNavOpen:false,isForcedOpen: false});
      }
      else{
        this.setState({isSubNavVisible:true,isSubNavOpen:true,isForcedOpen: true});
      }
    }
  }

  _onMouseOver (e) {
    e.stopPropagation();

    if(isBrowser) {
      this._open(e);
    }
  }

  _onMouseLeave (e) {
    if(isBrowser) {
      this._close(e);
    }
  }

  _open (e) {
    e.stopPropagation();

    // check for mouse events

    const isSublink = e.nativeEvent.target.className === 'subnav__sublink';

    if(e.nativeEvent.target.localName === 'span' && !isSublink) {
      const linkEl = e.nativeEvent.target.parentNode;
      const listItemEl = e.nativeEvent.target.parentNode.parentNode;

      const posX = linkEl.offsetLeft + listItemEl.offsetLeft + linkEl.childNodes[0].offsetLeft;
      const width = linkEl.childNodes[0].offsetWidth;

      this.props.onMouseOver({
        posX,
        width
      });
    }
    else if(e.nativeEvent.target.localName === 'a' && !isSublink) {
      const linkEl = e.nativeEvent.target;
      const listItemEl = e.nativeEvent.target.parentNode;

      const posX = linkEl.offsetLeft + listItemEl.offsetLeft + linkEl.childNodes[0].offsetLeft;
      const width = linkEl.childNodes[0].offsetWidth;

      this.props.onMouseOver({
        posX,
        width
      });
    }

    if(this.props.children) {
      const _this = this;
      this.setState({isSubNavOpen: true});
      setTimeout(() => {
        _this.setState({isSubNavVisible:true});
      },0);
    }
  }

  _close (e) {
    if(e.nativeEvent.target.localName === 'li') {
      this.props.onMouseOut();
    }

    this.setState({isSubNavVisible:false,isSubNavOpen:false});
  }

  render () {
    const {label,selected,children,link} = this.props;

    const {isForcedOpen} = this.state;

    const showSubnav = children && this.state.isSubNavOpen;

    let cssClass = selected ? 'selected' : '';
    showSubnav ? cssClass += ' active' : '';

    isForcedOpen ? cssClass += ' active' : '';

    const iconColor = '#ffffff';

    const hasChildren = children;
    hasChildren ? cssClass += ' hasChildren' : '';

    return (
      <li onMouseOver={this._onMouseOver} onMouseLeave={this._onMouseLeave} onClick={this._onClick} className={cssClass}   ref={(c) => { this.navitemref = c;}}>
        <a id={link.id} href={link.href} rel={link.rel}>
          <span ref={(c) => {
            this.text = c;
          }} dangerouslySetInnerHTML={{__html: label}}></span>
          {hasChildren && (
              <Icon icon="downarrow" fillColor={iconColor}/>
            )}
        </a>

      {hasChildren && (
          <SubnavPanel isVisible={showSubnav} isForcedOpen={isForcedOpen} items={children}/>
        )}
      </li>
    );
  }
}

DesktopNavItem.defaultProps = {
  link: {}
};

DesktopNavItem.propTypes = {
  label: PropTypes.string,
  link: PropTypes.object,
  selected: PropTypes.any,
  children: PropTypes.any,
  onSelected: PropTypes.func,
  onMouseOver: PropTypes.func,
  onMouseOut: PropTypes.func
};

module.exports = DesktopNavItem;
