import React from 'react';
import PropTypes from 'prop-types';

import './OpvHeaderBanner.scss';
import PartnerLogo from '../PartnerLogo/PartnerLogo';
import ResponsiveImage from '../ResponsiveImage/ResponsiveImage';
import Cta from '../Cta/Cta';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditableParagraph from '../Editable/EditableParagraph';

/** OpvHeaderBanner
  * @description description
  * @example
  ReactDOM.render(React.createElement(PriorityComponents.OpvHeaderBanner, {
    id : 'id',
    theme: {
      themeName : 'blackcurrant'
    },
    label : "",
    labelLink : '<a href="#">Articles</a>',
    article:{
      headline : "Flight cancelled? How to claim and what you're covered for.",
      date : { day:12, month:{ label:"December", number:12, }, year:2017, time:"12:00" },
      readTime : "5 min read",
      categoryTags :[
        { label:"Driving", href:"#" },
        { label:"Lorem", href:"#" },
        { label:"Ipsum", href:"#" }
      ],
      categoryHref : "http://www.category.com"
    }
  }), document.getElementById("element"));
*/
class OpvHeaderBanner extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      visible: true,
      imageVisible: false
    };

    this.ctaList = [];
    if (props.ctas) {
      this.ctaList = props.ctas.map((item, i) => {
        return (
          <Cta
            key={item.link + i}
            {...item.link}
            type={item.type}
            rawCta={item.link}
          />
        );
      });
    }
  }

  componentDidMount () {
    const imageSrc =
      this.props.mobImage && this.props.mobImage.src
        ? this.props.mobImage.src
        : this.props.desktopImage && this.props.desktopImage.src
        ? this.props.desktopImage.src
        : null;
    if (imageSrc !== null) {
      const image = new Image();
      image.src = imageSrc;
      image.onload = () => {
        this.setState({
          imageVisible: true
        });
      };
    }
  }

  render () {
    const {
      beta,
      overlayImage,
      theme,
      desktopImage,
      mobImage,
      article,
      headline,
      subheading,
      isProductHeader,
      isHomepageHeader,
      isGlobalHomepageHeader,
      isAltHeader,
      hasSecNav,
      isLeftAligned,
      isMobileContentUnderBanner,
      logo,
      logoHref
    } = this.props;

    const baseClass = 'opvHeaderBanner';

    let className = baseClass;

    if (beta) {
      className += ` ${baseClass}--beta`;
    }

    if (hasSecNav) {
      className += ` ${baseClass}--hasSecNav`;
    }

    if (isLeftAligned) {
      className += ` ${baseClass}--alignLeft`;
    }

    if (theme.themeName) {
      className += ` ${baseClass}--${theme.themeName} ${baseClass}--overlay`;
    }
    else {
      className += ` ${baseClass}--clover ${baseClass}--overlay ${baseClass}--overlay-default`;
    }

    if (theme.textColour) {
      className += ` theme--text-${theme.textColour}`;
    }

    if (theme.subHeadingTextColour) {
      className += ` theme--subHeadingText-${theme.subHeadingTextColour}`;
    }

    if (theme.mobileTextColour) {
      className += ` theme--text-${theme.mobileTextColour}-mobile`;
    }

    if (theme.mobileSubHeadingTextColour) {
      className += ` theme--subHeadingText-${theme.mobileSubHeadingTextColour}-mobile`;
    }

    if (!desktopImage) {
      className += ` ${baseClass}--noImage`;
    }

    if (isProductHeader) {
      className += ` ${baseClass}--product`;
    }

    if (isMobileContentUnderBanner) {
      className += ` ${baseClass}--mobileContentUnderBanner`;
    }

    if (isGlobalHomepageHeader) {
      className += ` ${baseClass}--global-home`;
    }
    else if (isHomepageHeader) {
      className += ` ${baseClass}--home`;
    }

    if (isAltHeader) {
      className += ` ${baseClass}--alt`;
    }

    const hasHeadline = !!headline;

    if (hasHeadline) {
      className += ` ${baseClass}--headlineOnly`;
    }

    const isVisibleClass = this.state.visible ? 'visible' : '';

    const isArticle = !!article;
    const hasOverlayImage = overlayImage !== null && overlayImage !== undefined;

    const hasMobileTheme = theme.themeAfterMobile === 'white';
    // currently only styled correctly when white
    if (hasMobileTheme) {
      className += ` theme-mobile-${theme.themeAfterMobile}`;
    }

    const curveTheme = theme.themeAfter ? theme.themeAfter : 'white';
    if (theme.themeAfter) {
      className += ` theme-${theme.themeAfter}`;
    }

    const dImage =
      desktopImage !== null
        ? desktopImage
        : {html: null, src: null, altText: null, size: null};
    const mImage =
      mobImage !== null
        ? mobImage
        : {html: null, src: null, altText: null, size: null};

    return (


        <div className={`${className} ${isVisibleClass}`}>
          <div className="opvHeaderBanner__image">
            {hasOverlayImage && (
              <div className="opvHeaderBanner__overlayImage">
                <EditableDiv className="container" html={overlayImage.heart} />
              </div>
            )}
            <ResponsiveImage
              visible={this.state.imageVisible}
              hasHeaderMask={true}
              isHomepageHeader={isHomepageHeader || isGlobalHomepageHeader}
              desktopImage={dImage.html || undefined}
              desktopImageSrc={dImage.src || undefined}
              mobImage={mImage.html || undefined}
              mobImageSrc={mImage.src || undefined}
              altText={dImage.altText || undefined}
              cssclass="bg-image"
              bgtheme={curveTheme}
              isBackground={true}
              mobImageSize={mImage.size || undefined}
              desktopImageSize={dImage.size || undefined}
              hasLazyLoad={false}
            />
          </div>
          <div className="container">
            {isArticle && (
              <div className={'opvHeaderBanner__body'}>
                <PartnerLogo img={logo} href={logoHref} />

                <Editable
                  tag="h1"
                  className="opvHeaderBanner__body__headline"
                  html={article.headline}
                />
                <Editable
                  className="opvHeaderBanner__body__headline"
                  html={article.content}
                />
                <EditableParagraph
                  className="opvHeaderBanner__body__text"
                  html={subheading}
                />
              </div>
            )}
          </div>
        </div>


    );
  }
}

OpvHeaderBanner.defaultProps = {
  theme: {
    themeAfter: 'white',
    textColour: 'white',
    mobileTextColour: 'white'
  },
  ctas: null,

  desktopImage: {},
  mobImage: {}
};

OpvHeaderBanner.propTypes = {
  theme: PropTypes.object,
  ctas: PropTypes.array,
  hasSecNav: PropTypes.bool,
  article: PropTypes.object,
  desktopImage: PropTypes.object,
  mobImage: PropTypes.object,
  headline: PropTypes.string,
  subheading: PropTypes.string,
  isProductHeader: PropTypes.bool,
  isHomepageHeader: PropTypes.bool,
  isAltHeader: PropTypes.bool,
  overlayImage: PropTypes.object,
  isLeftAligned: PropTypes.bool,
  isMobileContentUnderBanner: PropTypes.bool,
  isGlobalHomepageHeader: PropTypes.bool,
  beta: PropTypes.object,
  logo: PropTypes.string,
  logoHref: PropTypes.string
};

module.exports = OpvHeaderBanner;
