
/* function isElementInViewport (el) {
  const rect = el.getBoundingClientRect();
  return (rect.top >= 0 && rect.left >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && rect.right <= (window.innerWidth || document.documentElement.clientWidth));
}*/

function elementInViewport (el) {
  let top = el.offsetTop;
  let left = el.offsetLeft;
  const width = el.offsetWidth;
  const height = el.offsetHeight;

  if(el.offsetParent) {
    top += el.offsetParent.offsetTop;
    left += el.offsetParent.offsetLeft;
  }

  return (
    top < (window.pageYOffset + window.innerHeight) &&
    left < (window.pageXOffset + window.innerWidth) &&
    (top + height) > window.pageYOffset &&
    (left + width) > window.pageXOffset
  );
}

const Module = {
  register (element) {
    if(!SERVER) {
      if(typeof window === 'object') {
        if(window.IntersectionObserver) {
          if(!window.__modules) {
            // on intersection callback
            const onIntersection = (entries) => {
              entries.forEach((item) => {
                if(item.intersectionRatio > 0) {
                  item.target.classList.add('module--visible');
                  window.__modules.unobserve(item.target);
                }
              });
            };

            // settings
            const obSettings = {
              rootMargin: '-100px 0px',
              threshold: 0.01
            };

            window.__modules = new IntersectionObserver(onIntersection, obSettings);
          }

          if(element) {
            if(!elementInViewport(element)) {
              window.__modules.observe(element);
              element.classList.add('module--init');
            }
          }
        }
      }
    }
  }
};

export default Module;
