import React from 'react';
import PropTypes from 'prop-types';

import './search.scss';

import Overlay from '../Overlay/Overlay';
import ArrowLink from '../ArrowLink/ArrowLink';
import Cta from '../Cta/Cta';
import Editable from '../Editable/Editable';
import EditableImage from '../Editable/EditableImage';
import EditableDiv from '../Editable/EditableDiv';
import axios from 'axios';
import SitecoreSettings from '../SitecoreSettings';
import Loading from '../Loading/Loading';

import Icon from '../Icon/Icon';

class Search extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      open:false,
      searchTerm:'',
      isSearchOverlayOpen:false,
      items:props.items,
      searchActive:false,
      searchLabel:props.searchLabels.noSearch,
      noResults: false,
      isEditing:false,
      loading: false
    };

    this.statePushed = false;

    this.backForwardCount = 0;

    this._click = this._click.bind(this);
    this._open = this._open.bind(this);
    this._onFocus = this._onFocus.bind(this);
    this._closeSearchOverlay = this._closeSearchOverlay.bind(this);
    this._clickClose = this._clickClose.bind(this);
    this._onSubmit = this._onSubmit.bind(this);
    this._updateVal = this._updateVal.bind(this);
    this._getSearchResults = this._getSearchResults.bind(this);
    this._updateSearchResults = this._updateSearchResults.bind(this);
    this._resetSearchResults = this._resetSearchResults.bind(this);
  }

  componentDidMount () {
    const _this = this;
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }

    if(!SERVER) {
      history.replaceState({}, null, null);

      window.addEventListener('popstate',(event) => {
        if(event.state === null) {
          event.preventDefault();
          return false;
        }

        if(this.backForwardCount === 0) {
          // forward
          this.backForwardCount ++;
          if(!this.state.isSearchOverlayOpen && event.state.opened === 'search') {
            _this._open();
          }
        }
        else{
          // back
          this.backForwardCount --;
          if(this.state.isSearchOverlayOpen) {
            _this._closeSearchOverlay();
          }
        }
      });
    }
  }

  _click (e) {
    e.preventDefault();
    e.stopPropagation();

    history.pushState({opened:'search'}, null, null);
    this.backForwardCount ++;
    this.statePushed = true;

    this._open();
  }

  _open () {
    if (!SERVER) {
      // set body width to prevent it jumping when the scroll bar disappears
      const body = document.body;
      const scrollWidth = window.innerWidth - body.offsetWidth;
      body.style.width = `calc(100% - ${scrollWidth}px)`;
    }
    this.setState({
      isSearchOverlayOpen:true
    });
  }

  _clickClose () {
    if(this.statePushed) {
      window.history.back();
    }
    else{
      this._closeSearchOverlay();
    }
    // history.replaceState({opened:'search'}, null, null);
  }

  _closeSearchOverlay () {
    if (!SERVER) {
      // reset body width
      document.body.style.width = '100%';
    }

    this.setState({
      isSearchOverlayOpen:false
    });
  }

  _onSubmit () {
    // console.log(`do search for ${this.state.searchTerm} now`);
  }

  _onFocus () {
    /* this.setState({
      isSearchOverlayOpen:true
    });
    this.triggerLink.blur();*/
  }

  _updateVal (e) {
    this.setState({
      searchTerm:e.target.value
    });
    if(e.target.value.length >= this.props.charSearchLimit) {
      this._getSearchResults(e.target.value);
    }
    else if(e.target.value.length === 0) {
      this.setState({
        items:this.props.items,
        searchActive:false,
        searchLabel: this.props.searchLabels.noSearch,
        noResults: false
      });
    }
    else{
      this._resetSearchResults();
    }
  }

  _getSearchResults (term) {
    const ajaxlink = this.props.ajaxUrl;

    const config = {
      headers: {'Data-Type': 'json'},
      params: {
        term
      }
    };

    this.setState({
      loading: true
    });

    axios(ajaxlink,config)
      .then((response) => {
        // const jsonResponse = response.data;
        const jsonResponse = JSON.parse(response.data);
        this._updateSearchResults(jsonResponse.results);
        this.setState({
          loading: false
        });
      })
      .catch((/* error*/) => {
        // console.log(error);
        this.setState({
          loading: false
        });
      });
  }

  _updateSearchResults (items) {
    let label = '';
    if (items.length > 0) {
      label = this.props.searchLabels.results;
    }
    else {
      label = this.props.searchLabels.noResults;
    }
    this.setState({
      items,
      searchActive:true,
      searchLabel: label,
      noResults: items.length === 0
    });
  }

  _resetSearchResults () {
    this.setState({
      items:[],
      searchActive:true,
      searchLabel: '',
      noResults: false
    });
  }

  componentDidUpdate (prevProps, prevState) {
    if(prevState.isSearchOverlayOpen === false) {
      setTimeout(() => {
        if(this._input) {
          this._input.focus();
        }
      }, 500);
    }
  }

  handleClick () {
    this.form.submit();
  }
  handleSubmit () {
    this.form.submit();
  }

  render () {
    let className = 'search';

    const {placeholderText,label,searchImage,noResultsText,viewAllLabel,link, otherBusinessText} = this.props;

    if(this.props.isInvertedColour) className += ' invertColours';

    this.links = [];

    if(this.state.items) {
      this.links = this.state.items.map((item,i) => {
        return (
          <li key={i}>
            <ArrowLink link={item} />
          </li>
        );
      });
    }

    const isEditing = this.state.isEditing;

    return (
      <div className={className}  onClick={this._click}>
        <label htmlFor="site-search">
          <a id={link.id} rel={link.rel} ref={c => this.triggerLink = c} href="#" onFocus={this._onFocus} onClick={this._click}>
            <Icon icon="search" fillColor="#fff"/><Editable html={label} />
          </a>
        </label>
        <Overlay
            isOpen={ this.state.isSearchOverlayOpen }
            from='right'
            hasHeader={false}
            width='100%'
            className='overlay overlay--search'
            onRequestClose={this._clickClose}>
            <form method="get" action={link.href} onSubmit={this.handleSubmit.bind(this)} ref={item => this.form = item} >
              <div className="search-input">
                <input type="text" name="term" id="site-search" ref={(n) => this._input = n} value={this.state.searchTerm} onChange={this._updateVal} placeholder={placeholderText} />
              </div>
              <input type="submit" value="Go" className="cta cta--secondary cta--sm" onClick={this._onSubmit}/>
            </form>
            <div className="search__resultsPreview">
              <Loading className='search__loader' isActive={this.state.loading} />
              <Editable tag="h3" html={this.state.searchLabel} />
              {this.state.noResults && <EditableDiv html={noResultsText} />}
              <ul className="links">
                {this.links}
              </ul>
              {otherBusinessText && !this.state.searchActive && (
                <div className='search__other-business-text'>
                  <Editable tag="h3" html={otherBusinessText.heading} />
                  <ArrowLink link={otherBusinessText.link.html}  />
                </div>
              )}
              {!this.state.searchActive && <EditableImage image={searchImage} />}
              {this.state.searchActive && this.links.length > 0 && <Cta onClick={this.handleClick.bind(this)} label={viewAllLabel} type="secondary" isSmall={true}/>}

              {isEditing && <Cta href='#' onClick={this.handleClick.bind(this)} label={viewAllLabel} type="secondary" isSmall={true}/>}

            </div>

        </Overlay>

      </div>
    );
  }
}

Search.defaultProps = {
  label: 'Search',
  placeholderText: 'Search',
  searchLabels: {},
  link: {}
};

Search.propTypes = {
  label:PropTypes.string,
  placeholderText:PropTypes.string,
  isInvertedColour:PropTypes.bool,
  items:PropTypes.array,
  noResultsText:PropTypes.string,
  searchLabels:PropTypes.object,
  searchImage:PropTypes.string,
  charSearchLimit:PropTypes.number,
  ajaxUrl:PropTypes.string,
  viewAllLabel:PropTypes.string,
  link:PropTypes.object,
  otherBusinessText: PropTypes.object
};

module.exports = Search;
