import React from 'react';
import PropTypes from 'prop-types';

import './SectionTitle.scss';
import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';
import EditableLink from '../Editable/EditableLink';

class SectionTitle extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {heading, viewAllLink, viewAllLabel, icon} = this.props;
    const baseClass = 'section-head';

    const hasLink = viewAllLink && viewAllLabel;
    const hasIcon = icon && !hasLink;

    if(!heading && !hasLink) {
      return ('');
    }

    return (
      <div className={baseClass}>
        <Editable tag="h2" className={`${baseClass}__title`} html={heading} />
        {hasIcon && (
          <EditableImage className={`${baseClass}__icon`} image={icon} />
        )}
        {hasLink && (
          <span className="links desktopOnly">
            <EditableLink {...viewAllLink} html={viewAllLabel} />
          </span>
        )}
      </div>
    );
  }
}

SectionTitle.defaultProps = {
  heading: '',
  icon: '',
  viewAllLabel: ''
};

SectionTitle.propTypes = {
  heading: PropTypes.string,
  icon: PropTypes.string,
  viewAllLink: PropTypes.object,
  viewAllLabel: PropTypes.string
};

module.exports = SectionTitle;
