import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Cta from '../Cta/cta';
import './OpvPasswordChanged.scss';

class OpvPasswordChanged extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {submit, title, description, back} = this.props;
    const baseClass = 'password-changed';

    return (
      <div>
        <EditableDiv className={`${baseClass}__backLink`} html={back} />
        <Editable tag="h2" className={`${baseClass}__title`} html={title} />
        <EditableDiv className={`${baseClass}__content`} html={description} />
        <Cta type="primary" isSmall={true} rawCta={submit} />
      </div>
    );
  }
}

OpvPasswordChanged.defaultProps = {
  submit: '',
  title: '',
  description: '',
  back: ''
};

OpvPasswordChanged.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  submit: PropTypes.string,
  back: PropTypes.string
};

module.exports = OpvPasswordChanged;
