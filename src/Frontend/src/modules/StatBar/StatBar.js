import React from 'react';
import PropTypes from 'prop-types';

import './StatBar.scss';

import EditableParagraph from '../Editable/EditableParagraph';
import Editable from '../Editable/Editable';
import EditableImage from '../Editable/EditableImage';

class StatBar extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {headline, subheadline, items, image} = this.props;

    if(!items) {
      return ('');
    }

    const stats = items.map((item,i) => {
      return (
        <EditableParagraph className="stat" key={i} html={item.copy} />
      );
    });

    return (
      <div className="statbar">
        <div className="statbar__band">
          <div className="container statbar__container">
            <div className="statbar__main">
              <div>
                <Editable tag="h3" className="statbar__subhead" html={subheadline} />
                <Editable tag="h2" className="statbar__title" html={headline} />
                <EditableImage className="statbar__image" image={image} />
              </div>
              <div className="statbar__stats">
                {stats}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

StatBar.defaultProps = {
  headline: '',
  subheadline: '',
  items: [],
  image: null
};

StatBar.propTypes = {
  headline: PropTypes.string,
  subheadline: PropTypes.string,
  items: PropTypes.array,
  image: PropTypes.string
};

module.exports = StatBar;
