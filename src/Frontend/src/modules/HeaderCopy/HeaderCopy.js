import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';

import './HeaderCopy.scss';

/** HeaderCopy
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.HeaderCopy, {
    id : 'id',
    theme : {},
    pageTitle : "Partner Discounts",
    heading : "Like the idea of exclusive deals and partner discounts?",
    subheading : '<p>Sounds good to us too! Simply select the organisation you’re part of to explore our award winning insurance and the benefits on offer.</p>'
  }), document.getElementById("element"));
*/
class HeaderCopy extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {pageTitle,heading,subheading} = this.props;

    return (
      <div className="container">
        <div className="headerCopy">
          <Editable tag="h1" className="label" html={pageTitle} />
          <Editable tag="h2" className="h1 headerCopy__heading" html={heading} />
          <EditableDiv className="headerCopy__copy" html={subheading} />
        </div>
      </div>
    );
  }
}

HeaderCopy.propTypes = {
  pageTitle: PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string
};

module.exports = HeaderCopy;
