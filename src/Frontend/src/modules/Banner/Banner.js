import React from 'react';
import PropTypes from 'prop-types';

import './Banner.scss';

import Editable from '../Editable/Editable';
import EditableParagraph from '../Editable/EditableParagraph';

class Banner extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {image, headline, items} = this.props;

    let content = '';

    if(items) {
      content = items.map((item,i) => {
        return (<div className="banner__option" key={i}>
            <Editable tag="h4" html={item.title} />
            <EditableParagraph html={item.copy} />
          </div>
        );
      });
    }

    return (
      <div className="block banner" style={{backgroundImage: `url(${image})`}}>
        <div className="container banner__container">
          <div className="banner__main">
            <Editable tag="h2" className="banner__title" html={headline} />
            <div>
              {content}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Banner.defaultProps = {
  image: '',
  headline: '',
  items: []
};

Banner.propTypes = {
  image: PropTypes.string,
  headline: PropTypes.string,
  items: PropTypes.array
};

module.exports = Banner;
