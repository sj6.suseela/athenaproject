import React from 'react';
import PropTypes from 'prop-types';

import './SubnavPanel.scss';

class SubnavPanel extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      visible:false
    };
  }
// eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps  (props) {
    this.setState({
      visible:props.isVisible
    });
  }

  render () {
    const classname = this.props.isForcedOpen | this.state.visible ? 'subNav_panel visible' : 'subNav_panel';

    this.navItems = this.props.items.map((item,i) => {
      const style = {transitionDelay:`${(i + 1) * 150}ms`};

      const cssClass = this.state.visible | this.props.isForcedOpen ? 'visible' : 'hidden';

      const ariaHidden = this.state.visible | this.props.isForcedOpen ? 'false' : 'true';
      const tabindex = this.state.visible | this.props.isForcedOpen ? '' : '-1';

      const link = item.link ? item.link : {};

      return <li key={item.label} style={style} className={cssClass}><a className="subnav__sublink" id={link.id} rel={link.rel} aria-hidden={ariaHidden} tabIndex={tabindex} href={link.href} dangerouslySetInnerHTML={{__html: item.label}}></a></li>;
    });

    return (
    <div className={classname}>
      <ul>
        {this.navItems}
      </ul>
    </div>
    );
  }
}

SubnavPanel.propTypes = {
  isVisible: PropTypes.bool,
  items: PropTypes.array,
  isForcedOpen: PropTypes.bool
};

module.exports = SubnavPanel;
