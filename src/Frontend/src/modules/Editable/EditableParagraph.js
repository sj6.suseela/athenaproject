import React from 'react';
import PropTypes from 'prop-types';

class EditableParagraph extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    const {html, className} = this.props;

    if(!html) return ('');

    const atts = {};

    if(className) {
      atts.className = className;
    }

    return (
       <p {...atts} dangerouslySetInnerHTML={{__html: html}}/>
    );
  }
}

EditableParagraph.propTypes = {
  html: PropTypes.string,
  className: PropTypes.string
};

module.exports = EditableParagraph;
