import React from 'react';
import PropTypes from 'prop-types';

class Editable extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    const {tag, className,html} = this.props;

    if(!html) return ('');

    const CustomTag = tag;
    const atts = {};

    if(className) {
      atts.className = className;
    }

    return (
      <CustomTag {...atts} dangerouslySetInnerHTML={{__html: html}} />
    );
  }
}

Editable.defaultProps = {
  tag : 'span',
  html : '',
  className : ''
};

Editable.propTypes = {
  tag : PropTypes.string,
  html : PropTypes.string,
  className : PropTypes.string
};

module.exports = Editable;
