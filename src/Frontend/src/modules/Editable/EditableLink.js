import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from '../_ThirdParty/LazyLoad';

import {globalSettings} from '../utils.js';

class EditableLink extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    const {href, className, html, rel, id, hasLazyLoad} = this.props;

    if(!html) return ('');

    const atts = {};

    if(className) {
      atts.className = className;
    }

    let lazyLoadImages = true;
    if(!SERVER) {
      lazyLoadImages = globalSettings().site.lazyLoadImages;
    }

    if(hasLazyLoad !== null) {
      lazyLoadImages = hasLazyLoad;
    }

    return (
      <LazyLoad once height={0} offset={0} isActive={lazyLoadImages}>
        <a {...atts} id={id} href={href} rel={rel} dangerouslySetInnerHTML={{__html: html}} />
      </LazyLoad>
    );
  }
}

EditableLink.defaultProps = {
  href : '',
  html : '',
  className : '',
  hasLazyLoad:true
};

EditableLink.propTypes = {
  href : PropTypes.string,
  html : PropTypes.string,
  className : PropTypes.string,
  rel:PropTypes.string,
  id:PropTypes.string,
  hasLazyLoad:PropTypes.bool
};

module.exports = EditableLink;
