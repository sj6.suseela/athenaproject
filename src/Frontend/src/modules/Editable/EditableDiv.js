import React from 'react';
import PropTypes from 'prop-types';

class EditableDiv extends React.Component {
  constructor (props) {
    super(props);
  }

  componentDidMount () {
    if(this.props.didMount) {
      this.props.didMount(this.element);
    }
  }

  render () {
    const {html, className} = this.props;

    if(!html) return ('');

    const atts = {};

    if(className) {
      atts.className = className;
    }

    return (
       <div {...atts} dangerouslySetInnerHTML={{__html: html}} ref={(c) => {this.element = c;}}/>
    );
  }
}

EditableDiv.propTypes = {
  html: PropTypes.string,
  className: PropTypes.string,
  didMount:PropTypes.func
};

module.exports = EditableDiv;
