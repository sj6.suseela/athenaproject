import React from 'react';
import PropTypes from 'prop-types';

import LazyLoad from '../_ThirdParty/LazyLoad';
import SitecoreSettings from '../SitecoreSettings';

import {globalSettings} from '../utils.js';

class EditableImage extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing:false,
      placeholderHeight:0
    };
  }

  componentDidMount () {
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }


  render () {
    let img;

    const {image,className, defaultClass,style,onClick,isNotAsync,isCarousel,src,imageSize,imageExtension,hasInnerPlaceholder,onReplace,imageSrc} = this.props;

    if(!image) return ('');

    if(typeof image === 'string' && image.indexOf('<img') === -1) {
      img = `<img src="${image}" alt="" />`;
    }
    else {
      img = image;
    }

    const isEditing = this.state.isEditing;

    const placeholderHeight = imageSize ? imageSize.height : 0;
    const placeholderWidth = imageSize ? imageSize.width : 0;

    if(isCarousel) {
      if(!isNotAsync) {
        return (
          <span ref={(c) => {this.ref = c;}} className={`${defaultClass} ${className}`} style={style} onClick={onClick}>
            <img data-flickity-lazyload={src} />
          </span>
        );
      }
      else{
        return (
          <span ref={(c) => {this.ref = c;}} className={`${defaultClass} ${className}`} style={style} onClick={onClick} dangerouslySetInnerHTML={{__html: img}}/>
        );
      }
    }
    else{
      const isSvg = imageExtension === 'svg';
      let lazyLoadImages = true;
      if(!SERVER) {
        lazyLoadImages = globalSettings().site.lazyLoadImages;
      }

      return (
        isEditing || isNotAsync || isSvg || !lazyLoadImages ? (
          <span ref={(c) => {this.ref = c;}} className={`${defaultClass} ${className}`} style={style} onClick={onClick} dangerouslySetInnerHTML={{__html: img}}/>
        ) : (
        <LazyLoad once height={placeholderHeight} width={placeholderWidth} offset={0} isActive={lazyLoadImages} hasInnerStyle={hasInnerPlaceholder} onReplace={onReplace} imageSrc={imageSrc}>
          <span ref={(c) => {this.ref = c;}} className={`${defaultClass} ${className}`} style={style} onClick={onClick} dangerouslySetInnerHTML={{__html: img}}/>
        </LazyLoad>
        )
      );
    }
  }
}

EditableImage.defaultProps = {
  image: '',
  defaultClass: 'image',
  className: '',
  style: null,
  onClick:  null,
  imageSize:null,
  isNotAsync:false,
  isCarousel:false,
  src:'',
  imageExtension:'',
  hasInnerPlaceholder:false,
  onReplace:null,
  imageSrc:''
};

EditableImage.propTypes = {
  image: PropTypes.string,
  defaultClass: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.string,
  onClick:PropTypes.func,
  isNotAsync:PropTypes.bool,
  imageSize:PropTypes.object,
  isCarousel:PropTypes.bool,
  src:PropTypes.string,
  imageExtension:PropTypes.string,
  hasInnerPlaceholder:PropTypes.bool,
  onReplace:PropTypes.func,
  imageSrc:PropTypes.string
};

module.exports = EditableImage;
