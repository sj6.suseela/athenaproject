import React from 'react';

import IFrame from '../IFrame/IFrame';

class GoogleMap extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };
  }

  render () {
    return (<IFrame {...this.props} />);
  }
}

module.exports = GoogleMap;
