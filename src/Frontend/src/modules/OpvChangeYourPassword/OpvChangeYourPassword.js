import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvChangeYourPassword.scss';

class OpvChangeYourPassword extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const { back, formArea } = this.props;
    const baseClass = "change-your-password";

    return (
      <div>
        <EditableDiv className={`${baseClass}__backLink`} html={back} />
        {/* <Editable tag="h2" className={`${baseClass}__title`} html={title} />
        <EditableDiv className={`${baseClass}__content`} html={description} /> */}
        {/* <EditableDiv
          className={`${baseClass}__policyDetails`}
          html={tableDescription}
        /> */}
        <EditableDiv
          className={`${baseClass}__formArea`}
          html={formArea}
        ></EditableDiv>
      </div>
    );
  }
}

OpvChangeYourPassword.defaultProps = {
  // title: '',
  // description: '',
  back: '',
  // tableDescription: '',
  formArea: ''
};

OpvChangeYourPassword.propTypes = {
  // title: PropTypes.string,
  // description: PropTypes.string,
  back: PropTypes.string,
  // tableDescription: PropTypes.string,
  formArea: PropTypes.string
};

module.exports = OpvChangeYourPassword;
