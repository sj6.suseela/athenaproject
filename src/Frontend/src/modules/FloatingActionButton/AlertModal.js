import React from 'react';

import ReactDOM from 'react-dom';

class AlertModal extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return ReactDOM.createPortal(
      <div className="alertBg"></div>,
      document.getElementById('page'),
  );
  }
}

module.exports = AlertModal;
