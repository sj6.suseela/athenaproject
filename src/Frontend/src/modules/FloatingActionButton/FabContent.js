import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/Icon';

class FabContent extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};

    this._onClose = this._onClose.bind(this);
  }

  _onClose (e) {
    this.props.onClose(e);
  }

  render () {
    const {isOpen,isVisible} = this.props;

    const cssClass = 'fabContent';

    const openClass = isOpen ? 'fabContent--active' : '';
    const visibleClass = isVisible ? 'fabContent--visible' : '';

    return (
        <div className={`${cssClass} ${openClass} ${visibleClass}`}>
          <span className="close" onClick={this._onClose}><Icon icon="close" /></span>
          {this.props.children}
        </div>
    );
  }
}

FabContent.propTypes = {
  onClose: PropTypes.func,
  children: PropTypes.any,
  isOpen:PropTypes.bool,
  isVisible:PropTypes.bool
};

module.exports = FabContent;
