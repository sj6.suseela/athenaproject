import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Icon from '../Icon/Icon';
import ArrowLink from '../ArrowLink/ArrowLink';
import Cta from '../Cta/Cta';
import Contact from '../Contact/Contact';

class FabContentInner extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };

    if(props.links) {
      this.links = props.links.map((item,i) => {
        if(item.subLink) {
          if(item.isPDF) {
            return (
            <li key={item + i}>
                <div className="downloadLink">
                  <Icon icon="pdf"/>
                  <Editable className="downloadLink__label" html={item.link}/>
                </div>
                <ArrowLink link={item.subLink}/>
            </li>
            );
          }
          else{
            return (
            <li key={item + i}>
                <ArrowLink link={item.link}/>
                <ArrowLink link={item.subLink}/>
            </li>
            );
          }
        }
        else{
          if(item.isPDF) {
            return (
            <li key={item + i}>
                <div className="downloadLink">
                  <Icon icon="pdf"/>
                  <Editable className="downloadLink__label" html={item.link}/>
                </div>
                <ArrowLink link={item.subLink}/>
            </li>
            );
          }
          else{
            return (
            <li key={item + i}>
                <ArrowLink link={item.link} subText={item.subText}/>
            </li>
            );
          }
        }
      });
    }

    if(props.ctas) {
      this.ctaList = props.ctas.map((item,i) => {
        return (
        <Cta key={item.link + i} {...item.link} type={item.type} rawCta={item.link} isSmall={true} isWide={true} />
        );
      });
    }
  }

  render () {
    const {heading, copy, contact, intro} = this.props;

    const baseClass = 'fabContentInner';

    const hasContact = contact !== null;

    return (
        <div className={`${baseClass}`}>
          <Editable tag="h2" html={heading} />
          <EditableDiv className={`${baseClass}__intro`} html={intro} />
          <ul className="links links--multiline">
            {this.links}
          </ul>
          <EditableDiv className={`${baseClass}__copy`} html={copy} />
          {hasContact && (
            <div className="box box--sm box--alt">
              <Contact {...contact}/>
            </div>
          )}
          <div className={`${baseClass}__cta`}>
            {this.ctaList}
          </div>

        </div>
    );
  }
}

FabContentInner.defaultProps = {
  heading:'',
  intro:'',
  links:[],
  copy:'',
  contact:null,
  notes:'',
  ctas:[]
};

FabContentInner.propTypes = {
  heading:PropTypes.string,
  intro:PropTypes.string,
  links:PropTypes.array,
  copy:PropTypes.string,
  contact:PropTypes.object,
  notes:PropTypes.string,
  ctas:PropTypes.array
};

module.exports = FabContentInner;
