import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/Icon';
import AlertModal from './AlertModal';

import Editable from '../Editable/Editable';

class Alert extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      visible:true
    };

    this._onClose = this._onClose.bind(this);
  }

  _onClose (e) {
    this.props.onClose(e);
  }

  componentDidMount () {
  }

  render () {
    const cssClass = this.state.visible ? 'alert active' : 'alert';

    const {icon, children,isContactActive} = this.props;

    const hasContactClass = isContactActive ? '' : 'alert--noContact';

    return (
        <div className={`${cssClass} ${hasContactClass}`}>
          <AlertModal />
          <span className="close" onClick={this._onClose}><Icon icon="close" /></span>
          <span className="alert__icon">
            <Editable className="alert__icon__inner" html={icon} />
          </span>

          <div className="alert__inner">
            {children}
          </div>
        </div>
    );
  }
}

Alert.propTypes = {
  onClose: PropTypes.func,
  children: PropTypes.any,
  icon:PropTypes.string,
  isContactActive:PropTypes.bool
};

module.exports = Alert;
