import React from 'react';
import PropTypes from 'prop-types';

import './FloatingActionButton.scss';

import {Scrollbars} from 'react-custom-scrollbars';
import Overlay from '../Overlay/Overlay';
import MediaQuery from 'react-responsive';

import SitecoreSettings from '../SitecoreSettings';

import EditableParagraph from '../Editable/EditableParagraph';
import Editable from '../Editable/Editable';
import EditableImage from '../Editable/EditableImage';
import Cta from '../Cta/Cta';

import FabContent from './FabContent';
import FabContentInner from './FabContentInner';
import Alert from './Alert';

/** FloatingActionButton
  * @description description
  * @xexample
  ReactDOM.render(React.createElement(Components.FloatingActionButton, {
    id : 'id',
    theme : {}
  }), document.getElementById("element"));
*/
class FloatingActionButton extends React.Component {
  constructor (props) {
    super(props);

    const alertOpen = props.alert !== undefined;
    const alertDismissed = (!SERVER) ? this.alertDismissed() : true;

    let hasAlert = false;

    if (this.props.alert && this.props.alert.isEnabled && !alertDismissed) {
      hasAlert = true;
    }

    this.state = {
      contentOpen: false,
      contentVisible:false,
      buttonOpen: true,
      autoClosed: false,
      alertOpen,
      hasAlert,
      isEditing:false
    };

    this._click = this._click.bind(this);
    this._mouseOver = this._mouseOver.bind(this);
    this._mouseLeave = this._mouseLeave.bind(this);
    this._handleScroll = this._handleScroll.bind(this);
    this._closeOverlay = this._closeOverlay.bind(this);
    this._closePopup = this._closePopup.bind(this);
    this._openPopup = this._openPopup.bind(this);
    this._closeAlert = this._closeAlert.bind(this);
  }

  _click (e) {
    e.preventDefault();

    this.saveAlertDismissed();

    if(!this.state.contentOpen) {
      this._openPopup();
    }
    else{
      this._closePopup();
    }
  }

  _mouseOver () {
    this.setState({
      // buttonOpen:true
    });
  }

  _mouseLeave () {
    if (this.state.autoClosed) {
      this.setState({
        buttonOpen: false
      });
    }
  }

  saveAlertDismissed () {
    if(!this.state.isEditing) {
      this.setCookie(`alertDismissed-${this.props.alert.name}`, true);
    }
  }

  alertDismissed () {
    return !!this.getCookie(`alertDismissed-${this.props.alert.name}`);
  }

  setCookie (cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = `expires=${d.toUTCString()}`;
    document.cookie = `${cname}=${cvalue};${expires};path=/`;
  }

  getCookie (cname) {
    const name = `${cname}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }

  _handleScroll () {
    const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;

    if (scrollTop > 0) {
      this.setState({
        buttonOpen: false,
        autoClosed: true
      });
    }
    else {
      this.setState({
        buttonOpen: true,
        autoClosed: false
      });
    }
  }

  _closeOverlay () {
    this.setState({
      contentOpen: false
    });
  }

  _closePopup () {
    this.setState({
      contentVisible:false
    });

    setTimeout(() => {
      this.setState({
        contentOpen: false
      });
    },300);
  }

  _openPopup () {
    this.setState({
      contentOpen: true,
      alertOpen: false
    });

    setTimeout(() => {
      this.setState({
        contentVisible:true
      });
    },50);
  }

  _closeAlert () {
    this.saveAlertDismissed();

    this.setState({
      alertOpen: false
    });
  }

  componentDidMount () {
    if(!SERVER) {
      document.addEventListener('scroll', this._handleScroll);
    }
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  render () {
    const {contentOpen,contentVisible,hasAlert} = this.state;
    const {heart, alert,contactContent,isEnabled,scriptBase} = this.props;

    const buttonOpen = this.state.buttonOpen;
    const alertOpen = this.state.alertOpen;

    let btnClassName = 'btn--contactUs btn btn--withIcon';

    if (buttonOpen) {
      btnClassName += ' active';
    }
    

    return (
      <div className="floatingActionButton">
        <div className="container">
          <div className="floatingElement">

            {isEnabled && (
            <FabContent isOpen={contentOpen} isVisible={contentVisible} onClose={this._closePopup}>
              <Scrollbars
                renderTrackVertical={props => <div {...props} className="track-vertical" />}
                renderThumbVertical={props => <div {...props} className="thumb-vertical" />}
                style={{width: '100%', height: '100%'}}
                universal={true}>
                <FabContentInner {...contactContent} />
              </Scrollbars>
            </FabContent>
            )}

            {isEnabled && (
            <MediaQuery query="(max-width: 1024px)">
              <Overlay
                isOpen={this.state.contentOpen}
                from='right'
                hasHeader={false}
                width='100%'
                onRequestClose={this._closeOverlay}
                className='overlay overlay--contactUs'>

                <FabContentInner {...contactContent} />

              </Overlay>
            </MediaQuery>
            )}

            {hasAlert && alertOpen && (
              <Alert onClose={this._closeAlert} icon={this.props.alert.icon} isContactActive={isEnabled}>
                <Editable tag="h2" className="alert__title" html= {alert.title} />
                <EditableParagraph html= {alert.copy} />
                <Cta {...alert.cta} type="secondary" isWide={true} hasReducedPadding={true} rawCta={alert.cta}/>
              </Alert>
            )}

            {isEnabled && (
            <a href="#" onClick={this._click} onMouseOver={this._mouseOver} onMouseLeave={this._mouseLeave} className={btnClassName}>
              <Editable className="text" html={this.props.copy} />
              <EditableImage className="icon-wrap" image={`${scriptBase}${heart}`} defaultClass="" />
            </a>
            )}
          </div>
        </div>
      </div>
    );
  }
}

FloatingActionButton.defaultProps = {
  heart: 'images/lv_heart.png',
  scriptBase:'/dist/',
  isEnabled:true
};

FloatingActionButton.propTypes = {
  heart : PropTypes.string,
  alert: PropTypes.object,
  copy: PropTypes.string,
  contactContent: PropTypes.object,
  isEnabled:PropTypes.bool,
  scriptBase:PropTypes.string
};

module.exports = FloatingActionButton;
