import React from 'react';
import PropTypes from 'prop-types';

import './OpvMasterPage.scss';


class OpvMasterPage extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {theme} = this.props;
    const baseClass = 'opv-master';
    let themeClass = '';
    if (theme.themeName) {
      themeClass = `theme-${theme.themeName}`;
    }
    return (
      <div className={`${baseClass} ${themeClass}`}>
        <div className={`${baseClass}__container container`}>
          <div className={`${baseClass}__main`}>
            <div className={`${baseClass}__left-content`}>
              <div id="opv-left-nav"></div>

            </div>
            <div className={`${baseClass}__right-content`}>
                <div id="opv-right-nav"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


OpvMasterPage.propTypes = {
  theme: PropTypes.object
};

module.exports = OpvMasterPage;
