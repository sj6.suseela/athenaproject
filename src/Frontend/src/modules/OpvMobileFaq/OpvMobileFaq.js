import React from 'react';
import PropTypes from 'prop-types';
import './OpvMobileFaq.scss';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Overlay from '../Overlay/Overlay';
import MediaQuery from 'react-responsive';

class OpvMobileFaq extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      clicked: false,
      open:false,
      isSearchOverlayOpen:false
    };
    this.statePushed = false;
    this._click = this._click.bind(this);
    this._handleClose = this._handleClose.bind(this);
    // this._open = this._open.bind(this);

    this._closeSearchOverlay = this._closeSearchOverlay.bind(this);
    this._clickClose = this._clickClose.bind(this);
    this._mobFaqClick = this.__mobFaqClick.bind(this);

    this.links = [];
    if (this.props.links) {
      this.links =  this.props.links.map(link => {
        return (<li key={link.href}><a href={link.href}><Editable html={link.label} /></a></li>);
      });
    }
  }
  _click (e) {
    e.preventDefault();
    e.stopPropagation();
    this.statePushed = true;
    this._open();
  }

  _handleClose (e) {
    e.preventDefault();
    this.setState({clicked: false});
  }
  _open () {
    if (!SERVER) {
      // set body width to prevent it jumping when the scroll bar disappears
      const body = document.body;
      const scrollWidth = window.innerWidth - body.offsetWidth;
      body.style.width = `calc(100% - ${scrollWidth}px)`;
    }
    this.setState({
      isSearchOverlayOpen:true
    });
  }
  _clickClose () {
    this._closeSearchOverlay();
  }
  _closeSearchOverlay () {
    if (!SERVER) {
      // reset body width
      document.body.style.width = '100%';
    }

    this.setState({
      isSearchOverlayOpen:false
    });
  }
  __mobFaqClick (e) {
    e.preventDefault();
    this.state.clicked ? this.setState({clicked: false}) : this.setState({clicked: true});
  }

  render () {
    const {text, title, content, cta, faqContent, faqTxt} = this.props;
    const baseClass = 'opv-mobile-faq';
    const className = this.state.clicked ? 'on_faq' : 'off_faq';

    return (
      <MediaQuery query="(max-width: 768px)">
          <div className={`${baseClass}`} onClick={this._click}>
            <div className={`${baseClass}__help-cta`}>
              <EditableDiv
              className="copy"
                html={text}
              />
              <span className="hamburger-ico"><span></span><span></span><span></span></span>
            </div>
            <div className={`${baseClass}__main`}>
              <Overlay
                isOpen={ this.state.isSearchOverlayOpen }
                from='right'
                hasHeader={false}
                width='100%'
                className='overlay overlay--search opv-m-faqcont'
                onRequestClose={this._clickClose}>
                  <EditableDiv
                      className={`${baseClass}__subhead`}
                      html={text}
                    />
                  <div className={`${baseClass}__links`}>
                    <ul>
                      {this.links}
                    </ul>
                  </div>
                  <div className={`${baseClass}__content`}>

                    <Editable tag="h2" className={`${baseClass}__title`} html={title} />

                    <EditableDiv
                      className={`${baseClass}__copy`}
                      html={content}
                    />

                    <div className={`${baseClass}__footer`}>
                      <div className="cta cta--secondary cta--sm cta-faq" onClick={this._mobFaqClick} id="faq-note">
                        <Editable html={cta} />
                        <Editable html={faqTxt} className={`${baseClass}__faqTxt`}/>
                      </div>
                      <Overlay
                        isOpen={ this.state.clicked }
                        className='overlay overlay--faq'
                        onRequestClose={this._handleClose}>
                          <div className="faqNoteLink">
                            <div id="tooltip_faq" className={`${className} top_faq`}>
                                <div className="tooltip-inner_faq">
                                    <span className="close-cta" onClick={this._click}>X</span>
                                    <EditableDiv className="faqNoteContent" html={faqContent} />
                                </div>
                            </div>
                          </div>
                        </Overlay>

                    </div>
                  </div>
                </Overlay>

              </div>
            </div>
      </MediaQuery>
    );
  }
}


OpvMobileFaq.propTypes = {
  text: PropTypes.string,
  title: PropTypes.string,
  content: PropTypes.string,
  cta: PropTypes.string,
  links: PropTypes.array,
  faqTxt: PropTypes.string,
  faqContent: PropTypes.object
};

module.exports = OpvMobileFaq;
