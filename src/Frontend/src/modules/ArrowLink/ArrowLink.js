import React from 'react';
import PropTypes from 'prop-types';

import './ArrowLink.scss';
import Editable from '../Editable/Editable';
import Icon from '../Icon/Icon';

class ArrowLink extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

    };
  }

  render () {
    const {link,className,subText,isExternal} = this.props;

    if(!link) {
      return ('');
    }

    const iconColor = '#04589b';

    const cssclass = `arrowlink ${className} links`;

    return (
     <div className={cssclass}>
        <Icon icon="rightarrow" fillColor={iconColor}/>
        <div className='arrowlink__link-container'>
          <Editable className="arrowlink__link" html={link} />
          {isExternal && (
            <Icon icon='externalLink' />
          )}
        </div>
        <Editable className="arrowlink__subText" html={subText} />
      </div>
    );
  }
}

ArrowLink.defaultProps = {
  className:''
};

ArrowLink.propTypes = {
  link:PropTypes.string,
  className:PropTypes.string,
  subText:PropTypes.string,
  isExternal:PropTypes.bool
};

module.exports = ArrowLink;
