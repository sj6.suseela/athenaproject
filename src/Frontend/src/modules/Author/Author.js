import React from 'react';
import PropTypes from 'prop-types';

import './Author.scss';

import Editable from '../Editable/Editable';

/** Author
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Author, {
    id : 'id',
    title : 'From the desk of:',
    name : 'Name Surname',
    nameLink : '<a href="#">Name Surname</a>',
    nameLinkSrc : '#',
    position : 'Senior Officer LV=',
    twitterUrl : '<a target="_blank" href="http://www.twitter.com/handlehere">@twitter_handle</a>'
  }), document.getElementById("element"));
*/
class Author extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };
  }

  render () {
    const {title, name, position,twitterUrl,nameLink} = this.props;

    const theName = nameLink || name;

    return (
      <div className="module author">
        <div className="container author__container">
          <dl>
            <dt><Editable html={title}/></dt>
            <dd><Editable html={theName}/>

            </dd>
            <dd><Editable html={position}/></dd>
            <dd><Editable html={twitterUrl}/></dd>
          </dl>
        </div>
      </div>
    );
  }
}

Author.defaultProps = {
  title: '',
  name: '',
  position: ''
};

Author.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string,
  position: PropTypes.string,
  twitterUrl:PropTypes.string,
  nameLink:PropTypes.string,
  nameLinkSrc:PropTypes.string
};

module.exports = Author;
