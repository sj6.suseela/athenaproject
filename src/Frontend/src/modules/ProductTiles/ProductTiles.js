import React from 'react';
import PropTypes from 'prop-types';

import Cta from '../Cta/Cta';
import Editable from '../Editable/Editable';
import EditableImage from '../Editable/EditableImage';
import EditButton from '../CMS/EditButton';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';

import './ProductTiles.scss';

/** ProductTiles
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ProductTiles, {
    id : 'module-ProductTiles',
    theme : {
      themeName:'warm-grey',
    },
    items : [
      {
        heading : 'Home insurance',
        icon : '<img src="dist/images/cms/icon-decreasing-term.png" />',
        cta:  {type: "primary", link: "<a href='#'>More info</a>"},
        href:'#',
        extralink: '<a href="#">Existing customer?</a>',
      },
      {
        heading : 'Home insurance',
        icon : '<img src="dist/images/cms/icon-decreasing-term.png" />',
        cta:  {type: "primary", link: "<a href='#'>More info</a>"},
        href:'#',
        extralink: '<a href="#">Existing customer?</a>',
      },
      {
        heading : 'Home insurance',
        icon : '<img src="dist/images/cms/icon-decreasing-term.png" />',
        cta:  {type: "primary", link: "<a href='#'>More info</a>"},
        href:'#',
        extralink: '<a href="#">Existing customer?</a>',
      },
      {
        heading : 'Home insurance',
        icon : '<img src="dist/images/cms/icon-decreasing-term.png" />',
        cta:  {type: "primary", link: "<a href='#'>More info</a>"},
        href:'#',
        extralink: '<a href="#">Existing customer?</a>',
      },
      {
        heading : 'Home insurance',
        icon : '<img src="dist/images/cms/icon-decreasing-term.png" />',
        cta:  {type: "primary", link: "<a href='#'>More info</a>"},
        href:'#',
        extralink: '<a href="#">Existing customer?</a>',
      }
    ]
  }), document.getElementById("element"));
*/
class ProductTiles extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };

    this._toggleProducts = this._toggleProducts.bind(this);

    this.items = props.items.map((item,i) => {
      let link = {};
      if (item.link) {
        link = item.link;
      }
      return(
        <div key={props.id + i + item.heading} className="producttiles__item">
          <div className="producttiles__item__inner">
            <EditButton isSmall={true} align="right" html={item.editButton} />
            {!!link.href && (
              <a className="producttiles__item__top" id={link.id} rel={link.rel} href={link.href} title={item.heading}>
                <Editable tag="h3" className="producttiles__item__heading" html={item.heading} />
                <EditableImage className="producttiles__item__icon" image={item.icon} />
              </a>
            )}
            {!link.href && (
              <span className="producttiles__item__top producttiles__item__top--no-link">
                <Editable tag="h3" className="producttiles__item__heading" html={item.heading} />
                <EditableImage className="producttiles__item__icon" image={item.icon} />
              </span>
            )}
            <div className="producttiles__item__bottom">
              {item.cta && (
                <Cta {...item.cta} isSmall={true} isWide={true} rawCta={item.cta.link} />
              )}
              {item.cta2 && (
                <Cta {...item.cta2} isSmall={true} isWide={true} rawCta={item.cta2.link} />
              )}
              <Editable tag="span" className="producttiles__item__link links" html={item.extralink} />
              <Editable tag="span" className="producttiles__item__link links" html={item.extralink2} />
            </div>
          </div>
        </div>
      );
    });


    if (this.props.hasLargeFirstRow) {
      this.largeTiles = this.items.slice(0,2);
      this.otherTiles = this.items.slice(2);
    }
    else {
      this.otherTiles = this.items;
    }
  }

  _toggleProducts (e) {
    e.preventDefault();
    this.setState({
      showMoreProducts:!this.state.showMoreProducts
    });
  }

  

  render () {
    // get the prop settings
    const {isOverlappingPreviousElement, hasLargeFirstRow} = this.props;

    // base css class
    const baseClass = 'producttiles';

    let classes = '';
    if (isOverlappingPreviousElement) {
      classes = `${baseClass}--overlap`;
    }
    if (hasLargeFirstRow) {
      classes += ` ${baseClass}--largeFirstRow`;
    }

    return (
      <ModuleWrapper {...this.props} classes={classes} baseClass={baseClass} isProduct={true}>
        {this.largeTiles && this.largeTiles.length > 0 && (
          <div className="producttiles__items producttiles__items--wide">
             {this.largeTiles}
          </div>
        )}
       
        {this.otherTiles.length > 0 && (
          <div className="producttiles__items">
             {this.otherTiles}
          </div>
        )}
        
      </ModuleWrapper>
    );
  }
}

ProductTiles.defaultProps = {
  id:'producttiles',
  items:[]
};

ProductTiles.propTypes = {
  theme: PropTypes.object,
  items: PropTypes.array,
  id:PropTypes.string,
  isOverlappingPreviousElement: PropTypes.bool,
  isWide: PropTypes.bool,
  hasLargeFirstRow: PropTypes.bool
};

module.exports = ProductTiles;
