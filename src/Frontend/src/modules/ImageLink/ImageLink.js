import React from 'react';
import PropTypes from 'prop-types';

import './ImageLink.scss';
import Editable from '../Editable/Editable';
import EditableImage from '../Editable/EditableImage';

class ImageLink extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

    };
  }

  render () {
    const {link,image,className} = this.props;

    const cssclass = `imageLink ${className}`;

    return (
		<div className={cssclass}>
          <a href={link.href} id={link.id}><EditableImage className="imageLink__image" image={image} /></a>
          <Editable className="imageLink__link" html={link.html} />
        </div>
    );
  }
}

ImageLink.defaultProps = {
  link:{}
};

ImageLink.propTypes = {
  link:PropTypes.object,
  image:PropTypes.string,
  className:PropTypes.string
};

module.exports = ImageLink;
