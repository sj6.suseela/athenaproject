import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import Contact from '../Contact/Contact';
import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';

import './ContactQuick.scss';

/** ContactQuick
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ContactQuick, {
    id : 'id',
    theme : {},
    heading : 'Our 24 hour emergency helpline for medical emergencies and assistance',
    contact : {
      tel : '0800 032 2844',
      telAlt : '+44 1243 621 537',
      telAltIntro : 'From abroad:',
      opening : 'Lines open <b>24 hours</b>, 7 days a week',
      info : 'Calls may be recorded and/or monitored for training and audit purposes.'
    },
    image : '<img src="dist/images/cms/flourish-insurance.svg" />'
  }), document.getElementById("element"));
*/
class ContactQuick extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading, contact, image} = this.props;
    const baseClass = 'contact-quick';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass}>
        <Editable tag="h2" className={`title ${baseClass}__title`}  html={heading} />
        <div className="columns@md">
          <div>
            {contact && (
              <div className="box box--alt box--sm">
                <Contact {...contact} />
              </div>
            )}
          </div>
          <div>
            <EditableImage image={image} />
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

ContactQuick.defaultProps = {
  heading : '',
  contact : null,
  image : ''
};

ContactQuick.propTypes = {
  heading : PropTypes.string,
  contact : PropTypes.object,
  image : PropTypes.string
};

module.exports = ContactQuick;
