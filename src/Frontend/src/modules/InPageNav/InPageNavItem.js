import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/Icon';
import SubnavPanel from '../SubnavPanel/SubnavPanel';
import Editable from '../Editable/Editable';

import {isBrowser} from 'react-device-detect';

class InPageNavItem extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isForcedOpen:false,
      isSubNavVisible:false
    };

    this._onClick = this._onClick.bind(this);
    this._onMouseOver = this._onMouseOver.bind(this);
    this._onMouseLeave = this._onMouseLeave.bind(this);
    this._open = this._open.bind(this);
    this._close = this._close.bind(this);
    this._handleClickOutside = this._handleClickOutside.bind(this);
  }

  componentDidMount () {
    if(this.props.children && this.props.children.length > 0) {
      document.addEventListener('touchstart', this._handleClickOutside);
    }
  }

  _handleClickOutside (e) {
    if (this.navitemref && !this.navitemref.contains(e.target)) {
      this.setState({isSubNavVisible:false,isSubNavOpen:false,isForcedOpen:false});
    }
  }

  _onClick (e) {
    const hasChildren = this.props.children && this.props.children.length;
    const isSublink = e.nativeEvent.target.className === 'subnav__sublink';

    if(hasChildren && !isSublink) {
      e.preventDefault();

      if(this.state.isSubNavOpen) {
        this.setState({isSubNavVisible:false,isSubNavOpen:false,isForcedOpen: false});
      }
      else{
        this.setState({isSubNavVisible:true,isSubNavOpen:true,isForcedOpen: true});
      }
    }
  }

  _onMouseOver (e) {
    e.stopPropagation();

    if(isBrowser) {
      this._open(e);
    }
  }

  _onMouseLeave (e) {
    if(isBrowser) {
      this._close(e);
    }
  }

  _open (e) {
    const isSublink = e.nativeEvent.target.className === 'nav--desktop__sublink';

    if(e.nativeEvent.target.localName === 'span' && !isSublink) {
      const posX = e.nativeEvent.target.parentElement.parentElement.offsetLeft;
      const width = e.nativeEvent.target.offsetWidth;

      this.props.onMouseOver({
        posX,
        width
      });
    }

    if(this.props.children) {
      const _this = this;
      this.setState({isSubNavOpen: true});
      setTimeout(() => {
        _this.setState({isSubNavVisible:true});
      },0);
    }
  }

  _close (e) {
    if(e.nativeEvent.target.localName === 'li') {
      if(typeof(this.props.onMouseOut()) === 'function') {
        this.props.onMouseOut();
      }
    }

    this.setState({isSubNavVisible:false,isSubNavOpen:false});
  }

  render () {
    const {label,children,isSelected,link} = this.props;

    const hasChildren = children && children.length > 0;

    const {isForcedOpen} = this.state;

    const iconColor = '#ffffff';

    const showSubnav = hasChildren && this.state.isSubNavOpen;

    let cssClass = isSelected ? 'selected' : '';
    showSubnav ? cssClass += ' active' : '';

    isForcedOpen ? cssClass += ' active' : '';

    if(isSelected) {
      return (
          <li className={cssClass} key={ label} onMouseOver={this._onMouseOver} onMouseLeave={this._onMouseLeave} onClick={this._onClick}  ref={(c) => { this.navitemref = c;}}>
            <a ref={(c) => {this.selected = c;}} id={link.id} href={link.href} rel={link.rel}>
              <Editable html={label} />
              {hasChildren && (
                <Icon icon="downarrow" fillColor={iconColor}/>
              )}
            </a>
          </li>
      );
    }
    else{
      return (
          <li className={cssClass} key={label}  onMouseOver={this._onMouseOver} onMouseLeave={this._onMouseLeave} onClick={this._onClick}  ref={(c) => { this.navitemref = c;}}>
            <a id={link.id} href={link.href} rel={link.rel}>
              <Editable html={label} />

              {hasChildren && (
                <Icon icon="downarrow" fillColor={iconColor}/>
              )}
            </a>

            {hasChildren && (
              <SubnavPanel isVisible={showSubnav} isForcedOpen={this.state.isForcedOpen} items={children}/>
            )}
          </li>
      );
    }
  }
}

InPageNavItem.defaultProps = {
  link: {}
};

InPageNavItem.propTypes = {
  children:PropTypes.array,
  onMouseOver:PropTypes.func,
  onMouseOut:PropTypes.func,
  label:PropTypes.string,
  link:PropTypes.object,
  isSelected:PropTypes.bool,
  type:PropTypes.string
};

module.exports = InPageNavItem;
