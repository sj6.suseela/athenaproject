import React from 'react';
import PropTypes from 'prop-types';

import './InPageNav.scss';
import ContentExpander from '../ContentExpander/ContentExpander';
import MediaQuery from 'react-responsive';
import Icon from '../Icon/Icon';
import Editable from '../Editable/Editable';

import InPageNavItem from './InPageNavItem';

/** InPageNav
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.InPageNav, {
    id : 'id',
    theme : {},
    mobileLabel : "All articles",
    cssClass : "articleHubNav",
    hasMarker : true,
    items : [
      { label:'Most loved', link:{href:'#/category1'}, },
      { label:'Driving', link:{href:'#/category2'} },
      { label:'Home', link:{href:'#/category3'} },
      { label:'Family &amp; Pet', link:{href:'#/category4'} },
      { label:'Travel', link:{href:'#/category6'} },
      { label:'Retirement', link:{href:'#/category5'}, selected:true },
      { label:'Health &amp; Lifestyle', link:{href:'#/category7'} },
      { label:'Money', link:{href:'#/category8'} },
      { label:'News', link:{href:'#/category9'} }
    ]
  }), document.getElementById("element"));
*/
class InPageNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      navOpen:false,
      markerPos:null,
      markerWidth:null,
      markerVisible:false,
      defaultMarkerPos:null,
      defaultMarkerWidth:null,
      markerClass:'',
      hasSelected:null,
      navMouseOver:false
    };

    this._toggleNav = this._toggleNav.bind(this);
    this._closeNav = this._closeNav.bind(this);

    this._onMouseOver = this._onMouseOver.bind(this);
    this._onMouseLeave = this._onMouseLeave.bind(this);

    this._onMouseOverNav = this._onMouseOverNav.bind(this);
    this._onMouseOutNav = this._onMouseOutNav.bind(this);
  }

  componentDidMount () {
    if(this.selected) {
      const posX = this.selected.offsetLeft;
      const width = this.selected.offsetWidth;

      this.setState({
        markerPos:posX,
        markerWidth:width,
        markerClass:'visible animate selected',
        defaultMarkerPos:posX,
        defaultMarkerWidth:width,
        hasSelected:true
      });
    }
  }

  _onMouseOverNav () {
    const _this = this;
    setTimeout(() => {
      _this.setState({
        navMouseOver:true
      });
    },100);
  }

  _onMouseOutNav () {
    if(this.state.defaultMarkerPos !== null) {
      this.setState({
        navMouseOver:false,
        markerPos:this.state.defaultMarkerPos,
        markerWidth:this.state.defaultMarkerWidth,
        markerClass:'hover animate visible'
      });
    }
    else{
      this.setState({
        navMouseOver:false,
        markerClass:''
      });
    }
  }

  _onMouseOver (e) {
    const _this = this;
    if(e) {
      if(this.state.navMouseOver) {
        this.setState({
          markerPos:e.posX,
          markerWidth:e.width,
          markerClass:'hover visible animate'
        });
      }
      else{
        if(this.state.defaultMarkerPos !== null) {
          this.setState({
            markerPos:e.posX,
            markerWidth:e.width,
            markerClass:'hover animate invisible'
          });

          setTimeout(() => {
            _this.setState({
              markerClass:'hover animate visible'
            });
          },10);
        }
        else{
          this.setState({
            markerPos:e.posX,
            markerWidth:e.width,
            markerClass:'hover invisible'
          });

          setTimeout(() => {
            _this.setState({
              markerClass:'hover visible'
            });
          },10);
        }
      }
    }
  }

  _onMouseLeave () {
  }

  _toggleNav (e) {
    e.preventDefault();
    this.setState({
      navOpen:!this.state.navOpen
    });
  }

  _closeNav () {
    this.setState({
      navOpen:false
    });
  }

  render () {
    const {mobileLabel,cssClass,isFillScreen,hasClose} = this.props;

    const {markerPos,markerWidth} = this.state;

    const markerClass = `inPageNav__marker ${this.state.markerClass}`;

    const markerStyle = {
      left:markerPos,
      width:markerWidth
    };

    let triggerClass = 'inPageNav__trigger mobileOnly';

    const specificCssClass = `${cssClass} inPageNav`;

    if(this.state.navOpen) triggerClass += ' active';

    const items = this.props.items.map((item,i) => {
      if(item.type === 'heading') {
        return null;
      }
      if(!item.label) {
        return null;
      }
      return <InPageNavItem key={item + i} isSelected={item.selected} label={item.label} link={item.link} onMouseOver={this._onMouseOver} onMouseLeave={this._onMouseLeave} onClick={this._onClick}>{item.children}</InPageNavItem>;
    });

    const mobileItems = this.props.items.map((item,i) => {
      const label = (item.mobileLabel && item.mobileLabel.length > 0) ? item.mobileLabel : item.label;
      const link = item.link ? item.link : {};

      if(item.children && item.children.length > 0) {
        return item.children.map((item,i) => {
          const label = (item.mobileLabel && item.mobileLabel.length > 0) ? item.mobileLabel : item.label;
          const link = item.link ? item.link : {};
          return (
              <li key={item + i}>
                <a href={link.href}><Icon icon="rightarrow" fillColor="#46616e"/><Editable html={label} /></a>
              </li>
          );
        });
      }
      else{
        if(item.type === 'heading') {
          return (<li className="contentExpander__heading" key={item + i}><Editable html={label} /></li>);
        }
        else if(item.type === 'category') {
          return (<li className="contentExpander__category" key={item + i}><a id={link.id} href={link.href} rel={link.rel}><Icon icon="rightarrow" fillColor="#46616e"/><Editable html={label} /></a></li>);
        }
        else {
          return (<li key={item + i}><a id={link.id} href={link.href} rel={link.rel}><Icon icon="rightarrow" fillColor="#46616e"/><Editable html={label} /></a></li>);
        }
      }
    });

    return (
      <div className={`${specificCssClass}`}>
        <div className="container">
          <div className={triggerClass} ref={(c) => {this.trigger = c; }}>
            <a href="#" onClick={this._toggleNav} >
              <Editable html={mobileLabel} />
              <Icon icon="downarrow" fillColor="#04589b"/>
            </a>
          </div>
          <nav className="desktopOnly" onMouseEnter={this._onMouseOverNav} onMouseLeave={this._onMouseOutNav}>
            <span style={markerStyle} className={markerClass}></span>
            <ul>
              {items}
            </ul>
          </nav>
        </div>
        <MediaQuery query="(max-width: 1024px)">
          <ContentExpander isOpen={this.state.navOpen} isFillScreen={isFillScreen} hasClose={hasClose} onClose={this._closeNav} trigger={this.trigger}>
              <div className="container">
                <nav>
                  <ul>
                    {mobileItems}
                  </ul>
                </nav>
              </div>
          </ContentExpander>
        </MediaQuery>
      </div>
    );
  }
}

InPageNav.defaultProps = {
  items:[],
  isFillScreen:false,
  hasClose:false
};

InPageNav.propTypes = {
  isFillScreen:PropTypes.bool,
  hasClose:PropTypes.bool,
  items:PropTypes.array,
  mobileLabel:PropTypes.string,
  cssClass:PropTypes.string
};

module.exports = InPageNav;
