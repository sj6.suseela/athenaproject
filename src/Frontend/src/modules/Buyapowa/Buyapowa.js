/* global Buyapowa */
import React from 'react';
import PropTypes from 'prop-types';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';

import SitecoreSettings from '../SitecoreSettings';

import './Buyapowa.scss';

class BuyapowaModule extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing: false
    };
  }

  componentDidMount () {
    const sc = new SitecoreSettings();
    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }


    const {subdomain, id, slug} = this.props;
    if (id && subdomain && slug) {
      const buyapowa = new Buyapowa(`https://${subdomain}.co-buying.com/`, `${id}`);
      buyapowa.embedReferAFriend({
        'campaign': `${slug}`
      });
    }
  }

  render () {
    const {id} = this.props;
    const baseClass = 'buyapowa';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div id={id} style={{minHeight: '700px'}} >
        </div>
        {this.state.isEditing && (
          <div className={`${baseClass}__overlay`}>
          </div>
        )}
      </ModuleWrapper>
    );
  }
}

BuyapowaModule.defaultProps = {
};

BuyapowaModule.propTypes = {
  id: PropTypes.string,
  subdomain: PropTypes.string,
  slug: PropTypes.string
};

module.exports = BuyapowaModule;
