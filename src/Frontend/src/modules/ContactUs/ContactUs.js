import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableImage from '../Editable/EditableImage';
import EditableDiv from '../Editable/EditableDiv';
import ArrowLink from '../ArrowLink/ArrowLink';
import Contact from '../Contact/Contact';
import Icon from '../Icon/Icon';
import EditButton from '../CMS/EditButton';
import ScrollToY from '../scrolltoy';

import './ContactUs.scss';

/** ContactUs
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ContactUs, {
    id : 'id',
    theme : {},
    headline:'Let us help you get in touch with the right team',
    image: '<img src="http://placehold.it/200x150" />',
    items:[
        {
          title: 'Breakdown emergency',
          content: {
            contact:{
              telephone:'0800 707 6967',
              altHeading:'<b>From abroad:</b> +44 1243 621 537',
              subText:'Our <b>24 hour emergency helpline</b>  for medical emergencies and assistance'
            },
            copy:`<p>Have the following information ready</p>
                  <ul>
                    <li>Your policy number and the phone number your calling from more text more text more text</li><li>Your policy number and the phone number you're calling from</li><li>Your policy number and the phone number your calling from</li>
                    </ul>`
          },
          type:'featured'
        },
        {
          title: 'Home emergency',
          additional: 'For customers with home emergency cover',
          type:'featured'
        },
        {
          title: 'Asset allocation'
        },
        {
          title: 'Insurance',
          children: [
            {
              title: 'Car Insurance',
              content: {
                contact:{
                  telephone:'0800 707 6967',
                  altHeading:'<b>From abroad:</b> +44 1243 621 537',
                  subText:'Our <b>24 hour emergency helpline</b>  for medical emergencies and assistance'
                },
                links:[
                  {
                    link:'<a href="#">Make a claim</a>'
                  },
                  {
                    link:'<a href="#">Enquire about an existing claim</a>'
                  },
                  {
                    link:'<a href="#">Form name (PDF, 357KB)</a>',
                    isPDF:true,
                    subLink:'<a href="#">Death claim guidance notes (PDF,89KB)'
                  },
                  {
                    link:'<a href="#">Renew policy</a>'
                  },
                  {
                    link:'<a href="#">Request policy documents</a>',
                    subText:'Get copies of your insurance schedule or policy documents'
                  },
                  {
                    link:'<a href="#">Pre travel advice</a>'
                  },
                ]
              }
            },
            {
              title: 'Home Insurance',
              content: 'content home'
            },
            {
              title: 'Life Insurance',
              content: 'content life'
            },
            {
              title: 'Travel Insurance',
              content: {
                links:[
                  {
                    link:'<a href="contact-us--make-a-claim.html">Make a claim</a>'
                  }
                ]
              }
            },
            {
              title: 'Classic Car Insurance',
              content: 'content classic car'
            },
            {
              title: 'Caravan Insurance',
              content: 'content caravan'
            },
            {
              title: 'Motorbike Insurance',
              content: 'content motorbike'
            }
          ]
        },
        {
          title: 'Pensions and Retirement'
        },
        {
          title: 'Investments'
        },
        {
          title: 'General Enquiries'
        },
        {
          title: 'Complaints'
        }
      ]
  }), document.getElementById("element"));
*/
class ContactUs extends React.Component {
  constructor (props) {
    super(props);

    this._clickPrimary = this._clickPrimary.bind(this);
    this._clickSecondary = this._clickSecondary.bind(this);
    this._clickTertiary = this._clickTertiary.bind(this);
    this._reset = this._reset.bind(this);
    this._updateWindowDimensions = this._updateWindowDimensions.bind(this);

    this.primaryActiveItemIndex = null;
    this.secondaryActiveItemIndex = null;
    this.tertiaryActiveItemIndex = null;

    this.state = {
      stage:1,
      showIntro:true,
      mobileContainerHeight: 'auto',
      width:0,
      secondary: {
        active:'false',
        title:'',
        content:[],
        sections:[],
        headline:''
      },
      tertiary: {
        active:'false',
        title:'',
        content:[],
        sections:[],
        headline:''
      },
      quaternary:{
        active:'false',
        title:'',
        content:[],
        headline:''
      }
    };

    this.clickActive = true;
  }

  _updateWindowDimensions () {
    this.setState({width: window.innerWidth, height: window.innerHeight});
  }

  renderSections (sections,clickFunc,hasContent,stageCount) {
    const sectionCss = hasContent ? 'hasContent' : '';

    let stageActiveIndex = 0;

    if(stageCount === 0) {
      stageActiveIndex = this.primaryActiveItemIndex;
    }
    else if(stageCount === 1) {
      stageActiveIndex = this.secondaryActiveItemIndex;
    }
    else if(stageCount === 2) {
      stageActiveIndex = this.tertiaryActiveItemIndex;
    }


    return sections.map((section,sectionindex) => {
      const items = section.items.map((item,i) => {
        const html = `<span>${item.title}</span>${  item.additional ? ` <span>${item.additional}</span>` : ''}`;
        let type = item.type ? item.type : '';

        if(i === stageActiveIndex && sectionindex === 0) {
          type += ' selected';
        }

        return (
          <li tabIndex="0" key={i}
          onClick={(e) => {
            if(this.clickActive) {clickFunc(item, e, i);}
            else{e.preventDefault();}
          }}
          onKeyDown={(e) => {
            if(this.clickActive && (e.keyCode === 39 || e.keyCode === 13)) {
              clickFunc(item, e, i);
            }
          }}
          className={type}>
            <span>
              <ArrowLink link={html} />
            </span>
          </li>
        );
      });

      return (
        <div key={section.headline + sectionindex} className={`contact-us__section ${sectionCss}`}>
          <Editable tag="h4" className="contact-us__section__headline" html={section.headline} />
          <ul className="links contact-us__navlinks">
            {items}
          </ul>
        </div>
      );
    });
  }

  _clickPrimary (item, e, index) {
    e.preventDefault();
    this.primaryActiveItemIndex = index;
    this.secondaryActiveItemIndex = null;
    this.tertiaryActiveItemIndex = null;

    this.setState({
      stage:2,
      showIntro:false,
      secondary: {
        active:true,
        title:item.title,
        content:item.content,
        sections:item.sections,
        headline:item.headline
      },
      tertiary: {
        active:false,
        title:'',
        content:'',
        headline:'',
        sections:''
      }
    });

    const title = item.title.replace(/ /g, '-').toLowerCase();
    history.pushState({}, '', `#${title}`);

    const offset = 0;
    const element = document.querySelector('.contact-us');
    const yPos = element.getBoundingClientRect().top + (window.pageYOffset - offset);

    new ScrollToY(yPos, 500, 'easeInOutQuint');
  }

  _clickSecondary (item, e, index) {
    e.preventDefault();
    const category = this.state.secondary.title.replace(/ /g, '-').toLowerCase();
    const title = item.title.replace(/ /g, '-').toLowerCase();
    if(this.state.stage === 2) {
      history.pushState({}, '', `#${category}/${title}`);
    }
    else{
      history.replaceState({}, '', `#${category}/${title}`);
    }

    this.secondaryActiveItemIndex = index;
    this.tertiaryActiveItemIndex = null;

    this.setState({
      stage:3,
      tertiary: {
        active:true,
        title:item.title,
        content:item.content,
        sections:item.sections,
        headline:item.headline
      }
    });
  }

  _clickTertiary (item, e, index) {
    e.preventDefault();
    const parent = this.state.secondary.title.replace(/ /g, '-').toLowerCase();
    const category = this.state.tertiary.title.replace(/ /g, '-').toLowerCase();
    const title = item.title.replace(/ /g, '-').toLowerCase();
    if(this.state.stage === 3) {
      history.pushState({}, '', `#${parent}/${category}/${title}`);
    }
    else{
      history.replaceState({}, '', `#${parent}/${category}/${title}`);
    }

    this.tertiaryActiveItemIndex = index;

    this.setState({
      stage:4,
      quaternary: {
        active:true,
        title:item.title,
        content:item.content,
        headline:item.headline
      }
    });
  }

  _reset (stage) {
    const _this = this;

    this.clickActive = false;

    let currentStage = stage;


    if(typeof(stage) !== 'string') {
      if(this.state.stage === 1) {
        currentStage = 'primary';
      }
      if(this.state.stage === 2) {
        currentStage = 'secondary';
      }
      if(this.state.stage === 3) {
        currentStage = 'tertiary';
      }
      if(this.state.stage === 4) {
        currentStage = 'quaternary';
      }
    }

    let newStage = null;
    if(currentStage === 'quaternary') {
      newStage = 3;

      this.tertiaryActiveItemIndex = null;

      setTimeout(() => {
        if(this.state.width >= 1024) {
          _this.setState({
            quaternary:{
              active:false,
              title:'',
              content:''
            }
          },() => {
            _this.clickActive = true;
          });
        }
        else{
          _this.clickActive = true;
        }
      },500);
    }
    else if(currentStage === 'tertiary') {
      newStage = 2;

      this.secondaryActiveItemIndex = null;

      setTimeout(() => {
        if(this.state.width >= 1024) {
          _this.setState({
            tertiary:{
              active:false,
              title:'',
              content:''
            }
          },() => {
            _this.clickActive = true;
          });
        }
        else{
          _this.clickActive = true;
        }
      },500);
    }
    else if(currentStage === 'secondary') {
      newStage = 1;

      this.primaryActiveItemIndex = null;

      setTimeout(() => {
        _this.setState({
          showIntro:true
        });
        if(this.state.width >= 1024) {
          _this.setState({
            secondary:{
              active:'false',
              title:'',
              content:'',
              sections:[]
            }
          },() => {
            _this.clickActive = true;
          });
        }
        else{
          _this.clickActive = true;
        }
      },500);
    }

    this.setState({
      stage:newStage
    });

    if(newStage === 1) {
      history.replaceState({}, '', '#');
    }
    else if(newStage === 2) {
      const title = this.state.secondary.title.replace(/ /g, '-').toLowerCase();
      history.replaceState({}, '', `#${title}`);
    }
    else if(newStage === 3) {
      const category = this.state.secondary.title.replace(/ /g, '-').toLowerCase();
      const title = this.state.tertiary.title.replace(/ /g, '-').toLowerCase();
      history.replaceState({}, '', `#${category}/${title}`);
    }
  }


  componentDidMount () {
    window.addEventListener('popstate', this._reset);
    const _this = this;

    this._updateWindowDimensions();
    window.addEventListener('resize', this._updateWindowDimensions);

    const location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');

    let secondaryitem = {
      title:'',
      content:'',
      sections:[],
      headline:''
    };
    let tertiaryitem = {
      title:'',
      content:'',
      sections:[],
      headline:''
    };
    let quaternaryitem = {
      title:'',
      content:'',
      headline:''
    };
    let stage = 1;
    let secondaryActive,tertiaryActive,quaternaryActive = false;
    let showIntro = true;


    if(location[0] && location[0].length > 0) {
      for (const section of this.props.sections) {
        for (let i = 0;i < section.items.length;i++) {
          const item = section.items[i];
          const title = item.title.replace(/ /g, '-').toLowerCase();

          if(title === location[0]) {
            secondaryitem = item;
            stage = 2;
            secondaryActive = true;
            showIntro = false;
            _this.primaryActiveItemIndex = i;
          }
        }
      }

      if(location[1] && location[1].length > 0) {
        for (const section of secondaryitem.sections) {
          for (let i = 0;i < section.items.length;i++) {
            const item = section.items[i];
            const title = item.title.replace(/ /g, '-').toLowerCase();

            if(title === location[1]) {
              tertiaryitem = item;
              stage = 3;
              tertiaryActive = true;
              showIntro = false;
              this.secondaryActiveItemIndex = i;
            }
          }
        }
      }

      if(location[2] && location[2].length > 0) {
        for (const section of tertiaryitem.sections) {
          for (let i = 0;i < section.items.length;i++) {
            const item = section.items[i];
            const title = item.title.replace(/ /g, '-').toLowerCase();

            if(title === location[2]) {
              quaternaryitem = item;
              stage = 4;
              quaternaryActive = true;
              showIntro = false;
              this.tertiaryActiveItemIndex = i;
            }
          }
        }
      }


      this.setState({
        stage,
        secondary: {
          active:secondaryActive,
          title:secondaryitem.title,
          content:secondaryitem.content,
          sections:secondaryitem.sections,
          headline:secondaryitem.headline,
          activeItemIndex:2
        },
        tertiary: {
          active:tertiaryActive,
          title:tertiaryitem.title,
          content:tertiaryitem.content,
          sections:tertiaryitem.sections,
          headline:tertiaryitem.headline,
          activeItemIndex:2
        },
        quaternary:{
          active:quaternaryActive,
          title:quaternaryitem.title,
          content:quaternaryitem.content,
          headline:quaternaryitem.headline,
          activeItemIndex:2
        },
        showIntro
      });
    }
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this._updateWindowDimensions);
  }

  componentDidUpdate () {
    if (this.container && this.activePanel) {
      if (this.state.mobileContainerHeight !== this.activePanel.offsetHeight || this.state.mobileContainerHeight === 'auto') {
        this.setState({
          // this is overridden in CSS for desktop
          mobileContainerHeight: this.activePanel.offsetHeight
        });
      }
    }
  }

  mapContent (item,i) {
    let links = [];

    const additionalClasses = item.isImportant ? ' is-important' : '';

    if(item.type === 'copy') {
      return (
        <EditableDiv key={i} className={`contact-us__copy${additionalClasses}`} div html={item.copy} />
      );
    }
    else if(item.type === 'contact') {
      return (
        <div key={i} className={`box box--sm box--alt${additionalClasses}`}>
          <Contact {...item.contact}  isAlt={false} />
        </div>
      );
    }
    else if(item.type === 'links') {
      links = item.links.map((item,i) => {
        if(item.subLink) {
          if(item.isPDF) {
            return (
              <li key={item + i}>
                  <div className="downloadLink">
                    <Icon icon="pdf"/>
                    <Editable className="downloadLink__label" html={item.link}/>
                    <Editable className="downloadLink__subText" html={item.subText} />
                    <EditButton html={item.editButton} isSmall={true} />
                  </div>

                  <ArrowLink link={item.subLink}/>
              </li>
            );
          }
          else{
            return (
              <li key={item + i}>
                  <ArrowLink link={item.link}/>
                  <ArrowLink link={item.subLink}/>
                  <EditButton html={item.editButton} isSmall={true} />
              </li>
            );
          }
        }
        else{
          if(item.isPDF) {
            return(
              <li key={item + i}>
                  <div className="downloadLink">
                    <Icon icon="pdf"/>
                    <Editable className="downloadLink__label" html={item.link}/>
                    <Editable className="downloadLink__subText" html={item.subText} />
                    <EditButton html={item.editButton} isSmall={true} />
                  </div>
              </li>
            );
          }
          else{
            return (
              <li key={item + i}>
                  <ArrowLink link={item.link} subText={item.subText}/>
                  <EditButton html={item.editButton} isSmall={true} />
              </li>
            );
          }
        }
      });
    }

    return(
      <ul className={`links links--multiline contact-us__links${additionalClasses}`} key={i}>
        <h4 className="contact-us__section__headline">{item.headline}</h4>
        {links}
      </ul>
    );
  }

  render () {
    // get the prop settings
    const {headline,image,sectionsHeadline} = this.props;
    const baseClass = 'contact-us';

    let secondaryLinks = '';
    let secondaryContent = [];
    let hasSecondaryContent = false;


    this.primaryLinks = this.renderSections(this.props.sections,this._clickPrimary,null,0);

    if(this.state.secondary.active) {
      if(this.state.secondary.content && this.state.secondary.content.length > 0) {
        hasSecondaryContent = true;
        secondaryContent = this.state.secondary.content.map((item,i) => {
          return (this.mapContent(item,i));
        });
      }

      if(this.state.secondary.sections) {
        secondaryLinks = this.renderSections(this.state.secondary.sections,this._clickSecondary,hasSecondaryContent,1);
      }
    }

    let tertiaryLinks = '';
    let tertiaryContent = '';
    let hasTertiaryContent = false;

    if(this.state.tertiary.active) {
      if(this.state.tertiary.content && this.state.tertiary.content.length > 0) {
        hasTertiaryContent = true;
        tertiaryContent = this.state.tertiary.content.map((item,i) => {
          return (this.mapContent(item,i));
        });
      }

      if(this.state.tertiary.sections) {
        tertiaryLinks = this.renderSections(this.state.tertiary.sections,this._clickTertiary,hasTertiaryContent,2);
      }
    }


    let quaternaryContent = '';

    if(this.state.quaternary.active) {
      if(this.state.quaternary.content && this.state.quaternary.content.length > 0) {
        quaternaryContent = this.state.quaternary.content.map((item,i) => {
          return (this.mapContent(item,i));
        });
      }
    }

    const additionalClasses = `${baseClass}--stage${this.state.stage}`;

    return (
      <section className={`${baseClass} ${additionalClasses}`}>
        <div className={`container ${baseClass}__container`}>

          <div ref={(c) => { this.container = c;}} style={{height: this.state.mobileContainerHeight}} className={`${baseClass}__main`}>

            <div ref={(p) => {if (this.state.stage === 1) {this.activePanel = p;}}} className={`${baseClass}__primary`}>
              <Editable tag="h2" className="mobileOnly" html={headline} />
              <Editable tag="h2" className="contact-us__sectionHeading" html={sectionsHeadline} />
              {this.primaryLinks}
              <EditableImage className="mobileOnly" image={image.html} />
            </div>

            <div ref={(s) => {if (this.state.stage === 2) {this.activePanel = s;}}} className={`${baseClass}__secondary`}>

              {this.state.showIntro && (
                <div className={`${baseClass}__intro`}>
                  <Editable tag="h2" html={headline} />
                  <EditableImage image={image.html} />
                </div>
              )}

              <div className={`${baseClass}__secondaryitems`}>
                  <h3 className={`${baseClass}__backLink label`} onClick={() => this._reset('secondary')}><Icon icon="leftarrow" fillColor="#04589b"/><Editable html={this.state.secondary.title} /></h3>
                  <Editable className={`${baseClass}__headline`} tag="h2" html={this.state.secondary.headline} />
                  <div className="inner-items">
                  {secondaryLinks && (
                    <div className="links contact-us__navlinks">{secondaryLinks}</div>
                  )}
                  {secondaryContent}
                  </div>
              </div>


            </div>

            <div ref={(t) => {if (this.state.stage === 3) {this.activePanel = t;}}} className={`${baseClass}__tertiary`}>
              <div className={'inner'}>
                <h3 className={`${baseClass}__backLink label`}  onClick={() => this._reset('tertiary')}><Icon icon="leftarrow" fillColor="#04589b"/><Editable html={this.state.tertiary.title} /></h3>
                <Editable className={`${baseClass}__headline`} tag="h2" html={this.state.tertiary.headline} />
                <div className="inner-items">
                  {tertiaryLinks}
                  {tertiaryContent}
                  </div>
              </div>
            </div>

            <div ref={(q) => {if (this.state.stage === 4) {this.activePanel = q;}}} className={`${baseClass}__quaternary`}>
              <div className={'inner'}>
                <h3 className={`${baseClass}__backLink label`}  onClick={() => this._reset('quaternary')}><Icon icon="leftarrow" fillColor="#04589b"/><Editable html={this.state.quaternary.title} /></h3>
                <Editable className={`${baseClass}__headline`} tag="h2" html={this.state.quaternary.headline} />
                {quaternaryContent}
              </div>
            </div>

          </div>
        </div>
      </section>
    );
  }
}

ContactUs.defaultProps = {
  sections: []
};

ContactUs.propTypes = {
  sections: PropTypes.array.isRequired,
  image:PropTypes.object,
  headline:PropTypes.string,
  breadcrumbLabel:PropTypes.string,
  sectionsHeadline:PropTypes.string
};

module.exports = ContactUs;
