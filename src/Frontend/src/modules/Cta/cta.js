import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/Icon';
import Editable from '../Editable/Editable';

/** Cta
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Cta, {
    id : 'id',
    theme : {}
  }), document.getElementById("element"));
*/
class Cta extends React.Component {
  constructor (props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick (e) {
    if (this.props.onClick) {
      e.preventDefault();
      this.props.onClick(e);
    }
  }

  render () {
    const {
      label,
      href,
      type,
      cssclass,
      noWrap,
      isSmall,
      isWide,
      hasReducedPadding,
      rawCta,
      hasNoMargin
    } = this.props;

    let {icon} = this.props;
    const isRaw = !!rawCta;
    const isIconOnly = !label && !rawCta && icon;

    if (!label && !rawCta && !icon) {
      return '';
    }

    if (type === 'primary' && !icon) {
      icon = 'rightarrow';
    }

    const baseClass = 'cta';
    let cls = `${baseClass}`;

    cls += ` ${baseClass}--${type}`;

    if (isSmall) {
      cls += ` ${baseClass}--sm`;
    }

    if (isWide) {
      cls += ` ${baseClass}--wide`;
    }

    if (noWrap) {
      cls += ` ${baseClass}--nowrap`;
    }

    if (hasNoMargin) {
      cls += ` ${baseClass}--nomargin`;
    }

    if (hasReducedPadding) {
      cls += ` ${baseClass}--reducedPadding`;
    }

    if (cssclass) {
      cls += ` ${cssclass}`;
    }

    if (icon) {
      cls += ` ${baseClass}--icon`;
    }

    if (isIconOnly) {
      cls += ` ${baseClass}--icon-only`;
    }

    if (isRaw) {
      if (type === 'primary') {
        icon =
          '<span class="icon icon--rightarrow"><svg focusable="false" width="16" height="9" viewBox="0 0 16 9" style="transform: rotate(-90deg);"><path d="M8,8.4 15.4,1.5 14.5,0.4 8,5.3 1.5,0.4 0.6,1.5" fill="#fff"></path></svg></span>';
      }
      else if (type === 'back') {
        icon =
          '<span class="icon icon--leftarrow"><svg focusable="false" width="16" height="9" viewBox="0 0 16 9" style="transform: rotate(90deg);"><path d="M8,8.4 15.4,1.5 14.5,0.4 8,5.3 1.5,0.4 0.6,1.5" fill="#fff"></path></svg></span>';
      }
      else {
        icon = '';
      }

      return (
        <span
          className={cls}
          onClick={this.onClick}
          dangerouslySetInnerHTML={{__html: icon + rawCta}}
        ></span>
      );
    }

    const opts = {};
    if (!!cls && cls !== undefined && cls !== '') {
      opts.className = cls;
    }
    if (!!href && href !== undefined && href !== '') {
      opts.href = href;
    }

    return (
      <a {...opts} onClick={this.onClick}>
        <Editable html={label} />
        {icon && <Icon icon={icon} fillColor="#fff" />}
      </a>
    );
  }
}

Cta.defaultProps = {
  isSmall: false,
  isWide: false,
  noWrap: false,
  hasReducedPadding: false,
  cssclass: '',
  label: '',
  href: '',
  icon: '',
  type: 'primary',
  rawCta: ''
};

Cta.propTypes = {
  isSmall: PropTypes.bool,
  isWide: PropTypes.bool,
  noWrap: PropTypes.bool,
  hasReducedPadding: PropTypes.bool,
  cssclass: PropTypes.string,
  label: PropTypes.string,
  href: PropTypes.string,
  icon: PropTypes.string,
  type: PropTypes.string,
  rawCta: PropTypes.string,
  onClick: PropTypes.func,
  hasNoMargin: PropTypes.bool
};

module.exports = Cta;
