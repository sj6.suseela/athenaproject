import React from 'react';
import PropTypes from 'prop-types';

import SitecoreSettings from '../SitecoreSettings';

import EditableDiv from '../Editable/EditableDiv';
import Editable from '../Editable/Editable';

class ImageBlock extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing:false
    };
  }

  componentDidMount () {
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  render () {
    const isEditing = this.state.isEditing;
    const link = this.props.link;

    return(
      <div className="imageBlocks__block">
        <div className="imageBlocks__inner">
          {!isEditing && (
            <a id={link.id} href={link.href} rel={link.rel}><Editable html={this.props.image} /></a>
          )}
          {isEditing && (
            <div>
              <EditableDiv html={link.html} />
              <EditableDiv html={this.props.image} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

ImageBlock.defaultProps = {
  link: {}
};

ImageBlock.propTypes = {
  link:PropTypes.object,
  image:PropTypes.string
};

module.exports = ImageBlock;
