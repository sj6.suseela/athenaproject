import React from 'react';
import PropTypes from 'prop-types';

import './ImageBlocks.scss';

import ImageBlock from './ImageBlock';

/** ImageBlocks
  * @description description
  * @example
ReactDOM.render(React.createElement(Components.ImageBlocks, {
  items:[
    {
        link: {
          html:'<a href="#">Link here</a>',
          href:'#',
          rel:'nofollow',
        },
        image:'<img src="dist/images/partners/logo_boundless_col.png" alt="boundless" />'
    },
    {
        link: {
          html:'<a href="#">Link here</a>',
          href:'#',
        },
        image:'<img src="dist/images/partners/logo_GMB_col.png" alt="gmb" />'
    },
    {
        link: {
          html:'<a href="#">Link here</a>',
          href:'#',
        },
        image:'<img src="dist/images/partners/logo_NUT_col.png" alt="nut" />'
    },
    {
        link: {
          html:'<a href="#">Link here</a>',
          href:'#',
        },
        image:'<img src="dist/images/partners/logo_RCON_col.png" alt="rcon" />'
    },
    {
        link: {
          html:'<a href="#">Link here</a>',
          href:'#',
        },
        image:'<img src="dist/images/partners/logo_RCOM_col.png" alt="rcom" />'
    },
    {
        link: {
          html:'<a href="#">Link here</a>',
          href:'#',
        },
        image:'<img src="dist/images/partners/logo_unison_col.png" alt="Unison" />'
    },
    {
        link: {
          html:'<a href="#">Link here</a>',
          href:'#',
        },
        image:'<img src="dist/images/partners/logo_PCS_col.png" alt="pcs" />'
    }
  ]
}), document.getElementById("partnerBlocks"));
*/
class ImageBlocks extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };

    this.itemWrapper = props.items.map((item,i) => {
      return <ImageBlock key={item.link + i} link={item.link} image={item.image}></ImageBlock>;
    });
  }

  render () {
    // const {title} = this.props;

    return (
      <div className="container">
        <div className="imageBlocks">{this.itemWrapper}</div>
      </div>
    );
  }
}

ImageBlocks.defaultProps = {
  items: []
};

ImageBlocks.propTypes = {
  items: PropTypes.array
};

module.exports = ImageBlocks;
