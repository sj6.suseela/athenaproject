import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import List from '../_ContentModules/List/List';
import IconList from '../_ContentModules/List/IconList';

import './FaqHead.scss';

/** FaqHead
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.FaqHead, {
    id : 'id',
    theme:{
      themeName: 'warm-grey',
      themeBefore: 'warm-grey',
      themeAfter: 'white'
    },
    title : 'Answer',
    copy : `<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A quidem repellendus, repudiandae rerum cumque earum dolore alias veritatis omnis, quod laboriosam, veniam temporibus minima, at distinctio cupiditate sed adipisci voluptatum!</p>`,
    cta : '<a href="#">Further information about LAPR</a>',
    related: {
      heading:'Related questions',
      items:[
        '<a href="faq-answer.html">I\'d like to make a claim</a>',
        '<a href="faq-answer.html">Make changes to my policy</a>',
        '<a href="faq-answer.html">Renew my policy</a>'
      ]
    }
  }), document.getElementById("element"));
*/
class FaqHead extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {title, copy, links, related} = this.props;

    const baseClass = 'faq-head';

    const hasRelated = related && related.items && related.items.length > 0;

    return (
      <div>
      <ModuleWrapper baseClass={baseClass} isProduct={false}>
            <Editable tag="h2" className={`${baseClass}__title`} html={title} />
            <EditableDiv className={`${baseClass}__copy`} html={copy} />
            <IconList items={links} />
      </ModuleWrapper>
      {hasRelated && (
        <ModuleWrapper {...this.props.related} baseClass={`${baseClass}__related`} isProduct={true}>
            <Editable className={`${baseClass}__related__heading`} tag="h3" html={related.heading} />
            <List items={related.items} />
        </ModuleWrapper>
      )}
      </div>
    );
  }
}

FaqHead.defaultProps = {
  title : '',
  copy : '',
  links : null,
  related : null
};

FaqHead.propTypes = {
  title : PropTypes.string,
  copy : PropTypes.string,
  links : PropTypes.array,
  related : PropTypes.object
};

module.exports = FaqHead;
