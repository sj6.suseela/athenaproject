import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import List from '../_ContentModules/List/List';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditableImage from '../Editable/EditableImage';

import './FaqSection.scss';

/** FaqSection
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.FaqSection, {
    id : 'id',
    theme:{
      themeName: 'warm-grey',
      bottomCurve:{ type:'concave' }
    },
    contentLeft : {
      title : 'Section title',
      content : `<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat quam voluptatum laborum, perferendis. Error quod pariatur sed impedit tempora ullam omnis voluptatum aspernatur nam libero ipsam, ea facilis minima mollitia?</p>`,
      links : [
        '<a href="faq-answer.html">How can I trace an old policy?</a>',
        '<a href="faq-answer.html">Can I deal with someone else\'s policy?</a>',
        '<a href="faq-answer.html">I\'d like to make a claim</a>',
      ]
    },
    contentRight : {
      title : 'Section title',
      links : [
        '<a href="faq-answer.html">How can I trace an old policy?</a>',
        '<a href="faq-answer.html">Can I deal with someone else\'s policy?</a>'
      ]
    }
  }), document.getElementById("element"));
*/
class FaqSection extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {contentLeft, contentRight} = this.props;

    const baseClass = 'faq-section';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div className="columns@md">
          <div>
            {contentLeft && (
              <div>
                <Editable tag="h2" className={`${baseClass}__section-title`} html={contentLeft.title} />
                <EditableDiv html={contentLeft.content} />
                <List items={contentLeft.links} />
                <EditableImage image={contentLeft.image} />
              </div>
            )}
          </div>
          <div>
            {contentRight && (
              <div>
                <Editable tag="h2" className={`${baseClass}__section-title`} html={contentRight.title} />
                <EditableDiv html={contentRight.content} />
                <List items={contentRight.links} />
                <EditableImage image={contentRight.image} />
              </div>
            )}
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

FaqSection.defaultProps = {
  contentLeft : null,
  contentRight : null
};

FaqSection.propTypes = {
  contentLeft : PropTypes.object,
  contentRight : PropTypes.object
};

module.exports = FaqSection;
