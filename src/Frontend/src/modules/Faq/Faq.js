import React from 'react';
import PropTypes from 'prop-types';
import List from '../_ContentModules/List/List';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import Cta from '../Cta/Cta';

import Editable from '../Editable/Editable';
import EditableParagraph from '../Editable/EditableParagraph';

import './Faq.scss';

/** Faq
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Faq, {
    id : 'id',
    theme :{
      themeName : 'warm-grey',
      themeBefore : 'white',
      topCurve : { type :'convex' }
    },
    label: 'Frequently Asked Questions',
    headline: "You must have 100 questions, but let's start with 5",
    cta: `<a href="#">More related FAQs</a>`,
    items: [
        '<a href="faq-answer.html">Can I insure my parents?</a>',
        '<a href="faq-answer.html">What am I covered for?</a>',
        '<a href="faq-answer.html">Which life insurance?</a>',
        '<a href="faq-answer.html">Fourth most frequently asked life insurance question</a>',
        '<a href="faq-answer.html">Fifth most frequently asked life insurance question</a>'
    ]
  }), document.getElementById("element"));
*/
class Faq extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {icon, headline, intro, label, items, cta} = this.props;

    const baseClass = 'faq';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div className="head faq__head">
          <Editable className="icon" html={icon} />
          <div className="head__main">
            <Editable tag="h2" className="label" html={label} />
            <EditableParagraph className="h2 title faq__title" html={headline} />
            <EditableParagraph className="lead" html={intro} />
          </div>
        </div>
        <div className="faq__body">
          <List items={items} isUnderlined={true} />
          {cta && (
            <div className="faq__buttons">
              <Cta {...cta}  type="secondary" rawCta={cta}/>
            </div>
          )}
        </div>
      </ModuleWrapper>
    );
  }
}

Faq.defaultProps = {
  icon: '<img src="dist/images/cms/FAQs-80x80.svg" />',
  headline: '',
  cta: '',
  label: '',
  intro: '',
  items: [],
  id:'faq'
};

Faq.propTypes = {
  icon: PropTypes.string,
  cta: PropTypes.string,
  headline: PropTypes.string,
  label: PropTypes.string,
  intro: PropTypes.string,
  items: PropTypes.array,
  id:PropTypes.string
};

module.exports = Faq;
