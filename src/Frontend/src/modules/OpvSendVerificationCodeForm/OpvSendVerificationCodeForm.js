import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvSendVerificationCodeForm.scss';

class OpvSendVerificationCodeForm extends React.Component {

  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const {formArea1,content, formArea2} = this.props;
    const baseClass = 'opv-svc';
    return (
      <div>
          <EditableDiv className={`${baseClass}`} html={formArea1} />
          <Editable className={`${baseClass}__content`} html={content} />
          <EditableDiv className={`${baseClass}`} html={formArea2} />
      </div>
    );
  }
}

OpvSendVerificationCodeForm.propTypes = {
  formArea1: PropTypes.string,
  content : PropTypes.string,
  formArea2: PropTypes.string
};

module.exports = OpvSendVerificationCodeForm;
