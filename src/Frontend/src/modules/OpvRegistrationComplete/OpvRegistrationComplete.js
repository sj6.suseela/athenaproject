import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Cta from '../Cta/cta';
import './OpvRegistrationComplete.scss';

class OpvRegistrationComplete extends React.Component {

  constructor (props) {
    super(props);

    this.state = {

    };
  }

  render () {
    const {submit, title, description} = this.props;
    const baseClass = 'registration-complete';

    return (
        <div>
            <Editable tag="h2" className={`${baseClass}__title`} html={title} />
            <EditableDiv className={`${baseClass}__content`} html={description} />
            <Cta type="primary" isSmall={true} rawCta={submit} />
        </div>
    );
  }

}

OpvRegistrationComplete.defaultProps = {
  submit: '',
  title : '',
  description : ''
};

OpvRegistrationComplete.propTypes = {
  title : PropTypes.string,
  description : PropTypes.string,
  submit: PropTypes.string
};

module.exports = OpvRegistrationComplete;
