
import React from 'react';
import PropTypes from 'prop-types';

import ModuleCurve from '../ModuleCurve/ModuleCurve';
import Accordion from '../Accordion/Accordion';
import Stick from '../Stick/Stick';
import ScrollToY from '../scrolltoy';
import Editable from '../Editable/Editable';
import SitecoreSettings from '../SitecoreSettings';

import './Glossary.scss';

/** Glossary
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Glossary, {
    id : 'id',
    theme : {},
    label: 'Jump to',
      items: [
        {
          character : 'A',
          items: [
            {
              heading: 'Accidental damage cover for buildings',
              children : `Accidental damage to your buildings is when unexpected damage to the physical fabric of your home is caused suddenly, by external means, that isn't deliberate. For details of what accidental damage cover you get as standard with LV= home insurance, please read our policy document. You can also choose to extend your accidental damage cover to protect you against things like DIY accidents, such as putting your foot through your loft floor, or driving a nail through a pipe.`
            },
            {
              heading: 'Accidental damage cover for contents',
              children : `Things can go wrong at home that you weren't expecting and haven't planned for. This cover protects against accidental damage to your home contents, for example, dropping a television while fixing it to a wall. Find out what accidental damage cover for contents is included with LV= home insurance.`
            },
            {
              heading: 'Alternate accommodation allowance',
              children : `Imagine if your home was so badly damaged, perhaps by an escape of water or a fire, that you had to temporarily move out. Alternative accommodation cover means we pay for you to move into rented lodgings or a hotel while the repairs are going on at your home. If you have family pets, we make sure you can take them too – or pay for their boarding in kennels until you're able to move back home.`
            },
            {
              heading: 'Approved supplier / tradesperson',
              children : `An approved supplier is a company or person who forms part of our claims network, who we've appointed, approved and authorised to provide you with replacement goods – e.g. a new television if yours is stolen for example, or carry out repairs.`
            }
          ]
        },
        {
          character : 'B',
          items: [
            {
              heading: 'Buildings sum insured',
              children : `Buildings sum insured is the amount it would cost to demolish, clear and rebuild your home following an insured event. It's not the same as your home's market value, which might be higher or lower. You can work out how much it would be to rebuild your home using this ABI calculator but you'll need to know quite a lot about building materials and requirements yourself. A chartered surveyor is usually a more accurate way to calculate the buildings sum insured for you.`
            },
            {
              heading: 'Buildings and contents',
              children : `You can choose to insure just your buildings or just your contents with LV=. If you need both buildings and contents insurance, get a quote for your home insurance, which includes cover for the physical fabric of your home, as well as all your belongings. If you live in a flat, you might only need contents insurance, in which case, you can tell us when you fill out our online quote form.`
            },
            {
              heading: 'Buildings insurance',
              children : `Buildings insurance gives you cover for the cost of rebuilding or repairing your house, fixtures and fittings within the boundaries of your home, if they're damaged or destroyed.`
            }
          ]
        },
        {
          character : 'C',
          items: [
            {
              heading: 'Cancellation',
              children : `Cancellation is the ending of a policy before it's due to expire. There may be a cancellation clause in a policy setting out the conditions under which the policy may be cancelled by notice. Cancellation may mean you get back some of your premiums, but in many cases you won't get anything back. Please check the LV= home insurance document to find out our cancellation policy.`
            },
            {
              heading: 'Claim',
              children : `You make a claim when you ask your insurer to repair or replace any damaged items or damage to your home, or pay you the sum of money that is owed to you, under the terms of your insurance policy. There's lots of information about making a claim on our website, from what's covered to how long it'll take to get your money.`
            },
            {
              heading: 'Contents insurance',
              children : `Contents insurance gives you cover for your belongings and household goods within your home. Imagine turning your home upside down and giving it a shake. Everything that falls out would come under your contents insurance. Contents insurance can be bought alongside our buildings insurance.`
            },
            {
              heading: 'Contents sum insured',
              children : `Contents sum insured is the maximum amount that the policy will pay out if your entire home contents are destroyed.`
            },
            {
              heading: 'Cooling-off period',
              children : `A cooling off period is the time you have to change your mind when buying insurance or other financial products or services. The actual time varies, so check your policy details. If you do decide to cancel within your cooling off period, we'll give you a full refund.`
            }
          ]
        },
        {
          character : 'D',
          items: [
            {
              heading: 'Defaqto',
              children : `Defaqto are a leading UK independent financial research company, specialising in rating, comparing and analysing financial products. Defaqto experts have chosen what they believe to be the most important features of a home insurance policy (including limits and level of cover). They then score and assign a rating based on a scale of 1 to 5 to each one – so you can see at glance how the policies in the market compare. A 5 Star Rating is the highest rating you can receive from Defaqto.`
            },
            {
              heading: 'Document of home insurance',
              children : `The document of home insurance is the policy booklet we send with your other home insurance documents. It explains what level of cover you have, so you know exactly what you can claim for if something goes wrong at home.`
            }
          ]
        },
        {
          character : 'E',
          items: [
            {
              heading: 'Emergency Helpline',
              children : `If you suffer a domestic emergency in your home, such as a blocked toilet, hot water or heating failure, call our Domestic Emergency Assistant helpline. A trained operator will be able to help and advise you and if required arrange for emergency assistance or repairs to be completed by an approved tradesperson. If you use this service you will be responsible for paying the tradesperson's charges and any costs of materials incurred.`
            },
            {
              heading: 'Escape of water',
              children : `This term is used to describe any damage caused by water that has escaped from a fixed installation such as a pipe, bath, shower or radiator.`
            },
            {
              heading: 'Excess',
              children : `An excess is the amount that you have to pay towards a claim. A compulsory excess is an excess applied by your insurer and could vary depending on your circumstances. A voluntary excess is a figure agreed with your insurer, usually where you agree to pay a higher part of each claim in return for a lower premium. Your excess may be a total of a compulsory and voluntary excess added together. Excesses vary between different types of claim. You can find out how much your excesses are, depending on what you're claiming for, in your policy schedule.`
            },
            {
              heading: 'Exclusions',
              children : `Exclusions are things that your insurance won't cover, such as a result of wear and tear for example. It's worth reading your document of insurance to see what isn't covered; people are sometimes surprised to find they can't claim on their insurance for some things. Exclusions vary between insurance products but all exclusions should be clear and specific.`
            }
          ]
        },
        {
          character : 'F',
          items: [
            {
              heading: 'Fixtures and fittings',
              children : `<p>Fixtures and fittings are covered by your buildings insurance, even though some fittings may appear to be contents.</p>
                <p>Fixtures usually include:</p>
                <ul>
                  <li>central-heating boilers, systems and radiators</li>
                  <li>light-fittings</li>
                  <li>fitted kitchens, built-in wardrobes and bathroom furniture</li>
                </ul>
                <p>Fittings usually include:</p>
                <ul>
                  <li>curtain rails and poles (lampshades and curtains would come under your contents cover)</li>
                  <li>TV aerials and satellite dishes</li>
                </ul>`
            },
            {
              heading: 'Floodplain',
              children : `A floodplain is an area of land next to a river, stream, lake, estuary or other water body that is subject to flooding. These areas, if left undisturbed, act to store excess floodwater. If you live on a floodplain, it might be more difficult to buy home insurance. To find out more about buying affordable home insurance when you live in a flood risk area, visit the FloodRe website.`
            }
          ]
        },
        {
          character : 'G',
          items: null
        },
        {
          character : 'H',
          items: [
            {
              heading: 'Home',
              children : `Your home is the private property at the address shown on your schedule, together with its garages and domestic outbuildings. It's where you usually live in the UK.`
            },
            {
              heading: 'Home entertainment equipment',
              children : `Home entertainment equipment includes radios, televisions, digital, cable and satellite decoders/receivers, home computers, laptops, tablets, notebooks, e-readers and games consoles. Video, DVD, record, tape and CD players. These items are covered by your contents insurance; some policies also include cover for any downloaded digital information too, such as software, music or movies.`
            },
            {
              heading: 'Home emergency cover',
              children : `You can usually add this extra level of cover to your home insurance policy. It gives you access to 24-hour assistance for central heating, electrical emergencies, plumbing or draining issues.`
            },
            {
              heading: 'Home insurance',
              children : `Home insurance includes contents and buildings insurance. Contents insurance covers your personal possessions and household goods. Buildings insurance covers the property itself.`
            }
          ]
        },
        {
          character : 'I',
          items: null
        },
        {
          character : 'J',
          items: null
        },
        {
          character : 'K',
          items: null
        },
        {
          character : 'L',
          items: [
            {
              heading: 'Legal costs',
              children : `Legal costs are fees, costs and expenses (including Value Added Tax or equivalent local goods and services tax) which we agree to pay for you in connection with legal action. Also, any costs which you're ordered to pay by a court or arbitrator (other than damages, fines and penalties) or any other costs we agree to pay.`
            },
            {
              heading: 'Legal expenses cover',
              children : `Legal expenses cover is an optional extra for LV= home insurance and can help you with compensation for personal injury claims, or protecting your legal rights as a homeowner.`
            },
            {
              heading: 'Loss adjuster',
              children : `A loss adjuster is an independent person engaged by an insurance company to check that a claim is covered and negotiate with the policyholder the amount payable for a claim.`
            }
          ]
        },
        {
          character : 'M',
          items: null
        },
        {
          character : 'N',
          items: null
        },
        {
          character : 'O',
          items: null
        },
        {
          character : 'P',
          items: [
            {
              heading: 'Personal possessions, personal effects, personal belongings',
              children : `Personal possessions cover is an optional extra on the contents insurance policy. LV= offers additional cover for things like your mobile phone, tablet, golf clubs or jewellery that you take or wear outside of the home.`
            },
            {
              heading: 'Policy',
              children : `Your policy is a formal, legally-binding contract of insurance that includes the terms of your cover.`
            },
            {
              heading: 'Premium',
              children : `A premium is the amount you pay for your plan or policy. The frequency depends on the type of cover that you have, and could be monthly instalments, a single 'one-off' payment, three-monthly, six-monthly or yearly. If you choose monthly instalments, you'll often have to pay interest on your monthly payments as you're effectively taking out a short term credit agreement to cover the cost of your insurance.`
            }
          ]
        },
        {
          character : 'Q',
          items: null
        },
        {
          character : 'R',
          items: [
            {
              heading: 'Rebuild cost',
              children : `The cost to rebuild your home if it's destroyed or damaged by an insured event such as flood or fire. This includes the cost to clear the site and legal fees. If you have a mortgage, the rebuild cost will be stated in your lender's valuation report. The most accurate way of finding out a rebuild cost is to hire a surveyor. The rebuild cost is not the same as the market price. This is the same as 'Buildings sum insured'.`
            }
          ]
        },
        {
          character : 'S',
          items: [
            {
              heading: 'Special events',
              children : `During special events or occasions (e.g. Christmas, Hannukah or family weddings) some insurers, including LV=, increase your level of contents cover for one month before and one month after the event. This is to cover all the extra things you've got in your home, from presents to wedding dresses!`
            },
            {
              heading: 'Standard construction',
              children : `Standard construction means brick, stone or concrete walls, with a slate, tile, metal, asphalt or concrete roof.`
            },
            {
              heading: 'Subsidence, landslip and heave',
              children : `Subsidence is the vertical downward movement of a building foundation caused by the loss of support of the site beneath the foundations. Landslip is the sudden movement of soil on a slope or gradual creep of a slope over a period of time. Heave is the expansion of the ground beneath part or all of the building. Visit the Royal Institution of Chartered Surveyors (RICS) website for more information.`
            }
          ]
        },
        {
          character : 'T',
          items: [
            {
              heading: 'Trace and access',
              children : `If you smell gas, or see water staining in parts of your house it shouldn't, like ceilings or walls, it's possible there's a problem with some pipework behind the walls in your home. Trace and access is when a specialist comes to your home to find, access and repair the cause of the problem.`
            }
          ]
        },
        {
          character : 'U',
          items: [
            {
              heading: 'Unoccupied',
              children : `Unoccupied means the property is not lived in by you, or a member of your family, or does not have sufficient furniture or services for normal living purposes. 'Lived in' means slept in frequently. Regular visits or occasional overnight stays are not accepted as living in the property.`
            }
          ]
        },
        {
          character : 'V',
          items: [
            {
              heading: 'Valuables',
              children : `Your valuables are your jewellery, watches, furs, items made of gold, silver and other precious metals, pictures and other works of art, including stamp, coin and medal collections. These will most likely need to be 'specified items' under your contents insurance.`
            },
            {
              heading: 'Voluntary excess',
              children : `You may be able to specify a higher excess, known as a voluntary excess, in order to reduce your premium. The excess is the amount of an insurance claim that will be paid by you and is normally subtracted from the claim amount by your insurer.`
            }
          ]
        },
        {
          character : 'W',
          items: null
        },
        {
          character : 'X',
          items: null
        },
        {
          character : 'Y',
          items: null
        },
        {
          character : 'Z',
          items: null
        }
      ]
  }), document.getElementById("element"));
*/
class Glossary extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing:false
    };

    this._onClick = this._onClick.bind(this);
  }

  _onClick (e) {
    e.preventDefault();
    const elementId = e.target.parentNode.getAttribute('href').split('#')[1];
    const offset = 80;
    const element = document.getElementById(elementId);
    const yPos = element.getBoundingClientRect().top + (window.pageYOffset - offset);

    new ScrollToY(yPos, 500, 'easeInOutQuint');
  }

  componentDidMount () {
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  render () {
    // get the prop settings
    const {theme, items, label, id} = this.props;
    const baseClass = 'glossary';
    const hasCurveTop = !!theme.topCurve;
    const hasCurveBottom = !!theme.bottomCurve;
    const additionalClasses = `${hasCurveTop ? 'module-curve-top' : ''} ${hasCurveBottom ? 'module-curve-bottom' : ''}`;

    const header = items.map((item,i) => {
      const character = item.character;

      if(item.items && item.items.length) {
        const id = `glossary-${character}`.toLowerCase();

        return (
            <li key={i}>
              <a onClick={this._onClick} href={`#${id}`}><Editable html={character} /></a>
            </li>
        );
      }

      return (
        <li key={i}>
          <Editable html={character} />
        </li>
      );
    });

    const content = items.map((item,i) => {
      const character = item.character;

      if(this.state.isEditing) {
        if(item.items) {
          for(const subitems of item.items) {
            subitems.open = true;
          }
        }
      }

      if(item.items && item.items.length) {
        const id = `glossary-${character}`.toLowerCase();

        return (
          <div key={i} id={id} className={`${baseClass}__item`}>
            <Editable tag="h2" html={character} />
            <div>
              <Accordion closeSiblings={false} items={item.items} modifier="" />
            </div>
          </div>
        );
      }

      return ('');
    });

    return (
      <section className={`module ${baseClass} theme-${theme.themeName} ${additionalClasses}`} ref={(c) => {this.ref = c;}}>
        {hasCurveTop && (
          <ModuleCurve id={id} type={theme.topCurve.type} position="top" bgtheme={theme.themeBefore}/>
        )}
        <div className="wrapper">
          {label && (
            <div className={`container ${baseClass}__container`}>
              <Editable className="label" html={label} />
            </div>
          )}
          <Stick zIndex={99}>
            <nav className={`${baseClass}__header`}>
              <div className={`container ${baseClass}__container`}>
                <ul>{header}</ul>
              </div>
            </nav>
          </Stick>
          <div className={`container ${baseClass}__container`}>
            <div className={`${baseClass}__items`}>
              {content}
            </div>
          </div>
        </div>
        {hasCurveBottom && (
          <ModuleCurve id={id} type={theme.bottomCurve.type} position="bottom" bgtheme={theme.themeAfter}/>
        )}
      </section>
    );
  }
}

Glossary.defaultProps = {
  theme: {},
  label: '',
  items: [],
  id:'glossary'
};

Glossary.propTypes = {
  theme: PropTypes.object,
  label: PropTypes.string,
  items: PropTypes.array,
  id:PropTypes.string
};

module.exports = Glossary;
