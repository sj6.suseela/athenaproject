import React from 'react';
import PropTypes from 'prop-types';

import './Breadcrumb.scss';

import Editable from '../Editable/Editable';

/** Breadcrumb
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Breadcrumb, {
    id : 'id',
    items: [
      { label: "Home page",   link: {href: "index.html"} },
      { label: "Car",   link: {href: "#"} },
      { label: "Multi Car Insurance", link:{href: "#"} },
      { label: "Manage your policy", link:{href: ''} }
    ]
  }), document.getElementById("element"));
*/
class Breadcrumb extends React.Component {
  constructor (props) {
    super(props);
  }

  _onClick (item,e) {
    if(item.onClick) {
      e.preventDefault();
      item.onClick();
    }
  }

  render () {
    const {items} = this.props;

    let links = [];

    if(items) {
      links = items.map((item,i) => {
        if(item.link && item.link.href) {
          return (
            <li key={item + i}><a id={item.link.id} href={item.link.href} rel={item.link.rel} onClick={(e) => this._onClick(item, e)}><Editable html={item.label} /></a></li>
          );
        }
        else{
          return(
            <li key={item + i}><span className="a"><Editable html={item.label} /></span></li>
          );
        }
      });
    }

    if(!links) {
      return '';
    }

    return (
      <nav className="nav nav--breadcrumb">
        <div className="container">
          <ul className="list list--links">
            {links}
          </ul>
        </div>
      </nav>
    );
  }
}

Breadcrumb.defaultProps = {
  items: []
};

Breadcrumb.propTypes = {
  items: PropTypes.array
};

module.exports = Breadcrumb;
