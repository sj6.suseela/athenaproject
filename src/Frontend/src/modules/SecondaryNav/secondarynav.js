import React from 'react';
import PropTypes from 'prop-types';

import './secondarynav.scss';

import InPageNav from '../InPageNav/InPageNav';
import Editable from '../Editable/Editable';
import Icon from '../Icon/Icon';

/** SecondaryNav
  * @description description
  * @example
  ReactDOM.render(React.createElement(PriorityComponents.SecondaryNav, {
    id : 'id',
    theme : {},
    mobileLabel:"Menu",
    mobileHeading:'Travel Insurance',
    isInvertedColour: true,
    items:[
      {
        label:"Car Insurance",
        link:{href:"product-car.html"},
        children:[]
      },
      {
        label:"Cover Levels",
        link:{href:"#/cover-levels"},
        children:[]
      },
      {
        label:"Optional Extras",
        link:{href:"product-car--optional-extras.html"},
        children:[]
      },
      {
        label:"Activities covered",
        link:{href:"#/activities-covered/"},
        children:[]
      },
      {
        label:"More",
        mobileLabel:"More (mobile)",
        link:{href:"#"},
        children:[
          {
            label:"Sec Subnav 1",
            mobileLabel: "Sec Subnav 1 (Mobile)",
            link:{href:"#"}
          },
          {
            label:"Sec Subnav 2",
            link:{href:"#"}
          },
        ]
      }
    ],
    existingCustomers:{
      label:"Existing Customers",
      link:{href:'#'}
    }
  }), document.getElementById("element"));
*/
class SecondaryNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

    };

    this.items = props.items;
    this.items.unshift({
      mobileLabel:props.mobileHeading,
      type:'heading'
    });

    // push existing customers link as last item
    if(props.existingCustomers.label) {
      this.items.push({
        mobileLabel:props.existingCustomers.label,
        type:'category',
        link:props.existingCustomers.link
      });
    }
  }

  render () {
    const {label} = this.props.existingCustomers;
    const link = this.props.existingCustomers.link ? this.props.existingCustomers.link : {};

    const hasExisting = !!label;

    let cssClass = 'nav secondaryNav';
    if (this.props.isInvertedColour) {
      cssClass += ' invertColours';
    }

    return (
      <div className={cssClass}>
        <div className="container">
          <InPageNav cssClass="mini product" items={this.items} mobileLabel={this.props.mobileLabel} isFillScreen={true} hasClose={true} hasMarker={true}/>
          {hasExisting && (
            <div className="secondaryNav__existing">
              <a id={link.id} href={link.href} rel={link.rel}><Editable html={label} /><Icon icon='rightarrow' fillColor='#ffffff' /></a>
            </div>
          )}
        </div>
      </div>
    );
  }
}

SecondaryNav.defaultProps = {
  items: [],
  existingCustomers: {
    label: ''
  }
};

SecondaryNav.propTypes = {
  items: PropTypes.array,
  secondarynav: PropTypes.object,
  existingCustomers: PropTypes.object,
  mobileLabel:PropTypes.string,
  mobileHeading:PropTypes.string,
  isInvertedColour: PropTypes.bool
};

module.exports = SecondaryNav;
