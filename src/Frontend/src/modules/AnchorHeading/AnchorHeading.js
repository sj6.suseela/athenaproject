import React from 'react';
import PropTypes from 'prop-types';

import Module from '../Module';

import Editable from '../Editable/Editable';

import './AnchorHeading.scss';

/** AnchorHeading
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.AnchorHeading, {
    id : 'id',
    theme : {},
    heading : "1. Heading for guide section 1"
  }), document.getElementById("element"));
*/
class AnchorHeading extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  componentDidMount () {
    Module.register(this.ref);
  }

  render () {
    return (
      <div className="anchorHeading"  id={this.props.id}  ref={(c) => {this.ref = c;}}>
        <div className="container wrapper">
          <Editable tag="h3" html={this.props.heading} />
        </div>
      </div>
    );
  }
}

AnchorHeading.propTypes = {
  heading:PropTypes.string,
  id:PropTypes.string
};

module.exports = AnchorHeading;
