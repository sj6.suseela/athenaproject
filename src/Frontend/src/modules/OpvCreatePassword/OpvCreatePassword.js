import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvCreatePassword.scss';

class OpvCreatePassword extends React.Component {

  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const {content, formArea2} = this.props;
    const baseClass = 'opv-create-password';
    return (
      <div>
          <Editable className={`${baseClass}__content`} html={content} />
          <EditableDiv className={`${baseClass}`} html={formArea2} />
      </div>
    );
  }
}

OpvCreatePassword.propTypes = {
  content : PropTypes.string,
  formArea2: PropTypes.string
};

module.exports = OpvCreatePassword;
