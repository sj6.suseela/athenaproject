import './ContactUs.scss';

class ContactUs {
  constructor (elem) {
    this.element = elem;
    this.config = {};
   
    this.id = this.element.id;
    this.primaryLinks = this.element.querySelector('.contact-us__primary');

    this.primaryLinks.addEventListener('click',(e) => {
      this.clickPrimaryNav(e);
    });
    
    this.init();
  }

  init () {

  }

  clickPrimaryNav () {
  }

}



module.exports = ContactUs;