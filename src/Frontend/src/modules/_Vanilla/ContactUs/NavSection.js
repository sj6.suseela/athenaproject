import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../../Editable/Editable';
import ArrowLink from '../../ArrowLink/ArrowLink';

class NavSection extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

    };

    this.items = props.items.map((item,i) => {
      const html = `<span>${item.title}</span>${  item.additional ? ` <span>${item.additional}</span>` : ''}`;
      const type = item.type ? item.type : '';
      return (
        <li key={i} className={type}>
          <a href="#">
            <ArrowLink link={html} />
          </a>
        </li>
      );
    });
  }

  render () {
    const {headline} = this.props;

    return (
      <div className="contact-us__section">
        <Editable tag="h4" html={headline} />
        <ul className="links">
          {this.items}
        </ul>
      </div>
    );
  }
}

NavSection.defaultProps = {
};

NavSection.propTypes = {
  items:PropTypes.array,
  headline:PropTypes.string
};

module.exports = NavSection;
