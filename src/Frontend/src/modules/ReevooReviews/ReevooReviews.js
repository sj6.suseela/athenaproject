import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import EditButton from '../CMS/EditButton';

import '../../external/embedded_reviews.css';
import './ReevooReviews.scss';

class ReevooReviews extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  componentDidMount () {
    if(!SERVER) {
      const reevooScriptId = 'reevoomark-loader';
      const reevooScript = document.getElementById(reevooScriptId);
      if (!reevooScript) {
        const reevooInjector = document.createElement('script');
        reevooInjector.type = 'text/javascript';
        reevooInjector.id = reevooScriptId;
        reevooInjector.src = `${this.props.scriptBase}reevoo-script.js`;
        document.body.appendChild(reevooInjector);
      }
    }
  }

  render () {
    const {reevooParams, editButton} = this.props;
    const hasProps = !!reevooParams && !!reevooParams.trkref && !!reevooParams.sku;
    const baseClass = 'ReevooReviews';

    if(!reevooParams.per_page) {
      reevooParams.per_page = 5;
    }

    return (
      <ModuleWrapper baseClass={baseClass} {...this.props}>
        {editButton && (
          <EditButton html={editButton} isSmall={false} align="center" />
        )}
        {hasProps && (
          <div className={`${baseClass}__embed`}>
            <reevoo-embedded-product-reviews {...reevooParams}>
            </reevoo-embedded-product-reviews>
          </div>
        )}
      </ModuleWrapper>
    );
  }
}

ReevooReviews.defaultProps = {
  scriptBase:'/dist/'
};

ReevooReviews.propTypes = {
  theme: PropTypes.object,
  reevooParams: PropTypes.object,
  editButton: PropTypes.string,
  scriptBase:PropTypes.string
};

module.exports = ReevooReviews;
