import React from 'react';
// import PropTypes from 'prop-types';
import './OpvCustomAccessCodeForm.scss';

class OpvCustomAccessCodeForm extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      clicked: false
    };

    this._click = this._click.bind(this);
  }

  _click (e) {
    e.preventDefault();
    this.state.clicked ? this.setState({clicked: false}) : this.setState({clicked: true});
  }
  render () {
    const baseClass = 'opv-custom-forms';
    return (
      <div className={`${baseClass}`} >
        <div id="custom-form-area">

        </div>
        <div id="form-area">

        </div>
      </div>
    );
  }
}

OpvCustomAccessCodeForm.propTypes = {};

module.exports = OpvCustomAccessCodeForm;
