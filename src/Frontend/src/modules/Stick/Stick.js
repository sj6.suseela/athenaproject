import React from 'react';
import PropTypes from 'prop-types';

const util = require('dom-find');
import SitecoreSettings from '../SitecoreSettings';

class Stick extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      fix: false,
      width: null,
      height: null,
      visible:true,
      isEditing:false,
      isPreview:false
    };

    this._onresize = this.handleResize.bind(this);
    this._onscroll = this.onScroll.bind(this);
  }

  checkWidth () {
    let width = null;
    let height = null;

    if (this.duplicate) {
      width = this.duplicate.getBoundingClientRect().width;
      height = this.original.offsetHeight || this.original.scrollHeight;
    }
    else {
      width = this.original.getBoundingClientRect().width;
      height = this.original.offsetHeight || this.original.scrollHeight;
    }

    if (this.state.width !== width || this.state.height !== height) {
      this.setState({
        width,
        height
      });
    }
  }

  componentDidMount () {
    this.checkWidth();
    window.addEventListener('scroll', this._onscroll, false);
    window.addEventListener('resize', this._onresize);

    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }

    if(this.state.isPreview !== sc.isPreview()) {
      this.setState({
        isPreview:sc.isPreview()
      });
    }
  }

  componentWillUnmount () {
    window.removeEventListener('scroll', this._onscroll, false);
    window.removeEventListener('resize', this._onresize);
  }

  checkPositions () {
    const pos = util.findPosRelativeToViewport(this.node);

    const tooBig = this.state.height >= window.innerHeight;

    if (pos[1] <= this.props.offsetTop && !tooBig) {
      if(this.props.hasFadeIn) {
        if(!this.state.fix) {
          this.setState({fix: true,visible:false});

          const _this = this;

          setTimeout(() => {
            _this.setState({visible:true});
          },1);
        }
      }
      else{
        if(!this.state.fix) {
          this.setState({fix: true});
        }
      }
    }
    else {
      if(this.props.hasFadeIn) {
        if(this.state.fix) {
          this.setState({visible: false});

          const _this = this;

          setTimeout(() => {
            _this.setState({fix:false});
          },50);
        }
      }
      else{
        if(this.state.fix) {
          this.setState({fix: false});
        }
      }
    }
  }

  handleResize () {
    this.checkWidth();
    this.checkPositions();
  }

  onScroll () {
    this.checkWidth();
    this.checkPositions();
  }

  render () {
    let divStyle,divStyle2,opacity;

    const {offsetTop,className,children,zIndex} = this.props;

    const isEditing = this.state.isEditing;
    const isPreview = this.state.isPreview;

    const editingClass = isEditing ? 'isEditing' : '';
    const previewClass = isPreview ? 'isPreview' : '';

    if(this.state.visible) {
      opacity = '1';
    }
    else{
      opacity = '0';
    }

    if (this.state.fix && !isEditing) {
      divStyle = {
        display: 'block',
        position: 'fixed',
        opacity,
        width: this.state.width ? (`${this.state.width  }px`) : null,
        top: offsetTop
      };
      divStyle2 = {
        height: this.state.height ? (`${this.state.height  }px`) : null,
        visibility:'hidden',
        opacity
      };
      return <div ref={(c) => { this.node = c; }} className={`${className  }_container`} style={{zIndex, position:'relative', width:'100%'}}>
                <div ref={(c) => { this.duplicate = c; }} key='duplicate' className="duplicate" style={divStyle2}>

                </div>
                <div id="2" ref={(c) => { this.original = c; }} key='original' className={`${className } fixed ${previewClass}`} style={divStyle} >
                    {children}
                </div>
            </div>;
    }
    else {
      divStyle = {
        display: 'block',
        position: 'relative'
      };
      return <div ref={(c) => { this.node = c; }} className={`${className}_container`} style={{zIndex, position:'relative', width:'100%'}}>
                <div ref={(c) => { this.original = c; }} className={`${className} ${editingClass}`} key='original' style={divStyle}>
                    {children}
                </div>
            </div>;
    }
  }
}

Stick.defaultProps = {
  offsetTop: 0,
  className: '',
  zIndex: 9000
};

Stick.propTypes = {
  offsetTop: PropTypes.any,
  className: PropTypes.any,
  zIndex: PropTypes.any,
  children: PropTypes.any,
  hasFadeIn:PropTypes.bool
};

module.exports = Stick;
