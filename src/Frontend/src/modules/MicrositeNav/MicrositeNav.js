import React from 'react';
import PropTypes from 'prop-types';

import './micrositenav.scss';

import Cta from '../Cta/Cta';
import Logo from '../Logo/Logo';
import PartnerLogo from '../PartnerLogo/PartnerLogo';

/** MicrositeNav
  * @description description
  * @xexample
  ReactDOM.render(React.createElement(Components.MicrositeNav, {
    logo:'<img src="dist/images/lv-logo.png"></img>',
    logoLink: {
      href:"index.html",
      rel:"noreferrer",
    },
    partner:null,
    backCta: <a href="">back</a>
      
    }
  }), document.getElementById("element"));
*/
class MicrositeNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isInvertedColour: !!props.isInvertedColour
    };
  }

  componentDidMount () {
    if (!SERVER) {
      let txtColour;
      if (document && document.cookie.indexOf('TextColour') > -1) {
        txtColour = document.cookie.substring(document.cookie.indexOf('TextColour')).split(';')[0].split('=')[1];
        if (txtColour) {
          this.setState({
            isInvertedColour: txtColour !== 'white'
          });
        }
      }
    }
  }

  render () {
    const {backCta, hasPartnerLogo} = this.props;
    const baseClass = 'nav nav--microsite';

    const isInvertedTextColour = !!this.state.isInvertedColour;

    const ctaClass = !isInvertedTextColour ? 'nav--microsite__backLink--inverted' : '';
    
    const logoProps = {
      img: this.props.logo,
      width: this.props.logoWidth,
      link: this.props.logoLink,
      type: this.props.logoType
    };

    if (this.props.logoTag) {
      logoProps.tag = this.props.logoTag;
    }

    return (
      <div className={`${baseClass}`}>
        <Logo {...logoProps} />
        {hasPartnerLogo && (
          <PartnerLogo img={this.props.partner.logo} href={this.props.partner.logoHref} />
        )}
        {backCta && <Cta cssclass={`nav--microsite__backLink ${ctaClass}`} isBack={true} type='back' {...backCta} rawCta={backCta} />}
      </div>
    );
  }
}

MicrositeNav.defaultProps = {
  isInvertedColour: false,
  logoType: 'default',
  backCta: ''
};

MicrositeNav.propTypes = {
  backCta: PropTypes.string,
  logo: PropTypes.string,
  logoType: PropTypes.string,
  partner: PropTypes.object,
  logoLink: PropTypes.object,
  logoWidth: PropTypes.any,
  isInvertedColour: PropTypes.bool,
  isMini: PropTypes.bool,
  logoTag: PropTypes.string,
  hasPartnerLogo: PropTypes.bool
};

module.exports = MicrositeNav;
