import React from 'react';
import PropTypes from 'prop-types';

import Cta from '../Cta/Cta';
import Loading from '../Loading/Loading';

import './LoadMore.scss';

class LoadMore extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

    };

    this.click = this.click.bind(this);
  }

  click () {
    this.props.loadMoreFunc();
  }

  render () {
    const {btnType,btnLink,isBtnVisible,isBtnLoading,ajax} = this.props;
    const baseClass = 'loadMore';

    let cssClass = baseClass;

    if (isBtnLoading) {
      cssClass += ` ${baseClass}--loading`;
    }

    if(isBtnVisible) {
      return (
        <div className={cssClass}>
          {ajax && (
            <Cta type={btnType} rawCta={btnLink} onClick={this.click} />
          )}
          {!ajax && (
            <Cta type={btnType} rawCta={btnLink} />
          )}
          {isBtnLoading && (
            <Loading className={`${baseClass}__loading`} isActive={true} />
          )}
        </div>
      );
    }
    else{
      return null;
    }
  }
}

LoadMore.propTypes = {
  btnType:PropTypes.string,
  btnLink:PropTypes.string,
  loadMoreFunc:PropTypes.func,
  isBtnVisible:PropTypes.bool,
  isBtnLoading:PropTypes.bool,
  ajax:PropTypes.any
};

module.exports = LoadMore;
