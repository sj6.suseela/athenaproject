import React from 'react';
import PropTypes from 'prop-types';

import './SearchResults.scss';

import Editable from '../Editable/Editable';
import EditableParagraph from '../Editable/EditableParagraph';

/** SearchResults
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.SearchResults, {
    id : 'id',
    theme : {},
    results:[
      {
        headline:`Headline here ...`,
        linkSrc:'#',
        text:`<b>Lorem ipsum dolor</b> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam ...`
      },
      {
        headline:`Headline here ...`,
        linkSrc:'#',
        text:`<b>Lorem ipsum dolor</b> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam ...`
      },
      {
        headline:`Headline here ...`,
        linkSrc:'#',
        text:`<b>Lorem ipsum dolor</b> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam ...`
      },
      {
        headline:`Headline here ...`,
        linkSrc:'#',
        text:`<b>Lorem ipsum dolor</b> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam ...`
      },
      {
        headline:`Headline here ...`,
        linkSrc:'#',
        text:`<b>Lorem ipsum dolor</b> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam ...`
      }
    ]
  }), document.getElementById("element"));
*/
class SearchResults extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };

    if(props.results) {
      this.results = props.results.map((item,i) => {
        return (
          <div key={item + i} className="searchResults__item">
            <h3><Editable html={item.link.html} /></h3>
            <EditableParagraph className="searchResults__item__text" html={item.text} />
          </div>
        );
      });
    }
  }

  render () {
    const {noResults} = this.props;

    const hasResults = !!this.results;
    const results = this.results;

    return (
      <div className="searchResults">
        <div className="container">
          {hasResults && (
            [results]
          )}
          {noResults && (
            <Editable html={noResults.text} />
          )}

        </div>
      </div>
    );
  }
}

SearchResults.defaultProps = {
  results:[],
  noResults:{}
};

SearchResults.propTypes = {
  results:PropTypes.array,
  noResults:PropTypes.object
};

module.exports = SearchResults;
