import React from 'react';
import PropTypes from 'prop-types';
import EditableParagraph from '../../Editable/EditableParagraph';
import Editable from '../../Editable/Editable';

import './ComparisonTable.scss';

import Table from '../../Table/Table';

/** ComparisonTable
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ComparisonTable, {
    id : 'id',
    hasHighlight:true,
    isFirstRowPrice:false,
    footerText:'Source: Defaqto Matrix, data effective as of 15 December 2017',
    content: `<table>
          <thead>
                        <tr>
              <th></th>
              <th>LV=</th>
              <th>Direct Line</th>
              <th>Aviva</th>
              <th>M&amp;S</th>
              <th>John Lewis</th>
              <th>L&amp;G</th>
            </tr>
                      </thead>
          <tbody>
            <tr>
              <th>Level of cover used for comparison</th>
              <td>Home insurance</td>
              <td>Home insurance</td>
              <td>Home insurance</td>
              <td>Home insurance (Standard)</td>
              <td>Essential</td>
              <td>Home insurance (Essentials)</td>
            </tr>
            <tr>
              <th>Accidental damage to pipes</th>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>Optional</td>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>Optional</td>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
            </tr>
            <tr>
              <th>Contents in the garden i.e. patio set</th>
              <td>£1,000</td>
              <td>£1,000</td>
              <td>£1,000</td>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>£2,000</td>
              <td><img src="dist/images/icon/icon-cross.svg" /></span></td>
            </tr>
            <tr>
              <th>Plants in the garden</th>
              <td>£1,000</td>
              <td>£1,000</td>
              <td>£1,000</td>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>£5,000</td>
              <td><img src="dist/images/icon/icon-cross.svg" />
            </tr>
            <tr>
              <th>Theft from outbuildings</th>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>£2,500</td>
              <td>£2,500</td>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>£2,000</td>
              <td>£1,000</td>
            </tr>
            <tr>
              <th>Accidental damage to entertainment equipment</th>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>Optional</td>
              <td>Optional</td>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>Optional</td>
              <td>Optional</td>
            </tr>
            <tr>
              <th>Accidental damage to mirrors and glass furniture</th>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>Optional</td>
              <td>Optional</td>
              <td><img src="dist/images/icon/icon-tick.svg" alt="yes" /></td>
              <td>Optional</td>
              <td>Optional</td>
            </tr>
            <tr>
              <th>Optional Home Legal Expenses</th>
              <td>£100k</td>
              <td>£100k</td>
              <td>£100k</td>
              <td>£50k</td>
              <td>£100k</td>
              <td>£50k</td>
            </tr>
          </tbody>
        </table>`
  }), document.getElementById("element"));
*/
class ComparisonTable extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {content,hasHighlight,isFirstRowPrice,footerText, heading, subheading} = this.props;

    return (
      <div className="comparisonTable">
        <div className="container">
          <Editable tag="h2" html={heading} />
          <EditableParagraph html={subheading} />
          <Table content={content} hasHighlight={hasHighlight} isFirstRowPrice={isFirstRowPrice} footerText={footerText}/>
        </div>
      </div>
    );
  }
}

ComparisonTable.defaultProps = {
  content: '',
  className: ''
};

ComparisonTable.propTypes = {
  content: PropTypes.string,
  className: PropTypes.string,
  hasHighlight: PropTypes.bool,
  isFirstRowPrice: PropTypes.bool,
  footerText:PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string
};

module.exports = ComparisonTable;
