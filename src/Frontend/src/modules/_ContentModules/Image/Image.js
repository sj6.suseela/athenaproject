import React from 'react';
import PropTypes from 'prop-types';

import './Image.scss';

import EditableParagraph from '../../Editable/EditableParagraph';
import ResponsiveImage from '../../ResponsiveImage/ResponsiveImage';
import Module from '../../Module';

/** Image
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Image, {
    id : 'id',
    theme : {},
    width : 'full',
    mobImage : './dist/images/placeholder/carousel1.jpg',
    desktopImage : './dist/images/placeholder/image1.png',
    desktopImageSrc : 'dist/images/placeholder/image1.png',
    mobImageSrc : 'dist/images/placeholder/carousel1.jpg',
    altText : 'alt test goes here',
    legend : "this is an image"
  }), document.getElementById("element"));
*/
class Image extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };
  }

  componentDidMount () {
    Module.register(this.ref);
  }

  render () {
    const {mobImage,desktopImage,width,legend} = this.props;

    const baseClass = 'image-module';
    const cssClass = `${baseClass} ${baseClass}--${width}`;

    let wrapperClass = 'wrapper';
    if(width === 'full') wrapperClass = 'wrapper container';

    return (
      <div className={`module ${cssClass}`} ref={(c) => {this.ref = c;}}>
        <div className={wrapperClass}>
          <ResponsiveImage desktopImage={desktopImage.html} mobImage={mobImage.html} mobImageSrc={mobImage.src} desktopImageSrc={desktopImage.src} altText={desktopImage.altText} isBackground={false} doMobileFallback={true} mobImageSize={mobImage.size} desktopImageSize={desktopImage.size}/>
          <EditableParagraph className={`caption ${baseClass}__caption`} html={legend} />
        </div>
      </div>
    );
  }
}

Image.propTypes = {
  mobImage: PropTypes.object,
  desktopImage: PropTypes.object,
  width: PropTypes.string,
  legend:PropTypes.string
};

module.exports = Image;
