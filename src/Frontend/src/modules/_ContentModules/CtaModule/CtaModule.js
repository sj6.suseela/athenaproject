import React from 'react';
import PropTypes from 'prop-types';
import Cta from '../../Cta/Cta';
import Theme from '../../Theme';

import EditableImage from '../../Editable/EditableImage';
import Editable from '../../Editable/Editable';
import EditableLink from '../../Editable/EditableLink';
import EditableParagraph from '../../Editable/EditableParagraph';
import Module from '../../Module';
import EditButton from '../../CMS/EditButton';

import './CtaModule.scss';

/** CtaModule
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.CtaModule, {
    id : 'id',
    theme : { themeName:'light-meadow' },
    copy : "To purchase travel insurance for your next trip click here",
    phoneNumber : "",
    phoneText : "",
    cta : '<a href="#">This is a cta</a>',
    image : '<img src="./dist/images/beetle.png" alt="fdsfsdfsd">',
  }), document.getElementById("element"));
*/
class CtaModule extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

    };
  }

  componentDidMount () {
    Module.register(this.ref);
  }

  render () {
    const {theme,copy,phoneNumber,phoneText,image,cta, isSecondaryCta, editButton} = this.props;

    let cssClass = 'ctaModule';

    if(image) {
      cssClass += ' ctaModule--hasImage';
    }

    let themeClass = '';
    if (theme.themeName) {
      themeClass = `theme-${theme.themeName}`;
    }
    if (theme.textColour) {
      themeClass += ` theme-text-${theme.textColour}`;
    }
    else {
      themeClass += ' theme-text-contrast';
    }

    const bggradient = Theme.getTheme(theme.themeName);

    const gradientData = {
      start:bggradient[0],
      end:bggradient[1]
    };

    const hasPhone = phoneNumber || phoneText;

    return (
      <div className={`module ${cssClass}`}  ref={(c) => {this.ref = c;}}>
        <div className="wrapper">

          <div className="ctaModule__header">
            <svg viewBox="0 0 1440 34" preserveAspectRatio="xMinYMax slice">
              <defs>
                <linearGradient id={`gradient-top-${copy.replace(/ /g,'-')}`}>
                  <stop offset="0%" stopColor={gradientData.start} />
                  <stop offset="100%" stopColor={gradientData.end} />
                </linearGradient>
                <mask id="svgmask-top" x="0" y="0" width="1" height="1">
                  <path d="M0,34.2h1429.9c-209.3-20.8-455-34-717-34S207,13.4,0,34.2z" fill="white"/>
                </mask>
              </defs>
              <rect mask="url(#svgmask-top)"  x="0" y="0" width="1440" height="34" fill={`url(#gradient-top-${copy.replace(/ /g,'-')})`} />
            </svg>
          </div>
          <div className={`ctaModule__body ${themeClass}`}>
          {editButton && (
            <EditButton html={editButton} isSmall={false} align="right" />
          )}
            <div className="container">
              <EditableImage image={image.html} imageSize={image.size}/>
              <EditableParagraph className="ctaModule__heading" html={copy} />
              {hasPhone && (
                <p className="ctaModule__phone">
                  <EditableLink href={`tel:${ phoneNumber}`} className="ctaModule__phone__number" html={phoneNumber} hasLazyLoad={false}/>
                  <Editable className="ctaModule__phone__text" html={phoneText} />
                </p>
              )}
              <Cta {...cta} type={isSecondaryCta ? 'secondary' : 'primary'} rawCta={cta}/>
            </div>
          </div>
        <div className="ctaModule__footer">
            <svg viewBox="0 0 1440 34" preserveAspectRatio="xMinYMax slice">
              <defs>
                <linearGradient id={`gradient-bottom-${copy.replace(/ /g,'-')}`}>
                  <stop offset="0%" stopColor={gradientData.start} />
                  <stop offset="100%" stopColor={gradientData.end} />
                </linearGradient>
                <mask id="svgmask-bottom" x="0" y="0">
                    <path d="M1429.9,0.2H0c209.3,20.8,455,34,717,34S1222.9,21,1429.9,0.2z" fill="white" />
                </mask>
              </defs>
              <rect mask="url(#svgmask-bottom)"  x="0" y="0" width="1440" height="34" fill={`url(#gradient-bottom-${copy.replace(/ /g,'-')})`} />
            </svg>
          </div>
          <div className={`ctaModule__spacer theme-${theme.themeAfter}`}></div>
        </div>
      </div>
    );
  }
}

CtaModule.propTypes = {
  theme: PropTypes.object,
  copy: PropTypes.string,
  phoneNumber: PropTypes.string,
  phoneText: PropTypes.string,
  image: PropTypes.object,
  cta:PropTypes.string,
  isSecondaryCta: PropTypes.bool,
  editButton:PropTypes.string
};

module.exports = CtaModule;
