import React from 'react';
import PropTypes from 'prop-types';

import EditableImage from '../../Editable/EditableImage';
import Editable from '../../Editable/Editable';

/** IconList
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.IconList, {
    id : 'id',
    theme : {},
    items : [
      {
        icon : '<img src="dist/images/cms/icon-pound.png" />',
        link : '<a href="faq-answer.html">Make a claim</a>'
      },
      {
        icon : '<img src="dist/images/cms/icon-pound.png" />',
        link : '<a href="faq-answer.html">Enquire about an existing claim</a>'
      }
    ]
  }), document.getElementById("element"));
*/
class IconList extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {items, isSolid, className} = this.props;

    if(!items) {
      return ('');
    }

    const baseClass = 'list-icon';
    let additionalClasses = className;

    if(isSolid) {
      additionalClasses += ` ${baseClass}--solid`;
    }

    const links = items.map((item,i) => {
      return (
        <li key={i}>
          <EditableImage className="icon" image={item.icon.html} />
          <Editable html={typeof(item.link) === 'object' ? item.link.html : item.link} />
        </li>
      );
    });

    return (
      <ul className={`${baseClass} ${additionalClasses}`}>
        {links}
      </ul>
    );
  }
}

IconList.defaultProps = {
  items : null,
  isSolid : false
};

IconList.propTypes = {
  items : PropTypes.array,
  isSolid : PropTypes.bool,
  className: PropTypes.string
};

module.exports = IconList;
