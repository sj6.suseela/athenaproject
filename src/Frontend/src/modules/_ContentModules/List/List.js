import React from 'react';
import PropTypes from 'prop-types';

import ArrowLink from '../../ArrowLink/ArrowLink';

class List extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {items, isUnderlined} = this.props;

    if(!items) {
      return ('');
    }

    const baseClass = 'links';
    let classes = '';

    if(isUnderlined) {
      classes += ` ${baseClass}--underlined`;
    }

    let links = '';

    if(items) {
      links = items.map((item,i) => {
        return (
          <li key={i}>
            <ArrowLink link={item} />
          </li>
        );
      });
    }

    return (
      <ul className={`${baseClass}${classes}`}>
        {links}
      </ul>
    );
  }
}

List.defaultProps = {
  items: null,
  isUnderlined: false
};

List.propTypes = {
  items: PropTypes.array,
  isUnderlined: PropTypes.bool
};

module.exports = List;
