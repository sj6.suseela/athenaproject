import React from 'react';
import Vimeo from 'react-vimeo';
import Video from './Video';
import './VideoVimeo.scss';

/** Video
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.VideoVimeo, {
    id : 'id',
    theme : {
      themeName : 'cool-grey'
    },
    videoId : '1D-ussySlAQ',
    hasPriorContent : false,
    legend : "Aged 65+ and love to drive? Find out about 'Route 66', the road that has been voted the most retirement-friendly in the UK.",
    transcriptLinkText : "View video transcript",
    transcript : "<p>fdsfsd fsd f sdf sdfsd fsd f sdf sdfsdfsd fsd f sdf sdfsdf sdf sdf sdfsdf fdsfsd fsd f sdf sdfsd fsd f sdf sdfsdfsd fsd f sdf sdfsdf sdf sdf sdfsdf fdsfsd fsd f sdf sdfsd fsd f sdf sdfsdfsd fsd f sdf sdfsdf sdf sdf sdfsdf</p><p>fdsfsd fsd f sdf sdfsd fsd f sdf sdfsdfsd fsd f sdf sdfsdf sdf sdf sdfsdf</p>"
  }), document.getElementById("element"));
*/
class VideoVimeo extends Video {
  constructor (props) {
    super(props);

    this.modifier = 'vimeo';
  }

  renderPlayer () {
    const {videoId} = this.props;
    return <Vimeo videoId={videoId} />;
  }
}

VideoVimeo.defaultProps = {
  ...Video.defaultProps,
  ...{}
};

VideoVimeo.propTypes = {
  ...Video.propTypes,
  ...{}
};

module.exports = VideoVimeo;
