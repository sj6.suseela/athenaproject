import React from 'react';
import PropTypes from 'prop-types';
import './Video.scss';
import YouTube from 'react-youtube';
import ContentExpander from '../../ContentExpander/ContentExpander';
import Editable from '../../Editable/Editable';
import EditableDiv from '../../Editable/EditableDiv';
import Module from '../../Module';
import SitecoreSettings from '../../SitecoreSettings';

/** Video
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Video, {
    id : 'id',
    theme : {
      themeName : 'cool-grey'
    },
    videoId : '1D-ussySlAQ',
    hasPriorContent : false,
    legend : "Aged 65+ and love to drive? Find out about 'Route 66', the road that has been voted the most retirement-friendly in the UK.",
    transcriptLinkText : "View video transcript",
    transcript : "<p>fdsfsd fsd f sdf sdfsd fsd f sdf sdfsdfsd fsd f sdf sdfsdf sdf sdf sdfsdf fdsfsd fsd f sdf sdfsd fsd f sdf sdfsdfsd fsd f sdf sdfsdf sdf sdf sdfsdf fdsfsd fsd f sdf sdfsd fsd f sdf sdfsdfsd fsd f sdf sdfsdf sdf sdf sdfsdf</p><p>fdsfsd fsd f sdf sdfsd fsd f sdf sdfsdfsd fsd f sdf sdfsdf sdf sdf sdfsdf</p>"
  }), document.getElementById("element"));
*/
class Video extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      transcriptOpen: false,
      isEditing: false
    };

    this.modifier = '';

    this._onReady = this._onReady.bind(this);
    this._toggleTranscript = this._toggleTranscript.bind(this);

    this.playerOpts = {
      height: '390',
      width: '640',
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: false,
        cc_load_policy: this.props.hasForcedClosedCaptions ? 1 : 0,
        cc_lang_pref: this.props.hasForcedClosedCaptions ? 'en' : ''
      }
    };

    this.player = null;
  }

  componentDidMount () {
    Module.register(this.ref);
    const sc = new SitecoreSettings();

    if (this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing: sc.isExperienceEditor()
      });
    }
  }

  _onReady (event) {
    this.player = event.target;
  }

  _toggleTranscript (e) {
    e.preventDefault();
    this.setState({
      transcriptOpen: !this.state.transcriptOpen
    });
  }

  renderPlayer () {
    const {videoId} = this.props;
    return (
      <YouTube
        videoId={videoId}
        opts={this.playerOpts}
        onReady={this._onReady}
      />
    );
  }

  render () {
    const {
      theme,
      videoIdEditable,
      hasPriorContent,
      transcriptLinkText,
      transcript,
      legend
    } = this.props;
    const baseClass = 'video';

    let cssClass = `${this.modifier ? `${baseClass}--${this.modifier} ` : ''}theme-${theme.themeName} ${baseClass}`;

    if (hasPriorContent) {
      cssClass += ' video--hasPriorContent';
    }

    if (theme && theme.themeName && theme.themeName !== 'default') {
      cssClass += ' video--hasTheme';
    }

    const transcriptOpen = this.state.transcriptOpen;

    const triggerCss = this.state.transcriptOpen
      ? 'video__transTrigger video__transTrigger--open'
      : 'video__transTrigger';

    const isEditing = this.state.isEditing;

    return (
      <section
        className={`module ${cssClass}`}
        ref={c => {
          this.ref = c;
        }}
      >
        <div className="container wrapper">
          <div className="inner">{this.renderPlayer()}</div>
          {isEditing && (
            <div>
              <span>
                <strong>Youtube id:&nbsp;</strong>
              </span>
              <Editable html={videoIdEditable} />
            </div>
          )}
          <div className="video__info">
            <p className="caption video__caption">
              <Editable html={legend} />
            </p>
            <p className={triggerCss}>
              <span href="#" onClick={this._toggleTranscript}>
                <Editable className="text" html={transcriptLinkText} />
                <span className="icon">
                  <span />
                  <span />
                </span>
              </span>
            </p>
            <ContentExpander
              isOpen={transcriptOpen}
              className="video__transcript"
              onClose={this._toggleTranscript}
            >
              <EditableDiv html={transcript} />
            </ContentExpander>
          </div>
        </div>
      </section>
    );
  }
}

Video.defaultProps = {
  theme: 'default'
};

Video.propTypes = {
  theme: PropTypes.object,
  videoIdEditable: PropTypes.string,
  videoId: PropTypes.string,
  hasPriorContent: PropTypes.bool,
  transcriptLinkText: PropTypes.string,
  transcript: PropTypes.string,
  legend: PropTypes.string,
  hasForcedClosedCaptions: PropTypes.bool
};

module.exports = Video;
