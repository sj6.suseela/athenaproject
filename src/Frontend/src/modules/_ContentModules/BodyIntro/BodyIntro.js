import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../../Editable/Editable';

import './BodyIntro.scss';

/** BodyIntro
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.BodyIntro, {
    id : 'id',
    theme : {},
    heading : "This is a heading component"
  }), document.getElementById("element"));
*/
class BodyIntro extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {heading,width} = this.props;
    const baseClass = 'bodyIntro';

    if(!heading) {
      return ('');
    }

    return (
      <div className={`${baseClass} ${baseClass}--${width}`}>
        <div className="container">
          <Editable tag="h2" html={heading}/>
        </div>
      </div>
    );
  }
}

BodyIntro.propTypes = {
  heading: PropTypes.string,
  width: PropTypes.string
};

module.exports = BodyIntro;
