import React from 'react';
import PropTypes from 'prop-types';

import Avatar from '../../Avatar/Avatar';
import Editable from '../../Editable/Editable';
import EditableParagraph from '../../Editable/EditableParagraph';
import EditableImage from '../../Editable/EditableImage';
import EditableLink from '../../Editable/EditableLink';
import SitecoreSettings from '../../SitecoreSettings';
import Module from '../../Module';

import './Quote.scss';

/** Quote
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Quote, {
    copy : "I had the best time! Lorem ipsum dolor sit amet",
    author : "Barbara Rouse",
    role : "LV= member",
    image : {
      html:"<img src='http://placehold.it/500x400' />",
      altText:"alt text for quote image"
    },
  }), document.getElementById("element"));

  * @example
  ReactDOM.render(React.createElement(Components.Quote, {
    copy : "I had the best time! Lorem ipsum dolor sit amet",
    author : "Barbara Rouse",
    role : "LV= member",
  }), document.getElementById("element"));

  * @example
  ReactDOM.render(React.createElement(Components.Quote, {
    theme : 'pistachio',
    copy : "I had the best time! Lorem ipsum dolor sit amet",
    author : "Barbara Rouse",
    avatar : '<img src="http://i.pravatar.cc/60?img=45" />',
    role : "LV= member",
  }), document.getElementById("element"));
*/
class Quote extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing:false
    };
  }

  componentDidMount () {
    if (!this.props.noFadeInModule) {
      Module.register(this.ref);
    }

    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  render () {
    const {copy,image,author,authorLink,role,avatar, theme, themeTextColour, source, link} = this.props;
    const baseClss = 'quote';

    let cssClass = this.props.cssClass;
    cssClass += ` ${baseClss}`;

    if(theme) {
      cssClass += ` theme-${theme} ${baseClss}--themed`;
    }

    if (themeTextColour) {
      cssClass += ` theme-text-${themeTextColour}`;
    }
    else {
      cssClass += ' theme-text-contrast';
    }

    const hasImage = !!image.html;

    if(!hasImage)  cssClass += ` ${baseClss}--mini`;

    let theAuthor = author;

    if(authorLink && authorLink.html) {
      theAuthor = authorLink.html;

      if(author) {
        theAuthor = `<a href=${authorLink.href} id=${authorLink.id}>${author}</a>`;
      }
    }

    let hasAuthorLinkSrc = null;
    if(authorLink) {
      hasAuthorLinkSrc = !!authorLink.href;
    }

    const isEditing = this.state.isEditing;

    return (
        <div className={`module ${cssClass}`} ref={(c) => {this.ref = c;}}>
          <div className="container wrapper">
            <div className="quote__copy">
              <blockquote cite={author}>
                <EditableParagraph html={copy} />
                <footer>
                  <Avatar name={author} src={avatar} link={authorLink} />
                  <div className="quote__details">
                    {isEditing && (
                      <div>
                        <span className="quote__author"><Editable html={author} /></span>
                        <span className="quote__role"><Editable html={role}/></span>
                      </div>
                    )}
                    {!isEditing && hasAuthorLinkSrc && (
                      <div>
                        <Editable className="quote__author" html={theAuthor} />
                        <span className="quote__role">
                          <EditableLink {...authorLink} html={role} />
                        </span>
                      </div>
                    )}
                    {!isEditing && !hasAuthorLinkSrc && (
                      <div>
                        <Editable className="quote__author" html={theAuthor} />
                        <Editable className="quote__role" html={role}/>
                      </div>
                    )}
                  </div>
                  {source && (
                    <div className="quote__source">
                      <a href={link.href} id={link.id}>
                        { !source && (
                          <Editable className="quote__source__label" {...link}/>
                        )}

                        <Editable className="quote__source__label" />
                        <EditableImage defaultClass="" image={source}/>
                      </a>
                    </div>
                  )}
                </footer>
              </blockquote>
            </div>
            {hasImage && (
              <figure className="quote__image" ref={(c) => {this.parent = c;}}>
                <EditableImage image={image.html} imageSize={image.size} />
              </figure>
            )}
          </div>
        </div>
    );
  }
}

Quote.defaultProps = {
  theme: null,
  avatar: '',
  copy: '',
  image: {},
  author: '',
  role: '',
  source: '',
  authorLink:{},
  link:{}
};

Quote.propTypes = {
  theme: PropTypes.string,
  avatar: PropTypes.string,
  copy: PropTypes.string,
  image: PropTypes.object,
  author: PropTypes.string,
  role: PropTypes.string,
  source: PropTypes.any,
  authorLink:PropTypes.object,
  link:PropTypes.object,
  themeTextColour:PropTypes.string,
  cssClass: PropTypes.string,
  noFadeInModule: PropTypes.bool
};

module.exports = Quote;
