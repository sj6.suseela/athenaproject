import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../../Editable/Editable';
import EditableImage from '../../Editable/EditableImage';
import SitecoreSettings from '../../SitecoreSettings';
import Module from '../../Module';
import axios from 'axios';

import Loading from '../../Loading/Loading';

import './Quote.scss';

/** ReevooSingleQuote
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.StoryCards, {
    id: 'module-StoryCards-0',
    label: 'Our Philosophy',
    heading: 'Great customer service is just the beginning',
    cta: {
      type: 'secondary',
      label: 'More stories',
      href:'#'
    },
    theme: {
      themeName: 'dusty-mint',
      topCurve:{
        type:'convex'
      },
      bottomCurve:{
        type: 'convex'
      },
      themeBefore:'warm-grey',
      themeAfter:'warm-grey'
    },
    cards: [
      {
        type: 'reevoo',
        theme: {
          themeName: 'white',
        },
        reevooParams: {
          //url: 'https://api.reevoocloud.com/v4/reviews/',
          url: '',
          id: '23138632',
          trkref: 'LV-GI',
          sku: 'CINSTY',
        },
        link: {
          href: "/src/Frontend/reevoo-reviews.html#review_23138632",
          html: "<a id=\"23138632/review\" href=\"/reevoo-reviews.html#review_23138632\">FAQ</a>",
          id: "23138632",
          label: "Click to read more",
          rel: ""
        },
        role: 'LV= Customer',
        source: {
            html:'<img src="dist/images/reevoo.png" alt="Reevoo" />',
              size:{width:340,height:81}
          },
      },
    ]
  }), document.getElementById("element"));
*/
class ReevooSingleQuote extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing:false,
      loaded:false,
      reevooItem: []
    };
  }

  componentDidMount () {
    if (!this.props.noFadeInModule) {
      Module.register(this.ref);
    }

    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }

    let ajaxlink;

    if (this.props.reevooParams.url) {
      ajaxlink = `${this.props.reevooParams.url}/${this.props.reevooParams.id}?trkref=${this.props.reevooParams.trkref}`;
    }
    else {
      ajaxlink = 'dist/json/reevoo-quote.json';
    }

    const config = {
      headers: {
        'Data-Type': 'json'
      }
    };

    axios(ajaxlink,config)
      .then((response) => {
        const jsonResponse = response.data;
        if (jsonResponse !== '' && jsonResponse !== null) {
          this.setState({
            loaded:true,
            reevooItem: jsonResponse
          });
        }
      },
      () => {}
    );
  }

  render () {
    const {image, role, theme, themeTextColour, source, link} = this.props;
    const {reevooItem,loaded} = this.state;

    const baseClss = 'quote';

    let cssClass = this.props.cssClass;
    cssClass += ` ${baseClss}`;

    if(theme) {
      cssClass += ` theme-${theme} ${baseClss}--themed`;
    }

    if (themeTextColour) {
      cssClass += ` theme-text-${themeTextColour}`;
    }
    else {
      cssClass += ' theme-text-contrast';
    }

    const hasImage = !!image.html;

    if(!hasImage)  cssClass += ` ${baseClss}--mini`;

    return (
      <div className={`module ${cssClass}`} ref={(c) => {this.ref = c;}}>
        <div className="container wrapper">
          <div className="quote__copy">
            {loaded ?
              <blockquote cite={`${reevooItem.reviewer.first_name} - ${role}`}>
                <p>{reevooItem.good_points}</p>
                <footer>
                  <div className="quote__details">
                      <span className="quote__author">{reevooItem.reviewer.first_name}</span>
                      <span className="quote__role">{role}</span>
                  </div>
                  {source && (
                    <div className="quote__source reevooQuote__source">
                      <a href={link.href} id={link.id}>
                        <Editable className="quote__source__label" {...link} html={'More on'}/>
                        <EditableImage defaultClass="" image={source}/>
                      </a>
                    </div>
                  )}
                </footer>
              </blockquote>
            :
              <Loading isActive={!loaded} isFullScreen={false}/>
            }

          </div>
          {hasImage && (
            <figure className="quote__image" ref={(c) => {this.parent = c;}}>
              <EditableImage image={image.html} imageSize={image.size} />
            </figure>
          )}
        </div>
      </div>
    );
  }
}

ReevooSingleQuote.defaultProps = {
  theme: null,
  avatar: '',
  copy: '',
  image: {},
  author: '',
  role: '',
  source: '',
  authorLink:{},
  link:{}
};

ReevooSingleQuote.propTypes = {
  theme: PropTypes.string,
  avatar: PropTypes.string,
  copy: PropTypes.string,
  image: PropTypes.object,
  author: PropTypes.string,
  role: PropTypes.string,
  source: PropTypes.any,
  authorLink:PropTypes.object,
  link:PropTypes.object,
  themeTextColour:PropTypes.string,
  cssClass: PropTypes.string,
  noFadeInModule: PropTypes.bool,
  reevooParams: PropTypes.object
};

module.exports = ReevooSingleQuote;
