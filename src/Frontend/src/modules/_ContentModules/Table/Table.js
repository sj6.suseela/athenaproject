
import React from 'react';
import PropTypes from 'prop-types';

import EditableDiv from '../../Editable/EditableDiv';

/**
 * Example Module

  <div id="m-Example"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Example, {
      theme: {
        themeName: 'white',
        themeBefore: 'warm-grey',
        themeAfter: 'warm-grey',
        topCurve:
        {
          type: 'convex'
        },
        bottomCurve:
        {
          type: 'convex'
        },
      }
    }), document.getElementById("m-Example"));
  </script>
 */
class Table extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {content, className} = this.props;

    return (
      <EditableDiv html={content} className={className} />
    );
  }
}

Table.defaultProps = {
  content: '',
  className: ''
};

Table.propTypes = {
  content: PropTypes.string,
  className: PropTypes.string
};

module.exports = Table;
