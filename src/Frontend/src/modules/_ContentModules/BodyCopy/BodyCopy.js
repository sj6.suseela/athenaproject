import React from 'react';
import PropTypes from 'prop-types';

import EditableDiv from '../../Editable/EditableDiv';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';
import BackToTop from '../../BackToTop/BackToTop';

import './BodyCopy.scss';
import '../../BackToTop/BackToTop.scss';

/** BodyCopy
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.BodyCopy, {
    id : 'id',
    theme : {
      themeName : 'warm-grey'
    },
    copy : "<ul><li>Research among motorists aged 65 and over revealed that 74% cut back on the amount of driving they do once they reach retirement.</li><li>To overcome this lack of confidence on the roads, LV= has created the UK’s very own ‘Route 66’ along a section of Scottish coastal highway.</li><li>This August, motorists over 65 using Route 66 can enjoy  some roadside perks, courtesy of LV=</li></ul>",
    isBackToTopLinkHidden: false,
    backToTopLinkText: 'BACK TO TOP'
  }), document.getElementById("element"));
*/
class BodyCopy extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {
      copy,
      width,
      hasBackground,
      isIntro,
      isBullets,
      theme,
      hasAltHeadingStyle,
      backToTopLinkText,
      isBackToTopLinkHidden
    } = this.props;

    const baseClass = 'bodyCopy';

    let cssClass = `${baseClass}--${width}`;

    if (hasBackground) {
      cssClass += ` ${baseClass}--withBg`;
    }

    if (theme && theme.themeName && theme.themeName !== 'default') {
      cssClass += ` ${baseClass}--hasTheme`;
    }

    if (isIntro) {
      cssClass += ` ${baseClass}--intro`;
    }

    if (isBullets) {
      cssClass += ' bulleted';
    }

    if (hasAltHeadingStyle) {
      cssClass += ` ${baseClass}--alt-headings`;
    }

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} classes={cssClass}>
        <EditableDiv html={copy} />
        {!isBackToTopLinkHidden && <BackToTop text={backToTopLinkText} />}
      </ModuleWrapper>
    );
  }
}

BodyCopy.defaultProps = {
  theme: {themeName: 'default'},
  customClass: '',
  hasBackground: false,
  isBackToTopLinkHidden: true,
  backToTopLinkText: 'Back to top'
};

BodyCopy.propTypes = {
  theme: PropTypes.object,
  copy: PropTypes.string,
  width: PropTypes.string,
  hasBackground: PropTypes.bool,
  isIntro: PropTypes.bool,
  customClass: PropTypes.string,
  isBullets: PropTypes.bool,
  hasAltHeadingStyle: PropTypes.bool,
  isBackToTopLinkHidden: PropTypes.bool,
  backToTopLinkText: PropTypes.string
};

module.exports = BodyCopy;
