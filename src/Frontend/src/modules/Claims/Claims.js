import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';
import IconList from '../_ContentModules/List/IconList';
import NotificationDetail from '../Notification/NotificationDetail';

import './Claims.scss';

/** Claims
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Claims, {
    id : 'id',
    theme : {},
    heading : 'Have you been affected by the following',
    image : {
			html:`<img src="dist/images/cms/flourish-insurance.svg" />`
		},
    issues : {
      items : [
        {
          type : 'warning',
          title : `Monarch cease trading`,
          content : `<p>What you need to know about cancellation and compensation if you're travelling or booked with Monarch</p>`,
          link : `<a href="#">Read more</a>`
        },
        {
          type : 'warning',
          title : `Ryan Air flight cancellations`,
          content : `<p>Flight cancelled? We answer the key questions about compensation and what you can claim</p>`,
          link : `<a href="#">Read more</a>`
        }
      ]
    }
  }), document.getElementById("element"));
*/
class Claims extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading, links, image, issues} = this.props;
    const baseClass = 'claims';

    let notifications = null;

    if(issues && issues.items) {
      notifications = issues.items.map((item,i) => {
        return (
          <NotificationDetail key={i} {...item} />
        );
      });
    }

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div className="columns@md">
          <div>
            <Editable tag="h2" className={`title ${baseClass}__title`} html={heading} />
            <IconList items={links} isSolid={true} />
            <EditableImage className={`${baseClass}__image hide@sm`} image={image.html} />
          </div>
          <div>
            {notifications && (
              <div>
                <Editable tag="h3" className="title" html={issues.title} />
                {notifications}
              </div>
            )}
            <EditableImage className={`${baseClass}__image hide@md hide@lg`} image={image.html} />
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

Claims.defaultProps = {
  heading : '',
  links : null,
  image : {},
  issues : null
};

Claims.propTypes = {
  heading : PropTypes.string,
  links : PropTypes.array,
  image : PropTypes.object,
  issues : PropTypes.object
};

module.exports = Claims;
