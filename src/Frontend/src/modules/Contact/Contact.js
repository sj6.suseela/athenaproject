import React from 'react';
import PropTypes from 'prop-types';

import Cta from '../Cta/Cta';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditableImage from '../Editable/EditableImage';
import EditableParagraph from '../Editable/EditableParagraph';
import EditableLink from '../Editable/EditableLink';
import EditButton from '../CMS/EditButton';
import './Contact.scss';

/** Contact
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Contact, {
    id : 'id',
    tel : '01234 621 537',
    opening : 'Lines open <b>24 hours</b>, 7 days a week',
    info : 'Calls may be recorded and/or monitored for training and audit purposes.'
  }), document.getElementById("element"));

  * @example
  ReactDOM.render(React.createElement(Components.Contact, {
    title : `Medical Emergencies`,
    tel : '01234 621 537',
    telAlt : '+44 1243 621 537',
    telAltIntro : 'From abroad:',
    opening : 'Lines open <b>24 hours</b>, 7 days a week',
    info : 'Calls may be recorded and/or monitored for training and audit purposes.'
  }), document.getElementById("element"));
*/
class Contact extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {isAlt, image, title, lead, intro, telIntro, tel, telSuffix, telAlt, telAltIntro, opening, info, primaryCta, emailHeading, email, writeHeading, write, notes, cta, editButton} = this.props;

    const baseClass = 'contact';

    const altLayout = isAlt || (primaryCta !== '');
    const hasDetail = opening || info;

    // NOTE: path to heart will break in live

    return (
      <div className={`${baseClass} ${baseClass}--${altLayout ? 'alt' : 'default' }`}>
        <EditButton isSmall={true} html={editButton} align="right" />
        <Editable tag="h2" className={`${baseClass}__title h3`} html={title} />
        <div className={`${baseClass}__introWrapper`}>
          <EditableParagraph className={`${baseClass}__lead`} html={lead} />
          <EditableDiv className={`${baseClass}__intro`} html={intro} />
        </div>
        <Cta {...cta} type="primary" rawCta={primaryCta} isWide={true} />
        <div className={`${baseClass}__main`}>
          <EditableImage className={`${baseClass}__image`} image={image.html} />
          <div className={`${baseClass}__body`}>
            {tel && (
              <div className={`${baseClass}__primary-contact`}>
                <Editable className={`prefix ${baseClass}__tel-prefix`} html={telIntro} />
                <EditableLink html={tel} href={`tel:${tel}`} className={`${baseClass}__tel`} hasLazyLoad={false} />
                <Editable className={`suffix ${baseClass}__tel-suffix`} html={telSuffix} />
              </div>
            )}
            {telAlt && (
              <div className={`${baseClass}__secondary-contact`}>
                <Editable tag="b" className={`prefix ${baseClass}__tel-prefix`} html={telAltIntro} />
                <EditableLink html={telAlt} href={`tel:${telAlt}`} className={`${baseClass}__tel`} hasLazyLoad={false} />
              </div>
            )}
            <div className={`${baseClass}__details`}>
              {hasDetail && (
                <div className={`${baseClass}__detail`}>
                  <EditableDiv className={`${baseClass}__opening`} html={opening} />
                  <EditableDiv className={`${baseClass}__info`} html={info} />
                </div>
              )}
              {email && (
                <div className={`${baseClass}__email`}>
                    <Editable tag="b" className={`prefix ${baseClass}__email-prefix`} html={emailHeading} />
                    <EditableLink html={email} href={`mailto:${email}`} className={`${baseClass}__email-link`} hasLazyLoad={false}/>
                </div>
              )}
              {write && (
                <div className={`${baseClass}__write`}>
                    <Editable tag="b" className={`prefix ${baseClass}__write-prefix`} html={writeHeading} />
                    <Editable tag="span" html={write} />
                </div>
              )}
            </div>
            {cta && !notes && (
              <div className={`${baseClass}__cta`}>
                <Cta {...cta} type="secondary" rawCta={cta} isWide={true} />
              </div>
            )}
          </div>
        </div>
        <EditableDiv className={`${baseClass}__notes`} html={notes} />
        {cta && notes && (
          <div className={`${baseClass}__cta`}>
            <Cta {...cta} type="secondary" rawCta={cta} isWide={true} />
          </div>
        )}
      </div>
    );
  }
}

Contact.defaultProps = {
  isAlt : false,
  image : {
    html:'<img src="dist/images/LV_Heart.png" alt="" />'
  },
  title : '',
  lead : '',
  intro : '',
  primaryCta : '',
  telIntro : '',
  tel : '',
  telSuffix : '',
  telAltIntro : '',
  telAlt : '',
  opening : '',
  info : '',
  email : '',
  write : '',
  notes : '',
  cta : '',
  emailHeading:'',
  writeHeading:'',
  editButton:''
};

Contact.propTypes = {
  isAlt : PropTypes.bool,
  image : PropTypes.object,
  title : PropTypes.string,
  lead : PropTypes.string,
  intro : PropTypes.string,
  primaryCta : PropTypes.string,
  telIntro : PropTypes.string,
  tel : PropTypes.string,
  telSuffix : PropTypes.string,
  telAltIntro : PropTypes.string,
  telAlt : PropTypes.string,
  opening : PropTypes.string,
  info : PropTypes.string,
  email : PropTypes.string,
  write : PropTypes.string,
  notes : PropTypes.string,
  cta : PropTypes.string,
  emailHeading: PropTypes.string,
  writeHeading: PropTypes.string,
  editButton: PropTypes.string
};

module.exports = Contact;
