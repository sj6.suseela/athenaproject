
import React from 'react';
import PropTypes from 'prop-types';

import ContactCard from './ContactCard';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';

import './ContactCards.scss';

/** ContactCards
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ContactCards, {
    id : 'id',
    theme : {},
    heading : 'Press Office Contacts',
    content : `<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam reprehenderit obcaecati ratione unde accusamus necessitatibus impedit quidem. Quisquam omnis consequatur dolor sit, quas itaque blanditiis, nulla officia error commodi iusto.</p>`,
    items : [
        {
          image : '<img src="http://placehold.it/300x200" />',
        name : 'Jonathan Sellors',
        position : 'Head of Corporate Communications',
        email : '<a href="mailto:jon.sellors@lv.com">jon.sellors@lv.com</a>',
        tel : '020 7634 4447',
        mobile : '07777 7777 777'
        },
        {
          image : '<img src="http://placehold.it/300x200" />',
        name : 'Robyn Margetts',
        position : 'Head of Corporate Communications',
        email : '<a href="mailto:jon.sellors@lv.com">jon.sellors@lv.com</a>',
        tel : '020 7634 4447',
        mobile : '07777 7777 777'
        },
        {
          image : '<img src="http://placehold.it/300x200" />',
        name : 'Kaidee Horton (nee Sibborn)',
        position : 'Head of Corporate Communications',
        email : '<a href="mailto:jon.sellors@lv.com">jon.sellors@lv.com</a>',
        tel : '020 7634 4447',
        mobile : '07777 7777 777'
        },
        {
          image : '<img src="http://placehold.it/300x200" />',
        name : 'Hannah Fensome',
        position : 'Head of Corporate Communications',
        email : '<a href="mailto:jon.sellors@lv.com">jon.sellors@lv.com</a>',
        tel : '020 7634 4447',
        mobile : '07777 7777 777'
        },
        {
          image : '<img src="http://placehold.it/300x200" />',
        name : 'Lloyd Purnell',
        position : 'Head of Corporate Communications',
        email : '<a href="mailto:jon.sellors@lv.com">jon.sellors@lv.com</a>',
        tel : '020 7634 4447',
        mobile : '07777 7777 777'
        }
      ]
  }), document.getElementById("element"));
*/
class ContactCards extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading, content, items} = this.props;
    const baseClass = 'contact-cards';

    const cards = items.map((item,i) => {
      return (
        <ContactCard key={i} {...item} />
      );
    });

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <Editable tag="h2" className={`title ${baseClass}__title`} html={heading} />
        <EditableDiv className={`${baseClass}__desc`} html={content} />
        <div className={`${baseClass}__main`}>
          {cards}
        </div>
      </ModuleWrapper>
    );
  }
}

ContactCards.defaultProps = {
  items : [],
  heading : '',
  content : ''
};

ContactCards.propTypes = {
  items : PropTypes.array,
  heading : PropTypes.string,
  content : PropTypes.string
};

module.exports = ContactCards;
