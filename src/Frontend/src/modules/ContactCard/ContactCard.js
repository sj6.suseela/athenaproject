
import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableImage from '../Editable/EditableImage';

import './ContactCard.scss';

/** ContactCard
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ContactCard, {
    id : 'id',
    title : 'Press Contact',
    name : 'Jonathan Sellors',
    position : 'Head of Corporate Communications',
    email : '<a href="mailto:jon.sellors@lv.com">jon.sellors@lv.com</a>',
    tel : '020 7634 4447',
    mobile : '07777 7777 777'
  }), document.getElementById("element"));
*/
class ContactCard extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {name, position, email, tel, mobile, image, description} = this.props;
    const baseClass = 'contact-card';

    return (
      <div className={baseClass}>
        <div className={`${baseClass}__main`}>
          <EditableImage className={`${baseClass}__image`} image={image.html} />
          <div className={`${baseClass}__body`}>
            <dl>
              <Editable tag="dt" className={`h3 ${baseClass}__name`} html={name} />
              <Editable tag="dd" className={`${baseClass}__position`} html={position} />
              <Editable tag="dd" className={`${baseClass}__description`} html={description} />
              <Editable tag="dd" className={`${baseClass}__email`} html={email} />
              <Editable tag="dd" className={`${baseClass}__tel`}  html={tel} />
              <Editable tag="dd" className={`${baseClass}__mobile`}  html={mobile} />
            </dl>
          </div>
        </div>
      </div>
    );
  }
}

ContactCard.defaultProps = {
  name : '',
  position : '',
  email : '',
  tel : '',
  mobile : '',
  image : {},
  description:''
};

ContactCard.propTypes = {
  name : PropTypes.string,
  position : PropTypes.string,
  email : PropTypes.string,
  tel : PropTypes.string,
  mobile : PropTypes.string,
  image : PropTypes.object,
  description: PropTypes.string
};

module.exports = ContactCard;
