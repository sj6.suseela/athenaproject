import React from 'react';
import PropTypes from 'prop-types';
import './SubArticles.scss';
import Carousel from '../Carousel/Carousel';
import ArticlePreview from '../ArticlePreview/ArticlePreview';
import SectionTitle from '../Section/SectionTitle';

/** SubArticles
  * @description description
  * @example

  var item = {
    title : "Lorem ipsum lorem ipsum lorem ipsum lorem ...",
    category : 'Category',
    link:{
      href: "#/article-2/",
    },
    readTime : "5 min read",
    image : '<img src="http://placehold.it/600x400" alt="" />',
    date : { day:12, month:{ label:"December", number:12, }, year:2017, time:"12:00" },
  };

  ReactDOM.render(React.createElement(Components.SubArticles, {
    id : 'id',
    theme : {},
    heading : "Want more from driving?",
    icon : '<img src="dist/images/icon/icon-newsletter.svg">',
    items : [item, item, item, item]
  }), document.getElementById("element"));
*/
class SubArticles extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
    this.items = props.items.map((item,i) => {
      return (<div key={i}><ArticlePreview {...item} /></div>);
    });
  }

  render () {
    const baseClass = 'subArticles';
    const {theme} = this.props;

    const options = {
      cellAlign: 'left',
      prevNextButtons: false,
      pageDots: false,
      groupCells: true,
      wrapAround: false
    };

    const atts = {
      className : `module ${baseClass}`
    };

    if(theme) {
      atts.className += ` theme-${theme}`;
    }

    return (
      <section {...atts}>
        <div className="container">
          <SectionTitle {...this.props} />
        </div>
        <div className={`${baseClass}__container container`}>
          <div className="carousel carousel--4up">
            <Carousel items={this.items} options={options} id={this.props.id}/>
          </div>
        </div>
      </section>
    );
  }
}

SubArticles.defaultProps = {
  theme : '',
  items: [],
  heading: '',
  icon:'',
  id:''
};

SubArticles.propTypes = {
  theme:PropTypes.string,
  items:PropTypes.array,
  heading:PropTypes.string,
  icon:PropTypes.string,
  id:PropTypes.string
};

module.exports = SubArticles;