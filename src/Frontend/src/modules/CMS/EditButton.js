import React from 'react';
import PropTypes from 'prop-types';

import EditableDiv from '../Editable/EditableDiv';
import './EditButton.scss';

/**
 * EditButton Module

  <div id="m-EditButton"></div>
  <script>
    ReactDOM.render(React.createElement(Components.EditButton, {
    }), document.getElementById("m-EditButton"));
  </script>
 */
class EditButton extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {html, align, isSmall} = this.props;

    if(!html) {
      return ('');
    }

    return (
      <EditableDiv html={html} className={`cms-edit ${isSmall ? 'cms-edit--sm' : ''} align-${align}`} />
    );
  }
}

EditButton.defaultProps = {
  html : '',
  align : 'default',
  isSmall : false
};

EditButton.propTypes = {
  html : PropTypes.string,
  align : PropTypes.string,
  isSmall : PropTypes.bool
};

module.exports = EditButton;
