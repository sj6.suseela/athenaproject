import React from 'react';
import PropTypes from 'prop-types';

import './OpvSignInPage.scss';
// import Cta from '../Cta/Cta';

// import b2cauth from 'react-azure-adb2c';
import EditableDiv from '../Editable/EditableDiv';

// b2cauth.initialize({
//   instance: 'https://login.microsoftonline.com/tfp/',
//   tenant: 'lvfsbtocint.onmicrosoft.com',
//   signInPolicy: 'B2C_1_INT-SignupV1',
//   applicationId: '273ea147-fa3d-4c53-8015-87a6b9cc4040',
//   cacheLocation: 'sessionStorage',
//   scopes: ['https://lvfsbtocint.onmicrosoft.com/OPVPortal/user_impersonation'],
//   redirectUri: 'http://localhost:3000',
//   postLogoutRedirectUri: window.location.origin,
// });

class OpvSignInPage extends React.Component {

  constructor (props) {
    super (props);

    this.state = {};
  }

  render () {
    // get the prop settings
    const {formArea, content, link, termsCheckbox} = this.props;

    const baseClass = 'sign-in-page';

    return (
        <div className={`${baseClass}`}>
            <div className={`${baseClass}__main`}>
                <div className={`${baseClass}__signinform`}>
                    <EditableDiv className={`${baseClass}`} html={formArea} />
                    <EditableDiv html={termsCheckbox} className={`${baseClass}__checkbox`} />
                    <EditableDiv html={link} className={`${baseClass}__link`} />
                </div>
                <div className={`${baseClass}__createacc`}>
                    <EditableDiv className={`${baseClass}__content`} html={content} />
                </div>
            </div>
        </div>
    );
  }
}


OpvSignInPage.propTypes = {
  formArea: PropTypes.string,
  content : PropTypes.string,
  link : PropTypes.string,
  termsCheckbox: PropTypes.string
};

module.exports = OpvSignInPage;
