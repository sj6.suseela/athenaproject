import React from 'react';
import PropTypes from 'prop-types';
import EditButton from '../CMS/EditButton';

const style = {
  textAlign:'center',
  padding:'1rem 0'
};

const Anchor = (props) => (
  <div className="anchor" data-offset-desktop={props.offset.desktop} data-offset-mobile={props.offset.mobile} id={props.id}>
    {props.editButton && (
      <div style={style}>
        {props.id ? (
          <span>Anchor with ID: <strong>{props.id}</strong></span>
        ) : (
          <span>Anchor with no ID set</span>
        )}
        <EditButton html={props.editButton} align="center"  />
      </div>
    )}
  </div>
);

Anchor.propTypes = {
  id: PropTypes.string,
  offset:PropTypes.object,
  editButton:PropTypes.string
};

module.exports = Anchor;
