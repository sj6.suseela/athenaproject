/* global siteSettings*/
const SitecoreSettings = function () {
  this.isExperienceEditor = function () {
    return siteSettings.site.sitecore.editing;
  };
  this.isPreview = function () {
    return siteSettings.site.sitecore.preview;
  };
};

module.exports = SitecoreSettings;
