import React from 'react';
import PropTypes from 'prop-types';
import './OpvVerifyPhoneNumber.scss';
import Editable from '../Editable/Editable';

class OpvVerifyPhoneNumber extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
    
    };
  }

  render () {
    const {checkmark} = this.props;
    const baseClass = 'opv-verify-phone-number';
    return (
      <div className={`${baseClass}`} >
        <div id="verify-phone-send-code">

        </div>
        <div id="verify-phone-verify-code">
          
        </div>
        <div id="verify-phone-verified">
        
        </div>
        <Editable className={`${baseClass}__success-checkmark`} html={checkmark} />
      </div>
    );
  }
}

OpvVerifyPhoneNumber.propTypes = {
  checkmark : PropTypes.string
};

module.exports = OpvVerifyPhoneNumber;
