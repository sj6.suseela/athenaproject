import React from 'react';
import PropTypes from 'prop-types';

import './BackToTop.scss';
import Editable from '../Editable/Editable';
import Icon from '../Icon/Icon';
import ScrollToY from '../scrolltoy';

/** BackToTop
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.BackToTop, {
    text : 'Back to top'
  }), document.getElementById("element"));
*/
class BackToTop extends React.Component {
  constructor (props) {
    super(props);
    this.scrollToTop = this.scrollToTop.bind(this);
  }

  scrollToTop () {
    new ScrollToY(0, 500, 'easeInOutQuint');
  }

  render () {
    const {text} = this.props;

    const baseClass = 'backToTop';

    const cssClass = '';

    return (
      <div className={`${baseClass} ${cssClass}`}>
        <Icon icon="uparrow" fillColor="#04589b" />
        <a onClick={this.scrollToTop}>
          <Editable html={text} />
        </a>
      </div>
    );
  }
}

BackToTop.propTypes = {
  text: PropTypes.string
};

module.exports = BackToTop;
