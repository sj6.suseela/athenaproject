import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Cta from '../Cta/cta';
import './OpvSetupTwoStepVerification.scss';

class OpvSetupTwoStepVerification extends React.Component {

  constructor (props) {
    super(props);

    this.state = {

    };
  }

  render () {
    const {submit, title, description} = this.props;
    const baseClass = 'setup-two-step-verification';

    return (
        <div>
            <Editable tag="h2" className={`${baseClass}__title`} html={title} />
            <EditableDiv className={`${baseClass}__content`} html={description} />
            <div className={`${baseClass}__cta`}>
              <Cta type="primary" className='subbtn' isSmall={true} rawCta={submit} />
            </div>

        </div>
    );
  }

}


OpvSetupTwoStepVerification.propTypes = {
  title : PropTypes.string,
  description : PropTypes.string,
  submit: PropTypes.string
};

module.exports = OpvSetupTwoStepVerification;
