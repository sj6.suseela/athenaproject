import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableImage from '../Editable/EditableImage';
import EditableDiv from '../Editable/EditableDiv';
import ArrowLink from '../ArrowLink/ArrowLink';
import Contact from '../Contact/Contact';
import Icon from '../Icon/Icon';
import {isMobile} from 'react-device-detect';

import './ContactUs.scss';

/** ContactUs
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ContactUs, {
    id : 'id',
    theme : {},
    headline:'Let us help you get in touch with the right team',
    image: '<img src="http://placehold.it/200x150" />',
    items:[
        {
          title: 'Breakdown emergency',
          content: {
            contact:{
              telephone:'0800 707 6967',
              altHeading:'<b>From abroad:</b> +44 1243 621 537',
              subText:'Our <b>24 hour emergency helpline</b>  for medical emergencies and assistance'
            },
            copy:`<p>Have the following information ready</p>
                  <ul>
                    <li>Your policy number and the phone number your calling from more text more text more text</li><li>Your policy number and the phone number you're calling from</li><li>Your policy number and the phone number your calling from</li>
                    </ul>`
          },
          type:'featured'
        },
        {
          title: 'Home emergency',
          additional: 'For customers with home emergency cover',
          type:'featured'
        },
        {
          title: 'Asset allocation'
        },
        {
          title: 'Insurance',
          children: [
            {
              title: 'Car Insurance',
              content: {
                contact:{
                  telephone:'0800 707 6967',
                  altHeading:'<b>From abroad:</b> +44 1243 621 537',
                  subText:'Our <b>24 hour emergency helpline</b>  for medical emergencies and assistance'
                },
                links:[
                  {
                    link:'<a href="#">Make a claim</a>'
                  },
                  {
                    link:'<a href="#">Enquire about an existing claim</a>'
                  },
                  {
                    link:'<a href="#">Form name (PDF, 357KB)</a>',
                    isPDF:true,
                    subLink:'<a href="#">Death claim guidance notes (PDF,89KB)'
                  },
                  {
                    link:'<a href="#">Renew policy</a>'
                  },
                  {
                    link:'<a href="#">Request policy documents</a>',
                    subText:'Get copies of your insurance schedule or policy documents'
                  },
                  {
                    link:'<a href="#">Pre travel advice</a>'
                  },
                ]
              }
            },
            {
              title: 'Home Insurance',
              content: 'content home'
            },
            {
              title: 'Life Insurance',
              content: 'content life'
            },
            {
              title: 'Travel Insurance',
              content: {
                links:[
                  {
                    link:'<a href="contact-us--make-a-claim.html">Make a claim</a>'
                  }
                ]
              }
            },
            {
              title: 'Classic Car Insurance',
              content: 'content classic car'
            },
            {
              title: 'Caravan Insurance',
              content: 'content caravan'
            },
            {
              title: 'Motorbike Insurance',
              content: 'content motorbike'
            }
          ]
        },
        {
          title: 'Pensions and Retirement'
        },
        {
          title: 'Investments'
        },
        {
          title: 'General Enquiries'
        },
        {
          title: 'Complaints'
        }
      ]
  }), document.getElementById("element"));
*/
class ContactUs extends React.Component {
  constructor (props) {
    super(props);

    this._clickPrimary = this._clickPrimary.bind(this);
    this._clickSecondary = this._clickSecondary.bind(this);
    this._clickTertiary = this._clickTertiary.bind(this);
    this._reset = this._reset.bind(this);

    this.state = {
      stage:1,
      showIntro:true,
      secondary: {
        active:'false',
        title:'',
        content:'',
        children:[]
      },
      tertiary: {
        active:'false',
        title:'',
        content:''
      },
      quaternary:{
        active:'false',
        title:'',
        content:''
      }
    };

    this.primaryLinks = props.sections.map((section,i) => {
      const items = section.items.map((item,i) => {
        const html = `<span>${item.title}</span>${  item.additional ? ` <span>${item.additional}</span>` : ''}`;
        const type = item.type ? item.type : '';
        return (
          <li key={i} onClick={(e) => this._clickPrimary(item, e)} className={type}>
            <a href="#">
              <ArrowLink link={html} />
            </a>
          </li>
        );
      });

      return (
        <div key={section.headline + i} className="contact-us__section">
          <Editable tag="h4" html={section.headline} />
          <ul className="links">
            {items}
          </ul>
        </div>
      );
    });

    /* this.primaryLinks = props.items.map((item,i) => {
      const html = `<span>${item.title}</span>${  item.additional ? ` <span>${item.additional}</span>` : ''}`;
      const type = item.type ? item.type : '';
      return (
        <li key={i} onClick={(e) => this._clickPrimary(item, e)} className={type}>
          <a href="#">
            <ArrowLink link={html} />
          </a>
        </li>
      );
    });*/
  }

  _clickPrimary (item, e) {
    e.preventDefault();

    this.setState({
      stage:2,
      showIntro:false,
      secondary: {
        active:true,
        title:item.title,
        content:item.content,
        children:item.children
      },
      tertiary: {
        active:false,
        title:'',
        content:''
      }
    });

    const title = item.title.replace(/ /g, '-').toLowerCase();
    history.pushState({}, '', `#${title}`);
  }

  _clickSecondary (item, e) {
    e.preventDefault();

    const category = this.state.secondary.title.replace(/ /g, '-').toLowerCase();
    const title = item.title.replace(/ /g, '-').toLowerCase();
    if(this.state.stage === 2) {
      history.pushState({}, '', `#${category}/${title}`);
    }
    else{
      history.replaceState({}, '', `#${category}/${title}`);
    }

    this.setState({
      stage:3,
      tertiary: {
        active:true,
        title:item.title,
        content:item.content
      }
    });
  }

  _clickTertiary (item, e) {
    e.preventDefault();

    const category = this.state.tertiary.title.replace(/ /g, '-').toLowerCase();
    const title = item.title.replace(/ /g, '-').toLowerCase();
    if(this.state.stage === 3) {
      history.pushState({}, '', `#${category}/${title}`);
    }
    else{
      history.replaceState({}, '', `#${category}/${title}`);
    }

    this.setState({
      stage:4,
      quaternary: {
        active:true,
        title:item.title,
        content:item.content
      }
    });
  }

  _reset (stage) {
    const _this = this;

    let newStage = null;
    if(stage === 'tertiary') {
      newStage = 2;

      setTimeout(() => {
        if(!isMobile) {
          _this.setState({
            tertiary:{
              active:false,
              title:'',
              content:''
            }
          });
        }
      },500);
    }
    else if(stage === 'secondary') {
      newStage = 1;
      setTimeout(() => {
        _this.setState({
          showIntro:true
        });

        if(!isMobile) {
          _this.setState({
            secondary:{
              active:'false',
              title:'',
              content:'',
              children:[]
            }
          });
        }
      },500);
    }

    this.setState({
      stage:newStage
    });

    if(newStage === 1) {
      history.replaceState({}, '', '#');
    }
    else if(newStage === 2) {
      const title = this.state.secondary.title.replace(/ /g, '-').toLowerCase();
      history.replaceState({}, '', `#${title}`);
    }
  }

  componentDidMount () {
    const _this = this;

    window.onpopstate = function () {
      _this._reset();
    };

    const location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');

    let secondaryitem = {
      title:'',
      content:'',
      children:[]
    };
    let tertiaryitem = {
      title:'',
      content:''
    };
    let stage = 1;
    let secondaryActive,tertiaryActive = false;
    let showIntro = true;

    if(location.length) {
      for (const section of this.props.sections) {
        for (const item of section.items) {
          const title = item.title.replace(/ /g, '-').toLowerCase();

          if(title === location[0]) {
            secondaryitem = item;
            stage = 2;
            secondaryActive = true;
            showIntro = false;
          }
        }
      }

      if(location[1]) {
        for (const item of secondaryitem.children) {
          const title = item.title.replace(/ /g, '-').toLowerCase();

          if(title === location[1]) {
            tertiaryitem = item;
            stage = 3;
            tertiaryActive = true;
            showIntro = false;
          }
        }
      }

      this.setState({
        stage,
        secondary: {
          active:secondaryActive,
          title:secondaryitem.title,
          content:secondaryitem.content,
          children:secondaryitem.children
        },
        tertiary: {
          active:tertiaryActive,
          title:tertiaryitem.title,
          content:tertiaryitem.content
        },
        showIntro
      });
    }
  }

  render () {
    // get the prop settings
    const {headline,image,sectionsHeadline} = this.props;
    const baseClass = 'contact-us';

    let secondaryLinks = '';
    let secondaryContent = '';

    if(this.state.secondary.active) {
      if(this.state.secondary.children) {
        secondaryLinks = this.state.secondary.children.map((item,i) => {
          const html = `<span>${item.title}</span>${  item.additional ? ` <span>${item.additional}</span>` : ''}`;
          return (
            <li key={i} onClick={(e) => this._clickSecondary(item, e)}>
              <a href="#">
                <ArrowLink link={html} />
              </a>
            </li>
          );
        });
      }

      if(this.state.secondary.content) {
        secondaryContent = this.state.secondary.content;
      }
    }

    let tertiaryLinks = [];
    let tertiaryContent = {
      contact:{
        telephone:'',
        altHeading:'',
        subText:''
      },
      links:[]
    };

    if(this.state.tertiary.active) {
      if(this.state.tertiary.content && this.state.tertiary.content.links) {
        tertiaryLinks = this.state.tertiary.content.links.map((item,i) => {
          if(item.subLink) {
            if(item.isPDF) {
              return (
                <li key={item + i}>
                    <div className="downloadLink">
                      <Icon icon="pdf"/>
                      <Editable className="downloadLink__label" html={item.link}/>
                    </div>
                    <ArrowLink link={item.subLink}/>
                </li>
              );
            }
            else{
              return (
                <li key={item + i}>
                    <ArrowLink link={item.link}/>
                    <ArrowLink link={item.subLink}/>
                </li>
              );
            }
          }
          else{
            return (
              <li key={item + i}>
                  <ArrowLink link={item.link} subText={item.subText}/>
              </li>
            );
          }
        });
      }

      if(this.state.tertiary.content) {
        tertiaryContent = this.state.tertiary.content;
      }
    }

    const hasTertiary = this.state.tertiary.active;

    const additionalClasses = `${baseClass}--stage${this.state.stage}`;

    return (
      <section className={`${baseClass} ${additionalClasses}`}>
        <div className={`container ${baseClass}__container`}>

          <div className={`${baseClass}__main`}>

            <div className={`${baseClass}__primary`}>
              <Editable tag="h3" className="mobileOnly" html={headline} />
              <Editable tag="h3" className="contact-us__sectionHeading" html={sectionsHeadline} />
              {this.primaryLinks}
              <EditableImage className="mobileOnly" image={image} />
            </div>

            <div className={`${baseClass}__secondary`}>
              <div className={`${baseClass}__details`}>
                <div className={`${baseClass}__secondaryitems`}>

                  {this.state.showIntro && (
                    <div className={`${baseClass}__intro`}>
                      <Editable tag="h2" html={headline} />
                      <EditableImage image={image} />
                    </div>
                  )}

                  <div className={`${baseClass}__secondaryitems__list`}>
                      <h3 className={`${baseClass}__backLink label`} onClick={() => this._reset('secondary')}><Icon icon="leftarrow" fillColor="#04589b"/>{this.state.secondary.title} </h3>

                      {this.state.secondary.content && (
                        <div className="box box--sm box--alt">
                          <Contact {...secondaryContent.contact} tel={secondaryContent.contact.telephone} telAltIntro={secondaryContent.contact.telAltIntro} telAlt={secondaryContent.contact.telAlt} info={secondaryContent.contact.subText} isAlt={true} />
                        </div>
                      )}
                      {this.state.secondary.content && (
                        <EditableDiv className={`${baseClass}__copy`} html={secondaryContent.copy} />
                      )}
                      <ul className="links">{secondaryLinks}</ul>
                  </div>
                </div>

                <div className={`${baseClass}__tertiary`}>
                  <h3 className={`${baseClass}__backLink label`}  onClick={() => this._reset('tertiary')}><Icon icon="leftarrow" fillColor="#04589b"/>{this.state.tertiary.title}</h3>
                  {hasTertiary && tertiaryContent.contact && (
                    <div className="box box--sm box--alt">
                      <Contact {...tertiaryContent.contact} tel={tertiaryContent.contact.telephone} telAltIntro={tertiaryContent.contact.telAltIntro} telAlt={tertiaryContent.contact.telAlt} info={tertiaryContent.contact.subText} isAlt={true} />
                    </div>
                  )}
                  {tertiaryLinks && (
                    <div>
                      <ul className="links">{tertiaryLinks}</ul>
                    </div>
                  )}
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>
    );
  }
}

ContactUs.defaultProps = {
  sections: []
};

ContactUs.propTypes = {
  sections: PropTypes.array.isRequired,
  image:PropTypes.string,
  headline:PropTypes.string,
  breadcrumbLabel:PropTypes.string,
  sectionsHeadline:PropTypes.string
};

module.exports = ContactUs;
