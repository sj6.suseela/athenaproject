import React from 'react';
import PropTypes from 'prop-types';

import './advisorbar.scss';

/** AdvisorBar
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.AdvisorBar, {
    id : 'id',
    items:[
      {
        link:'<a href="/careers">Careers</a>'
      },
      {
        link:'<a href="/contact-us">Contact us</a>'
      },
      {
        link:'<a href="/advisors">Advisors</a>'
      }
    ],
  }), document.getElementById("element"));
*/
class AdvisorBar extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      className: 'advisorBar'
    };

    this.navItems = props.items.map((item) => {
      const baseClass = 'advisorBar__link';
      let cssClass = baseClass;
      cssClass += item.isBold ? ` ${baseClass}--bold` : '';
      cssClass += item.alignMobile ? ` ${baseClass}--mobile-${item.alignMobile}` : '';
      return <li key={item.link} className={cssClass}  dangerouslySetInnerHTML={{__html: item.link}} ></li>;
    });
  }

  render () {
    return (
      <div className={this.state.className}>
        <div className="container">
          <nav>
            <ul className='advisorBar__ul'>
              {this.navItems}
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

AdvisorBar.defaultProps = {
  items: []
};

AdvisorBar.propTypes = {
  items: PropTypes.array
};

module.exports = AdvisorBar;
