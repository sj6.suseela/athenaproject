import React from 'react';
import PropTypes from 'prop-types';

import './carousel.css';
import './ImageCarousel.scss';

const Carousel = require('react-responsive-carousel').Carousel;

class ImageCarousel extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

      legend:props.items[0].legend,
      selected:0
    };

    this.timer = null;

    this._onChange = this._onChange.bind(this);

    this.items = props.items.map((item) => {
      return <div key={item.image}><img src={item.image} /></div>;
    });
  }

  _onChange (e) {
    const _this = this;
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      _this.setState({
        selected:e,
        legend:_this.props.items[e].legend
      });
    },501);
  }

  render () {
    const {width} = this.props;

    const cssClass = `imageCarousel imageCarousel--${width}`;

    return (

      <div className={cssClass}>
        <Carousel showThumbs={false} showStatus={false} infiniteLoop onChange={this._onChange} selectedItem={this.state.selected} transitionTime={500}>
          {this.items}
        </Carousel>
        <p className="legend">{this.state.legend}</p>
      </div>

    );
  }
}

ImageCarousel.propTypes = {

  width: PropTypes.string,
  items:PropTypes.array

};

module.exports = ImageCarousel;
