import React from 'react';
import PropTypes from 'prop-types';

import './Avatar.scss';

import EditableImage from '../Editable/EditableImage';

class Avatar extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {src,link} = this.props;

    if(!src) return ('');

    return (
      <span className="avatar">
        {link.href && (
        <a id={link.id} href={link.href} rel={link.rel}>
          <EditableImage image={src} />
        </a>
        )}
        {!link.href && (
          <EditableImage image={src} />
        )}
      </span>
    );
  }
}

Avatar.defaultProps = {
  name: '',
  src: '',
  link: {}
};

Avatar.propTypes = {
  name: PropTypes.string,
  src: PropTypes.string,
  link: PropTypes.object
};

module.exports = Avatar;
