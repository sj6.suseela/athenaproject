/**
* All cookie management is handled here along side basket state updates.
*/
export default class DocumentLibraryBasketService {
  constructor (component, expiry) {
    this.componentRef = component;
    this.expiryTime = expiry;
  }

  /**
  * Set a cookie
  *
  * @param {string} cname The name of the cookie.
  * @param {string} cvalue The value of the cookie.
  * @param {int} exdays The expiry of the cookie in days.
  */
  setCookie (cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = `expires=${d.toUTCString()}`;
    document.cookie = `${cname}=${cvalue};${expires};path=/`;
    // console.log(cname, cvalue)
    // console.log("set cookie",cname, cvalue)
  }

  /**
  * Get a cookie
  *
  * @param {string} cname The name of the cookie.
  */
  getCookie (cname) {
    const name = `${cname}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        // console.log("get cookie",cname, c.substring(name.length, c.length))
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }

  /**
  * Preload a basket cookie with test data from JSON file.  Frontend dev only - we're pulling back document data from a flat json file, this makes sure that the cookie represents what is pulled back
  * Usually it would be the other way round.  The cookie would dictate what document data is pulled from the server.
  *
  * @param {string} type 'save' or 'post'.
  * @param {array} documents An array of documents pulled from the server.
  */
  preloadCookie (type, documents) {
    this.setCookie(`document-library-${type}-items`, '', this.expiryTime);
    documents.map(document => {
      this.setSaveOrPostBasketDocument(type, document, null, true);
    });
  }

  /**
  * Toggle a specific documents presence in a particular basket cookie.
  *
  * @param {string} type 'save' or 'post'.
  * @param {Object} obj The object we're adding to the session cookie (Only obj.id is stored in the cookie).
  * @todo fix this to pass in the existing cookie into the 2 methods
  */
  toggleDocumentInSessionCookie (type, obj) {
    const documentIdsString = this.getCookie(`document-library-${type}-items`);
    // console.log(obj.id, documentIdsString)
    const documentIds = documentIdsString.split('|');
    const index = documentIds.indexOf(obj.id);
    // console.log(index, documentIds, documentIdsString);
   // console.log(index)
    if (index === -1) {
      this.setSaveOrPostBasketDocument(type, obj, documentIdsString);
    }
    else {
      this.deleteSaveOrPostBasketDocument(type, obj, documentIdsString);
    }
  }

  /**
  * Save a specific document to particular basket cookie and update the component state array for that basket.
  *
  * @param {string} type 'save' or 'post'.
  * @param {Object} obj The object we're adding to the session cookie (Only obj.id is stored in the cookie).
  * @param {string} existingCookie We can pass the existing cookie if we already know it
  * @param {bool} cookieOnly Only save to the cookie and not state?  We don't save to state when preloading the cookie, the state is already sorted.
  */
  setSaveOrPostBasketDocument (type, obj, existingCookie, cookieOnly) {
    const documentIdsString = existingCookie || this.getCookie(`document-library-${type}-items`);

    const documentIds = documentIdsString === '' ? [] : documentIdsString.split('|') ;
    
    if (documentIds.indexOf(obj.id) === -1) {
      documentIds.push(obj.id);
      
      this.setCookie(`document-library-${type}-items`, documentIds.join('|'), this.expiryTime);

      if (type === 'post' && !cookieOnly) {
        const newPostBasketDocuments = this.componentRef.state.postBasketDocuments;
        newPostBasketDocuments.push(obj);

        this.componentRef.setState({postBasketDocuments: newPostBasketDocuments});
      }
      else if (type === 'save' && !cookieOnly) {
        const newSaveBasketDocuments = this.componentRef.state.saveBasketDocuments;
        newSaveBasketDocuments.push(obj);

        this.componentRef.setState({saveBasketDocuments: newSaveBasketDocuments});
      }
    }
  }

  /**
  * Delete a specific document to particular basket cookie and update the component state array for that basket.
  *
  * @param {string} type 'save' or 'post'.
  * @param {Object} obj The object we're removing from the session cookie..
  * @param {string} existingCookie We can pass the existing cookie if we already know it
  */
  deleteSaveOrPostBasketDocument (type, obj, existingCookie) {
    const documentIdsString = existingCookie || this.getCookie(`document-library-${type}-items`);
    const documentIds = documentIdsString.split('|');

    const index = documentIds.indexOf(obj.id);
    documentIds.splice(index, 1);

    this.setCookie(`document-library-${type}-items`, documentIds.join('|'), this.expiryTime);

    if (type === 'post') {
      const newPostBasketDocuments = this.componentRef.state.postBasketDocuments;
      // assume indexes are aligned
      newPostBasketDocuments.splice(index, 1);
     
      // no need for this to saved in state, may change this
      this.componentRef.setState({postBasketDocuments: newPostBasketDocuments});
    }
    else if (type === 'save') {
      const newSaveBasketDocuments = this.componentRef.state.saveBasketDocuments;
      // assume indexes are aligned
      newSaveBasketDocuments.splice(index, 1);
      // no need for this to saved in state, may change this
      this.componentRef.setState({saveBasketDocuments: newSaveBasketDocuments});
    }
  }

  emptyBasket (type) {
    this.setCookie(`document-library-${type}-items`, '', this.expiryTime);
  }


}