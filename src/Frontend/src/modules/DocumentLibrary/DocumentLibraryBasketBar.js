import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Stick from '../Stick/Stick';
import './DocumentLibraryBasketBar.scss';

class DocumentLibraryBasketBar extends Component {
  constructor (props) {
    super(props);
  }

  componentDidMount () {

  }

  render () {
    const {yourDocumentsTxt, saveBasketCount, postBasketCount, emailStickyButtonText, postStickyButtonText, onClickSaveBasket, onClickPostBasket} = this.props;

    const postText = postStickyButtonText.replace('{count}', postBasketCount).replace('{s}', postBasketCount > 1 ? 's' : '');
    const saveText = emailStickyButtonText.replace('{count}', saveBasketCount).replace('{s}', saveBasketCount > 1 ? 's' : '');

    return ReactDOM.createPortal(
            <Stick className='basket-bar'>
                <div className="container">
                    <h3 className='basket-bar__heading'>{yourDocumentsTxt}</h3>
                    <div>
                        {postBasketCount > 0 && <a className='sub-text btn btn--super-slim btn--withIcon' onClick={() => onClickPostBasket()}><img src='/dist/images/icon/icon-email-document.png' />{postText}</a>}
                        {saveBasketCount > 0 && <a className='sub-text btn btn--super-slim btn--withIcon' onClick={() => onClickSaveBasket()}><img src='/dist/images/icon/icon-document-saved.png' />{saveText}</a>}
                    </div>
                </div>
            </Stick>,
            document.getElementById('documentLibraryStickyBar'));
  }
}

DocumentLibraryBasketBar.propTypes = {
  yourDocumentsTxt: PropTypes.string,
  saveBasketCount: PropTypes.number,
  postBasketCount: PropTypes.number,
  emailStickyButtonText: PropTypes.string,
  postStickyButtonText: PropTypes.string,
  onClickSaveBasket: PropTypes.func,
  onClickPostBasket: PropTypes.func
};

DocumentLibraryBasketBar.defaultProps = {
  yourDocumentsTxt: 'Your documents',
  emailStickyButtonText: '{count} Document{s} saved',
  postStickyButtonText: '{count} Document{s} to post'
};

export default DocumentLibraryBasketBar;