import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import ArrowLink from '../ArrowLink/ArrowLink';
import Icon from '../Icon/Icon';
import axios from 'axios';
import Loading from '../Loading/Loading';
import Overlay from '../Overlay/Overlay';
import Cta from '../Cta/Cta';
import Form from '../Form/Form';
import Input from '../Form/Input';
import DocumentLink from './DocumentLink';
import DocumentLibraryBasketBar from './DocumentLibraryBasketBar';
import ScrollToY from '../scrolltoy';
import SitecoreSettings from '../SitecoreSettings';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';

import './DocumentLibrary.scss';
import DocumentLibraryBasketService from './DocumentLibraryBasketService';

/** ***
 * DocumentLibrary component
 *
 * Document Library hierachy stage definition as defined by the business:
 *
 * Stage 0 = 'Areas'.  Stage 1 = 'Categories'.  Stage2 = 'Sub Categories'.  Stage3 = 'Document Types'.  Stage4 = 'Documents'.
 *
 * The objects for each stage up to stage 2 have a categories array, document types have a documents array.
 *
 * Where the type 'save' is used it effectively relates to actions that involve the email/download basket is concerned.  The terminology used on the frontend was changed mid development.
 *
 *****/
class DocumentLibrary extends React.Component {
  constructor (props) {
    super(props);

    this._updateWindowDimensions = this._updateWindowDimensions.bind(this);

    this.baseClass = 'document-library';


    this.stageCount = 5;
    this.preloading = true;

    this.state = {
      ready: false,
      saveBasketOverlayOpen: false,
      saveBasketDocuments: [],
      postBasketOverlayOpen: false,
      postBasketDocuments: [],
      stage: 0,
      searchStage: 0,
      mobileContainerHeight: 'auto',
      width: 0,
      search1: {label: this.props.content.searchInProgressTxt}, // Search Results (As per to Document Types/Stage 3)
      search2: {}, // Documents
      stage0: this.props, // Areas
      stage1: {}, // Categories
      stage2: {}, // Sub Categories
      stage3: {}, // Document Types - has a documents array rather than a categories array like the other stages.
      stage4: {}, // Documents
      searchItemsToShow: this.props.searchItemsPerPane,
      emailInputs: ['']
      // categories:this._getCategories()
    };

    this.setPanelHeight = this.setPanelHeight.bind(this);
    this.onClickLoadMore = this.onClickLoadMore.bind(this);

    this.types = ['area', 'category', 'sub-category', 'document-type', 'document'];

    // all cookie management is handled here
  }

  componentDidMount () {
    const sc = new SitecoreSettings();

    if (this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing: sc.isExperienceEditor()
      });
    }

    if (this.state.isPreview !== sc.isPreview()) {
      this.setState({
        isPreview: sc.isPreview()
      });
    }


    this.setNavigationEvents();
    this.checkUrl(0);
  }


  componentWillUnmount () {
    window.removeEventListener('resize', this._updateWindowDimensions);
  }

  componentDidUpdate () {
    if (!this.preloading) {
      this.setPanelHeight();
    }
  }


  /**
  * Typically handles popstate events
  */
  setNavigationEvents () {
    window.addEventListener('popstate', (event) => {
      const newState = event.state;
      newState.ready = true;
      newState[`stage${newState.stage}`].loading = false;
      this.setState(event.state);
    });
  }


  /**
  * The component is ready to go.  Post url preload.
  */
  completePreload () {
    this.preloading = false;
    history.replaceState(this.state, document.title, document.location.href);
    window.addEventListener('resize', this._updateWindowDimensions);
    this.setPanelHeight();
    this.basketService = new DocumentLibraryBasketService(this, this.props.cookieExpiry || 180);
    this.getBasketData('save');
    this.getBasketData('post');
    setTimeout(() => {
      this.setState({ready: true});
    }, 300);
  }

  /**
  * a recursive function that handles the opening of panels where the url dictates
  *
  * @param stage What stage are we checking - 0 first.
  */
  checkUrl (stage) {
    if (typeof this.types[stage] === 'undefined') {
      this.completePreload();
      return;
    }

    const currentStage = this.state[`stage${stage}`];

    if (!currentStage.items || currentStage.items.length === 0) {
      setTimeout(() => {
        this.checkUrl(stage);
      }, 100);
      return;
    }
    else {
      const t = currentStage.items[0].type;
      const p = this.getUrlParameter(t);

      if (p === '') {
        this.completePreload();
        return;
      }

      const item = this.getItemByName(currentStage.items, p);
      this._clickItemLink(item, stage + 1, true, () => {
        this.checkUrl(stage + 1);
      });
    }
  }


  /**
  * get a url query string parameter
  *
  * @param n the name of the param
  */
  getUrlParameter (n) {
    const name = n.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    const regex = new RegExp(`[\\?&]${name}=([^&#]*)`);
    const results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  }

  /**
  * Set the window dimensions for use in the resize function
  */
  _updateWindowDimensions () {
    this.setState({width: window.innerWidth, height: window.innerHeight});
  }

  /**
  * Sets the panel height on mobile
  */
  setPanelHeight () {
    if (this.container && this.activePanel) {
      if (this.state.mobileContainerHeight !== this.activePanel.offsetHeight || this.state.mobileContainerHeight === 'auto') {
        this.setState({
          mobileContainerHeight: this.activePanel.offsetHeight
        });
      }
    }
  }

  /**
  * Reset each isActive flag from the clicked items stage (that will become the new active stage once this method has completed) up through to the current active stage.
  *
  * @param {int} stageNum What stage does the clicked link represent.
  * @return {object} the modified state
  */
  resetActiveStates (stageNum) {
    const state = this.state;

    if (state.isSearching) {
      if (state.searchStage >= stageNum) {
        for (let i = stageNum; i <= state.searchStage; i++) {
          state[`search${i}`].isActive = false;
        }
      }
    }
    else {
      for (let i = stageNum; i <= 4; i++) {
        state[`stage${i}`].isActive = false;

        // reset ajax based items
        if (i === 3) {
          state[`stage${i}`].items = [];
        }
      }
    }

    return state;
  }


  /**
  * Fired when a user has clicked on an item.
  *
  * @param {string} stageData The data for the clicked item (becomes the data for the next panel to be shown).
  * @param {int} stageNum What stage does the clicked link represent.  This is the next state.stage number.
  * @param {bool} persistUrl Do not change the url with this click - used when preloading active panels
  * @param {function} callback what to do after the click is successful
  */
  _clickItemLink (stageData, stageNum, persistUrl, callback) {
    if (typeof stageData === 'undefined') return;

    const state = this.resetActiveStates(stageNum);
    // we know when searching, the only stage you'll click for is 2 (1 is handled by the onSearch method) if it is 1- assume the user has clicked one of the areas beneath search
    if (this.state.isSearching && stageNum === 2) {
      state.searchStage = stageNum;
      state.searchItemsToShow = this.props.searchItemsPerPane;
      state[`search${stageNum}`] = stageData;
      state[`search${stageNum}`].isActive = true;
    }
    else {
      if (this.state.isSearching) {
        state.searchStage = 0;
        state.isSearching = false;
      }
      state.stage = stageNum;

      state[`stage${stageNum}`] = stageData;
      state[`stage${stageNum}`].isActive = true;
    }

    const requiresAjaxRequestForCategory = (stageData.type === 'category' && stageData.hasSubcategories === false);
    const requiresAjaxRequestForSubCategory = stageData.type === 'sub-category';
    // if the item is a sub category  - we'll need to request the document types from the server (unless we're searching).
    if ((requiresAjaxRequestForCategory || requiresAjaxRequestForSubCategory) && !this.state.isSearching) {
      state[`stage${stageNum}`].loading = true;
      this.setState(state);

      let url = (requiresAjaxRequestForCategory) ? this.props.categoryApiEndPoint : this.props.subCategoryApiEndPoint;

      if (this.props.isDev && typeof stageData.ajaxUrl !== 'undefined') {
        url = stageData.ajaxUrl;
      }

      const payload = {};
      payload[requiresAjaxRequestForCategory ? 'category' : 'subcategory'] = stageData.id;

      this.getData(url,
        payload,
        (data) => {
          setTimeout(() => {
            const state = this.state;

            state[`stage${stageNum}`].items = data.items;
            state[`stage${stageNum}`].loading = false;

            if (!persistUrl && !this.state.isSearching) this.setHistoryStateAndUrlParams(state);

            const offset = this.state.postBasketDocuments.length > 0 || this.state.saveBasketDocuments.length > 0 ? 100 : 0;
            const yPos = this.component.getBoundingClientRect().top + (window.pageYOffset - offset);
            new ScrollToY(yPos, 500, 'easeInOutQuint');

            this.setState(state);

            if (callback) callback.call();
          }, this.props.isDev ? 3000 : 0);
        },
        (data) => {
          this.setState({errorMessage: data.message || 'Unknown Error'});
        });
    }
    else {
      const offset = this.state.postBasketDocuments.length > 0 || this.state.saveBasketDocuments.length > 0 ? 100 : 0;
      const yPos = this.component.getBoundingClientRect().top + (window.pageYOffset - offset);
      new ScrollToY(yPos, 500, 'easeInOutQuint');

      this.setState(state);

      if (!persistUrl && !this.state.isSearching) this.setHistoryStateAndUrlParams(state);

      if (callback) callback.call();
    }
  }

  /**
  * Fired when a user has clicked the back link on a particular panel.
  *
  * @param {int} stageNum In what stage was the back link clicked.
  */
  _clickBackLink (stageNum) {
    const newState = this.resetActiveStates(stageNum);

    if (this.state.isSearching) {
      newState.searchStage = stageNum - 1;
      if (newState.searchStage === 0) {
        newState.isSearching = false;
      }

      newState[`search${stageNum}`].isActive = false;
    }
    else {
      newState[`stage${stageNum}`].isActive = false;
      newState.stage = stageNum - 1;
    }

    this.setHistoryStateAndUrlParams(newState);

    this.setState(newState);
  }


  /**
  * Returns the total documents for a list of categories.  Used on the search results panel.  The specific categories must have a documents array (these specific categories are known as 'document types'.) and are returned for any particular search (they're also returned via ajax in stage3 of the standard document hierachy view, but we dont show totals there)
  *
  * @param {array} categories The categories array taken from the search api return.
  * @return {int} The total amount of documents.
  */
  getTotalResults (items) {
    let count = 0;
    items.map(item => {
      count += item.items.length;
    });

    return count;
  }

  /**
  * Retreives data from the server before firing a callback to handle the data.
  *
  * @param {string} endpoint What api is being called.
  * @param {object} payload Post data
  * @param {function} callback How we're handling the data thats retrieved.
  * @param {function} errorCallback What happens when the request fails.
  */
  getData (endpoint, payload, callback, errorCallback) {
    if (this.props.isDev) {
      axios.get(endpoint, payload)
        .then((response) => {
          const jsonResponse = response.data;

          if (jsonResponse.success) {
            callback.call(this, response.data);
          }
          else {
            errorCallback.call(this, response.data);
            // console.error('request failure');
          }

          this.setState({
            loading: false
          });
        })
        .catch(() => {
          // console.error('failure to collect json');
        });
    }
    else {
      axios.post(endpoint, payload)
        .then((response) => {
          const jsonResponse = response.data;

          if (jsonResponse.success) {
            callback.call(this, response.data);
          }
          else {
            errorCallback.call(this, response.data);
            // console.error('request failure');
          }

          this.setState({
            loading: false
          });
        })
        .catch(() => {
          // console.error('failure to collect json');
        });
    }
  }

  /**
  * Takes the basket information from one of 2 cookies (id|id|id) and collects the associated document data from the server before loading to the data into the components state.
  *
  * @param {string} type - 'save' or 'post'
  */
  getBasketData (type) {
    const documentIds = this.basketService.getCookie(`document-library-${type}-items`);

    let endPoint = this.props.documentListApiEndpoint;
    if (this.props.isDev) {
      endPoint = `dist/json/${type}-cookie-documents.json`;
    }

    if (documentIds === '') return;

    this.getData(endPoint, {
      documents: documentIds.split('|')
    }, (data) => {
      if (type === 'post') {
        this.setState({postBasketDocuments: data.items});
      }
      else if (type === 'save') {
        this.setState({saveBasketDocuments: data.items});
      }

      if (this.props.isDev) {
        this.basketService.preloadCookie(type, data.items);
      }
    }, (data) => {
      this.setState({errorMessage: data.message || 'Unknown Error'});
    });
  }

  /**
  * Where an object has a documents array - return the length wrapped in brackets.  Otherwise return ''.
  *
  * @param {object} item - the document type object we wish to get a document count for.
  * @return {string} - The number of documents in a documents array wrapped in parentheses.
  */
  getCount (item) {
    if (item.type === 'document-type') {
      return `(${item.items.length})`;
    }

    return '';
  }

  /**
   * Check whether an array contains an object with a specific id.
   *
   * @param {array} arr - the array to search
   * @param {string} id - the id of the item to check for
   * @return {bool} - Whether or not the item exists
   */
  hasItemById (arr, id) {
    return arr.filter(item => {
      return item.id === id;
    }).length > 0;
  }

  /**
   * Get an object with a specific id.
   *
   * @param {array} arr - the array to search
   * @param {string} id - the id of the item to return
  * @return {object} - The object you're looking for or null
   */
  getItemById (arr, id) {
    return arr.filter(item => {
      return item.id === id;
    })[0];
  }


  /**
   * Get an object with a specific name.
   *
   * @param {array} arr - the array to search
   * @param {string} name - the name of the item to return
   */
  getItemByName (arr, name) {
    return arr.filter(item => {
      return item.name === name;
    })[0];
  }

  /**
  * Set the url for the current view and update the browser history state
  *
  * @param {object} historyState - The current state of the document library.
  */
  setHistoryStateAndUrlParams (historyState) {
    if (this.state.isEditing || this.state.isPreview) return;
    const baseUrl = window.location.pathname;

    let newUrl = baseUrl;
    for (let i = 1; i < 5; i++) {
      if (this.state[`stage${i}`].isActive) {
        newUrl += `${i === 1 ? '?' : '&'}${this.state[`stage${i}`].type}=${this.state[`stage${i}`].name}`;
      }
    }


    history.pushState(historyState, document.title, newUrl);
  }


  /**
  * Return either a list of links or documents.
  *
  * @param {object} items - The items to process.
  * @param {int} stageNum - The active stage number (see the top of this document to understand what this means).
  * @param {bool} isPostBasket - This is a list of items to display in the postal basket
  * @param {bool} isSaveBasket - This is a list of items to display in the save/email basket
  * @param {int} startIndex The start index of the items we want to return
  * @param {int} endIndex The end index of the items we want to return
  * @return {array} The link/document items to be rendered.
  */
  getItems (items, stageNum, isPostBasket, isSaveBasket, startIndex, endIndex) {
    const {
      saveDocumentTxt,
      postDocumentTxt,
      saveDocumentEnabledTxt,
      postDocumentEnabledTxt,
      cancelTxt,
      removeCta
    } = this.props.content;

    if (typeof items === 'undefined' || items.length === 0) {
      return this.state.isSearching ? <Editable html={this.props.content.searchResultsError} /> : <p>No items to display</p>;
    }

    const s = startIndex || 0;
    const e = typeof endIndex === 'undefined' ? items.length - 1 : endIndex;

    return items.map((item, i) => {
      if (item.type === 'document') {
        if (i >= s && i <= e) {
          // need to change this
          const isInSaveBasket = this.hasItemById(this.state.saveBasketDocuments, item.id);
          const isInPostBasket = this.hasItemById(this.state.postBasketDocuments, item.id);

          return <li key={`${i}-${item.id}`} className={item.isActive ? 'selected' : ''} onClick={() => { }}>
            {
              !isPostBasket &&
              <DocumentLink {...item}
                isInSaveBasket={isInSaveBasket}
                isInPostBasket={isInPostBasket}
                onExpand={this.setPanelHeight}
                isSaveBasket={isSaveBasket}
                cancelTxt={cancelTxt}
                removeCta={removeCta}
                saveDocumentTxt={saveDocumentTxt}
                postDocumentTxt={postDocumentTxt}
                saveDocumentEnabledTxt={saveDocumentEnabledTxt}
                postDocumentEnabledTxt={postDocumentEnabledTxt}
                onToggleSave={() => { this.basketService.toggleDocumentInSessionCookie('save', item); }}
                onTogglePost={() => { this.basketService.toggleDocumentInSessionCookie('post', item); }} />
            }
          </li>;
        }
      }
      else {
        return <li key={item.id} data-type={item.type} data-name={item.name} className={item.isActive ? 'selected' : ''} onClick={() => { this._clickItemLink(item, stageNum + 1); }}><span><ArrowLink link={`${item.label} ${this.getCount(item)}`} /></span></li>;
      }
    });
  }

  onClickLoadMore (e) {
    e.preventDefault();
    this.setState({searchItemsToShow: this.state.searchItemsToShow + this.props.searchItemsPerPane});
  }

  /**
  * Returns a panel of links or documents.
  *
  * @param {Object} stageData The current stage information. This will at least contain a name, id and an array named documents or categories.  Other properties will exist.
  * @param {int} stageNum The active stage number (see the top of this document to understand what this means).
  * @param {int} startIndex The start index of the items we want to display on this panel
  * @param {int} endIndex The end index of the items we want to display on this panel
  * @return {Object} The panel to be rendered.
  */
  getPanel (stageData, stageNum, startIndex, endIndex) {
    const type = this.state.isSearching ? 'search' : 'stage';

    const showLoadMore = typeof endIndex !== 'undefined' && typeof stageData.items !== 'undefined'
      && endIndex < stageData.items.length - 1
      && stageData.type === 'document-type';


    return <div className={`${this.baseClass}__panel ${this.baseClass}__panel--${type}${stageNum} container${stageData.loading ? ` ${this.baseClass}__panel--loading` : ''}${stageData.isActive ? ` ${this.baseClass}__panel--active` : ''}`} ref={(p) => { if (stageNum === this.state.stage) { this.activePanel = p; } }}>
      <div className={`${this.baseClass}__panel-inner `} >
        <Loading isActive={stageData.loading} isFullScreen={false} />
        <h3 className={`${this.baseClass}__backLink label`} onClick={() => { this._clickBackLink(stageNum); }}>
          <Icon icon="leftarrow" fillColor="#04589b" /><Editable html={stageData.label} />
        </h3>

        <ul className={typeof stageData.items !== 'undefined' && stageData.items.length > 0 && stageData.items[0].type === 'document' ? `${this.baseClass}__doclinks` : `${this.baseClass}__navlinks`}>
          {this.getItems(stageData.items, stageNum, false, false, startIndex, endIndex)}
        </ul>
        {
          showLoadMore && <Cta
            label='Load more'
            type='secondary'
            isSmall={true}
            onClick={this.onClickLoadMore} />
        }
      </div>
    </div>;
  }

  /**
  * Returns the panels (standard document hierachy panels, does not incldue search panels) from stage 1 up.  Stage 0 is already rendered on the page so we don't need to render that here.
  * We always render each panel and hide/show using the active state.  This helps with the animation aspect of the component.
  *
  * @return {array} The panels to be rendered.
  */
  getPanels () {
    const panels = [];

    if (SERVER) return panels;

    // ignore stage 0 as its rendered already
    for (let i = 1; i < this.stageCount; i++) {
      panels.push(this.getPanel(this.state[`stage${i}`], i));
    }

    return panels;
  }

  /**
  * Returns both search panels (search stage 1 and 2.).
  *
  * @return {array} The search panels to be rendered.
  */
  getSearchPanels () {
    const panels = [];
    if (SERVER) return panels;
    // ignore stage 0, we render search results for stages 1 and 2
    for (let i = 1; i <= 2; i++) {
      // if search result documents are showing

      panels.push(this.getPanel(this.state[`search${i}`], i, 0, this.state.searchItemsToShow - 1));
    }

    return panels;
  }

  /**
  * Fired when a user clicks the submit button next to the search input.
  */
  onSearch () {
    const value = this.searchInput.value;
    if (value === '') return;
    let endPoint = this.props.searchApiEndpoint.replace('{0}', value);
    if (this.props.isDev) {
      endPoint = 'dist/json/document-search-2.json';
    }

    const search1 = this.state.search1;
    search1.loading = true;
    search1.label = this.props.content.searchInProgressTxt;

    this.setState({isSearching: true, searchStage: 1, search1});

    this.getData(endPoint, {searchTerm: value}, (data) => {
      setTimeout(() => {
        const state = this.state;

        state[`search${1}`] = data;
        state[`search${1}`].label = `${this.props.content.searchHeadingTxt} (${this.getTotalResults(data.items)})`;

        this.setState(state);

        setTimeout(() => {
          state[`search${1}`].loading = false;
          this.setState(state);
        }, 100);
      }, this.props.isDev ? 2000 : 0);
    }, (data) => {
      this.setState({errorMessage: data.message || 'Unknown Search Error'});
    });
  }

  /**
  * Create a list of links specifically rendered for download purposes.
  *
  * @return {array} An array of links to be rendered.
  */
  getDownloadFiles () {
    const files = [];

    this.state.saveBasketDocuments.map(element => {
      files.push(<a target="_blank" rel="noopener noreferrer" href={element.documentUrl} download />);
    });

    return files;
  }

  /**
  * Kick off a download.
  */
  downloadFile (elem, i) {
    setTimeout(() => {
      elem.click();
    }, 1000 * (i + 1));
  }

  /**
  * Kick off a download.
  */
  downloadFiles () {
    const children = Array.from(this.downloadLinks.children);

    children.forEach((elem, i) => {
      this.downloadFile(elem, i);
    });
  }

  /**
  * Sets the state of the component when the email/download select box has been changed
  *
  * @param {string} value The value of the select box
  */
  onSelectDownloadOrEmail (value) {
    this.setState({downloadOrEmail: value, emailDocumentSuccess: false});
  }

  /**
  * What happens when an email is changed... we keep a seperate store of email addresses so we can persist the list when the overlay is closed
  *
  * @param {string} index What email address does this relate to
  * @param {string} value The value of the input
  */
  onEmailBlur (index, value) {
    // console.log(index);
    const emailInputs = this.state.emailInputs;

    emailInputs[index] = value;

    this.setState({emailInputs});
  }

  /**
   * Get the email form model to pass into the <Form> component
   *
   * @return {object} The form model
   */
  getEmailFormModel () {
    const {emailDocumentEndpoint} = this.props;
    const {emailAll, emailInputLabel, emailInputPlaceholder, addAnotherEmailTxt} = this.props.content;
    const documents = this.state.saveBasketDocuments;

    // list of hidden documents
    const documentFields = documents.map(document => {
      return {
        'type': 'hidden',
        'id': document.id,
        'name': document.id
      };
    });

    // we keep track of the emails entered so we can persist the value between state changes
    const emailFields = this.state.emailInputs.map((emailInput, i) => {
      return {
        'type': 'email',
        'id': `email-${i}`,
        'name': `email-${i}`,
        'isRequired': i === 0,
        'value': emailInput,
        'label': emailInputLabel,
        'placeholder': emailInputPlaceholder,
        'onBlur': (input) => {
          this.onEmailBlur(i, input.value);
        }
      };
    });

    // adds a new input type which is effectively an html insert.  used for the add another email link
    emailFields.push({
      'type': 'html',
      'html': <a className={`${this.baseClass}__add-more`} onClick={() => { this.state.emailInputs.push(''); this.setState({emailInputs: this.state.emailInputs}); }}><Icon icon='rightarrow' strokeColor='#04589b' /><span>{addAnotherEmailTxt}</span></a>
    });

    const model = {
      'endpoint': emailDocumentEndpoint,
      'submit': emailAll,
      'groups': [
        {
          'id': 'documents',
          'name': '',
          'image': '',
          'editButton': '',
          'headingTag': 'h3',
          'additionalHeadingTxt': '',
          'isHidden': true,
          'fields': documentFields
        },
        {
          'id': 'emailAddresses',
          'name': '',
          'image': '',
          'editButton': '',
          'headingTag': 'h3',
          'additionalHeadingTxt': '',
          'fields': emailFields
        }
      ]
    };

    return model;
  }

  /**
  * Successful email form submission callback
  */
  emailDocumentSuccess () {
    this.setState({emailDocumentSuccess: true});
    // this.basketService.emptyBasket('save');
  }

  /**
  * renders the email document form
  *
  * @return {object} The email form
  */
  renderEmailForm () {
    const {saveOverlayHeading, returnToDocumentLibraryTxt, emailAll, downloadAll, selectEmailOrDownloadLabel, selectDefault, emailDocumentSuccessMessage} = this.props.content;

    const options = [{label: selectDefault, value: ''}, {label: emailAll, value: 'email-all'}, {label: downloadAll, value: 'download-all'}];

    return <div>

      {!this.state.emailDocumentSuccess && <div>
      <h3>{saveOverlayHeading}</h3>
      <ul className={`${this.baseClass}__doclinks`}>
        {this.getItems(this.state.saveBasketDocuments, null, false, true)}
      </ul>

      <form className='form'>
        <Input type='native-select' label={selectEmailOrDownloadLabel} options={options} onChange={e => { this.onSelectDownloadOrEmail(e.target.value); }} />
      </form></div>}

      {this.state.downloadOrEmail === 'download-all' &&
        <Cta label={downloadAll} isSmall={true} onClick={e => { e.preventDefault(); this.downloadFiles(); }} />}

      {this.state.downloadOrEmail === 'email-all' &&
        <div>
          <Form {...this.getEmailFormModel()} isVertical={true} customSuccessMethod={this.emailDocumentSuccess.bind(this)} />
          {this.state.emailDocumentSuccess && <div className={`${this.baseClass}__overlay-message`}>
            <h3>{emailDocumentSuccessMessage}</h3>
            <img src="/dist/images/flourish/flourish-tick.png" alt="" />
            <Cta label={returnToDocumentLibraryTxt} isSmall={true} onClick={() => { this.setState({saveBasketOverlayOpen: false, emailDocumentSuccess: false, downloadOrEmail: '', postBasketOverlayOpen: false, postDocumentSuccess: false}); }} />
          </div>}
        </div>}

    </div>;
  }

  /**
  * renders the email document overlay
  *
  * @return {object} The email overlay
  */
  renderSaveBasketContent () {
    const {returnToDocumentLibraryTxt, emptySaveBasket} = this.props.content;
    return <div>
      {
        this.state.saveBasketDocuments.length > 0 || this.state.emailDocumentSuccess
          ?
          this.renderEmailForm()
          :
          <div className={`${this.baseClass}__overlay-message`}>
            <h3>{emptySaveBasket}</h3>
            <img src="/dist/images/flourish/flourish-tick.png" alt="" />
            <Cta label={returnToDocumentLibraryTxt} isSmall={true} onClick={() => { this.setState({saveBasketOverlayOpen: false, emailDocumentSuccess: false, downloadOrEmail: '', postBasketOverlayOpen: false, postDocumentSuccess: false}); }} />
          </div>
      }
    </div>;
  }

  /**
   * Get the post document form model to pass into the <Form> component
   *
   * @return {object} The form model
   */
  getPostFormModel () {
    const {postDocumentEndpoint} = this.props;
    const {companyNameLabel, companyNamePlaceholder, postDocumentsCtaTxt, mandatoryTxt, removeCta, quantityLabel, postOverlayHeading, addressHeading, nameLabel, namePlaceholder, addressLabel, addressPlaceholder, address2Label, address2Placeholder, address3Label, address3Placeholder, address4Label, address4Placeholder, postCodeLabel, postCodePlaceholder, personalEmailInputPlaceholder, emailInputLabel} = this.props.content;

    const documents = this.state.postBasketDocuments;
    const documentFields = documents.map(document => {
      return {
        'type': 'document-library-document',
        'id': document.id,
        'name': document.id,
        'label': '',
        'additional': '',
        'info': '',
        'value': '',
        'customError': 'Please set the quantity value',
        'isRequired': true,
        'editButton': '',
        'documentProps': {
          ...document,
          removeCta,
          quantityLabel,
          onTogglePost: () => { this.basketService.toggleDocumentInSessionCookie('post', document); }
        }
      };
    });

    const model = {
      'submit': postDocumentsCtaTxt,
      'endpoint': postDocumentEndpoint,
      'groups': [
        {
          'id': 'documents',
          'name': postOverlayHeading,
          'image': '',
          'editButton': '',
          'headingTag': 'h3',
          'additionalHeadingTxt': '',
          'fields': documentFields
        },
        {
          'id': 'address',
          'name': addressHeading,
          'image': '',
          'editButton': '',
          'headingTag': 'h3',
          'additionalHeadingTxt': mandatoryTxt,
          'fields': [{
            'type': 'text',
            'id': 'name',
            'name': 'name',
            'label': nameLabel,
            'placeholder': namePlaceholder,
            'additional': '',
            'info': '',
            'value': '',
            'customError': 'Please enter your name',
            'isRequired': true,
            'editButton': ''
          },
          {
            'type': 'text',
            'id': 'companyName',
            'name': 'companyName',
            'label': companyNameLabel,
            'placeholder': companyNamePlaceholder,
            'additional': '',
            'info': '',
            'value': '',
            'customError': 'Please enter your company name',
            'isRequired': false,
            'editButton': ''
          },
          {
            'type': 'text',
            'id': 'address',
            'name': 'address',
            'label': addressLabel,
            'placeholder': addressPlaceholder,
            'additional': '',
            'info': '',
            'value': '',
            'customError': 'Please enter your address',
            'isRequired': true,
            'editButton': ''
          },
          {
            'type': 'text',
            'id': 'address2',
            'name': 'address2',
            'label': address2Label,
            'placeholder': address2Placeholder,
            'additional': '',
            'info': '',
            'value': '',
            'customError': 'Please enter your address',
            'isRequired': true,
            'editButton': ''
          },
          {
            'type': 'text',
            'id': 'address3',
            'name': 'address3',
            'label': address3Label,
            'placeholder': address3Placeholder,
            'additional': '',
            'info': '',
            'value': '',
            'customError': 'Please enter your address',
            'isRequired': false,
            'editButton': ''
          },
          {
            'type': 'text',
            'id': 'address4',
            'name': 'address4',
            'label': address4Label,
            'placeholder': address4Placeholder,
            'additional': '',
            'info': '',
            'value': '',
            'customError': 'Please enter your address',
            'isRequired': false,
            'editButton': ''
          },
          {
            'type': 'text',
            'id': 'postcode',
            'name': 'postcode',
            'label': postCodeLabel,
            'placeholder': postCodePlaceholder,
            'additional': '',
            'info': '',
            'value': '',
            'customError': 'Please enter your post code',
            'isRequired': true,
            'editButton': ''
          },
          {
            'type': 'email',
            'id': 'email',
            'name': 'email',
            'label': emailInputLabel,
            'placeholder': personalEmailInputPlaceholder,
            'additional': '',
            'info': '',
            'value': '',
            'customError': 'Please enter your company name',
            'isRequired': false,
            'editButton': ''
          }]
        }
      ]
    };

    return model;
  }

  /**
  * Successful postal form submission callback
  */
  postDocumentSuccess () {
    this.setState({postDocumentSuccess: true, postBasketDocuments: []});
    this.basketService.emptyBasket('post');
  }

  /**
  * renders the post document form
  *
  * @return {object} The post document form
  */
  renderPostBasketContent () {
    const {
      returnToDocumentLibraryTxt,
      emptyPostalBasket,
      postDocumentSuccessMessage
    } = this.props.content;

    const formModel = this.getPostFormModel();

    return <div>
      {
        !this.state.postDocumentSuccess
          ?
          <div>
            {this.state.postBasketDocuments.length > 0
              ?
              <Form {...formModel} isVertical={true} customSuccessMethod={this.postDocumentSuccess.bind(this)} />
              :
              <div className={`${this.baseClass}__overlay-message`}>
                <h3>{emptyPostalBasket}</h3>
                <img src="/dist/images/flourish/flourish-tick.png" alt="" />
                <Cta label={returnToDocumentLibraryTxt} isSmall={true} onClick={() => { this.setState({postBasketOverlayOpen: false}); }} />
              </div>
            }
          </div>
          :
          <div className={`${this.baseClass}__overlay-message`}>
            <h3>{postDocumentSuccessMessage}</h3>
            <img src="/dist/images/flourish/flourish-tick.png" alt="" />
            <Cta label={returnToDocumentLibraryTxt} isSmall={true} onClick={() => { this.setState({postBasketOverlayOpen: false}); }} />
          </div>
      }
    </div>;
  }


  /**
  * renders the search form
  *
  * @return {object} The search form
  */
  renderSearch () {
    const {searchCtaTxt, searchPlaceholderTxt} = this.props.content;

    return <form onSubmit={e => { e.preventDefault(); this.onSearch(); }}>
      <div className={`${this.baseClass}__search`}>
        <div className={`${this.baseClass}__search-inner`}>
          <div className="search-input">
            <input type="text" ref={searchInput => this.searchInput = searchInput} name="term" id="site-search" placeholder={searchPlaceholderTxt} defaultValue="" onFocus={() => { this._clickBackLink(1); }} />
          </div>
          <input type="submit" className="cta cta--secondary cta--sm" value={searchCtaTxt} />
        </div>
      </div>
    </form>;
  }

  /**
  * Renders the document library
  */
  render () {
    const {content, popularDocuments, hasDocumentExpired} = this.props;
    const {yourDocumentsTxt, popularDocumentsHeadingTxt, postStickyButtonText, emailStickyButtonText, documentNotFoundLabel, documentNotFoundHeading, documentNotFoundBody} = content;

    const {stage, searchStage, saveBasketOverlayOpen, postBasketOverlayOpen} = this.state;

    const additionalClasses = `stage-${stage} search-stage-${searchStage} container`;

    const panels = this.getPanels();
    const searchPanels = this.getSearchPanels();



    return (
      <ModuleWrapper baseClass={this.baseClass} classes={`${this.state.ready ? `${this.baseClass}--ready` : ''} ${additionalClasses}`}>
        <div ref={component => this.component = component}>
      
     
        {/* {this.state.errorMessage && <Notification title={this.state.errorMessage} type='warning' />} */}



        {<Loading isActive={!this.state.ready} isFullScreen={false} />}

        {(this.state.saveBasketDocuments.length > 0 || this.state.postBasketDocuments.length > 0) &&
          <DocumentLibraryBasketBar
            yourDocumentsTxt={yourDocumentsTxt}
            emailStickyButtonText={emailStickyButtonText}
            postStickyButtonText={postStickyButtonText}
            saveBasketCount={this.state.saveBasketDocuments.length}
            postBasketCount={this.state.postBasketDocuments.length}
            onClickSaveBasket={() => this.setState({saveBasketOverlayOpen: true})}
            onClickPostBasket={() => this.setState({postBasketOverlayOpen: true})} />}

        <div className={`${this.baseClass}__inner`} style={{height: this.state.mobileContainerHeight}}>

          <div ref={(c) => { this.container = c; }} className={`${this.baseClass}__main container`} >

            <div ref={(p) => { this.activePanel = p; }} className={`${this.baseClass}__panel ${this.baseClass}__panel--stage0 ${this.baseClass}__panel--active`}>
              <div className={`${this.baseClass}__panel-inner`}>

                <div className={`${this.baseClass}__columns`}>
                  {hasDocumentExpired && <div className={`${this.baseClass}__expired`}>
                    <div className={`${this.baseClass}__column1`}>
                      <span className="label">{documentNotFoundLabel}</span>
                      <p className="title h2">{documentNotFoundHeading}</p>
                      <p className="">{documentNotFoundBody}</p>
                    </div>
                    <div className={`${this.baseClass}__column2`}>
                      <img src='/dist/images/flourish/flourish-404.png' />
                    </div>
                  </div>}
                  <div className={`${this.baseClass}__column1`}>


                    {this.renderSearch()}

                    <ul className={`${this.baseClass}__navlinks`}>
                      {this.getItems(this.state.stage0.items, 0)}
                    </ul>
                  </div>
                  <div className={`${this.baseClass}__column2`}>
                    <Editable tag="h3" className="label" html={popularDocumentsHeadingTxt} />
                    <ul className={`${this.baseClass}__doclinks`}>
                      {this.getItems(popularDocuments, null)}
                    </ul>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div className={`${this.baseClass}__panel-wrapper`}>
            {this.state.isSearching ? searchPanels : panels}
          </div>
        </div>

        <div className={`${this.baseClass}__download-links`} ref={downloadLinks => this.downloadLinks = downloadLinks}>{this.getDownloadFiles()}</div>
        <Overlay
          isOpen={saveBasketOverlayOpen}
          from='right'
          hasHeader={false}
          width='100%'
          className='overlay overlay--document-library'
          onRequestClose={() => { this.setState({saveBasketOverlayOpen: false, emailDocumentSuccess: false, downloadOrEmail: '', postBasketOverlayOpen: false, postDocumentSuccess: false}); }}
        >
          {this.renderSaveBasketContent()}
        </Overlay>

        <Overlay
          isOpen={postBasketOverlayOpen}
          from='right'
          hasHeader={false}
          width='100%'
          className='overlay overlay--document-library'
          onRequestClose={() => { this.setState({saveBasketOverlayOpen: false, emailDocumentSuccess: false, downloadOrEmail: '', postBasketOverlayOpen: false, postDocumentSuccess: false}); }}
        >
          {this.renderPostBasketContent()}
        </Overlay>


        </div>
      </ModuleWrapper>
    );
  }
}

DocumentLibrary.defaultProps = {
  searchApiEndpoint: '/doc-api/search/',
  documentListApiEndpoint: '/doc-api/document-list/',
  subCategoryApiEndPoint: '/doc-api/sub-category/',
  categoryApiEndPoint: '/doc-api/category/',
  postDocumentEndpoint: '',
  emailDocumentEndpoint: '',
  isDev: false,
  items: [],
  popularDocuments: [],
  searchItemsPerPane: 3,
  hasDocumentExpired: false,
  cookieExpiry: 180,
  content: {
    yourDocumentsTxt: 'Your selected documents:',
    openTxt: 'Open',
    closeTxt: 'Close',
    returnToDocumentLibraryTxt: 'Return to Document Library',
    popularDocumentsHeadingTxt: 'Popular documents',
    cancelTxt: 'Cancel',

    postOverlayHeading: 'Documents to post',
    postDocumentTxt: 'Post document',
    postDocumentEnabledTxt: 'Added to post list',
    addressHeading: 'Postal address',
    postDocumentsCtaTxt: 'Post now',
    mandatoryTxt: '* Mandatory',
    emptyPostalBasket: 'You do not have any documents selected to post.',
    removeCta: 'Remove',
    quantityLabel: 'Quantity',
    postStickyButtonText: '{count} Document{s} to post',

    saveDocumentTxt: 'Email / download document',
    saveDocumentEnabledTxt: 'Added to email / download list',
    saveOverlayHeading: 'Documents to email/download',
    emptySaveBasket: 'You do not have any documents selected to email/download.',
    downloadAll: 'Download All',
    emailAll: 'Email All',
    emailStickyButtonText: '{count} Document{s} email',


    searchPlaceholderTxt: 'Enter search term here',
    searchCtaTxt: 'Go',
    searchInProgressTxt: 'Searching...',
    searchHeadingTxt: 'All Results',
    searchResultsError: 'Your search didn\'t return any results, please try searching again',

    nameLabel: 'Name',
    namePlaceholder: 'Enter name here',
    companyNameLabel: 'Company name',
    companyNamePlaceholder: 'Enter company name here',
    addressLabel: 'Address',
    addressPlaceholder: 'Enter address here',
    address2Label: 'Address line 2',
    address2Placeholder: 'Enter address here',
    address3Label: 'Address line 3',
    address3Placeholder: 'Enter address here',
    address4Label: 'Address line 4',
    address4Placeholder: 'Enter address here',
    postCodeLabel: 'Post code',
    postCodePlaceholder: 'Enter post code here',
    postDocumentSuccessMessage: 'Your documents will be posted soon',

    emailInputLabel: 'Email address',
    emailInputPlaceholder: 'Enter your email address',
    personalEmailInputPlaceholder: 'Enter your email address',
    selectDefault: 'Choose you option',
    addAnotherEmailTxt: 'Add another email',
    emailDocumentSuccessMessage: 'You have successfully emailed your selected documents',

    selectEmailOrDownloadLabel: 'How do you want your documents?',

    documentNotFoundLabel: 'Missing Document',
    documentNotFoundHeading: 'Uh oh, we can\'t find the document you\'re looking for...',
    documentNotFoundBody: 'We may have moved it, taken it away or there may be a problem with it.'
  }
};

DocumentLibrary.propTypes = {
  hasDocumentExpired: PropTypes.bool,
  searchApiEndpoint: PropTypes.string,
  documentListApiEndpoint: PropTypes.string,
  subCategoryApiEndPoint: PropTypes.string,
  categoryApiEndPoint: PropTypes.string,
  emailDocumentEndpoint: PropTypes.string,
  postDocumentEndpoint: PropTypes.string,
  searchItemsPerPane: PropTypes.number,
  isDev: PropTypes.bool,
  content: PropTypes.object.isRequired,
  cookieExpiry: PropTypes.number.isRequired,
  items: PropTypes.array.isRequired,
  popularDocuments: PropTypes.array.isRequired,
  image: PropTypes.object,
  headline: PropTypes.string,
  breadcrumbLabel: PropTypes.string,
  sectionsHeadline: PropTypes.string
};

module.exports = DocumentLibrary;
