import {Component} from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon/Icon';
import ContentExpander from '../ContentExpander/ContentExpander';
import './DocumentLink.scss';

class DocumentLink extends Component {
  constructor (props) {
    super(props);

    this.state = {
      isOpen: this.props.isOpen,
      isInSaveBasket: this.props.isInSaveBasket,
      isInPostBasket: this.props.isInPostBasket
    };

    this._onClick = this._onClick.bind(this);

    this._onToggleSaveBasket = this._onToggleSaveBasket.bind(this);
    this._onTogglePostBasket = this._onTogglePostBasket.bind(this);
  }

  _onClick () {
    this.setState({isOpen: !this.state.isOpen});
    setTimeout(() => {
      this.props.onExpand.call();
    }, 300);
  }

  _onToggleSaveBasket (e) {
    e.preventDefault();
    this.props.onToggleSave();
    this.setState({isInSaveBasket: !this.state.isInSaveBasket});
  }

  _onTogglePostBasket (e) {
    e.preventDefault();
    this.props.onTogglePost();
    const active = !this.state.isInPostBasket;
    this.setState({isInPostBasket: active});
  }

  renderHead (showExpander) {
    const {
      id,
      type,
      name,
      reference,
      updatedDate,
      thumbnailSrc,
      documentUrl,
      openTxt,
      closeTxt
    } = this.props;

    return (
      <div className="document-link__head">
        <div className="document-link__thumb">
          <a
            href={documentUrl}
            data-id={id}
            data-type={type}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={thumbnailSrc} width="20" />
          </a>
        </div>

        <div className="document-link__content">
          <a
            href={documentUrl}
            data-id={id}
            data-type={type}
            target="_blank"
            rel="noopener noreferrer"
          >
            <span className="document-link__name">{name}</span>
          </a>
          <p className="document-link__ref sub-text--small">
            Ref: {reference} | <span>Last updated:{updatedDate}</span>
          </p>
          {showExpander && (
            <span
              className="document-link__toggle sub-text--small"
              onClick={this._onClick}
            >
              {this.state.isOpen ? closeTxt : openTxt}
              <Icon icon={this.state.isOpen ? 'minus' : 'plus'}></Icon>
            </span>
          )}
        </div>
      </div>
    );
  }

  render () {
    const {
      id,
      quantityLabel,
      onBlur,
      onChange,
      isInSaveBasket,
      isInPostBasket,
      summary,
      saveDocumentTxt,
      postDocumentTxt,
      saveDocumentEnabledTxt,
      postDocumentEnabledTxt,
      removeCta,
      documentIcons,
      isPostable
    } = this.props;

    return this.props.isPostBasketItem ? (
      <div className={'document-link document-link--post-basket'}>
        {this.renderHead()}
        <div className="document-link__quantity">
          <label className="document-link__quantity-label sub-text--small">
            {quantityLabel}
          </label>
          <input
            className="document-link__quantity-input"
            type="number"
            defaultValue={1}
            name={id}
            id={id}
            required={true}
            onBlur={onBlur}
            onChange={onChange}
          />
          <a
            href="#remove"
            className="document-link__remove"
            onClick={this._onTogglePostBasket}
          >
            {removeCta}
          </a>
        </div>
      </div>
    ) : (
      <div className={'document-link'}>
        {this.renderHead(true)}
        <ContentExpander
          isOpen={this.state.isOpen}
          className="document-link__expander"
        >
          <p className="document-link__body sub-text">{summary}</p>

          <ul className="document-link__icons">
            {documentIcons.map((icon, i) => {
              return (
                <li
                  key={`document-icon-${id}-${i}`}
                  className="document-link__icon"
                >
                  <img src={icon.iconSrc} />
                  <span className="sub-text--small">{icon.iconTxt}</span>
                </li>
              );
            })}
          </ul>

          {!isInSaveBasket ? (
            <div className="document-link__control">
              <a
                className="document-link__save document-link__download"
                onClick={this._onToggleSaveBasket}
                href="#"
              >
                {saveDocumentTxt}
              </a>
            </div>
          ) : (
            <div className="document-link__control">
              <a
                className="document-link__save document-link__save--saved"
                onClick={this._onToggleSaveBasket}
              >
                <Icon icon="tick" fillColor="#6FB700" />
                {saveDocumentEnabledTxt}
              </a>
            </div>
          )}

          {isPostable &&
            (!isInPostBasket ? (
              <div className="document-link__control">
                <a
                  className="document-link__save document-link__post"
                  onClick={this._onTogglePostBasket}
                  href="#"
                >
                  {postDocumentTxt}
                </a>
              </div>
            ) : (
              <div className="document-link__control">
                <a
                  className="document-link__save document-link__save--saved"
                  onClick={this._onTogglePostBasket}
                >
                  <Icon icon="tick" fillColor="#6FB700" />
                  {postDocumentEnabledTxt}
                </a>
              </div>
            ))}
        </ContentExpander>
      </div>
    );
  }
}

DocumentLink.propTypes = {
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  id: PropTypes.string,
  type: PropTypes.string,
  isInSaveBasket: PropTypes.bool,
  isInPostBasket: PropTypes.bool,
  isOpen: PropTypes.bool,
  onToggleSave: PropTypes.func,
  onTogglePost: PropTypes.func,
  onExpand: PropTypes.func,
  name: PropTypes.string,
  reference: PropTypes.string,
  updatedDate: PropTypes.string,
  thumbnailSrc: PropTypes.string,
  documentUrl: PropTypes.string,
  openTxt: PropTypes.string,
  closeTxt: PropTypes.string,
  // detail
  postDocumentTxt: PropTypes.string,
  saveDocumentTxt: PropTypes.string,
  postDocumentEnabledTxt: PropTypes.string,
  saveDocumentEnabledTxt: PropTypes.string,
  cancelTxt: PropTypes.string,
  moreTxt: PropTypes.string,
  lessTxt: PropTypes.string,
  summary: PropTypes.string,
  documentIcons: PropTypes.array,
  isPostable: PropTypes.bool,
  // post basket
  removeCta: PropTypes.string,
  quantityLabel: PropTypes.string,
  isPostBasketItem: PropTypes.bool,
  isSaveBasket: PropTypes.bool
};

DocumentLink.defaultProps = {
  isInSaveBasket: false,
  isInPostBasket: false,
  isOpen: false,
  onToggleSave: () => {},
  onTogglePost: () => {},
  onExpand: () => {},
  name: 'Document Name',
  reference: '0021072-2018',
  updatedDate: '21/02/2019',
  thumbnailSrc: '/dist/images/icon/doc-ico-green.png',
  documentUrl: '',
  openTxt: 'Open',
  closeTxt: 'Close',
  // detail
  postDocumentTxt: 'Post document',
  saveDocumentTxt: 'Email / download document',
  postDocumentEnabledTxt: 'Added to post list',
  saveDocumentEnabledTxt: 'Added to download list',
  cancelTxt: 'Cancel',
  moreTxt: 'More',
  lessTxt: 'Less',
  summary:
    'A sentence that briefly explains this document. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
  documentIcons: [],
  isPostable: true,
  // post basket
  removeCta: 'Remove',
  quantityLabel: 'Quantity',
  isPostBasketItem: false,
  isSaveBasket: false
};

export default DocumentLink;
