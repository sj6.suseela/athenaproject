import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/Icon';
import Editable from '../Editable/Editable';

class AccordionItem extends React.Component {
  constructor (props) {
    super(props);

    this.title = this.props.title;
    this.opentitle = this.props.opentitle ? this.props.opentitle : this.title;
    const t = this.props.isOpen ? this.opentitle : this.title;

    this.state = {
      open: this.props.isOpen,
      height: 'auto',
      className: 'accordion__item',
      title: t
    };

    this.cn = this.props.itemClass ? `accordion__head ${  this.props.itemClass}` : 'accordion__head';

    this.content = (typeof this.props.children === 'string') ?  <Editable html={this.props.children} /> : this.props.children;

    this._click = this._click.bind(this);
  }

  _click () {
    this.props.selectItem(this.props.index);
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps (props) {
    if(!props.isOpen) {
      this.setState({
        open:false,
        height:0,
        className: 'accordion__item',
        title: this.title
      });
    }
    else {
      this.setState({
        open:true,
        height:this.openHeight,
        className: 'accordion__item accordion__item--open',
        title: this.opentitle
      });
    }
  }

  componentDidMount () {
    this.openHeight = this.accordionSection.clientHeight;

    if(this.state.open) {
      this.setState({
        height:this.openHeight,
        className: 'accordion__item accordion__item--open'
      });
    }
    else    {
      this.setState({
        height:0,
        className: 'accordion__item'
      });
    }

    if(!SERVER) {
      if(typeof window === 'object') {
        window.addEventListener('resize', this._handleResize.bind(this));
      }
    }
  }

  componentWillUnmount () {
    if(!SERVER) {
      if(typeof window === 'object') {
        window.removeEventListener('resize', this._handleResize);
      }
    }
  }

  _handleResize () {
    if(this && this.accordionSection) {
      this.setState({
        height:'auto'
      });

      this.openHeight = this.accordionSection.clientHeight;

      if(this.state.open) {
        this.setState({
          height:this.openHeight
        });
      }
      else      {
        this.setState({
          height:0
        });
      }
    }
  }

  render () {
    return (
    <div className={this.state.className}>
      <div className={this.cn} onClick={this._click}>
        <Editable className="accordion__head__label" html={this.state.title} />
        <span className="accordion__icon"><Icon icon={this.props.icon} /></span>
      </div>
      <div className="accordion__section" style={{height: this.state.height}} ref={ (accordionSection) => this.accordionSection = accordionSection}>
        <div className="accordion__article">{this.content}</div>
      </div>
    </div>
    );
  }
}

AccordionItem.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  opentitle: PropTypes.string,
  itemClass:PropTypes.string,
  isOpen:PropTypes.bool,
  children:PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  selectItem:PropTypes.func,
  index:PropTypes.number
};

module.exports = AccordionItem;
