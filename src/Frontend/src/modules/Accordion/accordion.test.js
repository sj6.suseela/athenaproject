import React from 'react';

import {mount} from 'enzyme';
import Accordion from './Accordion';

test('Expect number of accordion items to equal 3', () => {
  // Render the component

  const accordion = mount(
    <Accordion closeSiblings={false} items={[{
      title: 'title 1',
      content: '<p>content 1</p>',
      open:false
    },
    {
      title: 'title 2',
      content: '<p>content 2</p>',
      open:false
    },
    {
      title: 'title 3',
      content: '<p>content 3</p>',
      open:false
    }]}/>
  );

  expect(accordion.find('.accordion__item')).toHaveLength(3);
});

test('Expect first accordion item to be open on load', () => {
  // Render the component
  const accordion = mount(
    <Accordion closeSiblings={false} items={[{
      title: 'title 1',
      content: '<p>content 1</p>',
      open:true
    },
    {
      title: 'title 2',
      content: '<p>content 2</p>',
      open:false
    },
    {
      title: 'title 3',
      content: '<p>content 3</p>',
      open:false
    }]}/>
  );

  accordion.find('.accordion__item').forEach((node,index) => {
    if(index === 0) {
      expect(node.hasClass('accordion__item--open')).toEqual(true);
    }
  });
});

test('Expect second accordion item to be closed on load and then opened after click on heading', () => {
  // Render the component
  const accordion = mount(
    <Accordion closeSiblings={false} items={[{
      title: 'title 1',
      content: '<p>content 1</p>',
      open:false
    },
    {
      title: 'title 2',
      content: '<p>content 2</p>',
      open:false
    },
    {
      title: 'title 3',
      content: '<p>content 3</p>',
      open:false
    }]}/>
  );

  accordion.find('.accordion__item').forEach((node,index) => {
    if(index === 1) {
      expect(node.hasClass('accordion__item--open')).toEqual(false);
      node.find('.accordion__head').simulate('click');
    }
  });

  accordion.update();

  accordion.find('.accordion__item').forEach((node,index) => {
    if(index === 1) {
      expect(node.hasClass('accordion__item--open')).toEqual(true);
    }
  });
});
