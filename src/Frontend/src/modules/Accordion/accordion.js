import React from 'react';
import PropTypes from 'prop-types';

import './accordion.scss';

import AccordionItem from './AccordionItem';
import SitecoreSettings from '../SitecoreSettings';

/** Simple Accordion.
  * @description this is a description of the accordion module
  * @param {bool} [closeSiblings=false] - Whether the other accordion sections close when you open a section
  * @param {object} items - The items in the accordion
  * @param {string} items.title - The tile of the accordion section
  * @param {string} items.content - The html of the content for the accordion section
  * @param {bool} items.open - Whether the accordion section is open by default
  * @example  ReactDOM.render(React.createElement(Components.Accordion, {
            closeSiblings:false,
            items: [{
                heading: "title 1",
                children: '<p>content 1</p>',
                open:false
            },
            {
                heading: "title 2",
                children: '<p>content 2</p>',
                open:false
            },
            {
                heading: "title 3",
                children: '<p>content 3</p>',
                open:false
            }]
        }), document.getElementById("Accordion"));
*/

class Accordion extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      selected: null,
      previous:null,
      items:this.props.items,
      isEditing:false
    };

    const {className, modifier} = this.props;

    this.className = `accordion ${modifier && `accordion--${modifier}`} ${className}`;

    // const items = this.props.items;

    this._selectItem = this._selectItem.bind(this);
  }

  componentDidMount () {
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  _selectItem (itemIndex) {
    this.setState({
      selected:itemIndex
    });

    let newState = {};

    newState = this.state.items.map((item, i) => {
      if(i === itemIndex) {
        item.open = !item.open;
      }
      else{
        if(this.props.closeSiblings) {
          item.open = false;
        }
      }

      return item;
    });

    this.setState({
      items:newState
    });
  }

  render () {
    const accordionItems = this.state.items.map((item,i) => {
      const isOpen = this.state.isEditing ? true : item.open;
      return <AccordionItem key={i} icon={this.props.icon} itemClass={this.props.itemClass} title={item.heading} opentitle={item.opentitle} isOpen={isOpen} selectItem={this._selectItem} index={i}>{item.children}</AccordionItem>;
    });

    return (
      <div className={this.className}>
        {accordionItems}
      </div>
    );
  }
}

Accordion.defaultProps = {
  modifier: 'default',
  icon: 'plus',
  className: '',
  items:  [],
  closeSiblings: false,
  itemClass: ''
};

Accordion.propTypes = {
  modifier: PropTypes.string,
  icon: PropTypes.string,
  className: PropTypes.string,
  items: PropTypes.array.isRequired,
  closeSiblings:PropTypes.bool,
  itemClass:PropTypes.string
};

module.exports = Accordion;
