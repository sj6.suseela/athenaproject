  import React from 'react';
  import PropTypes from 'prop-types';

  import SitecoreSettings from '../SitecoreSettings';
  import {globalSettings} from '../utils.js';
  import EditableImage from '../Editable/EditableImage';

  import LazyLoad from '../_ThirdParty/LazyLoad';

  import Theme from '../Theme';

  import './ResponsiveImage.scss';

  class ResponsiveImage extends React.Component {
    constructor (props) {
      super(props);

      this.state = {
        isEditing:false,
        picSupported:true
      };
    }

    componentDidMount () {
      if(!SERVER) {
        const pic = !!window.HTMLPictureElement;
        this.setState({
          picSupported:pic
        });
      }

      const sc = new SitecoreSettings();

      if(this.state.isEditing !== sc.isExperienceEditor()) {
        this.setState({
          isEditing:sc.isExperienceEditor()
        });
      }
    }

    render () {
      const {bgtheme, desktopImage, desktopImageSrc, cssclass, hasHeaderMask, isHomepageHeader, altText, isBackground, doMobileFallback,mobImageSize,desktopImageSize, onReplace, hasLazyLoad} = this.props;

      let {mobImage, mobImageSrc} = this.props;
      if (doMobileFallback && !mobImage) {
        mobImage = desktopImage;
        mobImageSrc = desktopImageSrc;
      }
      const isEditing = this.state.isEditing;
      let id;
      if(hasHeaderMask) {
        id = 'headermask';
      }
      else if(mobImageSrc && desktopImageSrc) {
        id = mobImageSrc.replace(/\//g, '') + desktopImageSrc.replace(/\//g, '');
      }
      else{
        id = cssclass;
      }
      const bggradient = Theme.getTheme(bgtheme);
      const hasDesktopImage = !!desktopImage;
      const hasMobImage = !!mobImage;

      const fallbackMobileStyle = {backgroundImage : `url('${mobImageSrc}')`};
      const fallbackDesktopStyle = {backgroundImage : `url('${desktopImageSrc}')`};

      const mobPlaceholderHeight = mobImageSize ? mobImageSize.height : 0;
      const mobPlaceholderWidth = mobImageSize ? mobImageSize.width : 0;

      const desktopPlaceholderHeight = desktopImageSize ? desktopImageSize.height : 0;
      const desktopPlaceholderWidth = desktopImageSize ? desktopImageSize.width : 0;

      let lazyLoadImages = true;
      if(!SERVER) {
        lazyLoadImages = globalSettings().site.lazyLoadImages;
      }

      if(hasLazyLoad !== null) {
        lazyLoadImages = hasLazyLoad;
      }

      let maskClass = '';
      if(this.props.visible) {
        maskClass = 'mask-hide';
      }

      return (
        <div className={`${maskClass}`}>
          {!isEditing && this.state.picSupported && (
            
              <picture className={cssclass}>
                {hasDesktopImage && (
                  <source media="(min-width: 1024px)" srcSet={desktopImageSrc} />
                )}
                
                {hasMobImage && !hasDesktopImage && (
                  <LazyLoad once height={mobPlaceholderHeight} width={mobPlaceholderWidth} offset={0} isActive={lazyLoadImages} onReplace={onReplace}>
                    <img className={'mobileOnly'} src={mobImageSrc} alt={altText} />
                  </LazyLoad>
                )}
                {!hasMobImage && hasDesktopImage && (
                  <LazyLoad once height={desktopPlaceholderHeight} width={desktopPlaceholderWidth} offset={0} isActive={lazyLoadImages} onReplace={onReplace}>
                    <img className={'desktopOnly'} src={desktopImageSrc} alt={altText} />
                  </LazyLoad>
                )}
                {hasMobImage && hasDesktopImage && (
                  <LazyLoad once height={mobPlaceholderHeight} width={mobPlaceholderWidth} offset={0} isActive={lazyLoadImages} onReplace={onReplace}>
                    <img src={mobImageSrc} alt={altText} />
                  </LazyLoad>
                )}
                
              </picture>
            
          )}
          {!isEditing && !this.state.picSupported && !isBackground && (
            <div className={cssclass}>
              {hasMobImage && (
                <img className={'mobileOnly'} src={mobImageSrc} alt={altText} />
              )}
              {hasDesktopImage && (
                <img className={'desktopOnly'} src={desktopImageSrc} alt={altText} />
              )}
            </div>
          )}
          {!isEditing && !this.state.picSupported && isBackground && (
            <div className={cssclass}>
              <div style={fallbackMobileStyle} className="mobileOnly"></div>
              <div style={fallbackDesktopStyle} className="desktopOnly"></div>
            </div>
          )}
          {isEditing && (
            <EditableImage className="mobileOnly" image={mobImage} />
          )}
          {isEditing && (
            <EditableImage className="desktopOnly" image={desktopImage} />
          )}
          {hasHeaderMask && (
            <div className="mask">
              {!isHomepageHeader && (
                <svg version="1.1" x="0px" y="0px" viewBox="0 0 1400 150" width="1400" height="150" preserveAspectRatio="xMinYMax">
                  <defs>
                    <linearGradient id={`${id}-gradbg`}>
                      <stop offset="0%" stopColor={bggradient[0]} />
                      <stop offset="100%" stopColor={bggradient[1]} />
                    </linearGradient>
                  </defs>
                  <path d="M1400,151H0V133s135,23,383,13C618,136,892,120,1400,0" fill={`url(#${id}-gradbg)`} />
                </svg>
              )}
              {isHomepageHeader && (
                <svg version="1.1" x="0px" y="0px" viewBox="0 0 1439 35.6" width="1439" height="35.6" preserveAspectRatio="xMinYMax">
                  <defs>
                    <linearGradient id={`${id}-gradbg`}>
                      <stop offset="0%" stopColor={bggradient[0]} />
                      <stop offset="100%" stopColor={bggradient[1]} />
                    </linearGradient>
                  </defs>
                  <path d="M1440,0c-1.3,0.1-2.7,0.3-4,0.4c-209.1,22.3-454.1,35.6-716,35.6h720V0z" fill={`url(#${id}-gradbg)`} />
                  <path d="M720,35.6C456.4,35.6,210,22.6,0,0v35.6H720z" fill={`url(#${id}-gradbg)`} />
                </svg>
              )}
            </div>
          )}
        </div>
      );
    }
}

  ResponsiveImage.defaultProps = {
    onReplace:null,
    hasLazyLoad:null,
    visible:true
  };

  ResponsiveImage.propTypes = {
    bgtheme: PropTypes.string,
    desktopImage: PropTypes.string,
    desktopImageSrc: PropTypes.string,
    mobImage: PropTypes.string,
    mobImageSrc: PropTypes.string,
    altText: PropTypes.string,
    cssclass: PropTypes.string,
    hasHeaderMask: PropTypes.bool,
    isHomepageHeader: PropTypes.bool,
    isBackground: PropTypes.bool,
    doMobileFallback: PropTypes.bool,
    mobImageSize:PropTypes.object,
    desktopImageSize:PropTypes.object,
    onReplace:PropTypes.func,
    visible:PropTypes.any,
    hasLazyLoad:PropTypes.bool
  };

  module.exports = ResponsiveImage;
