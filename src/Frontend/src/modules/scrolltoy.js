let ScrollToY;

if(!SERVER) {
  window.requestAnimFrame = (function () {
    return  window.requestAnimationFrame       ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame    ||
              function (callback) {
                window.setTimeout(callback, 1000 / 60);
              };
  })();

  ScrollToY = function (scrollTargetYProp, speedProp, easingProp) {
    const scrollY = window.scrollY,
      scrollTargetY = scrollTargetYProp || 0,
      speed = speedProp || 2000,
      easing = easingProp || 'easeOutSine';
    
    let currentTime = 0;
      

    const time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));

    const easingEquations = {
      easeOutSine (pos) {
        return Math.sin(pos * (Math.PI / 2));
      },
      easeInOutSine (pos) {
        return (-0.5 * (Math.cos(Math.PI * pos) - 1));
      },
      easeInOutQuint (pos) {
        let newPos = pos;
        if ((newPos /= 0.5) < 1) {
          return 0.5 * Math.pow(newPos, 5);
        }
        return 0.5 * (Math.pow((newPos - 2), 5) + 2);
      }
    };

    function tick () {
      currentTime += 1 / 60;

      const p = currentTime / time;
      const t = easingEquations[easing](p);

      if (p < 1) {
        window.requestAnimFrame(tick);
        window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
      }
      else {
        window.scrollTo(0, scrollTargetY);
      }
    }

    tick();
  };
}

module.exports = ScrollToY;