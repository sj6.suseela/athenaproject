import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/Icon';
import './Share.scss';

if(!SERVER) {
  require('../../external/pinit.js');
}

/** Share
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Share, {
    url:"http://www.google.com",
    title:"example title",
    titleIntro:"Check out this from LV: "
  }), document.getElementById("element"));
*/
class Share extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isOpen: false
    };

    this._click = this._click.bind(this);
  }

  _click (e) {
    e.preventDefault();

    this.setState({
      isOpen:!this.state.isOpen
    });
  }

  render () {
    const {title, titleIntro, link} = this.props;

    const isOpen = this.state.isOpen;
    const baseClass = 'share';

    let cssClass = baseClass;

    if(isOpen) {
      cssClass += ` ${baseClass}--open`;
    }

    return (
      <div className={cssClass}>
        <div className="container share__container">
          <div className="share__main">
            <span className="share__button">
                <button className="share__toggle" onClick={this._click}>
                  <Icon icon="share" />
                </button>
              </span>
            <div className="share__networks">
              <span className="share__button"><a target="_blank" rel="noopener noreferrer" id={link.id} href={`http://www.facebook.com/sharer.php?u=${link.href}&p[title]=${title}`}><Icon icon="facebook" /></a></span>
              <span className="share__button"><a target="_blank" rel="noopener noreferrer" id={link.id} href={`http://twitter.com/share?text=${title  }&url=${link.href}`}><Icon icon="twitter" /></a></span>
              <span className="share__button"><a target="_blank" rel="noopener noreferrer" id={link.id} href={`https://www.linkedin.com/shareArticle?mini=true&url=${link.href}&summary=${title}&source=LinkedIn`}><Icon icon="linkedIn" /></a></span>
              <span className="share__button"><a target="_blank" rel="noopener noreferrer" id={link.id} href={`mailto:?body=${link.href}&subject=${titleIntro} ${title}`}><Icon icon="email" /></a></span>
              <span className="share__button share__button--pinterest">
                <a data-pin-do="buttonBookmark" data-pin-tall="true" data-pin-round="true" rel={link.rel} id={link.id} href="https://www.pinterest.com/pin/create/button/"></a>
                <Icon icon="pinterest" />
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Share.defaultProps = {
  title: '',
  link: {}
};

Share.propTypes = {
  title: PropTypes.string,
  titleIntro:PropTypes.string,
  link:PropTypes.object
};

module.exports = Share;
