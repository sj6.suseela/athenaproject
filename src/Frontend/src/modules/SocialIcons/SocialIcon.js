import React from 'react';
import PropTypes from 'prop-types';

import EditableLink from '../Editable/EditableLink';

class SocialIcon extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

    };
  }

  render () {
    return (
      <EditableLink href={this.props.href} html={this.props.markup} />
    );
  }
}

SocialIcon.propTypes = {
  href: PropTypes.string,
  markup: PropTypes.string
};

module.exports = SocialIcon;
