import React from 'react';
import PropTypes from 'prop-types';

import SitecoreSettings from '../SitecoreSettings';

import EditableDiv from '../Editable/EditableDiv';
import EditableLink from '../Editable/EditableLink';

import './SocialIcons.scss';

class SocialIcons extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing:false
    };
  }

  componentDidMount () {
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  render () {
    const isEditing = this.state.isEditing;

    if(this.props.items) {
      this.items = this.props.items.map((item,i) => {
        if(isEditing) {
          return <div key={i}><EditableDiv html={item.logo} /><EditableDiv html={item.link.html} /></div>;
        }
        else{
          return <div key={i}><EditableLink href={item.link.href} id={item.link.id} html={item.logo} /></div>;
        }
      });
    }

    return (
		<div className="socialIcons">
      {this.items}
		</div>
    );
  }
}

SocialIcons.propTypes = {
  // prefix: PropTypes.string,
  items: PropTypes.array
};

module.exports = SocialIcons;
