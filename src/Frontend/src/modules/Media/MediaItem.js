
import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableParagraph from '../Editable/EditableParagraph';
import EditableImage from '../Editable/EditableImage';
import Cta from '../Cta/Cta';

import './MediaItem.scss';

class MediaItem extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {title, info, image, link} = this.props;
    const baseClass = 'media-item';

    return (
      <div className={baseClass}>
        <div className={`${baseClass}__main`}>
          <EditableImage className={`${baseClass}__image`} image={image.html} />
          <div className={`${baseClass}__body`}>
            <Editable tag="h3" className={`title ${baseClass}__title`} html={title} />
            <EditableParagraph className={`${baseClass}__info`} html={info} />
            <Cta type="secondary" isSmall={true} rawCta={link.html} />
          </div>
        </div>
      </div>
    );
  }
}

MediaItem.defaultProps = {
  title: '',
  info: '',
  image : '',
  link : null
};

MediaItem.propTypes = {
  title: PropTypes.string,
  info: PropTypes.string,
  image: PropTypes.object,
  link: PropTypes.object
};

module.exports = MediaItem;
