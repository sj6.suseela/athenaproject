
import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import MediaItem from './MediaItem';

import './Media.scss';

/** Media
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Media, {
    id : 'id',
    theme : {},
    heading : 'Media',
    items : [
      {
        title : 'LV= Brand guidelines',
        info : 'PDF (514 kb)',
        image : '<img src="http://placehold.it/300x200" />',
        link : '<a href="#/media">Download</a>'
      },
      {
        title : 'Image example',
        info : 'JPEG (829 kb)',
        image : '<img src="http://placehold.it/300x200" />',
        link : '<a href="#/media">Download</a>'
      }
    ]
  }), document.getElementById("element"));
*/
class Media extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading, items} = this.props;
    const baseClass = 'media';

    if(!items) {
      return ('');
    }

    const media = items.map((item, i) => {
      return (<MediaItem key={i} {...item} />);
    });

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <Editable tag="h2" className={`title ${baseClass}__title`} html={heading} />
        <div className={`${baseClass}__main`}>
          {media}
        </div>
      </ModuleWrapper>
    );
  }
}

Media.defaultProps = {
  heading: '',
  items: null
};

Media.propTypes = {
  heading: PropTypes.string,
  items: PropTypes.array
};

module.exports = Media;
