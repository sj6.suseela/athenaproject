import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';

import './OpvPensionLanding.scss';

class OpvPensionsDetails extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {
      title,
      description,
      back,
      descriptionRight,
      descriptionCard,
      arrowMark,
      descriptionBrd
    } = this.props;
    const baseClass = 'pension-details';

    return (
      <div>
        <div className={`${baseClass}`}>
          <div className={`${baseClass}__main`}>
            <div className={`${baseClass}__signinform`}>
              <EditableDiv className={`${baseClass}__backLink`} html={back} />
              <Editable
                tag="h2"
                className={`${baseClass}__title`}
                html={title}
              />
              <EditableDiv
                className={`${baseClass}__content`}
                html={description}
              />
              <Editable
                className={`${baseClass}__contentBorder`}
                html={descriptionBrd}
              />
            </div>
            <div className={`${baseClass}__createacc`}>
              <Editable
                tag="h4"
                className={`${baseClass}__contentRight`}
                html={descriptionRight}
              />
              <div className={`${baseClass}__card`}>
                <div className={`${baseClass}__cardcontent`}>
                  <div className={`${baseClass}__content`}>
                    <Editable
                      tag="h6"
                      html={descriptionCard.title}
                    />
                    <Editable
                      tag="p"
                      html={descriptionCard.policyNumber}
                    />
                    <p>
                      <Editable
                        html={descriptionCard.totalValueText}
                      />
                      <Editable
                        tag="strong"
                        html={descriptionCard.totalValue}
                      />
                    </p>
                  </div>

                </div>
                <div className={`${baseClass}__cardarrowMark`}>
                  <Editable html={arrowMark} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

OpvPensionsDetails.defaultProps = {
  title: '',
  description: '',
  back: '',
  descriptionRight: '',
  descriptionCard: '',
  arrowMark: '',
  descriptionBrd: ''
};

OpvPensionsDetails.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  backArrow: PropTypes.string,
  back: PropTypes.string,
  descriptionRight: PropTypes.string,
  descriptionCard: PropTypes.object,
  arrowMark: PropTypes.string,
  descriptionBrd: PropTypes.string
};

module.exports = OpvPensionsDetails;
