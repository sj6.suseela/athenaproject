import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvVerificationCodeForm.scss';

class OpvVerificationCodeForm extends React.Component {

  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const {formArea1,content, formArea2, link} = this.props;
    const baseClass = 'opv-vc';
    return (
      <div>
          <EditableDiv className={`${baseClass}`} html={formArea1} />
          <Editable html={link} className={`${baseClass}__link`} />
          <Editable className={`${baseClass}__content`} html={content} />
          <EditableDiv className={`${baseClass}`} html={formArea2} />
      </div>
    );
  }
}

OpvVerificationCodeForm.propTypes = {
  formArea1: PropTypes.string,
  content : PropTypes.string,
  formArea2: PropTypes.string,
  link : PropTypes.string
};

module.exports = OpvVerificationCodeForm;
