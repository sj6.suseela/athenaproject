
const Theme = {
  gradients: {
    'white': ['#ffffff', '#ffffff'],
    'warm-grey': ['#FCF9F2', '#FCF9F2'],
    'cool-grey': ['#f7f7f7', '#f7f7f7'],
    'mint': ['#61e2d2', '#dcf6ce'],
    'dusty-mint': ['#cbf6ee', '#eef7e8'],
    'pistachio': ['#cad443', '#f4f7da'],
    'dusty-pistachio': ['#f5f7c8', '#f9fae6'],
    'apricot': ['#fcce9a', '#fef9e2'],
    'light-apricot': ['#ffe7c9', '#fdf6e9'],
    'light-meadow': ['#d6ffd5', '#fdffda'],
    'light-bluebell': ['#e4f7ff', '#fffcf4'],
    'strawberry': ['#ef4c51', '#f59372'],
    'blackcurrant': ['#441765', '#8d5ca0'],
    'teal': ['#2f5b87', '#1488a4'],
    'clover': ['#2a8f27', '#a0d007']
  },
  getTheme (name) {
    if(this.gradients[name]) {
      return this.gradients[name];
    }

    return this.gradients.white;
  }
};

export default Theme;
