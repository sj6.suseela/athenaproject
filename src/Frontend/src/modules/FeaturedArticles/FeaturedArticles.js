import React from 'react';
import PropTypes from 'prop-types';

import './FeaturedArticles.scss';

import ArticlePreview from '../ArticlePreview/ArticlePreview';
import SectionTitle from '../Section/SectionTitle';

/** FeaturedArticles
  * @description description
  * @example

  var item = {
    title : "Lorem ipsum lorem ipsum lorem ipsum lorem ...",
    category : 'Category',
    link: {
      html : "#/article-1/",
      rel:" no-follow",
    },
    readTime : "5 min read",
    image : '<img src="http://placehold.it/600x400" alt="" />',
    date : { day:12, month:{ label:"December", number:12, }, year:2017, time:"12:00" },
  };

  ReactDOM.render(React.createElement(Components.FeaturedArticles, {
    id : 'id',
    theme : {},
    heading : "Take a look at our featured articles",
    icon : '<img src="dist/images/icon/icon-book.svg">',
    items : [item, item]
  }), document.getElementById("element"));
*/
class FeaturedArticles extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
    this.items = props.items.map((item,i) => {
      return (<div key={i}><ArticlePreview {...item} /></div>);
    });
  }

  render () {
    const baseClass = 'featuredArticles';

    return (
      <section className={`module ${baseClass}`}>
        <div className="container">
          <SectionTitle {...this.props} />
        </div>
        <div className="{`${baseClass}__container`} container">
          <div className={`${baseClass}__items`}>
            {this.items}
          </div>
        </div>
      </section>
    );
  }
}

FeaturedArticles.defaultProps = {
  items : [],
  heading : ''
};

FeaturedArticles.propTypes = {
  items:PropTypes.array,
  heading:PropTypes.string
};

module.exports = FeaturedArticles;
