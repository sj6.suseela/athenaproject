
import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import ModuleHeader from '../_Product/ModuleHeader/ModuleHeader';
import CaseStudyPreview from './CaseStudyPreview';
import Carousel from '../Carousel/Carousel';


import './CaseStudyCarousel.scss';

/** CaseStudyCarousel
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.CaseStudyCarousel, {
    id : 'id',
    theme: {
      themeName: 'light-bluebell',
      topCurve:
      {
        type: 'convex'
      },
      bottomCurve:
      {
        type: 'convex'
      }
    },
    label: 'Our case studies',
      heading: 'Lorem ipsum dolor sit amet, adipiscing elit',
      items: [
        {
          image: {
            html:`<img src="http://placehold.it/385x255" />`
          },
          title: `<a href="#">Matt's Story</a>`,
          content: `Matt wanted to use his recent inher Read how LV helped him make the right invest choices.`,
          cta: `<a href="#">Read more about our advice service</a>`,
        },
        {
          image: {
            html:`<img src="http://placehold.it/385x255" />`
          }
          title: `<a href="#">Matt's Story</a>`,
          content: `Matt wanted to use his recent inheritance to plan for the children's future education. Read how LV helped him make the right invest choices.`,
          cta: `<a href="#">Read more about our advice service</a>`,
        },
        {
          image: {
            html:`<img src="http://placehold.it/385x255" />`
          }
          title: `<a href="#">Matt's Story</a>`,
          content: `Matt wanted to use his recent inheritance to plan for the children's future education. Read how LV helped `,
          cta: `<a href="#">Read more about our advice service</a>`,
        },
        {
          image: {
            html:`<img src="http://placehold.it/385x255" />`
          }
          title: `<a href="#">Matt's Story</a>`,
          content: `Matt wanted to use his recent inheritance to plan for the children's future education. Read how LV helped him make the right invest choices.`,
          cta: `<a href="#">Read more about our advice service</a>`,
        },
        {
          image: {
            html:`<img src="http://placehold.it/385x255" />`
          }
          title: `<a href="#">Matt's Story</a>`,
          content: `Matt wanted to use his recent inheritance to plan for the children's future education. Read how LV helped him make the right invest choices.`,
          cta: `<a href="#">Read more about our advice service</a>`,
        }
      ]
  }), document.getElementById("element"));
*/
class CaseStudyCarousel extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    // get the prop settings
    const {id, label, heading, items} = this.props;

    

    const baseClass = 'casestudy-carousel';

    const articles = items.map((item,i) => {
      return (<CaseStudyPreview key={i} {...item} />);
    });

    const options = {
      cellAlign: 'left',
      prevNextButtons: false,
      pageDots: false,
      groupCells: true,
      wrapAround: true,
      imagesLoaded: true
    };

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <ModuleHeader headline={heading} label={label} align="center" />
        <div className="carousel carousel--3up">
          <Carousel items={articles} options={options} id={id} hasCustomControls/>
        </div>
      </ModuleWrapper>
    );
  }
}

CaseStudyCarousel.defaultProps = {
  id: '',
  label: '',
  heading: '',
  items: []
};

CaseStudyCarousel.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  heading: PropTypes.string,
  items: PropTypes.array
};

module.exports = CaseStudyCarousel;
