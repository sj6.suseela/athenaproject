
import React from 'react';
import PropTypes from 'prop-types';

import EditableImage from '../Editable/EditableImage';
import EditableParagraph from '../Editable/EditableParagraph';
import Editable from '../Editable/Editable';
import ArrowLink from '../ArrowLink/ArrowLink';
import './CaseStudyPreview.scss';


import SitecoreSettings from '../SitecoreSettings';

/**
 * CaseStudyPreview Module

  <div id="m-CaseStudyPreview"></div>
  <script>
    ReactDOM.render(React.createElement(Components.CaseStudyPreview, {
      theme: {
        themeName: 'white',
        themeBefore: 'warm-grey',
        themeAfter: 'warm-grey',
        topCurve:
        {
          type: 'convex'
        },
        bottomCurve:
        {
          type: 'convex'
        },
      }
    }), document.getElementById("m-CaseStudyPreview"));
  </script>
 */
class CaseStudyPreview extends React.Component {
  constructor (props) {
    super(props);

    let isEditing = false;
    if(!SERVER) {
      isEditing = new SitecoreSettings().isExperienceEditor();
    }

    this.state = {
      isEditing
    };
  }

  render () {
    // get the prop settings
    const {image, title, content, cta} = this.props;

    const isEditing = this.state.isEditing;

    const baseClass = 'casestudy-preview';

    return (
      <article className={`${baseClass}`}>
        <figure className={`${baseClass}__image`}>
          <EditableImage image={image.html} isNotAsync={!!isEditing} src={image.src} imageSize={image.size} imageExtension={image.extension}/>
        </figure>
        <div className={`${baseClass}__body`}>
          <Editable tag="h4" className={`${baseClass}__title`} html={title} />
          <EditableParagraph html={content} />
          <ArrowLink link={cta} />
        </div>
      </article>
    );
  }
}

CaseStudyPreview.defaultProps = {
  image: {},
  title: '',
  content: '',
  cta: ''
};

CaseStudyPreview.propTypes = {
  image: PropTypes.object,
  title: PropTypes.string,
  content: PropTypes.string,
  cta: PropTypes.string
};

module.exports = CaseStudyPreview;
