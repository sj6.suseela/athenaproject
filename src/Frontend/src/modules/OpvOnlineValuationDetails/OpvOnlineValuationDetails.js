import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvOnlineValuationDetails.scss';

class OpvOnlineValuationDetails extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      openCollapseOne: true,
      openCollapseTwo: false,
      borderValuetab1 : '2px solid #008000',
      borderValuetab2 : '2px solid #ded8d8e6',
      tableData : this.props.breakDown
    };

    console.log("tableData::::", this.state.tableData)

    this._clickTitle1 = this._clickTitle1.bind(this);
    this._clickBtn1 = this._clickBtn1.bind(this);
    this._clickClose1 = this._clickClose1.bind(this);
    // this._clickTitle2 = this._clickTitle2.bind(this);
    // this._clickBtn2 = this._clickBtn2.bind(this);
    // this._clickClose2 = this._clickClose2.bind(this);
  }

  _clickTitle1 () {
    this.setState ({
      openCollapseOne:true,
      borderValuetab1 : '2px solid #008000'
    });
  }

  _clickBtn1 () {
    let newColor = this.state.borderValuetab1 == '2px solid #008000'?'2px solid #ded8d8e6':'2px solid #008000';

    this.setState ({
      openCollapseOne:!this.state.openCollapseOne,
      borderValuetab1 : newColor
    });
  }

  _clickClose1 () {
    let newColor = this.state.borderValuetab1 == '2px solid #008000'?'2px solid #ded8d8e6':'2px solid #008000';

    this.setState ({
      openCollapseOne:!this.state.openCollapseOne,
      borderValuetab1 : newColor
    });
  }

  // _clickTitle2 () {
  //   this.setState ({
  //     openCollapseTwo:true,
  //     borderValuetab2 : '2px solid #008000'
  //   });
  // }

  // _clickBtn2 () {
  //   let newColor = this.state.borderValuetab2 == '2px solid #008000'?'2px solid #ded8d8e6':'2px solid #008000';

  //   this.setState ({
  //     openCollapseTwo:!this.state.openCollapseTwo,
  //     borderValuetab2 : newColor
  //   });
  // }

  // _clickClose2 () {
  //   let newColor = this.state.borderValuetab2 == '2px solid #008000'?'2px solid #ded8d8e6':'2px solid #008000';

  //   this.setState ({
  //     openCollapseTwo:!this.state.openCollapseTwo,
  //     borderValuetab2 : newColor
  //   });
  // }

  renderTableData () {
    return this.state.tableData.map((data,index) => {
      const {investment,numberOfUnits,currentValue} = data;
      return(
        <tr key={investment}>
          <td>{investment}</td>
          <td>{numberOfUnits}</td>
          <td>{currentValue}</td>
        </tr>
      )
    });
  }

  render () {
    const {title,backButtonLink, personalDetailsTitle, policyNumberLabel, personNameLabel, personAddressLabel, policyNumber, personName, personAddress, investmentTitle, investmentTotalValue, investmentTotalValueAmount, subInvestmentTitle,  valuationDateTitle, valuationDate, currentValueTitle, currentValue, breakdownTitle, breakdownMessage, subInvestmentTitle1, valuationDate1, currentValue1, breakdownTitle1, breakDown, breakdownMessageInfo, subInvestmentTitle2, valuationDate2, currentValue2, subInvestmentTitle3, valuationDate3, currentValue3, consentMessage, isbreakDownAvailable} = this.props;
    const baseClass = 'OpvOnlineValuationDetails';

    let displayContent;
    if (this.props.isbreakDownAvailable) {
      displayContent = <div>
      <table id="investment-table">
        <tbody>
          <tr>
            <th>Investment(s)</th>
            <th>Number of Units</th>
            <th>Current Value (£)</th>
          </tr>
          {this.renderTableData()}
        </tbody>
      </table>
      <Editable className={`${baseClass}__breakdownMessageInfo`} html={breakdownMessageInfo} />
    </div>
    }
    else {
      displayContent = <Editable className={`${baseClass}__breakdownMessage`} html={breakdownMessage} />
    }

    return (
        <div>
            {/* <Editable tag="h2" className={`${baseClass}__title`} html={title} /> */}
            {/* <div className="right-link">
                <Editable className={`${baseClass}__logoutLink`} html={logoutLink.html} />
                <Editable className={`${baseClass}__settingsLink`} html={settingsLink.html} />
                <EditableDiv className={`${baseClass}__backButtonLink`} html={backButtonLink.html} />
            </div> */}
            <EditableDiv className={`${baseClass}__backLink`} html={backButtonLink.html} />
            <Editable tag="h2" className={`${baseClass}__title`} html={title} />
            <EditableDiv className={`${baseClass}__personalDetailsTitle`} html={personalDetailsTitle} />
            <div className="content-div">
              <div className="personalDetailLabels">
                <Editable className={`${baseClass}__policyNumberLabel`} html={policyNumberLabel}/>
                <Editable className={`${baseClass}__personNameLabel`} html={personNameLabel}/>
                <Editable className={`${baseClass}__personAddressLabel`} html={personAddressLabel}/>
              </div>
              <div className="personalDetailValues">
                <Editable className={`${baseClass}__policyNumber`} html={policyNumber}/>
                <Editable className={`${baseClass}__personName`} html={personName}/>
                <Editable className={`${baseClass}__personAddress`} html={personAddress}/>
              </div>
            </div>
            <EditableDiv className={`${baseClass}__investmentTitle`} html={investmentTitle} />
            <div>
                <EditableDiv className={`${baseClass}__investmentTotalValue`} html={investmentTotalValue}/>
                <EditableDiv className={`${baseClass}__investmentTotalValueAmount`} html={investmentTotalValueAmount}/>
            </div>
            <div className="collapse-component">

              {/* 1st collapse Component */}
              <div className="collapseComponent" style={{border: this.state.borderValuetab1}}>
                <div >
                  <div onClick = {this._clickTitle1}>
                    <Editable tag="h2" className={`${baseClass}__subInvestmentTitle`} html={subInvestmentTitle} />
                  </div>
                  <div onClick = {this._clickBtn1}>
                    <Editable className={`${baseClass}__plus`} html={this.state.openCollapseOne? '-': '+'} />
                  </div>
                  <div className="subdata">
                    <EditableDiv className={`${baseClass}__valuationDateTitle`} html={valuationDateTitle} />
                    <EditableDiv className={`${baseClass}__valuationDate`} html={valuationDate} />
                    <EditableDiv className={`${baseClass}__currentValueTitle`} html={currentValueTitle} />
                    <EditableDiv className={`${baseClass}__currentValue`} html={currentValue} />
                  </div>
                </div>

                <div className={this.state.openCollapseOne? "panel-collapse": "panel-collapse panel-close"}>
                  <Editable tag="h2" className={`${baseClass}__breakdownTitle`} html={breakdownTitle} />
                  {displayContent}
                  <div onClick = {this._clickClose1}>
                    <div className={`${baseClass}__close`}>
                      close
                    </div>
                  </div>
                </div>
              </div>

              {/* 2nd collapse Component */}
              {/* <div className="collapseComponent" style={{border: this.state.borderValuetab2}}>
                <div>
                  <div onClick = {this._clickTitle2}>
                    <Editable tag="h2" className={`${baseClass}__subInvestmentTitle1`} html={subInvestmentTitle1} />
                  </div>
                  <div onClick = {this._clickBtn2}>
                    <Editable className={`${baseClass}__plus`} html={this.state.openCollapseTwo? minus: plus} />
                  </div>
                  <div className="subdata">
                    <EditableDiv className={`${baseClass}__valuationDateTitle`} html={valuationDateTitle} />
                    <EditableDiv className={`${baseClass}__valuationDate1`} html={valuationDate1} />
                    <EditableDiv className={`${baseClass}__currentValueTitle`} html={currentValueTitle} />
                    <EditableDiv className={`${baseClass}__currentValue1`} html={currentValue1} />
                  </div>
                </div>

                <div className={this.state.openCollapseTwo? "panel-collapse": "panel-collapse panel-close"}>
                  <Editable tag="h2" className={`${baseClass}__breakdownTitle1`} html={breakdownTitle1} />

                  <div>
                    <table id="investment-table">
                      <tbody>
                        <tr>
                          <th>Investment(s)</th>
                          <th>Number of Units</th>
                          <th>Current Value (£)</th>
                        </tr>
                        {this.renderTableData()}
                      </tbody>
                    </table>
                  </div>

                  <Editable className={`${baseClass}__breakdownMessage1`} html={breakdownMessage1} />
                  <div onClick = {this._clickClose2}>
                    <Editable className={`${baseClass}__close`} html={close} />
                  </div>
                </div>
              </div> */}

              {/* 3rd collapse Component */}
              {/* <div className="collapseComponent">
                <div >
                  <div>
                    <Editable tag="h2" className={`${baseClass}__subInvestmentTitle2`} html={subInvestmentTitle2} />
                  </div>
                  <div>
                    <Editable className={`${baseClass}__plus`} html={plus} />
                  </div>
                  <div className="subdata">
                    <EditableDiv className={`${baseClass}__valuationDateTitle`} html={valuationDateTitle} />
                    <EditableDiv className={`${baseClass}__valuationDate2`} html={valuationDate2} />
                    <EditableDiv className={`${baseClass}__currentValueTitle`} html={currentValueTitle} />
                    <EditableDiv className={`${baseClass}__currentValue2`} html={currentValue2} />
                  </div>
                </div>
              </div> */}

              {/* 4th collapse Component */}
              {/* <div className="collapseComponent">
                <div >
                  <div>
                    <Editable tag="h2" className={`${baseClass}__subInvestmentTitle3`} html={subInvestmentTitle3} />
                  </div>
                  <div>
                    <Editable className={`${baseClass}__plus`} html={plus} />
                  </div>
                  <div className="subdata">
                    <EditableDiv className={`${baseClass}__valuationDateTitle`} html={valuationDateTitle} />
                    <EditableDiv className={`${baseClass}__valuationDate3`} html={valuationDate3} />
                    <EditableDiv className={`${baseClass}__currentValueTitle`} html={currentValueTitle} />
                    <EditableDiv className={`${baseClass}__currentValue3`} html={currentValue3} />
                  </div>
                </div>
              </div> */}

            </div>

            <EditableDiv className={`${baseClass}__consentMessage`} html={consentMessage} />
        </div>
    );
  }
}

OpvOnlineValuationDetails.propTypes = {
  title: PropTypes.string,
  backButtonLink : PropTypes.object,
  personalDetailsTitle : PropTypes.string,

  // personDetails : PropTypes.string,
  // personDetailsValues : PropTypes.string,
  policyNumberLabel : PropTypes.string,
  personNameLabel : PropTypes.string,
  personAddressLabel : PropTypes.string,
  policyNumber : PropTypes.string,
  personName : PropTypes.string,
  personAddress : PropTypes.string,
  investmentTitle : PropTypes.string,
  investmentTotalValue : PropTypes.string,
  investmentTotalValueAmount: PropTypes.string,
  subInvestmentTitle : PropTypes.string,

  valuationDateTitle : PropTypes.string,
  valuationDate : PropTypes.string,
  currentValueTitle : PropTypes.string,
  currentValue : PropTypes.string,
  breakdownTitle : PropTypes.string,
  breakdownMessage : PropTypes.string,

  // subInvestmentTitle1 : PropTypes.string,
  // valuationDate1 : PropTypes.string,
  // currentValue1 : PropTypes.string,
  // breakdownTitle1 : PropTypes.string,
  breakDown : PropTypes.array,
  breakdownMessageInfo : PropTypes.string,
  // subInvestmentTitle2 : PropTypes.string,
  // valuationDate2 : PropTypes.string,
  // currentValue2 : PropTypes.string,
  // subInvestmentTitle3 : PropTypes.string,
  // valuationDate3 : PropTypes.string,
  // currentValue3 : PropTypes.string,
  consentMessage : PropTypes.string,
  isbreakDownAvailable : PropTypes.bool
};

module.exports = OpvOnlineValuationDetails;
