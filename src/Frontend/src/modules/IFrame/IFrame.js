import React from 'react';
import PropTypes from 'prop-types';
import SitecoreSettings from '../SitecoreSettings';

import './IFrame.scss';

/** IFrame
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.IFrame, {
    id : 'id',
    src : 'https://www.youtube.com/embed/wrVmiZB06wM',
    width : '800',
    height : '600',
    isCentered : true
  }), document.getElementById("element"));
*/
class IFrame extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isPreview:false,
      isEditing:false
    };
  }

  componentDidMount () {
    const sc = new SitecoreSettings();
    if(this.state.isPreview !== sc.isPreview()) {
      this.setState({
        isPreview:sc.isPreview()
      });
    }
    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  render () {
    const {id,width,height,src,isCentered,themeName} = this.props;

    let cssClass = 'iframe';

    if (themeName) {
      cssClass += ` theme-${themeName}`;
    }
    if(isCentered) {
      cssClass += ' iframe--alignCenter';
    }

    if(this.state.isPreview) {
      cssClass += ' isPreview';
    }
    if(this.state.isEditing) {
      cssClass += ' isEditing';
    }

    const style = {
      width:'100%',
      maxWidth:`${width}px`
    };

    return (
    <iframe id={id} className={cssClass} src={src} height={height} style={style}></iframe>
    );
  }
}

IFrame.propTypes = {
  id:PropTypes.string,
  width:PropTypes.string,
  height:PropTypes.string,
  src:PropTypes.string,
  isCentered:PropTypes.bool,
  themeName:PropTypes.string
};

module.exports = IFrame;
