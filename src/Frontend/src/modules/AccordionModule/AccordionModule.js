import React from 'react';
import PropTypes from 'prop-types';

import Accordion from '../Accordion/Accordion';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import Editable from '../Editable/Editable';
import EditableParagraph from '../Editable/EditableParagraph';

import './AccordionModule.scss';


class AccordionModule extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {items, heading, intro} = this.props;
    const baseClass = 'product-accordion';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div className={`${baseClass}__main`}>
            {(heading || intro) && <div className={`${baseClass}__heading-wrapper`}>
                <Editable html={heading} tag="h2" className={`${baseClass}__heading h2`} />
                <EditableParagraph html={intro} tag="div" className={`${baseClass}__intro lead`} />
            </div>}
            <Accordion closeSiblings={false} items={items} modifier="" />
        </div>
      </ModuleWrapper>
    );
  }
}

AccordionModule.defaultProps = {
  theme:{},
  items:[],
  heading: '',
  intro: ''
};

AccordionModule.propTypes = {
  id:PropTypes.string,
  theme:PropTypes.object,
  items:PropTypes.object,
  heading:PropTypes.string,
  intro:PropTypes.string
};

module.exports = AccordionModule;
