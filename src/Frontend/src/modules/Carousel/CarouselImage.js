import React from 'react';
import PropTypes from 'prop-types';

import Carousel from './Carousel';

import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';
import SitecoreSettings from '../SitecoreSettings';
import Module from '../Module';
import EditButton from '../CMS/EditButton';

/** CarouselImage
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.CarouselImage, {
    id :'CarouselImage',
    width :'full',
    items :[
      {
        image : '<img src="https://placeimg.com/1440/751/animals" width="1440" height="751" />',
        caption : "legend 1 goes here with a really really really really really really really long legend"
      },
      {
        image : '<img src="https://placeimg.com/1440/751/nature" width="1440" height="751" />',
        caption : "legend 2 goes here"
      },
      {
        image : '<img src="https://placeimg.com/1440/751/people" width="1440" height="751" />',
        caption : "legend 3 goes here"
      },
      {
        image : '<img src="https://placeimg.com/1440/751/technology" width="1440" height="751" />',
        caption : "legend 4 goes here"
      }
    ]
  }), document.getElementById("element"));
*/
class CarouselImage extends React.Component {
  constructor (props) {
    super(props);

    let isEditing = false;
    if(!SERVER) {
      isEditing = new SitecoreSettings().isExperienceEditor();
    }

    this.state = {
      caption:(this.props.items && this.props.items.length > 0) ? this.props.items[0].caption : '',
      selected:0,
      isEditing
    };

    this._onChange = this._onChange.bind(this);
    this._onReplace = this._onReplace.bind(this);
    this.timer = null;
  }

  componentDidMount () {
    Module.register(this.ref);
  }


  _onChange (index) {
    const _this = this;
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      _this.setState({
        selected:index,
        caption:_this.props.items[index].caption
      });
    },1);
  }

  _onReplace () {
    const _this = this;
    if(_this.carousel && _this.carousel.resize) {
      setTimeout(() => {
        _this.carousel.resize();
      },1000);
    }
  }

  render () {
    const isEditing = this.state.isEditing;

    const captionClass = 'caption carousel__caption';

    this.items = this.props.items.map((item,i) => {
      return (
        <div key={item.caption + i}>
          {item.editButton && (
            <EditButton html={item.editButton} isSmall={false} align="right" />
          )}
          {i === 0 ? (
            <EditableImage image={item.image.html} src={item.image.src} isCarousel={false} imageSize={item.image.size} onReplace={this._onReplace} isNotAsync={!!isEditing}/>
          ) : (
            <EditableImage image={item.image.html} src={item.image.src} isCarousel={true} isNotAsync={!!isEditing}/>
          )}
          
          {isEditing && (
            <p className={`${captionClass} editing`}><Editable html={item.caption} /></p>
          )}

        </div>
      );
    });

    const options = {
      prevNextButtons: false,
      pageDots: false,
      lazyLoad: true
    };

    if(SERVER) {
      options.lazyLoad = false;
    }

    return (
      <div className="module carousel carousel--image" ref={(c) => {this.ref = c;}}>
        <div className="wrapper">
          <Carousel ref={(c) => {this.carousel = c;}} {...this.props} options={options} items={this.items} onChange={this._onChange} hasCustomControls controlsAlign="right"/>
          {!isEditing && (
            <p className={captionClass}><Editable html={this.state.caption} /></p>
          )}
        </div>
      </div>
    );
  }
}

CarouselImage.defaultProps = {
  items: []
};

CarouselImage.propTypes = {
  items: PropTypes.array
};

module.exports = CarouselImage;
