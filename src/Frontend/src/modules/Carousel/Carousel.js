import React from 'react';
import PropTypes from 'prop-types';

import SitecoreSettings from '../SitecoreSettings';

import './Flickity.scss';
import './Carousel.scss';

let Flickity = null;

if(!SERVER) {
  Flickity = require('flickity-imagesloaded');

  Flickity.prototype._createResizeClass = function () {
    const _this = this;
    setTimeout(() => {
      _this.element.classList.add('flickity-resize');
    },500);
  };

  Flickity.createMethods.push('_createResizeClass');

  const resize = Flickity.prototype.resize;
  Flickity.prototype.resize = function () {
    this.element.classList.remove('flickity-resize');
    resize.call(this);
    this.element.classList.add('flickity-resize');
  };
}

/** Carousel
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Carousel, {
    id : 'id',
    theme : {}
  }), document.getElementById("element"));
*/
class Carousel extends React.Component {
  constructor (props) {
    super(props);

    let isEditing = false;
    if(!SERVER) {
      isEditing = new SitecoreSettings().isExperienceEditor();
    }

    this.state = {
      dots:0,
      currentSlide:0,
      isEditing
    };

    const {id} = props;

    this.id = id;

    this._initCarousel = this._initCarousel.bind(this);
    this._prevSlide = this._prevSlide.bind(this);
    this._nextSlide = this._nextSlide.bind(this);
    this._goToSlide = this._goToSlide.bind(this);
    this.resize = this.resize.bind(this);
  }

  append (items) {
    const newitems = [];
    for(const item of items) {
      const slide = document.createElement('div');
      slide.className = 'slider__cell';
      slide.innerHTML = `<div>${item}</div>`;
      newitems.push(slide);
    }

    this.flkty.append(newitems);
  }


  resize () {
    this.flkty.resize();
  }

  componentDidMount () {
    this._initCarousel();
  }

  _initCarousel () {
    const {onChange, options,items,isLazyLoad, noMobileCarousel} = this.props;

    if(noMobileCarousel) {
      return false;
    }

    if(items.length < 2) {
      return false;
    }

    const settings = Object.assign({}, {
      imagesLoaded: true,
      wrapAround: true,
      resize: true,
      watchCSS: true,
      cellSelector: '.slider__cell'
      /* on: {
        ready: function() {
          if(_this.state.isEditing){
            let heightChecker = setInterval(function(){
              let height = _this.flkty.viewport.clientHeight;
              if(height===0){
                if(_this.flkty){
                  _this.flkty.resize();
                }
              }
              else{
                clearInterval(heightChecker);
              }
            },500)
          }
        }
      }*/
    }, options);

    if(!SERVER) {
      const flkty = new Flickity(`#${this.id}`, settings);
      this.flkty = flkty;

      // bind events
      flkty.on('settle', () => {
        if(isLazyLoad && onChange) {
          onChange(flkty.selectedIndex,flkty.cells.length) ;
        }
      });
      
      flkty.on('select', () => {
        if(!isLazyLoad && onChange) {
          onChange(flkty.selectedIndex,flkty.cells.length) ;
        }
        this.setState({
          currentSlide:this.flkty.selectedIndex
        });
      });

      if(typeof window === 'object') {
        window.addEventListener('resize', this._handleResize.bind(this));
      }

      if(flkty.slides) {
        this.setState({
          dots:flkty.slides.length
        });
      }

      this._handleResize();
    }
  }

  _destroyCarousel () {
    this.flkty.destroy();
  }

  disableDrag () {
    this.flkty.unbindDrag();
  }

  enableDrag () {
    this.flkty.bindDrag();
  }

  _prevSlide () {
    this.flkty.previous();
  }

  _nextSlide () {
    this.flkty.next();
  }

  _goToSlide (index) {
    this.flkty.select(index);
  }

  _handleResize () {
    const self = this;

    setTimeout(() => {
      // if the lides length is 1 (nothing to slide) disable dragging
      if(self.flkty.slides && self.flkty.slides.length === 1) {
        self.flkty.unbindDrag();
      }
      else {
        self.flkty.bindDrag();
      }
    }, 2);
    if(this.flkty.slides) {
      setTimeout(() => {
        self.setState({
          dots:this.flkty.slides.length
        });
      },300);
    }
  }

  render () {
    const {width, classes,hasCustomControls,controlsAlign,itemsPerRow,items} = this.props;
    let cssClass = `slider slider--${width}`;

    if(classes) {
      cssClass += ` ${classes}`;
    }



    this.items = [];
    if(this.items) {
      const itemCount = this.props.items.length;
      const rowCount = Math.ceil(itemCount / itemsPerRow);

      this.items = items.map((item,i) => {
        const isLast = (i >= (rowCount - 1) * itemsPerRow);
        const isLastClass = isLast ? 'last' : '';

        return (<div className={`slider__cell ${isLastClass}`} key={item + i}>{item}</div>);
      });
    }

    const dots = [];
    for(let i = 0;i < this.state.dots;i++) {
      const selected = this.state.currentSlide === i ? 'selected' : '';
      dots.push(<li key={i} className={`slider__controls__paging ${selected}`} onClick={() => this._goToSlide(i)}>*</li>);
    }

    const controlsAlignCss = controlsAlign ? `slider__controls--${controlsAlign}` : '';
    return (
      <div>
        <div id={this.id} className={cssClass}>
          {this.items}
        </div>
        {hasCustomControls && (
          <div className={`slider__controls ${controlsAlignCss}`}>
            <ul>
              <li className="slider__controls__prev" onClick={this._prevSlide}>Prev</li>
              {dots}
              <li className="slider__controls__next" onClick={this._nextSlide}>Next</li>
              </ul>
          </div>
        )}
      </div>
    );
  }
}

Carousel.defaultProps = {
  items: [],
  id: null,
  options: {},
  onChange: null,
  width: '',
  classes: '',
  isLazyLoad:false,
  controlsAlign:'center',
  itemsPerRow:0
};

Carousel.propTypes = {
  items: PropTypes.array,
  id: PropTypes.string,
  options: PropTypes.object,
  onChange: PropTypes.func,
  width: PropTypes.string,
  classes: PropTypes.string,
  isLazyLoad:PropTypes.bool,
  hasCustomControls:PropTypes.bool,
  controlsAlign:PropTypes.string,
  itemsPerRow:PropTypes.number,
  noMobileCarousel:PropTypes.bool
};

module.exports = Carousel;
