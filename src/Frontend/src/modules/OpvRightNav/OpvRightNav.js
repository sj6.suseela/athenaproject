import React from 'react';
import PropTypes from 'prop-types';
import './OpvRightNav.scss';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';

class OpvRightNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      clicked: false
    };
    this._click = this._click.bind(this);
  }

  _click (e) {
    e.preventDefault();
    this.state.clicked ? this.setState({clicked: false}) : this.setState({clicked: true});
  }
  render () {
    const {title, content, cta, faqContent, faqTxt} = this.props;
    const baseClass = 'opv-right-nav';
    const className = this.state.clicked ? 'on_faq' : 'off_faq';

    return (
      <div className={`${baseClass}`}>
        <div className={`${baseClass}__main`}>
        <Editable tag="h2" className={`${baseClass}__title`} html={title} />
          <EditableDiv
            className={`${baseClass}__copy`}
            html={content}
          />
          <div className={`${baseClass}__footer`}>
            <div className="cta cta--secondary cta--sm cta--wide cta-faq" onClick={this._click} id="faq-note">
              <Editable html={cta} />
              <Editable html={faqTxt} className={`${baseClass}__faqTxt`}/>
            </div>

            <div className="faqNoteLink">
              <div id="tooltip_faq" className={`${className} top_faq`}>
                  <div className="tooltip-inner_faq">
                      <span className="close-cta" onClick={this._click}>X</span>
                      <EditableDiv className="faqNoteContent" html={faqContent} />
                  </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}


OpvRightNav.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
  cta: PropTypes.string,
  faqTxt: PropTypes.string,
  faqContent: PropTypes.object
};

module.exports = OpvRightNav;
