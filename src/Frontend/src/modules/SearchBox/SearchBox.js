import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import Cta from '../Cta/Cta';
import SitecoreSettings from '../SitecoreSettings';


class SearchBox extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      inputValue:props.searchTerm,
      isEditing:false
    };

    this._onChange = this._onChange.bind(this);
  }

  componentDidMount () {
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  _onChange (value) {
    this.setState({
      inputValue: value
    });
  }

  render () {
    // get the prop settings
    const {placeholderText,searchLabel,searchFormAction, otherBusinessLink} = this.props;

    const isEditing = this.state.isEditing;

    return (
      <div className="searchBox">
          <form method="GET" action={searchFormAction}>

            {isEditing ?
              <Editable className="editable-input" html={placeholderText} />
            :
              <input type="text" name="term" placeholder={placeholderText} value={this.state.inputValue} id="searchText" onChange={e => this._onChange(e.target.value)}/>
            }


            {isEditing ?
              <Cta cssclass="editable-inputBtn" rawCta={searchLabel} />
            :
              <Cta type="primary" rawCta={`<input type ="submit" value=${searchLabel} />`} cssclass="cta--isInput"/>
            }

          </form>

          {otherBusinessLink && (
            <Editable className='searchBox__link links' html={otherBusinessLink.html} />
          )}
      </div>
    );
  }
}

SearchBox.defaultProps = {
  searchLabel:''
};

SearchBox.propTypes = {
  placeholderText:PropTypes.string,
  searchTerm:PropTypes.string,
  searchLabel:PropTypes.string,
  searchFormAction:PropTypes.string,
  searchResultsCount:PropTypes.string,
  searchResultsText:PropTypes.string,
  otherBusinessLink: PropTypes.object
};

module.exports = SearchBox;
