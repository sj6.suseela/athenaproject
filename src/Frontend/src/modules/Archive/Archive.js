
import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import Editable from '../Editable/Editable';
import ArticlePreview from '../ArticlePreview/ArticlePreview';

import './Archive.scss';

/** Archive
  * @description description
  * @Archive

ReactDOM.render(React.createElement(Components.Archive, {
  id   : 'archive',
  theme : {
    themeName :'cool-grey'
  },
  items : [
    {
      title : "January 2018",
      items : [
        {
          title : "LV and Allianz announce strategic partnership business transfer plans",
          content : "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove",
              category : 'Business area',
              link: {
                href: "#/article-1/",
                rel:'nofollow',
                id: 'alkdj'
              },
              image: {
                html:'<img src="dist/images/placeholder/article-1.jpg" alt="" />',
                size:{width:384,height:240}
              },
              date : { day:31, month:{ label:"January", number:12, }, year:2018, time:"12:00" },
        },
        {
          title : "Driving demands prompt second-car surge",
          content : "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. ",
              category : 'Business area',
              link: {
                rel:'noreferrer',
                href : "#/article-1/",
              },
              image: {
                html:'<img src="dist/images/placeholder/article-1.jpg" alt="" />',
                size:{width:384,height:240}
              },
              date : { day:27, month:{ label:"January", number:12, }, year:2018, time:"12:00" },
        },
        {
          title : "Millions of ‘DINOs’ ill-prepared for financial shocks",
          content : "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove",
              category : 'Business area',
              link: {
                href:'#/article-1'
              },
              image: {
                html:'<img src="dist/images/placeholder/article-1.jpg" alt="" />',
                size:{width:384,height:240}
              },
              date : { day:15, month:{ label:"January", number:12, }, year:2018, time:"12:00" },
        },
        {
          title : "Millions of ‘DINOs’ ill-prepared for financial shocks",
          content : "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove",
              category : 'Business area',
              link: {href : "#/article-1/"},
              date : { day:15, month:{ label:"January", number:12, }, year:2018, time:"12:00" },
        }
      ]
    },
    {
      title : "January 2018",
      items : [
        {
          title : "LV and Allianz announce strategic partnership business transfer plans",
          content : "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove",
              category : 'Business area',
              link:{href : "#/article-1/"},
              image: {
                html:'<img src="dist/images/placeholder/article-1.jpg" alt="" />',
                size:{width:384,height:240}
              },
              date : { day:21, month:{ label:"December", number:12, }, year:2018, time:"12:00" },
        },
        {
          title : "Driving demands prompt second-car surge",
          content : "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. ",
              category : 'Business area',
              link:{href : "#/article-1/"},
              image: {
                html:'<img src="dist/images/placeholder/article-1.jpg" alt="" />',
                size:{width:384,height:240}
              },
              date : { day:18, month:{ label:"December", number:12, }, year:2018, time:"12:00" },
        }
      ]
    }
  ]
}), document.getElementById("articles"));
*/
class Archive extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  getListing (group, key, baseClass) {
    const items = group.items.map((item,i) => {
      return (<ArticlePreview key={`article-${i}`} {...item} isAlt={true} />);
    });

    return (
      <div className={`${baseClass}__section`} key={key}>
        <Editable tag="h2" className={`${baseClass}__title`} html={group.title} />
        {items}
      </div>
    );
  }

  render () {
    // get the prop settings
    const {items} = this.props;
    const baseClass = 'archive';

    const results = items.map((item,i) => {
      return this.getListing(item, i, baseClass);
    });

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        {results}
      </ModuleWrapper>
    );
  }
}

Archive.defaultProps = {
  items : []
};

Archive.propTypes = {
  items : PropTypes.array
};

module.exports = Archive;
