import React from 'react';
import PropTypes from 'prop-types';

import './mobilenav.scss';

// import Overlay from '../Overlay/Overlay';
// import Icon from '../Icon/Icon';
// import ContentExpander from '../ContentExpander/ContentExpander';

import NavItem from './NavItem';

class MobileNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isOverlayOpen: false,
      navItemsVisible:false,
      subNavItems:null,
      navItems:props.items,
      showMoreLabel:false,
      moreNavItems:[],
      overlayData:[]
    };

    for(let i = 0;i < this.state.navItems.length;i++) {
      this.state.overlayData[i] = false;
    }

    this.itemsToHide = [];
    this.childPosData = {};

    // this._open = this._open.bind(this);
    // this._close = this._close.bind(this);
    this._openItem = this._openItem.bind(this);
    this._closeItem = this._closeItem.bind(this);
    this._onClickItem = this._onClickItem.bind(this);
    this._updateNav = this._updateNav.bind(this);
    this._onChildMounted = this._onChildMounted.bind(this);
  }

  _updateNav () {
    this.setState({
      moreNavItems:[]
    });

    const _this = this;

    // const {clientHeight, clientWidth} = this.container;
    const parentWidth = this.container.parentNode.clientWidth;

    // let childTotalWidth = 0;
    let moreWidth = 0;

    for(const i in this.childPosData) {
      const child = this.childPosData[i];
      if(!child.isOverflow) {
        // childTotalWidth += (child.width + child.marginRight);
      }
      else{
        moreWidth += (child.width + child.marginRight);
      }
    }

    for(const i in this.childPosData) {
      const child = this.childPosData[i];

      if(!child.isOverflow) {
        if(child.position === Object.keys(this.childPosData).length - 2) {
          moreWidth = 0;
        }

        if(child.pos + child.width + moreWidth > parentWidth) {
          _this.itemsToHide.push(child.label);
        }
        else{
          const index = _this.itemsToHide.indexOf(child.label);
          _this.itemsToHide.splice(index, 1);
        }
      }
    }

    const updatedNavItems = this.state.navItems.map((item) => {
      if(_this.itemsToHide.includes(item.label)) {
        item.visible = false;

        if(item.items) {
          // loop through items and children and drop all into more nav
          for(let i = 0;i < item.items.length;i++) {
            if(item.items[i].children) {
              for(let e = 0;e < item.items[i].children.length;e++) {
                _this.state.moreNavItems.push(item.items[i].children[e]);
              }
            }
          }
        }
        else{
          _this.state.moreNavItems.push(item);
        }

        // }
      }
      else{
        item.visible = true;
      }
      return item;
    });

    const showMore = _this.itemsToHide.length > 0;

    this.setState({
      navItems:updatedNavItems,
      showMoreLabel:showMore
    });
  }

  componentDidMount () {
    const _this = this;
    if(!SERVER) {
      window.addEventListener('resize', this._updateNav);
      setTimeout(() => {
        _this._updateNav();
      },0);
    }
  }

  _onClickItem (data) {
    let isOpenedItem = false;

    // check to see if an overlay is already open
    let hasOpenedItem = false;
    for(let i = 0;i < this.state.overlayData.length;i++) {
      if(this.state.overlayData[i] === true) {
        hasOpenedItem = true;
        if(i ===  data.position) {
          isOpenedItem = true;
        }
      }
    }

    // if opened item then close that first
    if(isOpenedItem) {
      this._closeItem(data.position);
    }
    else if(hasOpenedItem) {
      const newData = this.state.overlayData.map(() => {
        return false;
      });

      this.setState({
        overlayData:newData
      },function () {
        const _this = this;

        setTimeout(() => {
          _this._openItem(data.position);
        },400);
      });
    }
    else{
      this._openItem(data.position);
    }
  }

  _openItem (itemPos) {
    const newData = this.state.overlayData.map((item,i) => {
      if(i === itemPos) {
        return true;
      }
    });

    this.setState({
      overlayData:newData
    });
  }

  _closeItem (itemPos) {
    const newData = this.state.overlayData.map((item,i) => {
      if(i === itemPos) {
        return false;
      }
    });

    this.setState({
      overlayData:newData
    });
  }

  _onChildMounted (data) {
    this.childPosData[data.label] = data;
  }

  render () {
    this.navItems = this.state.navItems.map((item,i) => {
      let itemWrapper = null;
      if(item.items && !item.groupOnMobile) {
        itemWrapper = item.items.map((item) => {
          const label = item.mobileLabel || item.label;
          return <NavItem position={item.position} key={item.label} link={item.link} label={label} onMounted={this._onChildMounted}></NavItem>;
        });
        return itemWrapper;
      }
      else{
        const overlayOpen = this.state.overlayData[i];

        const label = item.mobileLabel || item.label;

        if(item.isOverflow) {
          return <NavItem position={item.position} isOverflow={true} isVisible={this.state.showMoreLabel} key={item.label} link={item.link} label={label} onMounted={this._onChildMounted} onClickItem={this._onClickItem} isOverlayOpen={overlayOpen}>{this.state.moreNavItems}</NavItem>;
        }
        else{
          if(item.visible) {
            const label = item.mobileLabel || item.label;

            if(item.items) {
              return <NavItem position={item.position} key={item.label} link={item.link} label={label} onMounted={this._onChildMounted} onClickItem={this._onClickItem} isOverlayOpen={overlayOpen}>{item.items}</NavItem>;
            }
            else{
              return <NavItem position={item.position} key={item.label} link={item.link} label={label} onMounted={this._onChildMounted} onClickItem={this._onClickItem} isOverlayOpen={overlayOpen}></NavItem>;
            }
          }
        }
      }
    });

    let cssClass = 'nav nav--mobile';

    if(this.props.isInvertedColour) cssClass += ' invertColours';

    return (
        <div className={cssClass} ref={(c) => {  this.container = c;}}>
          <nav>
            <ul>
              {this.navItems}
            </ul>
          </nav>
        </div>
    );
  }
}

MobileNav.propTypes = {
  items: PropTypes.array,
  isInvertedColour:PropTypes.bool
};

module.exports = MobileNav;
