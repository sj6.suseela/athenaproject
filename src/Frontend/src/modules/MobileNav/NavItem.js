import React from 'react';
import PropTypes from 'prop-types';

import './mobilenav.scss';

// import Overlay from '../Overlay/Overlay';
import Icon from '../Icon/Icon';
import ContentExpander from '../ContentExpander/ContentExpander';

import Editable from '../Editable/Editable';
import EditableLink from '../Editable/EditableLink';

class NavItem extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      visible:true
    };

    this._click = this._click.bind(this);
  }

  componentDidMount () {
    const _this = this;
    setTimeout(() => {
      _this.setState({visible:true});
    },this.props.navDelay);

    let marginRight;

    if(!SERVER) {
      marginRight = parseInt(window.getComputedStyle(this.listitem).getPropertyValue('margin-right'));
    }

    this.props.onMounted({
      label:this.props.label,
      pos:this.listitem.offsetLeft,
      width:this.listitem.clientWidth,
      marginRight,
      isOverflow:this.props.isOverflow,
      position:this.props.position
    });
  }

  _click (e) {
    if(this.props.children) {
      e.preventDefault();

      this.props.onClickItem({
        position:this.props.position,
        open:!this.props.isOverlayOpen
      });

      // call parent component
    }
  }

  render () {
    const {link,label, mobileLabel} = this.props;

    const classname = this.props.isOverlayOpen ? 'active' : '';
    const hasChildren = this.props.children;

    const overlayOpen = this.props.isOverlayOpen;

    let style = {
    };

    if(this.props.isVisible === false) {
      style = {
        position:'absolute',
        left:'-2000em'
      };
    }
    else{
      style = {
        left:'auto'
      };
    }

    this.childitems = {};

    if(this.props.children) {
      this.childitems = this.props.children.map((item) => {
        if(!item.children) {
          const theLabel = item.mobileLabel || item.label;

          return <li key={item.label}><EditableLink {...item.link} html={theLabel} /></li>;
        }
        else{
          let navWrapper = null;
          navWrapper = item.children.map((item) => {
            const theLabel = item.mobileLabel || item.label;

            return <li key={item.label}><EditableLink {...item.link} html={theLabel} /></li>;
          });
          return navWrapper;
        }
      });
    }

    const theLabel = mobileLabel || label;

    return (
      <li ref={(c) => {
        this.listitem = c;
      }} style={style} className={classname}><a onClick={this._click} id={link.id} rel={link.rel} href={link.href}>
        <Editable html={theLabel} />
        {hasChildren &&
          <Icon icon="downarrow" fillColor="#fff"/>
        }
        </a>
        {hasChildren && (
          <ContentExpander isOpen={overlayOpen} className="nav--mobile__overlay" isFillScreen={true} hasClose={true} onClose={this._click}>
            <div className="nav--mobile__overlay__inner">
              <nav>
                <ul>
                  {this.childitems}
                </ul>
              </nav>
            </div>
          </ContentExpander>
        )}
      </li>
    );
  }

}

NavItem.defaultProps = {
  link: {}
};

NavItem.propTypes = {
  children: PropTypes.array,
  navDelay: PropTypes.any,
  onMounted: PropTypes.func,
  label: PropTypes.string,
  mobileLabel: PropTypes.string,
  isOverflow: PropTypes.bool,
  isVisible: PropTypes.bool,
  position: PropTypes.any,
  link: PropTypes.object,
  onClickItem:PropTypes.func,
  isOverlayOpen:PropTypes.bool
};

module.exports = NavItem;
