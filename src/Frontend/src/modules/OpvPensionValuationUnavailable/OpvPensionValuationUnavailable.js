import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';

import './OpvPensionValuationUnavailable.scss';

class OpvPensionValuationUnavailable extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const {
      title,
      description,
      back,
      descriptionCard,
      descriptionBrd
    } = this.props;
    const baseClass = 'pension-details-unavailable';

    return (
      <div>
        <div className={`${baseClass}`}>
          <div className={`${baseClass}__main`}>
            <div className={`${baseClass}__signinform`}>
              <EditableDiv className={`${baseClass}__backLink`} html={back} />
              <Editable
                tag="h2"
                className={`${baseClass}__title`}
                html={title}
              />
              <div className={`${baseClass}__brdCls`}>
                <EditableDiv
                  className={`${baseClass}__content`}
                  html={description}
                />
                <Editable
                  className={`${baseClass}__contentBorder`}
                  html={descriptionBrd}
                />
              </div>
            </div>
            <div className={`${baseClass}__createacc`}>
              <div className={`${baseClass}__card`}>
                <div className={`${baseClass}__cardcontent`}>
                  <Editable
                    className={`${baseClass}__content`}
                    html={descriptionCard}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

OpvPensionValuationUnavailable.defaultProps = {
  title: '',
  description: '',
  back: '',
  descriptionCard: '',
  descriptionBrd: ''
};

OpvPensionValuationUnavailable.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  back: PropTypes.string,
  descriptionCard: PropTypes.string,
  descriptionBrd: PropTypes.string
};

module.exports = OpvPensionValuationUnavailable;
