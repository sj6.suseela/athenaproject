import React from 'react';
// import PropTypes from 'prop-types';
import './OpvForgotPassword.scss';

class OpvForgotPassword extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      clicked: false
    };

    this._click = this._click.bind(this);
  }

  _click (e) {
    e.preventDefault();
    this.state.clicked ? this.setState({clicked: false}) : this.setState({clicked: true});
  }
  render () {
    const baseClass = 'opv-forgot-password';
    return (
      <div className={`${baseClass}`} >
        <div id="form-area">

        </div>
      </div>
    );
  }
}

OpvForgotPassword.propTypes = {};

module.exports = OpvForgotPassword;
