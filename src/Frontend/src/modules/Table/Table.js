import React from 'react';
import PropTypes from 'prop-types';

import './Table.scss';

import EditableDiv from '../Editable/EditableDiv';

class Table extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      overflow:false,
      columnCount:0
    };

    this.childComponentDidMount = this.childComponentDidMount.bind(this);
    this.getColumnCount = this.getColumnCount.bind(this);
  }

  childComponentDidMount (el) {
    const table = el.querySelector('table');

    if(table) {
      this.getColumnCount(table);
    }

    if(table && table.offsetWidth > el.offsetWidth) {
      this.setState({
        overflow:true
      });
    }
  }

  getColumnCount (table) {
    const colCount = table.querySelectorAll('thead th').length;
    this.setState({
      columnCount:colCount
    });
  }

  render () {
    // get the prop settings
    const {content,hasHighlight,isFirstRowPrice,footerText,noStripes} = this.props;

    let cssClass = 'table';

    if(hasHighlight) {
      cssClass += ' table--highlight';
    }

    if(isFirstRowPrice) {
      cssClass += ' table--firstRowPrice';
    }

    if(noStripes) {
      cssClass += ' table--noStripes';
    }

    if(this.state.overflow) {
      cssClass += ' table--overflow';
    }

    const {columnCount} = this.state;

    return (
      <div className={`${cssClass} cols-${columnCount}`}>
          <EditableDiv html={content} didMount={this.childComponentDidMount} className="scrollableTable" ref={(c) => {this.table = c;}}/>
          <EditableDiv html={content} className="clone" />
          <EditableDiv className="table__footer" html={footerText} />
      </div>
    );
  }
}

Table.defaultProps = {
  content: '',
  className: ''
};

Table.propTypes = {
  content: PropTypes.string,
  className: PropTypes.string,
  hasHighlight: PropTypes.bool,
  isFirstRowPrice: PropTypes.bool,
  footerText: PropTypes.string,
  noStripes:PropTypes.bool
};

module.exports = Table;
