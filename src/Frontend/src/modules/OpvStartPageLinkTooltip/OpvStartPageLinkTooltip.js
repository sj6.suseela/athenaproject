import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvStartPageLinkTooltip.scss';

class OpvStartPageLinkTooltip extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      clicked: false
    };

    this._click = this._click.bind(this);
  }

  _click (e) {
    e.preventDefault();
    this.state.clicked ? this.setState({clicked: false}) : this.setState({clicked: true});
  }
  render () {
    const {link, title, description} = this.props;
    const baseClass = 'opv-startpage-link-tooltip';
    const className = this.state.clicked ? 'on-rac' : 'off-rac';

    return (

      <div className={`${baseClass}`}>
        <div className={`${baseClass}__info-notelink`} onClick={this._click}>
          <Editable html={link} className={`${baseClass}__info-link`} />
        </div>
        <div id="tooltip-rac" className={`${className} top`}>
          <div className={`${baseClass}__tooltip-inner-rac`}>
              <span className={`${baseClass}__closenote-rac`} onClick={this._click}>X</span>
              <Editable tag="h6" className={`${baseClass}__infoheading`} html={title} />
              <EditableDiv className="infoNoteContent1" html={description} />
          </div>
        </div>
      </div>

    );
  }
}



OpvStartPageLinkTooltip.propTypes = {
  link : PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.object
};

module.exports = OpvStartPageLinkTooltip;
