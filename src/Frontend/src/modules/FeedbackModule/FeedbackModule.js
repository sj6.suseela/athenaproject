import React from 'react';
import PropTypes from 'prop-types';

import './FeedbackModule.scss';

import Cta from '../Cta/Cta';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';

import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';

/** FeedbackModule
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.FeedbackModule, {
    id : 'id',
    theme : {
      textColour : 'light',
      themeName : 'clover',
      themeBefore : 'white',
      themeAfter : 'white',
      topCurve : { type:'convex' }
    },
    headline : 'This is a best site - please leave feedback',
    icon :'<img src="http://placehold.it/50x50" />',
    ctas : [
      { type:'primary', link:'<a href="#">Tell us what you think!</a>' },
      { type:'secondary', link:'<a href="#">Secondary</a>' }
    ]
  }), document.getElementById("element"));
*/
class FeedbackModule extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  componentDidMount () {
    const div = document.getElementById('nebula_div_btn');

    if(div === null) {
      console.warn('Kamyple feedback ribbon is not present - please liaise with the GTM owbner to ensure this is injected into the page');
    }
    else    {
      this.button = div.querySelector('.kampyle_button-text');
    }
  }

  onClick (e) {
    e.preventDefault();

    this.button.click();
  }

  render () {
    // get the prop settings
    const {headline, icon, primaryCta} = this.props;

    // base css class
    const baseClass = 'feedback-module';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
          <EditableImage image={icon} className={`${baseClass}__container`} />
          <div className={`container ${baseClass}__container`}>
            <div className={`${baseClass}__content`}>
              <Editable tag="h2" className={`h1 ${baseClass}__title`} html={headline} />
            </div>
            {primaryCta && (
              <Cta type={primaryCta.type} rawCta={primaryCta.link} onClick={this.onClick.bind(this)}/>
            )}
        </div>
      </ModuleWrapper>
    );
  }
}

FeedbackModule.defaultProps = {
  theme: {},
  backgroundImage: '',
  headline: '',
  primaryCta: null,
  icon:'',
  id:'FeedbackModule'
};

FeedbackModule.propTypes = {
  theme: PropTypes.object,
  backgroundImage: PropTypes.string,
  headline: PropTypes.string,
  primaryCta: PropTypes.object,
  icon:PropTypes.string,
  id:PropTypes.string
};

module.exports = FeedbackModule;
