import React from 'react';
import PropTypes from 'prop-types';

import ArticlePreview from '../ArticlePreview/ArticlePreview';

import Carousel from '../Carousel/Carousel';
import SectionTitle from '../Section/SectionTitle';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import Cta from '../Cta/Cta';

import './Related.scss';

/** Related
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Related, {
    id : 'id',
    theme : {},
    heading : "We've also got a few helpful guides",
    viewAllHref : "/#/viewall",
    viewAllLabel : "More guides",
    items : [
      {
        title : "Title",
        link:{href : "#/article-1/"},
        readTime : "5 min read",
        image : '<img src="//placehold.it/600x400" alt="" />',
      },
      {
        title : "Title",
        link:{href : "#/article-1/"},
        readTime : "5 min read",
        image : '<img src="//placehold.it/600x400" alt="" />',
      },
      {
        title : "Title",
        link:{href : "#/article-1/"},
        readTime : "5 min read",
        image : '<img src="//placehold.it/600x400" alt="" />',
      },
      {
        title : "Title",
        link:{href : "#/article-1/"},
        readTime : "5 min read",
        image : '<img src="//placehold.it/600x400" alt="" />',
      }
    ]
  }), document.getElementById("element"));
*/
class Related extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };

    const items = this.props.items;

    this.items = items.map((item,i) => {
      return (<div key={i}><ArticlePreview {...item} /></div>);
    });
  }

  render () {
    const {items,id,viewAllLabelMobile,viewAllHref,viewAllLabel} = this.props;

    let {theme} = this.props;

    // TODO: refactor this module to make sure that the serverside passes theme as an object, and remove this
    if(typeof(theme) === 'string') {
      theme = {
        themeName : theme
      };
    }

    const baseClass = 'related';
    const itemCount = items.length;

    const cssClass = `${baseClass}--${itemCount}up`;

    const options = {
      cellAlign: 'left',
      prevNextButtons: false,
      pageDots: false,
      groupCells: true,
      wrapAround: false
    };

    const ctaLabel = viewAllLabelMobile ? viewAllLabelMobile : viewAllLabel;

    return (
      <ModuleWrapper {...this.props} theme={theme} baseClass={baseClass} classes={cssClass}  isProduct={true}>
        <SectionTitle {...this.props} />
        <div className={`carousel carousel--${itemCount}up`}>
          <Carousel items={this.items} options={options} id={id}/>
        </div>
        {this.props.viewAllHref && (
          <div className="related__cta mobileOnly">
            <Cta type="secondary" label={ctaLabel} href={viewAllHref}/>
          </div>
        )}
      </ModuleWrapper>
    );
  }
}

Related.defaultProps = {
  theme: 'warm-grey',
  heading: '',
  items: []
};

Related.propTypes = {
  theme: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  heading: PropTypes.string,
  items: PropTypes.array,
  carousel: PropTypes.any,
  viewAllHref: PropTypes.string,
  viewAllLabel: PropTypes.string,
  viewAllLabelMobile:PropTypes.string,
  id:PropTypes.string
};

module.exports = Related;
