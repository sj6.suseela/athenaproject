import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Cta from '../Cta/cta';
import './OpvSettings.scss';

class OpvSettings extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {title, description, back, tableDescription, deRegisterCta} = this.props;
    const baseClass = 'settings';

    return (
      <div>
        <EditableDiv className={`${baseClass}__backLink`} html={back} />
        <Editable tag="h2" className={`${baseClass}__title`} html={title} />
        <EditableDiv className={`${baseClass}__content`} html={description} />
        <EditableDiv
          className={`${baseClass}__policyDetails`}
          html={tableDescription}
        />
        <div className={`${baseClass}__footer`}>
          <Cta type="primary" isSmall={true} rawCta={deRegisterCta.html} />
        </div>
      </div>
    );
  }
}

OpvSettings.defaultProps = {
  title: '',
  description: '',
  back: '',
  tableDescription: '',
  deRegisterCta: ''
};

OpvSettings.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  back: PropTypes.string,
  tableDescription: PropTypes.string,
  deRegisterCta: PropTypes.object
};

module.exports = OpvSettings;
