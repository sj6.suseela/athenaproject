import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvAccessCodeMsg.scss';

class OpvAccessCodeMsg extends React.Component {

  constructor (props) {
    super(props);

    this.state = {

    };
  }

  render () {
    const {title, description} = this.props;
    const baseClass = 'accessCodeMsg';

    return (
      <div>
          <Editable tag="h2" className={`${baseClass}__title`} html={title} />
          <EditableDiv className={`${baseClass}__content`} html={description} />
          {/* <div className={'form-field form-field--button'}>
                      <button className="cta cta--sm  cta--no-icon enter-access-code-btn" name="submit">{submit}</button>
          </div> */}
      </div>
    );
  }
}

OpvAccessCodeMsg.propTypes = {
  title : PropTypes.string,
  description : PropTypes.string
};

module.exports = OpvAccessCodeMsg;
