import React from 'react';
import PropTypes from 'prop-types';

import ModuleHeader from '../_Product/ModuleHeader/ModuleHeader';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import EditableParagraph from '../Editable/EditableParagraph';
import EditButton from '../CMS/EditButton';

import '../../external/experiences.css';
import './ReevooMoments.scss';

class ReevooMoments extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      scriptLoaded:false
    };
  }

  componentDidMount () {
    const _this = this;
    const script = document.createElement('script');
    script.onload = function () {
      _this.setState({
        scriptLoaded:true
      });
    };
    script.async = true;
    script.src = this.props.script;
    document.body.appendChild(script);
  }

  render () {
    const {heading, label, copy, theme, reevooParams, editButton} = this.props;
    const hasProps = !!reevooParams && !!reevooParams.trkref && !!reevooParams.tags;

    const {scriptLoaded} = this.state;
    const baseClass = 'reevooMoments';

    if (!reevooParams['embed-title']) {
      reevooParams['embed-title'] = ' ';
    }

    return (
      <ModuleWrapper baseClass={baseClass} {...this.props} isProduct={true}>
        <div className={`${baseClass}__header`}>
          <ModuleHeader headline={heading} label={label} themeColour={theme.textColour} align='center' isCompact={true} />
          <EditableParagraph className={`${baseClass}__paragraph`} html={copy} />
        </div>
        {hasProps && (
          <div className={`${baseClass}__embed`}>
            {scriptLoaded && (
              <reevoo-experiences-embedded {...reevooParams}>
              </reevoo-experiences-embedded>
            )}
          </div>
        )}
        {editButton && (
          <EditButton html={editButton} isSmall={false} align="right" />
        )}
      </ModuleWrapper>
    );
  }
}

ReevooMoments.defaultProps = {
};

ReevooMoments.propTypes = {
  heading: PropTypes.string,
  label: PropTypes.string,
  copy: PropTypes.string,
  theme: PropTypes.object,
  reevooParams: PropTypes.object,
  editButton: PropTypes.string,
  script:PropTypes.string
};

module.exports = ReevooMoments;
