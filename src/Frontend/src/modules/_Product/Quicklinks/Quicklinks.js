import React from 'react';
import PropTypes from 'prop-types';
import List from '../../_ContentModules/List/List';
import ModuleHeader from '../ModuleHeader/ModuleHeader';
import EditableImage from '../../Editable/EditableImage';
import Cta from '../../Cta/Cta';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import './Quicklinks.scss';

import Reevoo from '../../Reevoo/Reevoo';
import Contact from '../../Contact/Contact';
import EditableDiv from '../../Editable/EditableDiv';

/** Quicklinks
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Quicklinks, {
    heading:"When it comes to life insurance, we come highly recommended",
    flourish:'<img src="dist/images/cms/clouds1.svg" />',
    links: {
      label:'Existing customers',
      heading:'Do you need to make a claim, change or renew your policy?',
      items:[
        '<a href="#">I would like to make a claim</a>',
        '<a href="#">Make policy changes</a>',
        '<a href="#">Renew your policy</a>'
      ]
    },
    reevoo: {
      icon: '<img src="dist/images/cms/iPad-80x80.svg" />',
      reevooParams: {
        trkref: 'LV-GI',
        sku: 'CINSTY',
        variant: 'newBrand'
      }
    },
    theme:{
      themeName:'warm-grey'
    },
  }), document.getElementById("element"));

  * @example
  ReactDOM.render(React.createElement(Components.Quicklinks, {
    heading:"When it comes to life insurance, we come highly recommended",
    flourish:'<img src="dist/images/cms/clouds1.svg" />',
    links: {
      text: '<p>I am not in a box!</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
      inBox: false
    },
    reevoo: {
      icon: '<img src="dist/images/cms/iPad-80x80.svg" />',
      reevooParams: {
        trkref: 'LV-GI',
        sku: 'CINSTY',
        variant: 'newBrand'
      }
    },
    theme:{
      themeName:'cool-grey'
    },
  }), document.getElementById("element"));

  * @example
  ReactDOM.render(React.createElement(Components.Quicklinks, {
    heading:"When it comes to life insurance, we come highly recommended",
    flourish:'<img src="dist/images/cms/clouds1.svg" />',
    links: {
      label:'Existing customers',
      heading:'Do you need to make a claim, change or renew your policy?',
      text: '<p>I have text: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>',
      items:[
        '<a href="#">I would like to make a claim</a>'
      ]
    },
    reevoo: {
      icon: '<img src="dist/images/cms/iPad-80x80.svg" />',
      reevooParams: {
        trkref: 'LV-GI',
        sku: 'CINSTY',
        variant: 'newBrand'
      }
    },
    theme:{
      themeName:'warm-grey'
    },
  }), document.getElementById("element"));

  * @example
  ReactDOM.render(React.createElement(Components.Quicklinks, {
    heading:"Our other award winning products",
    isThirds: true,
    hasReducedTopPadding: true,
    intro: {
      paragraph: '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in euismod est. Integer dictum nisl turpis, eu ultricies eros finibus vitae. Aliquam</p>',
      image: '<img src="dist/images/cms/Car-Cover-Features-360x140.svg" />'
    },
    links: {
      label:'Our Products',
      items:[
        '<a href="#">Multi car insurance</a>',
        '<a href="#">Pet insurance</a>',
        '<a href="#">Breakdown cover</a>',
        '<a href="#">Motorbike insurance</a>',
        '<a href="#">Caravan Insurance</a>',
        '<a href="#">Landlord insurance</a>',
        '<a href="#">Classic car</a>',
        '<a href="#">Inclome protection</a>'
      ]
    },
    theme:{
      themeName:'warm-grey'
    },
  }), document.getElementById("element"));
*/
class Quicklinks extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };

    if(props.intro && props.intro.ctas) {
      this.ctaList = props.intro.ctas.map((item,i) => {
        return (
          <Cta key={item + i} {...item.link} type={item.type} rawCta={item.link}/>
        );
      });
    }
  }

  render () {
    const {heading, flourish, reevoo, contact, links, intro, hasExtraTopPadding, isThirds, hasReducedTopPadding} = this.props;

    const baseClass = 'module--quicklinks';

    let cssClass = '';
    let linkClass = `${baseClass}__list`;

    if(hasExtraTopPadding) {
      cssClass += ` ${baseClass}--extraTopPadding `;
    }
    else if (hasReducedTopPadding) {
      cssClass += ` ${baseClass}--reducedTopPadding `;
    }
    if(isThirds) {
      cssClass += ` ${baseClass}--thirds`;
    }
    if(links.inBox !== false) {
      linkClass += ' box ';
    }

    const hasReevoo = reevoo && reevoo.reevooParams && reevoo.reevooParams.trkref && reevoo.reevooParams.trkref !== '';

    return (
      <ModuleWrapper {...this.props} isProduct={true} baseClass={baseClass} classes={cssClass}>
        <EditableImage className={`${baseClass}__flourish`} image={flourish}/>
        <div className="columns@md product-intro__main spaced">
          <div className='spaced'>
            <div className={`${baseClass}__info`}>
              <ModuleHeader headline={heading} />

              {contact && (
                <Contact {...contact} />
              )}
              {hasReevoo && (
                <Reevoo {...reevoo} />
              )}
              {intro && (
                <div>
                  <ModuleHeader label={intro.label} headline={intro.headline} />
                  {this.ctaList}
                  <EditableDiv html={intro.paragraph} />
                  <EditableImage image={intro.image} className={`${baseClass}__image`} />
                </div>
              )}
            </div>
          </div>
          <div>
            {links && (
              <div className={linkClass}>
                <ModuleHeader label={links.label} intro={links.heading} isCompact={true}/>
                <EditableDiv html={links.text}/>
                <List items={links.items} />
              </div>
            )}
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

Quicklinks.defaultProps = {
  backgroundImage: '',
  flourish: '',
  theme: {},
  heading: '',
  links: null,
  reevoo: null,
  contact: null,
  intro:null,
  hasExtraTopPadding:false,
  id:'quicklinks'
};

Quicklinks.propTypes = {
  backgroundImage: PropTypes.string,
  flourish: PropTypes.string,
  theme: PropTypes.object,
  heading: PropTypes.string.isRequired,
  links: PropTypes.object,
  reevoo: PropTypes.object,
  contact: PropTypes.object,
  intro:PropTypes.object,
  hasExtraTopPadding:PropTypes.bool,
  id:PropTypes.string,
  isThirds: PropTypes.bool,
  hasReducedTopPadding: PropTypes.bool
};

module.exports = Quicklinks;
