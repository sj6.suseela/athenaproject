import React from 'react';
import PropTypes from 'prop-types';

import './OverlayContent.scss';

import EditableImage from '../../Editable/EditableImage';
import Editable from '../../Editable/Editable';
import EditableDiv from '../../Editable/EditableDiv';
import ArrowLink from '../../ArrowLink/ArrowLink';
import ImageLink from '../../ImageLink/ImageLink';
import Cta from '../../Cta/Cta';

class OverlayContent extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {label,image,heading,imageLink,links,text,cta} = this.props;

    const linksList = links.map((item,i) => {
      return (
        <li key={item + i}><ArrowLink className="overlayContent__cta" link={item} /></li>
      );
    });

    return (
      <div className="overlayContent overlayContent--product">
        <EditableImage className="overlayContent__image" image={image} />
        <div className="overlayContent__label"><Editable html={label} /></div>
        <Editable tag="h2" className="overlayContent__heading" html={heading} />
        <EditableDiv className="overlayContent__text" html={text} />
        <Cta {...cta} type="secondary" noWrap={true} rawCta={cta}/>
        <ul className="overlayContent__links">
          {linksList}
        </ul>
        {imageLink && (
          <ImageLink className="overlayContent__imageLink" image={imageLink.image} link={imageLink.link} linkSrc={imageLink.linkSrc}/>
        )}
      </div>
    );
  }
}

OverlayContent.defaultProps = {
  image: '',
  heading: '',
  imageLink: null,
  label: '',
  links: [],
  text:'',
  cta:''
};

OverlayContent.propTypes = {
  image: PropTypes.string,
  heading: PropTypes.string,
  imageLink: PropTypes.object,
  label: PropTypes.string,
  links: PropTypes.array,
  text:PropTypes.string,
  cta:PropTypes.string
};

module.exports = OverlayContent;
