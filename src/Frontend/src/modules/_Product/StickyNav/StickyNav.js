import React from 'react';
import PropTypes from 'prop-types';

import './StickyNav.scss';

import Stick from '../../Stick/Stick';
import Cta from '../../Cta/Cta';
import Editable from '../../Editable/Editable';
import EditableImage from '../../Editable/EditableImage';

/** StickyNav
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.StickyNav, {
    id : 'id',
    theme : {}
  }), document.getElementById("element"));
*/
class StickyNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };
  }

  render () {
    const {cta,contact} = this.props;

    return (
        <div className="product-stickyNav">
          <Stick className="product-stickyNav__content" hasFadeIn={true}>
            <div className="container">
              {!!cta && (
                <Cta type={cta.type} isSmall={true} rawCta={cta.link}/>
              )}
              {!!contact && (
                <div className="product-stickyNav__contact">
                  <Editable className="product-stickyNav__intro" html={contact.intro} />
                  <Editable className="product-stickyNav__number" html={contact.number} />
                  <EditableImage className="product-stickyNav__image" image={contact.icon} />
                </div>
              )}
            </div>
          </Stick>
        </div>
    );
  }
}

StickyNav.propTypes = {
  cta:PropTypes.object,
  contact:PropTypes.object
};

module.exports = StickyNav;
