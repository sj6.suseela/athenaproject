import React from 'react';
import PropTypes from 'prop-types';

import './StartQuote.scss';

import Cta from '../../Cta/Cta';

import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import EditableImage from '../../Editable/EditableImage';
import Editable from '../../Editable/Editable';

/** StartQuote
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.StartQuote, {
    id : 'id',
    theme : {
      textColour : 'light',
      themeName : 'clover',
      themeBefore : 'white',
      themeAfter : 'white',
      topCurve : { type:'convex' }
    },
    headline : 'Now sit back, relax and start your quote',
    icon :'<img src="http://placehold.it/50x50" />',
    ctas : [
      { type:'primary', link:'<a href="#">Start your quote</a>' },
      { type:'secondary', link:'<a href="#">Your saved quote </a>' }
    ]
  }), document.getElementById("element"));
*/
class StartQuote extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {headline,ctas,icon} = this.props;

    // base css class
    const baseClass = 'start-quote';

    const buttons = ctas.map((item,i) => {
      return (
        <Cta key={item + i} type={item.type} rawCta={item.link}/>
      );
    });

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
          <EditableImage image={icon} className="start-quote__flourish" />
          <div className={`container ${baseClass}__container`}>
            <div className={`${baseClass}__content`}>
              <Editable tag="h2" className={`h1 ${baseClass}__title`} html={headline} />
            </div>
            {buttons && (
              <div className={`${baseClass}__buttons`}>{buttons}</div>
            )}
        </div>
      </ModuleWrapper>
    );
  }
}

StartQuote.defaultProps = {
  theme: {},
  backgroundImage: '',
  headline: '',
  ctas: [],
  icon:'',
  id:'startquote'
};

StartQuote.propTypes = {
  theme: PropTypes.object,
  backgroundImage: PropTypes.string,
  headline: PropTypes.string,
  ctas: PropTypes.array,
  icon:PropTypes.string,
  id:PropTypes.string
};

module.exports = StartQuote;
