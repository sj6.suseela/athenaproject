import React from 'react';
import PropTypes from 'prop-types';

import Quote from '../../_ContentModules/Quote/Quote';
import ModuleHeader from '../ModuleHeader/ModuleHeader';

import EditableImage from '../../Editable/EditableImage';
import EditableDiv from '../../Editable/EditableDiv';
import Editable from '../../Editable/Editable';
import EditButton from '../../CMS/EditButton';
import Cta from '../../Cta/Cta';
import IconList from '../../_ContentModules/List/IconList';

import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import './Features.scss';

/** Features
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Features, {
    id : 'id',
    theme: {
      themeName:'white',
      topCurve: { type: 'convex' }
    },
    label: 'What is advice',
    headline: "Your money deserves a safe pair of hands. With advice you’ll get",
    body: `<p><b>Advice is a way of handing over the hard work of shopping for and setting up financial products</b>, like pensions or retirement products, to a professional. As part of the service your personal circumstances will be reviewed, and you’ll get recommendations on what options are most suited to your needs. It’s regulated and given by professionals, like us.</p>`
  }), document.getElementById("element"));
*/
class Features extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };

    const reducedPadding = props.ctas.length > 1;
    this.ctaList = props.ctas.map((item,i) => {
      return (
        <Cta key={item.link + i} {...item.link} type={item.type} rawCta={item.link} hasReducedPadding={reducedPadding}/>
      );
    });
  }

  render () {
    const {body, quote, imageLeft, imageRight, price, editButton, flourish, iconList} = this.props;
    const baseClass = 'product-features';

    const hasPrice = price && price.text && price.text.length > 0;

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <EditableImage className={`${baseClass}__flourish`} image={flourish}/>
        <div className={`columns@md ${baseClass}__main`}>
          <div>
            <ModuleHeader {...this.props} />
            {hasPrice && (
              <div className={`price ${baseClass}__price`}>
                <Editable className="price__prefix" html={price.prefix} />
                <Editable className="price__value" html={price.text} />
              </div>
            )}
            {editButton && (
              <EditButton html={editButton} isSmall={false} align="right" />
            )}
            {quote && (
              <div className="hide@sm">
                <Quote theme="dusty-mint" {...quote} />
              </div>
            )}
            <EditableImage className={`${baseClass}__image hide@sm`} image={imageLeft.html} imageSize={imageLeft.size}/>
          </div>
          <div>
            <EditableDiv className={`${baseClass}__body bulleted`} html={body} />
            {!!iconList && iconList.length > 0 && (
              <IconList className={`${baseClass}__iconList`} items={iconList} />
            )}
            {this.ctaList.length > 0 && (
              <div className={`${baseClass}__cta`}>
                {this.ctaList}
              </div>
            )}
            {quote && (
              <div className="hide@md hide@lg">
                <Quote theme="dusty-mint" {...quote} />
              </div>
            )}
            <EditableImage className="hide@sm" image={imageRight.html} imageSize={imageRight.size}/>
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

Features.defaultProps = {
  flourish: '',
  label: '',
  headline: '',
  intro: '',
  price: null,
  body: '',
  imageRight: {},
  imageLeft: {},
  ctas: [],
  quote: null,
  id:'features',
  editButton : ''
};

Features.propTypes = {
  flourish: PropTypes.string,
  price: PropTypes.object,
  label: PropTypes.string,
  headline: PropTypes.string,
  intro: PropTypes.string,
  body: PropTypes.string,
  imageLeft: PropTypes.object,
  imageRight: PropTypes.object,
  ctas: PropTypes.array,
  quote: PropTypes.object,
  id:PropTypes.string,
  editButton : PropTypes.string,
  iconList: PropTypes.array
};

module.exports = Features;
