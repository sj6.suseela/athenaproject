
import React from 'react';
import PropTypes from 'prop-types';

import ModuleHeader from '../ModuleHeader/ModuleHeader';
import EditableImage from '../../Editable/EditableImage';
import EditableParagraph from '../../Editable/EditableParagraph';
import Editable from '../../Editable/Editable';
import Icon from '../../Icon/Icon';
import Overlay from '../../Overlay/Overlay';
import OverlayContent from '../OverlayContent/OverlayContent';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import './Timeline.scss';

/** Timeline
  * @description description
  * @example

  var item = {
    image : '<img src="http://placehold.it/300x150" />',
    label : 'One-on-one chat',
    heading : 'Lorem ipsum dolor sit amet, adipiscing elit sed do',
    content : ''
  };

  ReactDOM.render(React.createElement(Components.Timeline, {
    id : 'id',
    theme : {
      themeName : 'dusty-pistachio',
      themeBefore : 'white',
      themeAfter : 'white'
    },
    label : 'Our advice process',
    heading : 'Our tried and tested process follows four clear steps:',
    items : [item],
  }), document.getElementById("element"));

  * @example

  ReactDOM.render(React.createElement(Components.Timeline, {
    id : 'id',
    theme : {
      themeName : 'pistachio',
      themeBefore : 'white',
      themeAfter : 'white'
    },
    label : 'Our advice process',
    heading : 'Our tried and tested process follows four clear steps:',
    items : [item, item],
  }), document.getElementById("element"));

  * @example

  ReactDOM.render(React.createElement(Components.Timeline, {
    id : 'id',
    theme : {
      themeName : 'pistachio',
      themeBefore : 'white',
      themeAfter : 'white'
    },
    label : 'Our advice process',
    heading : 'Our tried and tested process follows four clear steps:',
    items : [item, item, item],
  }), document.getElementById("element"));

  * @example

  ReactDOM.render(React.createElement(Components.Timeline, {
    id : 'id',
    theme : {
      themeName : 'dusty-pistachio',
      themeBefore : 'white',
      themeAfter : 'white'
    },
    label : 'Our advice process',
    heading : 'Our tried and tested process follows four clear steps:',
    items : [item, item, item, item],
  }), document.getElementById("element"));
*/
class Timeline extends React.Component {
  constructor (props) {
    super(props);

    this._click = this._click.bind(this);
    this._closeOverlay = this._closeOverlay.bind(this);
    this._clickClose = this._clickClose.bind(this);

    this.statePushed = false;

    this.state = {
      isOverlayOpen: false,
      overlayContent:{
        image:'',
        label:'',
        Heading:'',
        links:[]
      }
    };
  }

  componentDidMount () {
    const _this = this;
    if(!SERVER) {
      history.replaceState({}, null, null);

      window.addEventListener('popstate',(event) => {
        if(event.state === null) {
          event.preventDefault();
          return false;
        }
        
        _this._closeOverlay();
      });
    }
  }

  _clickClose () {
    if(this.statePushed) {
      window.history.back();
    }
    else{
      this._closeOverlay();
    }
    history.replaceState({}, null, null);
  }

  _click (content, e) {
    if(content) {
      e.preventDefault();
      e.stopPropagation();

      if (!SERVER) {
        // set body width to prevent it jumping when the scroll bar disappears
        const body = document.body;
        const scrollWidth = window.innerWidth - body.offsetWidth;
        body.style.width = `calc(100% - ${scrollWidth}px)`;
      }

      history.pushState({}, null, null);
      this.statePushed = true;

      this.setState({
        isOverlayOpen: true,
        overlayContent: content
      });
    }
  }

  _closeOverlay () {
    if (!SERVER) {
      // reset body width
      document.body.style.width = '100%';
    }

    this.setState({
      isOverlayOpen: false
    });
  }

  render () {
    // get the prop settings
    const {label, heading, items} = this.props;
    const baseClass = 'timeline';

    const timeline = items.map((item,i) => {
      return (
          <div key={i} className={`${baseClass}__item`}>
            <span className="timeline__item__speech" />
            <span className="timeline__item__marker"><Icon icon="downarrow" fillColor="#CDDC29"/></span>
            <EditableImage className="timeline__item__image" image={item.image} />
            <h4 className="timeline__item__label" onClick={(e) => this._click(item.content, e)}><Editable html={item.label} /></h4>
            <EditableParagraph className="timeline__item__text" html={item.heading} />
            <button className={`${baseClass}__more`}  onClick={(e) => this._click(item.content, e)}>Read More</button>
          </div>
      );
    });

    const additionalClasses = `${baseClass}--${items.length}`;

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} classes={additionalClasses} isProduct={true}>
        <ModuleHeader headline={heading} label={label} align="center" />
        <div className={`${baseClass}__items`}>
          {timeline}
        </div>
        <Overlay
            isOpen={ this.state.isOverlayOpen }
            from='right'
            hasHeader={false}
            width='100%'
            className='overlay'
            onRequestClose={this._clickClose}>
            <OverlayContent {...this.state.overlayContent} />
        </Overlay>
      </ModuleWrapper>
    );
  }
}

Timeline.defaultProps = {
  label: '',
  title: '',
  items: [],
  id:'timeline'
};

Timeline.propTypes = {
  label: PropTypes.string,
  heading: PropTypes.string,
  items: PropTypes.array,
  id:PropTypes.string
};

module.exports = Timeline;
