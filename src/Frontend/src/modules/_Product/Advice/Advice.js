import React from 'react';
import PropTypes from 'prop-types';

import Contact from '../../Contact/Contact';
import ModuleCurve from '../../ModuleCurve/ModuleCurve';
import ModuleHeader from '../ModuleHeader/ModuleHeader';
import ArrowLink from '../../ArrowLink/ArrowLink';

import Theme from '../../Theme';
import Module from '../../Module';
import EditableImage from '../../Editable/EditableImage';
import EditableDiv from '../../Editable/EditableDiv';

import './Advice.scss';

/** Advice
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Advice, {
    id : 'id',
    theme : {
      themeName :'mint',
      themeBefore : 'white',
      themeAfter : 'white',
      bottomCurve : { type:'concave' },
    },
    headline : '&#8220;Looking for advice?',
    intro : "Not sure if Life Insurance is the right product for you? Don’t worry; help is just a call away",
    image : '<img src="dist/images/placeholder/advisor-female.png" />',
    copy : `<p>Our friends at <a href="#">LifeSearch</a> can help you find the right insurance for you and your family's needs.</p>`,
    contact : {
      tel : '0800 032 2844',
      opening : '<b>Mon to Thu</b> 8am-8pm Mon-Fri <b>Fri</b> 8am - 6pm',
      info : 'Calls may be recorded and/or monitored for training and audit purposes.'
    },
    cta : '<a href="#">Lorem ipsum</a>'
  }), document.getElementById("element"));
*/
class Advice extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  componentDidMount () {
    Module.register(this.ref);
  }

  render () {
    const {theme, hasCurveTop, image, imageAlignment, copy, contact,cta,id} = this.props;

    // base css class
    const baseClass = 'advice';

    const useTheme = (!theme.themeName || theme.themeName === 'default') ? 'mint' : theme.themeName;

    const hasCurveBottom = !!theme.bottomCurve;

    const gradient = Theme.getTheme(theme.themeBefore);

    return (
      <section className={`module module--product ${baseClass} theme-${useTheme}`} ref={(c) => {this.ref = c;}}>
        {hasCurveTop && (
          <div className="desktopOnly">
            <div className="curve top">
              <svg width="1439" height="173" viewBox="0 0 1439 173" preserveAspectRatio="xMinYMax">
                <defs>
                  <linearGradient id={`${id}-gradtop`}>
                    <stop offset="0%" stopColor={gradient[0]} />
                    <stop offset="100%" stopColor={gradient[1]} />
                  </linearGradient>
                </defs>
                <path d="M0,173V0h1440c-319.3,0-587.7,15.7-805,47S206,120.3,0,173z" fill={`url(#${id}-gradtop)`}/>
              </svg>
            </div>
          </div>
        )}
        <div className="mobileOnly">
          <ModuleCurve id={id} type="convex" position="top" bgtheme={theme.themeBefore}/>
        </div>
        <div className="wrapper">
          <div className="container advice__container">
            <div className="advice__main">
              <div className={`advice__image advice__image--${imageAlignment}`}>
                <EditableImage image={image.html} imageSrc={image.src} imageSize={image.size} imageExtension={image.extension} hasInnerPlaceholder={true}/>
              </div>
              <div className="advice__copy">
                <div className="advice__speech">
                  <ModuleHeader {...this.props} />
                  <EditableDiv className='advice__copyText' html={copy} />
                  <Contact {...contact} />
                  {cta && cta.length > 0 && (
                    <div className="advice__cta">
                      <ArrowLink link={cta} />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        {hasCurveBottom && (
          <ModuleCurve id={`${id}-bottom`} className="advice__bottomCurve" type={theme.bottomCurve.type} position="bottom" bgtheme={theme.themeAfter}/>
        )}
      </section>
    );
  }
}

Advice.defaultProps = {
  theme: {},
  themeBefore: 'default',
  hasCurveTop: true,
  hasCurveBottom: true,
  image: {},
  imageAlignment:'bottom',
  headline: '',
  intro: '',
  copy: '',
  contact: null,
  id:'advice'
};

Advice.propTypes = {
  label:PropTypes.string,
  theme: PropTypes.object,
  image: PropTypes.object,
  imageAlignment:PropTypes.string,
  hasCurveTop: PropTypes.bool,
  headline: PropTypes.string,
  intro: PropTypes.string,
  copy: PropTypes.string,
  contact: PropTypes.object,
  cta:PropTypes.string,
  id:PropTypes.string
};

module.exports = Advice;
