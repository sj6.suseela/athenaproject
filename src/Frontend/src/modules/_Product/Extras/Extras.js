import React from 'react';
import PropTypes from 'prop-types';

import './Extras.scss';

import ExtrasItem from './ExtrasItem';
import Cta from '../../Cta/Cta';

import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';
import ModuleHeader from '../ModuleHeader/ModuleHeader';

/** Extras
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Extras, {
    id : 'id',
    theme: {
      themeName: 'clover',
      topCurve:{ type:'convex' }
    },
    items: [
      {
        heading: 'Media library',
        icon: :
          html:'<img src="dist/images/icon/icon-clipboard.svg" />'
        },
        copy: `<p>Logos, infographics, viseo content, and images of our spokespeople and offices</p>`,
        links:[
          `<a href="media-library.html">Media Library</a>`
        ]
      },
      {
        heading: 'Press office contacts',
        icon::{
          html:'<img src="dist/images/icon/icon-clipboard.svg" />'
        },
        copy: `<p>Are you a journalist looking for something moe specific?</p>`,
        links:[
          `<a href="press-contact.html">Press office contacts</a>`
        ]
      }
    ]
  }), document.getElementById("element"));
*/
class Extras extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {label, headline, items, cta} = this.props;

    // base css class
    const baseClass = 'product-overview';

    const options = items.map((item,i) => {
      return (
          <div key={i}>
            <ExtrasItem {...item} />
          </div>
      );
    });

    const hasHeader = label || headline;

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        {hasHeader && (
          <ModuleHeader {...this.props} align="center"/>
        )}
        <div className="columns@md product-overview__main">
          {options}
        </div>
        {cta && (
          <div className={`${baseClass}__buttons align-center`}>
            <Cta {...cta} type="secondary" noWrap={true} rawCta={cta}/>
          </div>
        )}
      </ModuleWrapper>
    );
  }
}

Extras.defaultProps = {
  theme: {
    themeName: 'default'
  },
  label: '',
  backgroundImage: '',
  headline: '',
  items: [],
  cta: '',
  id:'extras'
};

Extras.propTypes = {
  theme: PropTypes.object,
  backgroundImage: PropTypes.string,
  label: PropTypes.string,
  headline: PropTypes.string,
  items: PropTypes.array,
  cta: PropTypes.string,
  id:PropTypes.string
};

module.exports = Extras;
