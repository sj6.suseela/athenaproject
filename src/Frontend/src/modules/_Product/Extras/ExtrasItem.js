import React from 'react';
import PropTypes from 'prop-types';

import EditableImage from '../../Editable/EditableImage';
import EditableDiv from '../../Editable/EditableDiv';
import Editable from '../../Editable/Editable';
import Cta from '../../Cta/Cta';
import List from '../../_ContentModules/List/List';

class OverviewItem extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {subheading, heading, icon, copy, cta, links} = this.props;

    // base css class
    const baseClass = 'product-overview__item';

    // root classes for this component
    const cssClass = `${baseClass}`;

    return (
      <div className={cssClass}>
        <div className="product-overview__header">
          <div className="product-overview__heading">
            <Editable tag="h3" className="product-overview__subhead" html={subheading} />
            <Editable tag="h2" className="product-overview__title" html={heading} />
          </div>
          <EditableImage className="product-overview__image" image={icon.html} imageSize={icon.size} imageExtension={icon.extension}/>
        </div>
        <div className="product-overview__body">
          <EditableDiv html={copy} />
          {links && (
          <div className="product-overview__body__links">
            <List items={links} />
          </div>
          )}
          {cta && (
            <div className="align-center">
              <Cta {...cta} type="secondary" noWrap={true} rawCta={cta} isSmall={true} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

OverviewItem.defaultProps = {
  subheading: '',
  heading: '',
  icon: {},
  copy: '',
  links:null,
  cta: ''
};

OverviewItem.propTypes = {
  subheading: PropTypes.string,
  heading: PropTypes.string,
  icon: PropTypes.object,
  copy: PropTypes.string,
  links:PropTypes.array,
  cta:PropTypes.string
};

module.exports = OverviewItem;
