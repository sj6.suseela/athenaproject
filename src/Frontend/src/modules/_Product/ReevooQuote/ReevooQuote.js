import React from 'react';
import PropTypes from 'prop-types';
import Quote from '../../_ContentModules/Quote/Quote';
import ModuleHeader from '../ModuleHeader/ModuleHeader';
import EditButton from '../../CMS/EditButton';

import Reevoo from '../../Reevoo/Reevoo';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import './ReevooQuote.scss';

/** ReevooQuote
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ReevooQuote, {
    id : 'id',
    heading : 'Our retirement advisors are second to none, but don’t just take our word for it',
    theme : {
      themeName : 'warm-grey',
      bottomCurve : { type: 'convex' },
    },
    reevoo : {
      icon : '<img src="dist/images/cms/iPad-80x80.svg" />',
      reevooParams: {
        trkref: 'LV-GI',
        sku: 'CINSTY',
        variant: 'newBrand'
      }
    },
    quote : {
      theme : 'dusty-pistachio',
      copy : "Very satisfied with the way they responded to my calls, also documentation summary is clear and easy to download and store.",
      author : "Tim Warren",
      role : "St Albans",
      source : '<img src="dist/images/reevoo.png" alt="Reevoo" />',
      avatar : '<img src="dist/images/placeholder/person.jpg" />',
    }
  }), document.getElementById("element"));
*/
class ReevooQuote extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {quote,reevoo,heading,quoteEditButton,reevooEditButton} = this.props;

    // base css class
    const baseClass = 'product-quote';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div className={`columns@md ${baseClass}__main`}>
          <div>
            {quote && (
              <div>
                {quoteEditButton && (
                  <EditButton html={quoteEditButton} isSmall={false} align="right" />
                )}
                <Quote theme="dusty-mint" {...quote} />
              </div>
            )}
          </div>
          <div>
            <ModuleHeader headline={heading} />
            {reevoo && (
              <div>
                {reevooEditButton && (
                  <EditButton html={reevooEditButton} isSmall={false} align="right" />
                )}
                <Reevoo {...reevoo} />
              </div>
            )}
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

ReevooQuote.defaultProps = {
};

ReevooQuote.propTypes = {
  theme: PropTypes.object,
  quote: PropTypes.object,
  reevoo:PropTypes.object,
  heading:PropTypes.string,
  id:PropTypes.string,
  quoteEditButton:PropTypes.string,
  reevooEditButton:PropTypes.string
};

module.exports = ReevooQuote;
