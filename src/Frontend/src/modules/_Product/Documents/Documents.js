import React from 'react';
import PropTypes from 'prop-types';
import List from '../../_ContentModules/List/List';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';
import EditableParagraph from '../../Editable/EditableParagraph';
import Editable from '../../Editable/Editable';
import EditableImage from '../../Editable/EditableImage';

import './Documents.scss';

/** Documents
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Documents, {
    id : 'id',
    theme : {
      themeName : 'warm-grey'
    },
    label : 'Policy Documents',
    icon : '<img src="dist/images/cms/PolicyDocs-160x120.svg" />',
    headline : "A little extra info never hurt anyone, have a read of our policy documents",
    copy : "You can get this and other documents from us in Braille, large print or audio by contacting us",
    links : [
      '<a href="#">Policy summary (142KB)</a>',
      '<a href="#">Plan conditions (136KB)</a>',
      '<a href="#">Our services (27.3KB)</a>'
    ]
  }), document.getElementById("element"));
*/
class Documents extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {copy, links, label, headline, icon} = this.props;

    const baseClass = 'documents';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div className="columns@md documents__main">
          <div>
            <Editable tag="h2" html={label} className="label" />
            <EditableParagraph className={`h2 title ${baseClass}__title`} html={headline} />
            <EditableParagraph html={copy} />
          </div>
          <div>
            <EditableImage className="icon documents__icon" image={icon.html} imageSize={icon.size}/>
            <List items={links} />
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

Documents.defaultProps = {
  headline: '',
  label: '',
  icon: '',
  copy: '',
  links:  null,
  id:'documents'
};

Documents.propTypes = {
  headline: PropTypes.string,
  icon: PropTypes.string,
  label: PropTypes.string,
  copy: PropTypes.string,
  links: PropTypes.array,
  id:PropTypes.string
};

module.exports = Documents;
