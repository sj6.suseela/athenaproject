import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../../Editable/Editable';
import EditableParagraph from '../../Editable/EditableParagraph';

import './ModuleHeader.scss';

class Header extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {icon, label, headline, intro, isCompact, align,themeColour} = this.props;

    if(!label && !headline && !intro) {
      return ('');
    }

    const baseClass = 'head';
    const additionalClasses = isCompact ? ` ${baseClass}--compact` : '';
    const alignClass = align ? `align-${align}` : '';

    return (
      <div className={`${baseClass} ${additionalClasses} ${alignClass} theme-text-${themeColour}`}>
        <Editable className="icon" html={icon} />
        <div className={`${baseClass}__main`}>
          <Editable tag="h2" html={label} className="label" />
          <EditableParagraph className="title h2" html={headline} />
          <EditableParagraph className="lead" html={intro} />
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  icon: '',
  align: '',
  label: '',
  headline: '',
  intro: '',
  isCompact: false,
  themeColour:'core'
};

Header.propTypes = {
  icon: PropTypes.string,
  align: PropTypes.string,
  label: PropTypes.string,
  headline: PropTypes.string,
  intro: PropTypes.string,
  isCompact: PropTypes.bool,
  themeColour:PropTypes.string
};

module.exports = Header;
