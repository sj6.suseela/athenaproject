import React from 'react';
import PropTypes from 'prop-types';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';
import EditableParagraph from '../../Editable/EditableParagraph';
import EditableDiv from '../../Editable/EditableDiv';
import Editable from '../../Editable/Editable';
import Accordion from '../../Accordion/Accordion';
import Table from '../../Table/Table';

import './UnitPrice.scss';

/** UnitPrice
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.UnitPrice, {
    id : 'id',
    theme : {},
    updated: `Averaged price as at 08 January 2018`,
      items : [item, item, item],
      notes : {
        title: 'Fund Value',
        intro: 'Fund value as at 31 December 2016 £131.6 million',
        heading: 'Fund notes',
        children: `
      <p>The RNPFN One ISA and RNPFN With Profits Bond are unitised with-profits products. Regular bonuses declared are added by increasing the unit price.</p>
      <p>Your annual statement will show you the number of units held at the statement date and the value of a policy can be calculated by multiplying this by the current unit price. Please be aware that the actual value can be affected by the following:</p>
      <ul>
        <li>Changing number of units (through monthly policy charges, or withdrawals and regular savings).</li>
        <li>Market Value Reduction which, if applied, would reduce the value.</li>
        <li>Final Bonus - which would increase the value.</li>
      </ul>`
      },
      historicalTitle: `Fund Price History`,
      historicalInfo: `Historical prices as at end of year`,
      historical: `<table>
                <thead>
                  <th></th>
                  <th>2016</th>
                  <th>2015</th>
                  <th>2014</th>
                  <th>2013</th>
                  <th>2012</th>
                  <th>2011</th>
                  <th>2010</th>
                  <th>2009</th>
                </thead>
                <tbody>
                  <tr>
                    <th>Bid Price</th>
                    <td>£4,806</td>
                    <td>£4,806</td>
                    <td>£4,806</td>
                    <td>£4,806</td>
                    <td>£4,806</td>
                    <td>£4,806</td>
                    <td>£4,806</td>
                    <td>£4,806</td>
                  </tr>
                  <tr>
                    <th>Growth</th>
                    <td>13.2%</td>
                    <td>13.2%</td>
                    <td>13.2%</td>
                    <td>13.2%</td>
                    <td>13.2%</td>
                    <td>13.2%</td>
                    <td>13.2%</td>
                    <td>13.2%</td>
                  </tr>
                </tbody>
              </table>
              `,
  }), document.getElementById("element"));
*/
class UnitPrice extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {updated, heading, items, notes, historical, historicalTitle, historicalInfo} = this.props;

    // base css class
    const baseClass = 'unit-price';

    const options = items.map((item,i) => {
      return (
        <div key={i} className={`${baseClass}-item`}>
          <div className={`${baseClass}-item__section`}>
            <p><b><Editable html={item.title} /></b> <Editable html={item.price} /></p>
          </div>
          {item.additional && (
            <div className={`${baseClass}-item__section`}>
              <EditableDiv html={item.additional} />
            </div>
          )}
        </div>
      );
    });

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <Editable tag="h2" html={heading} />
        <EditableParagraph html={updated} />
        <div className={`${baseClass}__items`}>
          {options}
        </div>
        {historical && (
          <div>
            {historicalTitle && (
              <div className="table-caption">
                <Editable html={historicalTitle} />
                <Editable tag="small" html={historicalInfo} />
              </div>
            )}
            <Table className={`${baseClass}__table`} content={historical} noStripes={true}/>
          </div>
        )}
        {notes && (
          <div>
            <Editable tag="h3" html={notes.title} />
            <EditableParagraph html={notes.intro} />
            {notes.children && (
            <Accordion closeSiblings={false} items={[notes]} modifier="" />
            )}
          </div>
        )}
      </ModuleWrapper>
    );
  }
}

UnitPrice.defaultProps = {
  id:'unitprice',
  updated:'',
  heading:'',
  items:[],
  notes:null,
  historicalTitle:'',
  historicalInfo:'',
  historical:''
};

UnitPrice.propTypes = {
  id:PropTypes.string,
  updated:PropTypes.string,
  heading:PropTypes.string,
  items:PropTypes.array,
  notes:PropTypes.object,
  historicalTitle:PropTypes.string,
  historicalInfo:PropTypes.string,
  historical:PropTypes.string
};

module.exports = UnitPrice;
