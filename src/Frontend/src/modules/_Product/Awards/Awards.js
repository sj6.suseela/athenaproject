import React from 'react';
import PropTypes from 'prop-types';

import Quote from '../../_ContentModules/Quote/Quote';
import ModuleHeader from '../ModuleHeader/ModuleHeader';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';
import SitecoreSettings from '../../SitecoreSettings';
import EditableImage from '../../Editable/EditableImage';
import EditableDiv from '../../Editable/EditableDiv';
import EditButton from '../../CMS/EditButton';

import './Awards.scss';

/** Awards
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Awards, {
    id : 'id',
    theme : {
      themeName : 'warm-grey'
    },
    label: 'Our Awards',
    headline: "You can be confident that we're right for the job",
    items: [
      {
        copy: '<p><b>5 Star cover</b> for behind the wheel</p>',
        image: '<img src="dist/images/defacto.png" alt="" />'
      },
      {
        copy: "<p>We've been named most recommended insurer for <b>13 years in a row</b></p>",
        image: '<img src="dist/images/yougov.png" alt="" />'
      },
      {
        copy: "Proud to be the consumer champion's <b>best insurance brand</b>",
        image: '<img src="dist/images/which.png" alt="" />'
      }
    ],
    quote: {
      copy:"Very satisfied with the way they responded to my calls, also documentation summary is clear and easy to download and store.",
      author:"Tim Warren",
      role:"St Albans",
      source: '<img src="dist/images/reevoo.png" alt="Reevoo" />'
    }
  }), document.getElementById("element"));
*/
class Awards extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing:false
    };
  }

  componentDidMount () {
    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  render () {
    const {items, quote, editButton} = this.props;

    const baseClass = 'awards';
    const isEditing = this.state.isEditing;
    let awards = '';
    const hasQuote = !!quote && !!quote.copy;
    const classes = !hasQuote ? `${baseClass}--no-quote` : '';

    if(items) {
      awards = items.map((item,i) => {
        const href = (item.link && item.link.href) ? item.link.href : null;
        const link = (item.link && item.link.html) ? item.link.html : null;
        const image = item.image;

        return (<li key={i}>
            <EditableDiv className={`${baseClass}__copy`} html={item.copy} />
            {isEditing && (
              <div className={`${baseClass}__logo`}>
                <EditableDiv html={link} />
                <EditableImage image={image.html} imageSize={image.size}/>
              </div>
            )}
            {!isEditing && (
              <div className={`${baseClass}__logo`}>
                {link && (
                  <a href={href}>
                    <EditableImage image={image.html} imageSize={image.size}/>
                  </a>
                )}
                {!link && (
                  <EditableImage image={image.html} imageSize={image.size}/>
                )}
              </div>
            )}
          </li>
        );
      });
    }

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true} classes={classes}>
        <div className={hasQuote ? 'columns@md' : ''}>
          <ModuleHeader {...this.props} />
        </div>
        <div className={hasQuote ? 'columns@md' : ''}>
          <div className="awards__content">
            <ul>
              {awards}
            </ul>
          </div>
          {hasQuote && (
            <div className="awards__quote">
              {editButton && (
                <EditButton html={editButton} isSmall={false} align="right" />
              )}
              <Quote theme="dusty-mint" {...quote} />
            </div>
          )}
        </div>
      </ModuleWrapper>
    );
  }
}

Awards.defaultProps = {
  headline : '',
  label : '',
  icon : '<img src="dist/images/cms/Awards-80x80.svg" />',
  items : [],
  quote : null,
  editButton : ''
};

Awards.propTypes = {
  headline : PropTypes.string,
  icon : PropTypes.string,
  label : PropTypes.string,
  items : PropTypes.array,
  quote : PropTypes.object,
  editButton : PropTypes.string,
  id : PropTypes.string
};

module.exports = Awards;
