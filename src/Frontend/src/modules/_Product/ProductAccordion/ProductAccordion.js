import React from 'react';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';
import ModuleHeader from '../ModuleHeader/ModuleHeader';
import PropTypes from 'prop-types';
import Editable from '../../Editable/Editable';
import Accordion from '../../Accordion/Accordion';

import './ProductAccordion.scss';

class ProductAccordion extends React.Component {
  constructor (props) {
    super(props);
  }
  
  render () {
    const {title, subTitle, heading, description, items} = this.props;
    const baseClass = 'product-accordion';

    const accordionItems = items.map((item) => {
      return {
        ...item,
        children: item.copy
      };
    });

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true} hasSpacer={true}>
        <ModuleHeader baseClass={baseClass} headline={title} intro={subTitle} />
        <Editable html={heading} tag='h3' />
        <Editable html={description} tag='p' />
        <Accordion items={accordionItems} modifier="product" />
      </ModuleWrapper>
    );
  }
}
  
ProductAccordion.defaultProps = {
  title: '',
  subTitle: '',
  heading: '',
  description: '',
  theme: {},
  items: []
};
  
ProductAccordion.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  heading: PropTypes.string,
  description: PropTypes.string,
  theme: PropTypes.object,
  items: PropTypes.array.isRequired
};

module.exports = ProductAccordion;