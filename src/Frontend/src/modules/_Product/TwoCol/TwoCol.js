import React from 'react';
import PropTypes from 'prop-types';

import ModuleHeader from '../ModuleHeader/ModuleHeader';
import EditableDiv from '../../Editable/EditableDiv';

import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import './TwoCol.scss';

/** TwoCol
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.TwoCol, {

  }), document.getElementById("element"));
*/
class TwoCol extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {
      heading,
      label,
      bodyLeft,
      bodyRight,
      bodyLeftBulletStyle,
      bodyRightBulletStyle
    } = this.props;
    const baseClass = 'twocol';

    const leftClass = bodyLeftBulletStyle === 'green' ? 'bulleted' : '';
    const rightClass = bodyRightBulletStyle === 'green' ? 'bulleted' : '';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <ModuleHeader headline={heading} label={label} align="center" />
        <div className={`columns@md ${baseClass}__main`}>
          <div>
            <EditableDiv className={leftClass} html={bodyLeft} />
          </div>
          <div>
            <EditableDiv className={rightClass} html={bodyRight} />
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

TwoCol.defaultProps = {
  bodyLeft: '',
  bodyRight: '',
  bodyLeftBulletStyle: 'black',
  bodyRightBulletStyle: 'black'
};

TwoCol.propTypes = {
  heading: PropTypes.string,
  label: PropTypes.string,
  bodyLeft: PropTypes.string,
  bodyRight: PropTypes.string,
  bodyLeftBulletStyle: PropTypes.string,
  bodyRightBulletStyle: PropTypes.string
};

module.exports = TwoCol;
