import React from 'react';
import PropTypes from 'prop-types';

import ModuleHeader from '../ModuleHeader/ModuleHeader';

import Editable from '../../Editable/Editable';
import EditableParagraph from '../../Editable/EditableParagraph';
import EditableImage from '../../Editable/EditableImage';
import ArrowLink from '../../ArrowLink/ArrowLink';

import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import './BlockLinks.scss';

/** BlockLinks
  * @description description
  * @example

  var item = {
    label : 'Our advisers',
    heading : 'A big part of what makes LV= different is our advisors, lorem ipsum dolor set amet consident',
    cta : '<a href="#">Meet the team</a>',
    icon : '<img src="http://placehold.it/300x150" />'
  };

  ReactDOM.render(React.createElement(Components.BlockLinks, {
    id : 'id',
    theme : {
      themeName : 'warm-grey'
    },
    layout : 'featured',
    label : 'Why choose us',
    heading : 'Join the XXX other people who have benefited from the experience of our advice team.',
    items : [item, item, item]
  }), document.getElementById("element"));

  * @example
  ReactDOM.render(React.createElement(Components.BlockLinks, {
    id : 'id',
    theme : {
      themeName : 'cool-grey'
    },
    layout : 'inline',
    label : 'Why choose us',
    heading : 'Join the XXX other people who have benefited from the experience of our advice team.',
    items : [item, item, item]
  }), document.getElementById("element"));
*/
class BlockLinks extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };

    this.items = props.items.map((item,i) => {
      const featured = (i === 0 && props.layout === 'featured') ? 'featured' : '';

      if(i === 0 && props.layout === 'featured') {
        item.heading = item.content;
        item.content = null;
      }

      const hasImage = !!item.icon;
      const imageCss = hasImage ? 'image' : '';

      return(
        <div key={props.id + i + item.label + item.heading} className={`blocklinks__item ${featured} ${imageCss}`}>
          <EditableImage className="blocklinks__item__icon" image={item.icon} />
          <div className="blocklinks__item__info">
            <Editable tag="h3" className="blocklinks__item__label label" html={item.label} />
            <Editable tag="h4" className="blocklinks__item__heading" html={item.heading} />
            <EditableParagraph className="blocklinks__item__content" html={item.content} />
            <ArrowLink className="blocklinks__item__cta" link={item.cta} />
          </div>
        </div>
      );
    });
  }

  render () {
    // get the prop settings
    const {label, heading, theme, layout} = this.props;

    // base css class
    const baseClass = 'blocklinks';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} classes={`${baseClass}--${layout}`} isProduct={true}>
        <ModuleHeader headline={heading} label={label} align="center" themeColour={theme.textColour}/>
        <div className="blocklinks__items">
           {this.items}
        </div>
      </ModuleWrapper>
    );
  }
}

BlockLinks.defaultProps = {
  id:'blocklinks',
  items:[]
};

BlockLinks.propTypes = {
  theme: PropTypes.object,
  label: PropTypes.string,
  heading: PropTypes.string,
  items: PropTypes.array,
  content:PropTypes.string,
  layout:PropTypes.string,
  backgroundImage: PropTypes.string,
  id:PropTypes.string
};

module.exports = BlockLinks;
