import React from 'react';
import PropTypes from 'prop-types';

import EditableImage from '../../Editable/EditableImage';
import Editable from '../../Editable/Editable';
import EditableDiv from '../../Editable/EditableDiv';

import Quote from '../../_ContentModules/Quote/Quote';
import ModuleHeader from '../ModuleHeader/ModuleHeader';
import Cta from '../../Cta/Cta';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import './ImportantInformation.scss';

/** ImportantInformation
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ImportantInformation, {
    id : 'id',
    theme : {themeName:'warm-grey'},
    label: 'Important Information',
    headline : "Here’s the need to know info, so you’ve got the full picture",
    ctas:  [
      { type: "primary", link: "<a href=''>Primary</a>"},
      { type: "secondary", link: "<a href=''>Secondary</a>"}
    ],
    image : {
      html: '<img src="dist/images/cms/404-360x140.svg" />',
      src:'dist/images/cms/404-360x140.svg',
      size:null,
      extension:'svg'
    },
    bodyLeft : `<ul>
      <li><b>You can receive a pay out</b> if you’re diagnosed with a terminal illness. We’ll reduce the amount of cover we pay out by 3% to reflect the fact that we’re paying out on your plan even though you haven’t died.</li>
      <li><b>You can't receive a pay</b> out if you’re diagnosed with a terminal illness in the last 12 months of the plan – take our Critical Illness to evade this.</li>
      <li><b>We won’t pay out a claim</b> if you take your own life within the first 12 months of your policy.</li>
    </ul>`,
    bodyRight: `<ul>
      <li><b>Your age, health and any smoking habits</b> will affect the amount of insurance available to you.</li>
      <li><b>After your plan has been issued</b>, you can't change it and if you stop paying your premiums, your insurance may end.</li>
      <li><b>Once your policy ends</b> the insurance stops and you get nothing back. There is no cash in value.</li>
      <li><b>If you pass away</b>, we'll normally pay the lump sum to your estate. Inheritance tax could apply if your estate is worth over £325,000 (tax years 2017/18 to 2020/21).</li>
    </ul>`,
  }), document.getElementById("element"));
*/
class ImportantInformation extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {bodyLeft, bodyRight, quote, image, label, ctas, isLeadParagraph} = this.props;

    const baseClass = 'important-information';

    const leadClass = isLeadParagraph ? `${baseClass}__body--lead` : '';

    let buttons = [];

    if(ctas) {
      buttons = ctas.map((item,i) => {
        return (
          <Cta key={item + i} type={item.type} rawCta={item.link}/>
        );
      });
    }

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <Editable tag="h2" html={label} className="label" />
        <div className="columns@md">
          <div>
            <ModuleHeader {...this.props} label="" />
          </div>
          <div className={`${baseClass}__flourish`}>
            <EditableImage image={image.html} imageSize={image.size} imageExtension={image.extension}/>
          </div>
        </div>
        <div className="columns@md important-information__main">
          <div>
            <EditableDiv className={leadClass} html={bodyLeft} />
            {quote && (
              <Quote theme="dusty-mint" {...quote} />
            )}
          </div>
          <div>
            <EditableDiv html={bodyRight} />
          </div>
        </div>
        {buttons && (
          <div className={`${baseClass}__buttons`}>{buttons}</div>
        )}
      </ModuleWrapper>
    );
  }
}

ImportantInformation.defaultProps = {
  bodyLeft: '',
  bodyRight: '',
  quote: null,
  label: '',
  ctas : null,
  id:'importantInfo',
  image:null
};

ImportantInformation.propTypes = {
  bodyLeft: PropTypes.string,
  bodyRight: PropTypes.string,
  quote: PropTypes.object,
  image: PropTypes.object,
  label: PropTypes.string,
  ctas:PropTypes.array,
  id:PropTypes.string,
  isLeadParagraph: PropTypes.bool
};

module.exports = ImportantInformation;
