import React from 'react';
import PropTypes from 'prop-types';
import ModuleHeader from '../ModuleHeader/ModuleHeader';
import EditableDiv from '../../Editable/EditableDiv';
import EditableImage from '../../Editable/EditableImage';
import Editable from '../../Editable/Editable';
import Cta from '../../Cta/Cta';
import Overlay from '../../Overlay/Overlay';
import OverlayContent from '../OverlayContent/OverlayContent';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';
import EditButton from '../../CMS/EditButton';
import Icon from '../../Icon/Icon';

import './Cover.scss';

/** Cover
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Cover, {
    id : 'id',
    theme:{
      themeName:'mint',
      themeBefore: 'warm-grey',
      themeAfter: 'warm-grey',
      topCurve:{
        type:'convex'
      },
      bottomCurve:{
        type:'convex'
      }
    },
    label:'Overview',
    headline:"So, how do we really stand out from the crowd? ",
    flourish: {
        html:'<img src="./dist/images/placeholder/keys-new.png" />',
        extension:'png'
      },
    items: [
      {
        icon: '<img src="dist/images/cms/icon-pound.png" />',
        copy: "10% Discounts when you buy your car insurance online"
      },
      {
        icon: '<img src="dist/images/cms/icon-pound.png" />',
        copy: "24hour Helpline should you need to make a claim day or night"
      },
      {
        icon: '<img src="dist/images/cms/icon-pound.png" />',
        copy: "5 year Repair guarantee on any repairs we make for added peace of mind "
      }
    ]
  }), document.getElementById("element"));
*/
class Cover extends React.Component {
  constructor (props) {
    super(props);

    this._click = this._click.bind(this);
    this._closeOverlay = this._closeOverlay.bind(this);
    this._clickClose = this._clickClose.bind(this);

    this.statePushed = false;

    this.state = {
      isOverlayOpen: false,
      overlayContent: {
        image: '',
        label: '',
        Heading: '',
        links: []
      }
    };
  }

  componentDidMount () {
    const _this = this;
    if (!SERVER) {
      history.replaceState({}, null, null);

      window.addEventListener('popstate', event => {
        if (event.state === null) {
          event.preventDefault();
          return false;
        }

        _this._closeOverlay();
      });
    }
  }

  _clickClose () {
    if (this.statePushed) {
      window.history.back();
    }
    else {
      this._closeOverlay();
    }
    history.replaceState({}, null, null);
  }

  _click (content, e) {
    if (content) {
      e.preventDefault();
      e.stopPropagation();

      if (!SERVER) {
        // set body width to prevent it jumping when the scroll bar disappears
        const body = document.body;
        const scrollWidth = window.innerWidth - body.offsetWidth;
        body.style.width = `calc(100% - ${scrollWidth}px)`;
      }

      history.pushState({}, null, null);
      this.statePushed = true;

      this.setState({
        isOverlayOpen: true,
        overlayContent: content
      });
    }
  }

  _closeOverlay () {
    if (!SERVER) {
      // reset body width
      document.body.style.width = '100%';
    }

    this.setState({
      isOverlayOpen: false
    });
  }

  render () {
    const {intro, cta, items, flourish} = this.props;

    const baseClass = 'product-cover';

    let hasContent = false;

    const list = items.map((item, i) => {
      if (item.content) {
        hasContent = true;
      }

      const liClass = `${baseClass}__cell${item.content ? ' linked' : ''}`;
      const price = item.price;

      return (
        <li
          key={i}
          className={liClass}
          onClick={e => this._click(item.content, e)}
        >
          <EditableImage
            className={`${baseClass}__icon`}
            image={item.icon.html}
            imageSize={item.icon.size}
          />
          <div className={`${baseClass}__data`}>
            <div className={`${baseClass}__copy links`}>
              {item.content && (
                <a href="#">
                  <Editable html={item.copy} />
                </a>
              )}
              {!item.content && <Editable html={item.copy} />}
            </div>
            {price && (
              <div className={`${baseClass}__price`}>
                <Editable tag="small" html={price.prefix} />
                <Editable html={price.value} />
              </div>
            )}
            {item.editButton && (
              <EditButton html={item.editButton} isSmall={true} align="right" />
            )}
          </div>
          {item.content && (
            <div className={`${baseClass}__link-icon`}>
              <Icon icon="rightarrow" />
            </div>
          )}
        </li>
      );
    });

    let cssClass = '';

    if (flourish && flourish.html) {
      cssClass += ` ${baseClass}--flourish`;
    }

    if (cta) {
      cssClass += ` ${baseClass}--hasButton`;
    }

    if (intro) {
      cssClass += ` ${baseClass}--hasIntro`;
    }

    if (items && items.length < 3) {
      cssClass += ` ${baseClass}--shortItems`;
    }

    return (
      <ModuleWrapper
        {...this.props}
        isProduct={true}
        baseClass={baseClass}
        classes={cssClass}
        hasSpacer={true}
      >
        <div className={`${baseClass}__wrapper`}>
          <div className={`${baseClass}__main`}>
            <div className="columns@md columns@md--wide">
              <div>
                <div className={`${baseClass}__cell`}>
                  <ModuleHeader {...this.props} intro="" isCompact={true} />
                </div>
                {intro && (
                  <div className={`${baseClass}__cell sm`}>
                    <EditableDiv html={intro} className="lead" />
                  </div>
                )}
              </div>
              <div>
                <ul>{list}</ul>
              </div>
            </div>
            {cta && (
              <div className={`${baseClass}__buttons align-center`}>
                <Cta
                  {...cta}
                  type="secondary"
                  hasNoMargin={true}
                  rawCta={cta}
                />
              </div>
            )}
          </div>
          <EditableImage
            image={flourish.html}
            imageExtension={flourish.extension}
            className={`${baseClass}__flourish`}
          />
        </div>

        {hasContent && (
          <Overlay
            isOpen={this.state.isOverlayOpen}
            from="right"
            hasHeader={false}
            width="100%"
            className="overlay"
            onRequestClose={this._clickClose}
          >
            <OverlayContent {...this.state.overlayContent} />
          </Overlay>
        )}
      </ModuleWrapper>
    );
  }
}

Cover.defaultProps = {
  theme: {},
  backgroundImage: '',
  label: '',
  headline: '',
  intro: '',
  copy: '',
  items: [],
  cta: '',
  flourish: {},
  id: 'cover',
  hasExtraTopPadding: false
};

Cover.propTypes = {
  theme: PropTypes.object,
  label: PropTypes.string,
  backgroundImage: PropTypes.string,
  headline: PropTypes.string,
  intro: PropTypes.string,
  copy: PropTypes.string,
  items: PropTypes.array,
  cta: PropTypes.string,
  flourish: PropTypes.object,
  id: PropTypes.string,
  hasExtraTopPadding: PropTypes.bool
};

module.exports = Cover;
