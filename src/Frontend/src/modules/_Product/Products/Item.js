
import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../../Editable/Editable';
import EditableImage from '../../Editable/EditableImage';
import EditableDiv from '../../Editable/EditableDiv';
import Cta from '../../Cta/Cta';
import ArrowLink from '../../ArrowLink/ArrowLink';

import './Item.scss';

/** Item
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Item, {
    id : 'id',
    theme : {}
  }), document.getElementById("element"));
*/
class Item extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {title, image, award, content, quote, more, offer, isBlank} = this.props;
    const baseClass = 'product-item';

    let cssClass = baseClass;
    if (isBlank) {
      cssClass += ` ${baseClass}--blank`;
    }

    const hasInfo = award || offer;

    return (
      <div className={cssClass} ref={(c) => {this.ref = c;}}>
        {!isBlank && (
          <div className={`${baseClass}__main`}>
            {image && (
              <figure className={`${baseClass}__image`}>
                <EditableImage image={image.html} />
                {hasInfo && (
                  <div className={`${baseClass}__info`}>
                    <EditableImage className={`${baseClass}__award`} image={award.html} />
                    {offer && (
                      <p><Editable html={offer.value} /> <Editable html={offer.label} /></p>
                    )}
                  </div>
                )}
              </figure>
            )}
            <div className={`${baseClass}__content`}>
              <Editable tag="h2" className={`${baseClass}__title`} html={title} />
              <EditableDiv className={`${baseClass}__copy`} html={content} />
              <div className={`${baseClass}__footer`}>
                <Cta type="primary" isWide={true} isSmall={true} rawCta={quote}/>
                <ArrowLink link={more} />
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

Item.defaultProps = {
  title: '',
  image: {},
  offer: null,
  award: '',
  content: '',
  quote: '',
  more: ''
};

Item.propTypes = {
  title: PropTypes.string,
  image: PropTypes.object,
  offer: PropTypes.object,
  award: PropTypes.string,
  content: PropTypes.string,
  quote: PropTypes.string,
  more: PropTypes.string,
  isWide:PropTypes.bool,
  isBlank: PropTypes.bool
};

module.exports = Item;
