
import React from 'react';
import PropTypes from 'prop-types';

import Item from './Item';
import ModuleHeader from '../ModuleHeader/ModuleHeader';
import ModuleWrapper from '../../ModuleWrapper/ModuleWrapper';

import './ProductList.scss';

/** ProductList
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ProductList, {
    id : 'id',
    theme : {}
  }), document.getElementById("element"));
*/
class ProductList extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {columns, isFirst, isLast, heading, items} = this.props;
    const baseClass = 'product-list';

    const isFirstClass = isFirst ? `${baseClass}--first` : '';
    const isLastClass = isLast ? `${baseClass}--last` : '';

    // create dummy items to align them correctly
    const extraItems = columns - items.length % columns;
    for (let i = 0; i < extraItems; i++) {
      items.push({isBlank: true});
    }

    const products = items.map((item,i) => {
      return (
        <Item key={i} {...item} />
      );
    });

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} classes={`${baseClass}--${columns}up ${isFirstClass} ${isLastClass}`} isProduct={true}>
        <ModuleHeader headline={heading} align='center' />
        <div className={`${baseClass}__main`}>
          {products}
        </div>
      </ModuleWrapper>
    );
  }
}

ProductList.defaultProps = {
  theme: {},
  columns: 1,
  items: [],
  id:'productlist'
};

ProductList.propTypes = {
  theme: PropTypes.object,
  columns: PropTypes.number,
  items: PropTypes.array,
  id:PropTypes.string,
  isFirst:PropTypes.bool,
  isLast:PropTypes.bool,
  heading:PropTypes.string
};

module.exports = ProductList;
