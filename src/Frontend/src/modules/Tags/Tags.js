import React from 'react';
import PropTypes from 'prop-types';

import './Tags.scss';

import EditableLink from '../Editable/EditableLink';
import Icon from '../Icon/Icon';

/** Tags
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Tags, {
    id : 'id',
    items :[
      { label:"Driving", href:"#" },
      { label:"Lorem", href:"#" },
      { label:"Ipsum", href:"#" }
    ]
  }), document.getElementById("element"));
*/
class Tags extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
    };

    this.tags = props.items.map((item,i) => {
      return <EditableLink key={item.label + i} href={item.href} html={item.label} />;
    });
  }

  render () {
    return (
      <div className="module tags">
        <div className="container">
          <Icon icon="tag" /> {this.tags}
        </div>
      </div>
    );
  }
}

Tags.defaultProps = {
  items:[]
};

Tags.propTypes = {
  items:PropTypes.array
};

module.exports = Tags;
