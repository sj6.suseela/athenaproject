
import React from 'react';
import PropTypes from 'prop-types';

import ModuleCurve from '../ModuleCurve/ModuleCurve';
import EditableImage from '../Editable/EditableImage';
import Module from '../Module';
import SitecoreSettings from '../SitecoreSettings';
import LazyLoad from '../_ThirdParty/LazyLoad';

import {globalSettings} from '../utils.js';

class ModuleWrapper extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isEditing:false
    };
  }

  componentDidMount () {
    if(this.props.hasAnimation) {
      Module.register(this.ref);
    }

    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }

    // Debugging
    if(!SERVER) {
      if(window.location.hostname === 'localhost') {
        // this.ref.classList.add('module--wrapped');
      }
    }
  }

  render () {
    // get the prop settings
    const {theme, baseClass, classes, backgroundImage, children, hasSpacer,id,isProduct} = this.props;

    const hasCurveTop = theme.topCurve && theme.topCurve.type && theme.topCurve.type.length > 0;
    const hasCurveBottom = theme.bottomCurve && theme.bottomCurve.type && theme.bottomCurve.type.length > 0;
    const additionalClasses = `${hasCurveTop ? `module-curve-top-${theme.topCurve.type}` : 'module-noCurveTop'} ${hasCurveBottom ? `module-curve-bottom-${theme.bottomCurve.type}` : 'module-noCurveBottom'} ${theme.bottomCurveBefore ? `module-curve-before-${theme.bottomCurveBefore}` : ''} ${theme.topCurveAfter ? `module-curve-after-${theme.topCurveAfter}` : ''} ${theme.isFirstOnPage ? 'module-first' : ''} `;

    const textColour = theme.textColour ? `theme--text-${theme.textColour}` : '';
    const subHeadingTextColour = theme.subHeadingTextColour ? `theme--subHeadingText-${theme.subHeadingTextColour}` : '';

    const style = {};
    const isEditing = this.state.isEditing;

    if(backgroundImage && !isEditing) {
      const matches = backgroundImage.match(/src="([^"]+)"/);

      if(matches) {
        style.backgroundImage = `url('${matches[1]}')`;
      }
    }

    const productClass = isProduct ? 'module--product' : '';
    const themeClass = theme.themeName ? `theme-${theme.themeName}` : '';

    let lazyLoadImages = true;
    if(!SERVER) {
      lazyLoadImages = globalSettings().site.lazyLoadImages;
    }

    return (
      <div>
        <section className={`module ${baseClass} ${themeClass} ${productClass} ${additionalClasses} ${classes} ${textColour} ${subHeadingTextColour}`} ref={(c) => {this.ref = c;}}>
          {style.backgroundImage && (
            <LazyLoad once height={0} offset={0}  isActive={lazyLoadImages}>
              <div style={style} className={`module-background ${baseClass}__background`}></div>
            </LazyLoad>
          )}
          {!style.backgroundImage && (
            <EditableImage image={backgroundImage} className={`module-background ${baseClass}__background`} />
          )}
          {hasCurveTop && (
            <ModuleCurve id={`${id}-top`} type={theme.topCurve.type} position="top" bgtheme={theme.themeBefore}/>
          )}
          <div className="wrapper">
            <div className={`container ${baseClass}__container`}>
              {children}
            </div>
          </div>
          {hasCurveBottom && (
            <ModuleCurve id={`${id}-bottom`} type={theme.bottomCurve.type} position="bottom" bgtheme={theme.themeAfter}/>
          )}
        </section>
        {hasSpacer && (
          <div className={`${baseClass}__spacer theme-${theme.themeAfter}`}></div>
        )}
      </div>
    );
  }
}

ModuleWrapper.defaultProps = {
  id: null,
  theme: {
    themeName: 'default'
  },
  hasAnimation : true,
  backgroundImage: '',
  children: '',
  baseClass: '',
  classes: '',
  hasSpacer:false,
  isProduct:false
};

ModuleWrapper.propTypes = {
  id : PropTypes.string,
  theme: PropTypes.object,
  hasAnimation: PropTypes.bool,
  backgroundImage: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.string, PropTypes.object]),
  baseClass: PropTypes.string,
  classes: PropTypes.string,
  hasSpacer:PropTypes.bool,
  isProduct:PropTypes.bool
};

module.exports = ModuleWrapper;
