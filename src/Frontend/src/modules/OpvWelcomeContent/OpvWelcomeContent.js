import React from 'react';
import PropTypes from 'prop-types';

import './OpvWelcomeContent.scss';
import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Cta from '../Cta/cta';

class OpvWelcomeContent extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {title, image, header, signinbuttontext, registerbuttontext, orbuttontext} = this.props;
    const baseClass = 'opv-welcome-content';
    return (
      <div className={`${baseClass}`}>

          <div className={`${baseClass}__main`}>
            <div className={`${baseClass}__content`}>
              <Editable tag="h2" className={`${baseClass}__title`} html={title} />
              <EditableDiv
                className={`${baseClass}__copy`}
                html={header}
              />
              <div className={`${baseClass}__footer`}>
                <Cta type="primary" isSmall={true} rawCta={signinbuttontext.html} />
                <Editable tag="span" className={`${baseClass}__ortxt`} html={orbuttontext}/>
                <Cta type="primary" isSmall={true} rawCta={registerbuttontext.html} />
              </div>
            </div>
            {image && (
              <figure className={`${baseClass}__image`}>
                <EditableImage image={image} />
              </figure>
            )}
          </div>

      </div>
    );
  }
}

OpvWelcomeContent.defaultProps = {
  title: '',
  image: '',
  content: '',
  quote: ''
};

OpvWelcomeContent.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  header: PropTypes.string,
  signinbuttontext: PropTypes.object,
  registerbuttontext: PropTypes.object,
  orbuttontext: PropTypes.string
};

module.exports = OpvWelcomeContent;
