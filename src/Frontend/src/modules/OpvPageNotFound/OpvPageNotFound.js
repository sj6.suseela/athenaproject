import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Cta from '../Cta/cta';
import './OpvPageNotFound.scss';

class OpvPageNotFound extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {submit, title, description, descriptionAlter} = this.props;
    const baseClass = 'page-not-found';

    return (
      <div className={`${baseClass}`}>
        <Editable tag="h2" className={`${baseClass}__title`} html={title} />
        <EditableDiv className={`${baseClass}__content`} html={description} />
        <Cta type="primary" isSmall={true} rawCta={submit} />
        <Editable html={descriptionAlter} />
      </div>
    );
  }
}

OpvPageNotFound.defaultProps = {
  submit: '',
  title: '',
  description: '',
  descriptionAlter: ''
};

OpvPageNotFound.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  submit: PropTypes.string,
  descriptionAlter: PropTypes.string
};

module.exports = OpvPageNotFound;
