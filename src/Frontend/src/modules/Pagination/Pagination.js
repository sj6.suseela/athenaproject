import React from 'react';
import PropTypes from 'prop-types';

import './Pagination.scss';

import Editable from '../Editable/Editable';

import Cta from '../Cta/Cta';

/** Pagination
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Pagination, {
    id : 'id',
    theme : {},
    indicator:{
      current:'1',
      total:'12',
      prefix:'Page',
      divider:'of'
    },
    buttons:{
      prev : `<a href="#">Previous</a>`,
      next : `<a href="#">Next</a>`
    }
  }), document.getElementById("element"));
*/
class Pagination extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
    let pages = props.pages;
    if (pages === null) {
      pages = [];
    }

    this.pageList = pages.map((item,i) => {
      return(
        <Editable tag="li" key={item + i} html={item} />
      );
    });
  }

  render () {
    const {indicator,buttons, theme} = this.props;

    const baseClass = 'pagination';
    const showNumbers = false;

    return (
      <div className={`${baseClass} theme-${theme.themeName}`}>
        <div className={`container ${baseClass}__container`}>
          <div className={`${baseClass}__nav`}>
            {indicator && (
              <span className={`${baseClass}__nav__indicator`}><Editable html={indicator.prefix}/> <b>{indicator.current}</b> <Editable html={indicator.divider} /> <b>{indicator.total}</b></span>
            )}
            {showNumbers && (
            <ul className={`${baseClass}__pageList`}>
              {this.pageList}
            </ul>
            )}
            <div className={`${baseClass}__buttons`}>
              <Cta type="secondary" isSmall={true} rawCta={buttons.prev} />
              <Cta type="secondary" isSmall={true} rawCta={buttons.next} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Pagination.defaultProps = {
  theme : {themeName:'cool-grey'},
  pages:[],
  indicator:null,
  buttons:{}
};

Pagination.propTypes = {
  theme : PropTypes.object,
  pages:PropTypes.array,
  indicator:PropTypes.object,
  buttons:PropTypes.object
};

module.exports = Pagination;
