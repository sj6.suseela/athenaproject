import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';

class Date extends React.Component {
  constructor (props) {
    super(props);

    this.state = {

    };
  }

  render () {
    const date = this.props.date;

    if(!date) {
      return '';
    }

    let cssclass = this.props.cssclass;

    const dateTime = `${date.year}-${date.month.number}-${date.day} ${date.time}`;
    const dateText = `<b>${date.day} ${date.month.label}</b> ${date.year}`;

    cssclass += ' date';

    return (
        <time dateTime={dateTime} className={cssclass}><Editable html={dateText} /></time>
    );
  }
}

Date.defaultProps = {
  date: null,
  cssclass: ''
};

Date.propTypes = {
  date:PropTypes.object,
  cssclass:PropTypes.string
};

module.exports = Date;
