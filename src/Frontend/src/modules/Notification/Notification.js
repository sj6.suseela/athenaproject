import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import Icon from '../Icon/Icon';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import './Notification.scss';

/** Notification
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Notification, {
    id : 'id',
    type : 'warning',
    title : `Affected by volcanic eruption in Bali`,
    link : `<a href="#">Read more</a>`
  }), document.getElementById("element"));
*/
class Notification extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {title, link, type} = this.props;
    const baseClass = 'notification';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div className={`${baseClass}__main ${type}`}>
          <Icon icon="warning" />
          <div className={`${baseClass}__title-container`}>
            <Editable className={`${baseClass}__title`} html={title} />
            {!!link.html && (
              <div className={`${baseClass}__link`}>
                <Icon icon='rightarrow' />
                <Editable html={link.html} />
              </div>
            )}
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

Notification.defaultProps = {
  title : '',
  link : {},
  type : 'warning'
};

Notification.propTypes = {
  title : PropTypes.string,
  link : PropTypes.object,
  type : PropTypes.string
};

module.exports = Notification;
