import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import ArrowLink from '../ArrowLink/ArrowLink';
import './NotificationDetail.scss';

/** NotificationDetail
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.NotificationDetail, {
    id : 'id',
    type : 'warning',
    title : `Monarch cease trading`,
    content : `<p>What you need to know about cancellation and compensation if you're travelling or booked with Monarch</p>`,
    link : `<a href="#">Read more</a>`
  }), document.getElementById("element"));
*/
class NotificationDetail extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {title, content, link, type} = this.props;
    const baseClass = 'notification-detail';

    return (
      <div className={`${baseClass} ${type}`}>
        <div className={`${baseClass}__main`}>
          <Editable tag="h3" className={`${baseClass}__title`} html={title} />
          <EditableDiv className={`${baseClass}__content`} html={content} />
          <ArrowLink link={link} />
        </div>
      </div>
    );
  }
}

NotificationDetail.defaultProps = {
  title : '',
  link : '',
  content : '',
  type : 'default'
};

NotificationDetail.propTypes = {
  title : PropTypes.string,
  link : PropTypes.string,
  content : PropTypes.string,
  type : PropTypes.string
};

module.exports = NotificationDetail;
