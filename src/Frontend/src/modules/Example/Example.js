
import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';

import './Example.scss';

/** Example
  * @description description
  * @example

  ReactDOM.render(React.createElement(Components.Example,{
    id : 'id',
    heading : 'example 1',
    theme : {
      themeName : 'mint'
    }
  }), document.getElementById("element"));
*/
class Example extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading} = this.props;
    const baseClass = 'example';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass}>
        <h2>{heading}</h2>
      </ModuleWrapper>
    );
  }
}

Example.defaultProps = {
  heading: ''
};

Example.propTypes = {
  heading: PropTypes.string
};

module.exports = Example;
