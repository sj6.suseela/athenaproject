import React from 'react';
import PropTypes from 'prop-types';

import './primarynav.scss';

import Logo from '../Logo/Logo';
import Search from '../Search/Search';
import MobileNav from '../MobileNav/MobileNav';
import DesktopNav from '../DesktopNav/DesktopNav';
import PartnerLogo from '../PartnerLogo/PartnerLogo';
import EditableLink from '../Editable/EditableLink';
import Editable from '../Editable/Editable';
import Icon from '../Icon/Icon';

/** PrimaryNav
  * @description description
  * @xexample
  ReactDOM.render(React.createElement(Components.PrimaryNav, {
    logo:'<img src="dist/images/lv-logo.png"></img>',
    logoLink: {
      href:"index.html",
      rel:"noreferrer",
    },
    search:{
      label:"Search",
      placeholderText:"Enter search term here",
      charSearchLimit:3,
      ajaxUrl:`dist/json/search-results.json`,
      link:{
        href:"search-results.html",
        html:null,
        id:"F9A377A42CDC47F99BDA779609DA6FB1-2C7D3C1889C349709B1656B45914154C/search-page",
        label:"",
        rel:""
      },
      linkRel:"noreferrer",
      searchLabels:{
        noSearch:'Popular Searches',
        results:'Top Search Results',
        noResults: 'No Results Found'
      },
      viewAllLabel:'View all results',
      searchImage:'dist/images/cms/flourish-insurance.svg',
      noResultsText: '<p>Sorry, your search did not match any results.</p><p><b>Suggestions:</b><ul><li>foo</li><li>bar</li></ul></p><p>Lpoohods aehfa ihf acuaeffuae fquagaueguaedgf y</p>',
      items:[
        `<a href='#'>Lorem ipsum dolor</a>`,
        `<a href='#'>Lorem ipsum dolor</a>`,
        `<a href='#'>Lorem ipsum dolor</a>`,
      ]
    },
    partner:null,
    contactUs: {
      label: 'Contact Us',
      link: {href:'#'},
    },
    primarynav:{
      groups:[
        {
          label:"Insurance",
          mobileLabel: "Insurance (Mobile)",
          link:{
            href:"/insurance",
          },
          visible:true,
          position:0,
          groupOnMobile:true,
          items:[
            {
              label:"Life Insurance",
              mobileLabel:"Life",
              link: {
                href:"product-car.html",
                rel:"noreferrer"
              }
            },
            {
              label:"Investments",
              link: {
                href:"#",
              },
            },
            {
              label:"Penstions & Retirement",
              link: {
                href:"product-life.html"
              },
            },
          ]
        },

        {
          label:"More",
          isOverflow:true,
          position:3,
          link: {
          href:"#",
          },
          visible:false
        }
      ]
    }
  }), document.getElementById("element"));
*/
class PrimaryNav extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      isInvertedColour: !!props.isInvertedColour
    };
  }

  componentDidMount () {
    if (!SERVER) {
      let txtColour;
      if (document && document.cookie.indexOf('TextColour') > -1) {
        txtColour = document.cookie.substring(document.cookie.indexOf('TextColour')).split(';')[0].split('=')[1];
        if (txtColour) {
          this.setState({
            isInvertedColour: txtColour !== 'white'
          });
        }
      }
    }
  }

  render () {
    const {search, primarynav, optionalTitle} = this.props;

    const hasPartnerLogo = this.props.partner;

    const isMini = !!this.props.isMini;

    const baseClass = 'nav nav--primary';

    const hasSearch = search !== null;

    const hasNav = primarynav !== null && primarynav.groups.length > 0;

    const isInvertedTextColour = !!this.state.isInvertedColour;

    const logoProps = {
      img: this.props.logo,
      width: this.props.logoWidth,
      link: this.props.logoLink,
      type:this.props.logoType
    };

    if (this.props.logoTag) {
      logoProps.tag = this.props.logoTag;
    }

    return (
      <div className={`${baseClass}`}>
        <Logo {...logoProps}/>
        {hasPartnerLogo && (
          <PartnerLogo img={this.props.partner.logo} href={this.props.partner.logoHref}/>
        )}
        {optionalTitle !== '' && <Editable tag="h1" html={optionalTitle} />}
        {!isMini && hasNav && (
          <DesktopNav items={primarynav.groups} isInvertedColour={isInvertedTextColour}/>
        )}
        {!isMini && this.props.contactUs && (
        <div className={`nav--primary__contact${this.props.contactUs.selected ? ' selected' : ''}${isInvertedTextColour ? ' invertColours' : ''}`}>
            <div className='nav--primary__contact__icon'><Icon icon='contact' /></div>
            <EditableLink {...this.props.contactUs.link} html={this.props.contactUs.label} />
        </div>
        )}
        {!isMini && hasSearch && (
          <Search link={search.link} otherBusinessText={search.otherBusinessText} isInvertedColour={isInvertedTextColour} viewAllLabel={search.viewAllLabel} ajaxUrl={search.ajaxUrl} label={search.label} placeholderText={search.placeholderText} items={search.items} charSearchLimit={search.charSearchLimit} searchLabels={search.searchLabels} searchImage={search.searchImage} noResultsText={search.noResultsText}/>
        )}
        {!isMini && hasNav && (
          <MobileNav isInvertedColour={isInvertedTextColour} items={primarynav.groups}/>
        )}

      </div>
    );
  }
}

PrimaryNav.defaultProps = {
  isInvertedColour: false,
  logoType:'default'
};

PrimaryNav.propTypes = {
  search: PropTypes.object,
  logo: PropTypes.string,
  logoType:PropTypes.string,
  partner: PropTypes.object,
  primarynav: PropTypes.object,
  logoLink: PropTypes.object,
  logoWidth: PropTypes.any,
  isInvertedColour:PropTypes.bool,
  isMini:PropTypes.bool,
  contactUs: PropTypes.object,
  logoTag: PropTypes.string,
  optionalTitle: PropTypes.string
};

module.exports = PrimaryNav;
