import React from 'react';
import PropTypes from 'prop-types';

import './headerBanner.scss';

import HeaderHeadline from './HeaderHeadline';

import ResponsiveImage from '../ResponsiveImage/ResponsiveImage';

import Date from '../Date/Date';
import Icon from '../Icon/Icon';
import Cta from '../Cta/Cta';
import SearchBox from '../SearchBox/SearchBox';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditableParagraph from '../Editable/EditableParagraph';
import ModuleCurve from '../ModuleCurve/ModuleCurve';

import Contact from '../Contact/Contact';
import EditButton from '../CMS/EditButton';

/** HeaderBanner
  * @description description
  * @example
  ReactDOM.render(React.createElement(PriorityComponents.HeaderBanner, {
    id : 'id',
    theme: {
      themeName : 'blackcurrant'
    },
    label : "",
    labelLink : '<a href="#">Articles</a>',
    article:{
      headline : "Flight cancelled? How to claim and what you're covered for.",
      date : { day:12, month:{ label:"December", number:12, }, year:2017, time:"12:00" },
      readTime : "5 min read",
      categoryTags :[
        { label:"Driving", href:"#" },
        { label:"Lorem", href:"#" },
        { label:"Ipsum", href:"#" }
      ],
      categoryHref : "http://www.category.com"
    }
  }), document.getElementById("element"));
*/
class HeaderBanner extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      visible:true,
      imageVisible:false
    };

    this.ctaList = [];
    if(props.ctas) {
      this.ctaList = props.ctas.map((item,i) => {
        return (
          <Cta key={item.link + i} {...item.link} type={item.type} rawCta={item.link}/>
        );
      });
    }
  }


  componentDidMount () {
    const imageSrc = this.props.mobImage && this.props.mobImage.src ? this.props.mobImage.src : this.props.desktopImage && this.props.desktopImage.src ? this.props.desktopImage.src : null;
    if(imageSrc !== null) {
      const image = new Image();
      image.src = imageSrc;
      image.onload = () => {
        this.setState({
          imageVisible:true
        });
      };
    }
  }


  render () {
    const {beta, overlayImage, theme, desktopImage, mobImage, article, label, labelLink, headline, subheading,isProductHeader,isHomepageHeader,isGlobalHomepageHeader,isAltHeader,hasSecNav,contact,isLeftAligned,search, subtext, link, isMobileContentUnderBanner, editButton} = this.props;

    const baseClass = 'headerBanner';

    let className = baseClass;

    if(beta) {
      className += ` ${baseClass}--beta`;
    }

    if(hasSecNav) {
      className += ` ${baseClass}--hasSecNav`;
    }

    if(isLeftAligned) {
      className += ` ${baseClass}--alignLeft`;
    }

    if(theme.themeName) {
      className += ` ${baseClass}--${theme.themeName} ${baseClass}--overlay`;
    }
    else{
      className += ` ${baseClass}--petroleum ${baseClass}--overlay ${baseClass}--overlay-default`;
    }

    if(theme.textColour) {
      className += ` theme--text-${theme.textColour}`;
    }

    if(theme.subHeadingTextColour) {
      className += ` theme--subHeadingText-${theme.subHeadingTextColour}`;
    }

    if(theme.mobileTextColour) {
      className += ` theme--text-${theme.mobileTextColour}-mobile`;
    }

    if(theme.mobileSubHeadingTextColour) {
      className += ` theme--subHeadingText-${theme.mobileSubHeadingTextColour}-mobile`;
    }

    if(!desktopImage) {
      className += ` ${baseClass}--noImage`;
    }

    if(isProductHeader) {
      className += ` ${baseClass}--product`;
    }

    if(isMobileContentUnderBanner) {
      className += ` ${baseClass}--mobileContentUnderBanner`;
    }

    if(isGlobalHomepageHeader) {
      className += ` ${baseClass}--global-home`;
    }
    else if(isHomepageHeader) {
      className += ` ${baseClass}--home`;
    }

    if(isAltHeader) {
      className += ` ${baseClass}--alt`;
    }

    const hasHeadline = !!headline;

    if(hasHeadline) {
      className += ` ${baseClass}--headlineOnly`;
    }

    const isVisibleClass = this.state.visible ? 'visible' : '';

    const hasSearch = !!search;

    const headerLabel = labelLink ? labelLink : label;

    const isArticle = !!article;
    const hasOverlayImage = overlayImage !== null && overlayImage !== undefined;
    const hasDate = !!article && !!article.date && article.date.day.toString().length > 0;
    const hasReadTime = !!article && !!article.readTime && article.readTime.length > 0;

    const hasMobileTheme = theme.themeAfterMobile === 'white';
    // currently only styled correctly when white
    if (hasMobileTheme) {
      className += ` theme-mobile-${theme.themeAfterMobile}`;
    }

    const curveTheme = theme.themeAfter ? theme.themeAfter : 'white';
    if(theme.themeAfter) {
      className += ` theme-${theme.themeAfter}`;
    }

    const hasMeta = hasDate || hasReadTime;

    const dImage = (desktopImage !== null) ? desktopImage : {html:null,src:null,altText:null,size:null};
    const mImage = (mobImage !== null) ? mobImage : {html:null,src:null,altText:null,size:null};

    return (
      <div className={`${className} ${isVisibleClass}`}>
        <div className="headerBanner__image">
          {hasOverlayImage && (
            <div className="headerBanner__overlayImage">
              <EditableDiv className="container" html={overlayImage.html} />
            </div>
          )}
          <ResponsiveImage visible={this.state.imageVisible} hasHeaderMask={true} isHomepageHeader={isHomepageHeader || isGlobalHomepageHeader} desktopImage={dImage.html || undefined} desktopImageSrc={dImage.src || undefined} mobImage={mImage.html || undefined} mobImageSrc={mImage.src || undefined} altText={dImage.altText || undefined} cssclass="bg-image" bgtheme={curveTheme} isBackground={true} mobImageSize={mImage.size || undefined} desktopImageSize={dImage.size || undefined}  hasLazyLoad={false}/>
        </div>

        <div className="container">
        <EditButton isSmall={true} align="right" html={editButton} />
        {isArticle && (
          <div className={'headerBanner__body'}>
            
            <Editable className="headerBanner__category" html={headerLabel} />
            <Editable tag="h1" className="headerBanner__body__headline" html={article.headline} />
            <EditableParagraph html={subheading} />
            {hasMeta && (
              <div className="headerBanner__body__meta">
                {hasDate && (
                  <Date date={article.date} cssclass="headerBanner__body__date"/>
                )}
                {hasReadTime && (
                  <p className="headerBanner__body__readingTime">
                    <span className="readtime">
                      <Icon icon="clock" />
                      <Editable html={article.readTime} />
                    </span>
                  </p>
                )}
              </div>
            )}
          </div>
        )}
        
        {hasHeadline && (
          <div className="headerBanner__body">

            <HeaderHeadline isAltHeader={isAltHeader} headerLabel={headerLabel} headline={headline} beta={beta} />
            <EditableParagraph html={subheading} />
            
            {contact && (
              <Contact {...contact} />
            )}
            <EditableDiv className='headerBanner__subtext' html={subtext} />
            {this.ctaList}
            {link && (
              <p className='headerBanner__link links'><Editable html={link} /></p>
            )}
            {hasSearch && (
              <SearchBox {...search}  />
            )}
          </div>
        )}
        </div>
        {hasMobileTheme && (
          <ModuleCurve type='convex' position="bottom" bgtheme={theme.themeAfter}/>
        )}
      </div>
    );
  }
}

HeaderBanner.defaultProps = {
  theme: {
    themeAfter: 'white',
    textColour:'white',
    mobileTextColour:'white'
  },
  ctas: null,
  contact: null,
  desktopImage:{},
  mobImage:{}
};

HeaderBanner.propTypes = {
  theme: PropTypes.object,
  contact: PropTypes.object,
  ctas: PropTypes.array,
  hasSecNav: PropTypes.bool,
  article: PropTypes.object,
  desktopImage: PropTypes.object,
  mobImage: PropTypes.object,
  label: PropTypes.string,
  labelLink: PropTypes.string,
  headline:PropTypes.string,
  subheading:PropTypes.string,
  isProductHeader:PropTypes.bool,
  isHomepageHeader:PropTypes.bool,
  isAltHeader:PropTypes.bool,
  overlayImage:PropTypes.object,
  isLeftAligned:PropTypes.bool,
  search:PropTypes.object,
  subtext:PropTypes.string,
  link:PropTypes.string,
  isMobileContentUnderBanner:PropTypes.bool,
  isGlobalHomepageHeader: PropTypes.bool,
  editButton:PropTypes.string,
  beta: PropTypes.object
};

module.exports = HeaderBanner;
