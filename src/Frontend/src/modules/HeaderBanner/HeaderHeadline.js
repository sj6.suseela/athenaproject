import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';

class HeaderHeadline extends Component {

  componentDidMount () {

  }

  getBetaTag () {
    const {beta} = this.props;
    if (!beta || SERVER) return null;

    const div = document.getElementById('nebula_div_btn');

    if (div === null) {
      console.warn('Kamyple feedback ribbon is not present - please liaise with the GTM owner to ensure this is injected into the page');
    }
    else {
      this.button = div.querySelector('.kampyle_button-text');
      this.button.style.opacity = 0;
    }

    return <span>
      <img className="headerBanner__beta-tag" src="/dist/images/icon/beta-badge.svg" alt={beta.betaTxt} title={beta.betaTxt} />
      {div !== null && <a className="headerBanner__beta-feedback sub-text--small" href='#' onClick={this.onFeedbackClick.bind(this)}>{beta.feedbackCta}</a>}
    </span>;
  }

  onFeedbackClick (e) {
    e.preventDefault();

    this.button.click();
  }

  render () {
    const {isAltHeader, headerLabel, headline} = this.props;

    return isAltHeader ?
      <div>
        <Editable className="headerBanner__category" html={headerLabel} />{this.getBetaTag()}
        <Editable tag="h1" className="headerBanner__body__headline" html={headline} />
      </div>
      :
      <div>
        <Editable tag="h1" className="headerBanner__category" html={headerLabel} />{this.getBetaTag()}
        <Editable tag="h2" className="headerBanner__body__headline" html={headline} />
      </div>;
  }
}

HeaderHeadline.defaultProps = {
  headline: '',
  headerLabel: '',
  beta: null
};

HeaderHeadline.propTypes = {
  headline: PropTypes.string,
  headerLabel: PropTypes.string,
  beta: PropTypes.object,
  isAltHeader: PropTypes.bool
};

module.exports = HeaderHeadline;