import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../Icon/Icon';
import Date from '../Date/Date';
import Cta from '../Cta/Cta';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditableLink from '../Editable/EditableLink';
import EditableImage from '../Editable/EditableImage';
import './ArticlePreview.scss';

class ArticlePreview extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {title, category, readTime, image, date, content, cta, isAlt, link} = this.props;

    let categories = category;

    if(categories) {
      if(typeof categories === 'string') {
        categories = [categories];
      }
      categories = categories.join(',');
    }

    const showFooter = date || readTime;
    const baseClass = 'article';

    const hasImage = image && image.html;

    return (
      <article className={`${baseClass} ${isAlt ? (`${baseClass  }--alt`) : '' }`}>
        <div className={`${baseClass}__wrapper`}>
          {hasImage && (
            <figure className={`${baseClass}__media`}>
              <EditableImage image={image.html} imageSize={image.size} imageSrc={image.src}/>
              {categories && (
                <div className={`${baseClass}__category`}>
                  {categories}
                </div>
              )}
            </figure>
          )}
          <div className={`${baseClass}__main`}>
            <div className={`${baseClass}__body`}>
              <h3 className={`links links--multiline ${baseClass}__title`}>
                {link ? (
                <EditableLink {...link} html={title} hasLazyLoad={false}/>
              ) : (
                title
              )}
              </h3>
              <EditableDiv className={`${baseClass}__content`} html={content} />
              <Cta type="secondary" rawCta={cta} isSmall={true} isWide={true} />
            </div>
            {!hasImage && categories && (
              <div className={`${baseClass}__category`}>
                {categories}
              </div>
            )}
            {showFooter && (
              <footer className={`${baseClass}__footer`}>
                <Date date={date} cssclass={`${baseClass}__date`} />
                {readTime && (
                  <span className={`readtime ${baseClass}__readtime`}>
                    <Icon icon="clock" />
                    <Editable html={readTime} />
                  </span>
                )}
              </footer>
            )}
          </div>
        </div>
      </article>
    );
  }
}

ArticlePreview.defaultProps = {
  isAlt : false,
  title: '',
  url: '',
  category: null,
  content : '',
  cta : '',
  readTime: '',
  image: {},
  date:null
};

ArticlePreview.propTypes = {
  isAlt : PropTypes.bool,
  title: PropTypes.string,
  category: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  content : PropTypes.string,
  cta : PropTypes.string,
  readTime: PropTypes.string,
  image: PropTypes.object,
  date:PropTypes.object,
  link: PropTypes.object
};

module.exports = ArticlePreview;
