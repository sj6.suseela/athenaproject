import React from 'react';
import PropTypes from 'prop-types';

import './OpvStartPage.scss';
import Cta from '../Cta/Cta';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';

class OpvStartPage extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      checked: false
    };

    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
  }

  handleCheckboxChange () {
    this.setState({
      checked: !this.state.checked
    });
  }

  render () {
    // get the prop settings
    const {title, content, termsCheckbox, cta} = this.props;
    const baseClass = 'start-page-reg-detail1';

    return (
      <div className={`${baseClass}`}>
        <div className={`${baseClass}__main`}>
          <Editable tag="h2" className={`${baseClass}__title`} html={title} />
          <EditableDiv className={`${baseClass}__content`} html={content} />

          <Cta
            isSmall={true}
            type={cta.type}
            rawCta={cta.link}
            cssclass={this.state.checked ? 'startbtn1' : 'startbtn'}
          />
          <div className={`${baseClass}__checkbox`}>

            <input
              type="checkbox"
              name="termsCheckbox"
              id="termsCheckbox"
              onChange={this.handleCheckboxChange}
              checked={this.state.checked}
              className="chkBox"
            ></input>
            <Editable html={termsCheckbox} />
          </div>
        </div>
      </div>
    );
  }
}

OpvStartPage.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
  termsCheckbox: PropTypes.string,
  cta: PropTypes.object
};

module.exports = OpvStartPage;
