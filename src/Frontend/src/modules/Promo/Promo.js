import React from 'react';
import PropTypes from 'prop-types';

import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';
import EditableParagraph from '../Editable/EditableParagraph';
import Cta from '../Cta/Cta';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import './Promo.scss';

/** Promo
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Promo, {
    id : 'id',
    theme : {},
    heading : "There's plenty more where that came from, so let's stay in touch",
    copy : '',
    icon : '<img src="dist/images/icon/icon-newsletter.svg">',
    cta : '<a href="http://example.com">Sign up here</a>',
  }), document.getElementById("element"));
*/
class Promo extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    const {icon, copy, heading, cta, hasNoMargin, hasNotopMargin} = this.props;

    const baseClass = 'promo';
    let marginClass;
    if (hasNoMargin) {
      marginClass = 'promo--noMargin';
    }
    else if (hasNotopMargin) {
      marginClass = 'promo--noTopMargin';
    }

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} classes={marginClass}>
        <div className="promo__main">
          <span className="promo__icon">
            <EditableImage image={icon.html} imageSize={icon.size}/>
          </span>
          <div className="promo__body">
            <Editable tag="h3" className="promo__title" html={heading} />
            <EditableParagraph html={copy} />
          </div>
          {cta && (
            <div className="promo__footer">
              <Cta {...cta} type="secondary" isSmall={true} noWrap={false} rawCta={cta}/>
            </div>
          )}
        </div>
      </ModuleWrapper>
    );
  }
}

Promo.defaultProps = {
  heading: '',
  copy: '',
  icon: {},
  cta: null,
  hasNoMargin:false
};

Promo.propTypes = {
  heading: PropTypes.string,
  copy: PropTypes.string,
  icon: PropTypes.object,
  cta: PropTypes.string,
  hasNoMargin:PropTypes.bool,
  hasNotopMargin:PropTypes.bool
};

module.exports = Promo;
