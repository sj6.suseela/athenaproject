import React from 'react';
import PropTypes from 'prop-types';
import Overlay from '../Overlay/Overlay';
import EditableDiv from '../Editable/EditableDiv';
import Editable from '../Editable/Editable';
import Icon from '../Icon/Icon';
import EditButton from '../CMS/EditButton';
import {isMobile} from 'react-device-detect';
import './Form.scss';
import {validate, Message} from './validation/validate';
import DocumentLink from '../DocumentLibrary/DocumentLink';

function toDate (str) {
  const date = new Date(str);

  if (date instanceof Date && !isNaN(date)) {
    const day = (date.getDate() + 1 < 10 ? '0' : '') + String(date.getDate() + 1);
    const month = (date.getMonth() + 1 < 10 ? '0' : '') + String(date.getMonth() + 1);
    const year = date.getFullYear();

    return `${day}/${month}/${year}`;
  }

  return str;
}

class Input extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      isOverlayOpen: false,
      hasError: false,
      isInfoOpen: false,
      value: '',
      selectedFile: '',
      label: ''
    };

    if (this.props.type === 'select') {
      for (const option of this.props.options) {
        if (option.selected) {
          this.state.label = option.label;
          this.state.value = option.value;
        }
      }
    }

    this._closeOverlay = this._closeOverlay.bind(this);
  }

  _closeOverlay () {
    this.setState({
      isOverlayOpen: false
    });
  }

  onInfoClick (e) {
    e.preventDefault();

    this.setState({
      isInfoOpen: !this.state.isInfoOpen
    });
  }

  onSelectClick (e) {
    if (!SERVER) {
      if (isMobile) {
        e.preventDefault();

        this.setState({
          isOverlayOpen: true
        });
      }
    }
  }

  onOverlaySelect (value/* , e */) {
    this.input.value = value.value;

    this.setState({
      isOverlayOpen: false,
      value: value.value,
      label: value.label
    });
  }

  @validate
  _onBlur (valid) {
    if(this.props.onBlur) this.props.onBlur.call(this, this.input);
    // console.log('onBlur will get called valid / error');
    this.setState({
      hasError: !valid
    });


    if(this.props.isOpvField) {
      // console.log("this.props:::::::::::",this.props)
      // console.log("this.props.isOpvField::::::::::",this.props.isOpvField)


      // console.log("inside opv-req loop")
      if (document.getElementById('opvmm') && document.getElementById('opvyy')) {
        if ((document.getElementById('opvmm').classList.contains('error')) || (document.getElementById('opvyy').classList.contains('error')) === true) {
          // console.log("inside month & date if loop")
          const value = document.getElementsByClassName('fieldErrors');
          value[2].style.height = '27px';
        }
        if ((document.getElementById('opvmm').classList.contains('valid')) && (document.getElementById('opvyy').classList.contains('valid')) === true) {
          // console.log("inside month & date if loop")
          const value = document.getElementsByClassName('fieldErrors');
          value[2].style.height = '0px';
        }
      }



      // console.log("inside opv-access-code loop")
      if (document.getElementById('oac1')) {
        if (document.getElementById('oac1').classList.contains('error')) {
         // console.log("oac1: error")
        }
      }
      if ((document.getElementById('oac')) && (document.getElementById('oac1'))) {
        if ((document.getElementById('oac').classList.contains('error')) || (document.getElementById('oac1').classList.contains('error'))) {
          // console.log("inside access code if loop")
          const oacval = document.getElementsByClassName("fieldErrors");
          oacval[0].style.height = '27px';
        }
      }



      // if ((document.getElementById("opvyy").classList.contains("error"))&&(document.getElementById("opvmm").classList.contains("valid"))== true){
      //   let value = document.getElementsByClassName("fieldErrors");
      //   value[2].style.height="27px"
      // }
      // if ((document.getElementById("opvyy").classList.contains("valid"))&&(document.getElementById("opvmm").classList.contains("valid"))== true){
      //   let value = document.getElementsByClassName("fieldErrors");
      //   value[2].style.height="0px"
      // }
      // if ((document.getElementById("opvyy").classList.contains("valid"))&&(document.getElementById("opvmm").classList.contains("error"))== true){
      //   let value = document.getElementsByClassName("fieldErrors");
      //   value[2].style.height="27px"
      // }
      // if ((document.getElementById("oac").classList.contains("error"))||(document.getElementById("oac1").classList.contains("error"))){
      //   console.log("inside access code if loop")
      //   let oacval = document.getElementsByClassName("fieldErrors");
      //   oacval[0].style.height="27px"
      // }
    }
  }

  _onChange (e) {
    if(this.props.onChange) this.props.onChange.call(this, e);

    if (e.target.type === 'file') {
      let fileName = '';

      if (this.files && this.files.length > 1) {
        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
      }
      else {
        fileName = e.target.value.split('\\').pop();
      }

      this.setState({
        selectedFile: fileName,
        value: e.target.value
      });
    }
    else {
      if (this.props.options) {
        this.setState({
          value: e.target.value,
          label: this.input.options[this.input.selectedIndex].innerHTML
        });
      }
      else {
        this.setState({
          value: e.target.value
        });
      }
    }
  }

  getField () {
    const {label, id, type, options, isRequired, isAllRequired, placeholder, size, value, data, documentProps, onChange, html, isOpvField} = this.props;

    let optionList = [];

    const atts = {};

    if (type === 'password') {
      atts['data-password'] = true;
    }

    if (type === 'number') {
      atts['data-numeric'] = true;
    }

    if (type === 'email') {
      atts['data-email'] = true;
    }

    if (type === 'checkbox' && isAllRequired) {
      atts['data-checkbox-requireAll'] = true;
    }

    if (isRequired) {
      atts['data-required'] = true;
    }

    if (isOpvField) {
      atts['data-required'] = true;
    }

    if (data.accept) {
      atts.accept = data.accept;
    }

    if (data.accept) {
      atts.accept = data.accept;
    }

    if (data) {
      for (const i in data) {
        if(data[i]) {
          atts[`data-${i}`] = data[i];
        }
      }
    }

    if (type === 'date') {
      atts['date-format'] = 'mm/dd/yyyy';
    }

    switch (type) {
    case 'radio':
      optionList = options.map((option, i) => {
        return (
            <li key={option.value + i}>
              <input {...atts} type="radio" id={id + option.value + i} name={id} value={option.value} defaultChecked={option.selected} />
              <label htmlFor={id + option.value + i}>
                <span></span>
                {option.label}
              </label>
            </li>
        );
      });

      return (<ul>{optionList}</ul>);

    case 'checkbox':

      if (options) {
        optionList = options.map((option, i) => {
          return (
              <li key={option.value + i}>
                <input type="checkbox" id={id + option.value + i} name={id} value={option.value} defaultChecked={option.selected} />
                <label htmlFor={id + option.value + i}>
                  <span>
                    <Icon icon="tick" />
                  </span>
                  {option.label}
                </label>
              </li>
          );
        });

        return (<ul>{optionList}</ul>);
      }
      else {
        return (<ul></ul>);
      }

    case 'native-select':

      optionList = options.map((option, i) => {
        return (
            <option key={option.value + i} value={option.value}>{option.label}</option>
        );
      });

      return <select ref={(c) => { this.input = c; }} onChange={onChange}>{optionList}</select>;

    case 'select':

      optionList = options.map((option, i) => {
        return (
            <option key={option.value + i} value={option.value}>{option.label}</option>
        );
      });

      const selectHTML = options.map((option, i) => {
        const selected = this.state.label === option.label;

        return (
            <li key={option.value + i} className={`${selected ? 'selected' : ''}`} onClick={this.onOverlaySelect.bind(this, option)}>
              {selected && (
                <Icon icon="tick" />
              )}
              <Editable html={option.label} />
            </li>
        );
      });

      const overlayTitle = this.state.label;

      return (<div>
          <select value={this.state.value} ref={(c) => { this.input = c; }} id={id} onClick={(e) => this.onSelectClick(e)} onChange={this._onChange.bind(this)}>{optionList}</select>
          <Overlay
            isOpen={this.state.isOverlayOpen}
            from='right'
            hasHeader={true}
            width='100%'
            className='overlay overlay--form'
            title={overlayTitle}
            onRequestClose={this._closeOverlay}>
            <div>
              <ul>{selectHTML}</ul>
            </div>
          </Overlay>
        </div>);

    case 'textarea':
      return (<textarea ref={(c) => { this.input = c; }} defaultValue={value} className={size} placeholder={placeholder} data-validate name={id} id={id} onBlur={this._onBlur.bind(this)} onChange={this._onChange.bind(this)}></textarea>);
    case 'html':
      return html;
    case 'document-library-document':
      return (<DocumentLink {...documentProps} id={id} onBlur={this._onBlur.bind(this)} isPostBasketItem={true} />);

    default:
      return (
          <input {...atts} ref={(c) => { this.input = c; }} defaultValue={value} className={size} placeholder={`${placeholder ? placeholder : type === 'date' ? 'dd/mm/yyy' : placeholder}`} data-validate type={type} name={id} id={id} required={isRequired} onBlur={this._onBlur.bind(this)} onChange={this._onChange.bind(this)} />
      );
    }
  }

  render () {
    const {id, type, isRequired, isAllRequired, info, additional, customError, formatError, placeholder, editButton} = this.props;
    const field = this.getField();
    if(type === 'hidden') {
      return field;
    }

    let {label} = this.props;

    if (isRequired || isAllRequired) {
      label += ' <em title="Required">*</em>';
    }

    const atts = {};
    const containerAtts = {};
    if (isAllRequired) {
      atts.id = id;
      atts.name = id;
      containerAtts['data-checkbox-allrequired'] = true;
    }

    else if (isRequired) {
      atts.id = id;
      atts.name = id;

      if (type === 'checkbox') {
        containerAtts['data-checkbox-anyrequired'] = true;
      }
      else if (type === 'radio') {
        containerAtts['data-radio-anyrequired'] = true;
      }
    }

    const hasError = this.state.hasError;
    const valid = hasError ? 'invalid' : 'valid';
    const baseClass = 'form-field';
    const isInfoOpen = this.state.isInfoOpen;
    const isFile = type === 'file';

    const filePlaceholder = placeholder || 'No file selected';



    if (field) {
      return (
        <div className={`${baseClass} ${baseClass}--${type} ${baseClass}--${valid}`} htmlFor={id} {...containerAtts}>
          {editButton && (
            <EditButton html={editButton} isSmall={false} align="right" />
          )}
          <label className={`${baseClass}__label`} htmlFor={id} data-label={this.props.label}>
            <Editable className="labelText" html={label} />
            {!SERVER && (
              <span className="fieldErrors" htmlFor={id}>
                <Message ref={(m) => { window.messages[`${id}_password`] = m; }}>Please enter a valid password</Message>
                <Message ref={(m) => { window.messages[`${id}_email`] = m; }}>Please enter a valid email address</Message>
                <Message ref={(m) => { window.messages[`${id}_size`] = m; }}>Filesize is too large</Message>
                {type === 'date' && (<Message ref={(m) => { window.messages[`${id}_mindate`] = m; }}>Date must be greater than {toDate(field.props.min)}</Message>)}
                {type === 'date' && (<Message ref={(m) => { window.messages[`${id}_maxdate`] = m; }}>Date must be less than {toDate(field.props.max)}</Message>)}
                <Message ref={(m) => { window.messages[`${id}_pattern`] = m; }}>{customError}</Message>
                <Message ref={(m) => { window.messages[`${id}_required`] = m; }}>{customError}</Message>
                <Message ref={(m) => { window.messages[`${id}_minlength`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_maxlength`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_exactlength`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_alphanumeric`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_numeric`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_alphanumericspace`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_postcode`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_month`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_year`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_policynumber`] = m; }}>{formatError}</Message>
                <Message ref={(m) => { window.messages[`${id}_changepassword`] = m; }}>{formatError}</Message>
              </span>
            )}
          </label>
          {additional && !isFile && (
            <Editable html={additional} className={`${baseClass}__additional`} />
          )}
          <div className={`${baseClass}__field`}>
            {field}
            {isFile && (
              <label className="custom-file" htmlFor={id}>
                <span className="input custom-file__input" data-placeholder={filePlaceholder}>{this.state.selectedFile}</span>
                <span>
                  <span className="cta cta--secondary cta--nowrap cta--sm"><Editable html="Choose file" /></span>
                  {additional && (
                    <Editable html={additional} className="custom-file__additional" />
                  )}
                </span>
              </label>
            )}
            {info && (
              <span className={`${baseClass}__infoicon ${isInfoOpen ? 'open' : ''}`} onClick={(e) => this.onInfoClick(e)}>
                <Icon icon="infoTooltop" />
              </span>
            )}
          </div>
          {info && (
            <EditableDiv className={`${baseClass}__info ${isInfoOpen ? 'open' : ''}`} html={`<div>${info}</div>`} />
          )}
        </div>
      );
    }

    return ('');
  }
}

Input.defaultProps = {
  label: '',
  data: {},
  customError: '',
  formatError: '',
  //alphanumericErrorMsg:'',
  //postcodeErrorMsg:'',
  //policynumberErrorMsg:'',
  //monthError:'',
  //yearError:'',
  //minError: '',
  //maxError: '',
  //exactError:'',
  validate: '',
  additional: '',
  placeholder: '',
  value: '',
  size: '',
  id: '',
  isRequired: false,
  isAllRequired: false,
  options: null,
  type: 'text',
  info: '',
  editButton: '',
  hasSpinners: false
};

Input.propTypes = {
  label: PropTypes.string,
  data: PropTypes.object,
  customError: PropTypes.string,
  formatError: PropTypes.string,
  // alphanumericErrorMsg: PropTypes.string,
  // postcodeErrorMsg: PropTypes.string,
  // policynumberErrorMsg: PropTypes.string,
  // monthError: PropTypes.string,
  // yearError: PropTypes.string,
  // minError: PropTypes.string,
  // maxError: PropTypes.string,
  // exactError: PropTypes.string,
  validate: PropTypes.string,
  additional: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  size: PropTypes.string,
  id: PropTypes.string,
  isRequired: PropTypes.bool,
  isAllRequired: PropTypes.bool,
  options: PropTypes.array,
  type: PropTypes.string,
  info: PropTypes.string,
  editButton: PropTypes.string,
  hasSpinners: PropTypes.string,
  pattern: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  documentProps: PropTypes.object,
  html: PropTypes.any,
  isOpvField: PropTypes.bool
};

module.exports = Input;
