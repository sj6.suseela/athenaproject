import React from 'react';
import PropTypes from 'prop-types';

import Feedback from './Feedback';
import Fieldset from './Fieldset';
import Icon from '../Icon/Icon';
import Input from './Input';
import Editable from '../Editable/Editable';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import axios from 'axios';
import EditButton from '../CMS/EditButton';
import Loading from '../Loading/Loading';

import {validateAll} from './validation/validate';

/** Form
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Form, {
    id : 'id',
    theme : {}
  }), document.getElementById("element"));
*/
class Form extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      isSuccess     : false,
      isFailure     : false,
      isOpv         : false,
      otacErr       : null,
      errors : [],
      loading: false
    };

    this._onSubmit = this._onSubmit.bind(this);
  }

  getGroups () {
    return this.props.groups.map((fieldset,i) => {
      const fields = fieldset.fields.map((field,e) => {
        return (<Input key={field.label + i + e} {...field} />);
      });

      return (
        <Fieldset key={fieldset.name + i} {...fieldset} legend={fieldset.name} baseClass="form" editButton={fieldset.editButton} isVertical={this.props.isVertical}>
          {fields}
        </Fieldset>
      );
    });
  }

  showForm () {
    this.setState({
      isSuccess : false,
      isFailure : false,
      errors : []
    });
  }

  processFile (element,inputId) {
    const _this = this;
    const fileObj = {};
    this.fileCount++;
    fileObj.Type = element.files[0].type;
    fileObj.Title = element.files[0].name;

    this.getBase64(element.files[0],
      (result) => {
        _this.fileCount--;
        fileObj.Data = result;
      // console.log("Added file " + fileObj);
        if (!_this.formFields.Attachments) {
          _this.formFields.Attachments = {};
        }

        _this.formFields.Attachments[inputId]  = fileObj;
      },
      (/* error*/) => {
        _this.fileCount--;
        // console.log("error", error);
      }
    );
  }

  getBase64 (file, success, error) {
    const reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = function () {
      success(reader.result);
    };
    reader.onerror = function (e) {
      error(e);
    };
  }

  sendData (endpoint, formData) {
    const _this = this;
    const config = {
      // headers : {'Content-Type': 'application/json'},
      headers : {'Content-Type': 'multipart/form-data'}
    };

    this.setState({
      loading: true
    });

    const newFormData = new FormData();
    newFormData.append('payload', JSON.stringify(formData));

    this.files.forEach((val, i) => {
      newFormData.append(`file${  i}`, val);
    });

    axios.post(endpoint, newFormData, config)
      .then((response) => {
        const jsonResponse    = response.data;
        if(jsonResponse.success) {
          if (this.props.isOpvForm) {
            location.href = jsonResponse.redirectUrl;
          }
          this.setState({
            isSuccess           : true,
            isFailure           : false,
            errors              : []
          },() => {
            _this._wrapper.ref.scrollIntoView();
          });

          if(this.props.customSuccessMethod) {
            this.props.customSuccessMethod.call();
          }
        }
        else {
          if (this.props.isOpvForm) {
            this.setState({
              isOpv              : true,
              otacErr             : jsonResponse.errorMessage,
              isSuccess           : false,
              isFailure           : false,
              errors              : []
            },() => {
              _this._wrapper.ref.scrollIntoView();
            });
          }
          this.setState({
            isSuccess           : false,
            isFailure           : true,
            errors              : []
          },() => {
            _this._wrapper.ref.scrollIntoView();
          });
        }
        this.setState({
          loading: false
        });
      })
      .catch(() => {
        if(this.props.isOpvForm) {
          this.setState({
            isOpv               : true,
            isSuccess           : false,
            isFailure           : false,
            errors              : []
          },() => {
            _this._wrapper.ref.scrollIntoView();
          });
        }
        this.setState({
          isSuccess           : false,
          isFailure           : true,
          errors              : [],
          loading: false
        },() => {
          _this._wrapper.ref.scrollIntoView();
        });
      });
  }

  @validateAll
  _onSubmit (valid, e) {
    // console.log('_onSubmit called when the form is submitted');
    // console.log("form is valid:"+valid);

    e.preventDefault();

    const self                  = this;
    const {id, endpoint, groups, testSuccess, testFailure}          = this.props;

    // console.log('form data', formData);

    if(valid) {
      const formData              = {
        id,
        testSuccess,
        testFailure
      };
      this.formFields = {};
      this.fileCount = 0;
      this.files = [];

      for(const i in groups) {
        const group               = groups[i];
        const groupId             = group.id;
        this.formFields[groupId]         = {};

        for(const j in group.fields) {
          const input             = group.fields[j];
          const inputId           = input.id;
          if (input.type === 'radio' || input.type === 'checkbox') {
            const options = document.getElementsByName(inputId);
            if (options.length > 0) {
              let optionValues = '';
              for (let jj = 0; jj < options.length; jj++) {
                if (options[jj].checked) {
                  optionValues +=  `,${  options[jj].value}`;
                }
              }
              this.formFields[groupId][inputId] = optionValues.substr(1);
            }
          }
          else {
            const element           = document.getElementById(inputId);

            if(element) {
              if (element.type === 'file')            {
                if (element.files.length > 0)                {
                  this.files.push(element.files[0]);
                  // this.processFile(element,inputId);
                }
              }
              else            {
                if (groupId === 'opv-req') {
                  this.formFields[groupId].opvmm  = document.getElementById('opvyy').value + '-' + document.getElementById('opvmm').value;
                  this.formFields[groupId].opvpn = document.getElementById('opvpn').value;
                  this.formFields[groupId].opvpc = document.getElementById('opvpc').value;
                }
                else if (groupId === 'opv-access-code') {
                  this.formFields[groupId].oac  = document.getElementById('oac').value + '-' + document.getElementById('oac1').value + '-' + document.getElementById('oac2').value;
                  this.formFields[groupId].policyNumber = document.getElementById('policyNumber').value;
                }
                else {
                  this.formFields[groupId][inputId]  = element.value;
                }
              }
            }
            else {
              // console.warn(`cannot find #${inputId}`);
            }
          }
        }
      }

      if (this.fileCount === 0)      {
        formData.groupData = JSON.stringify(this.formFields);
        this.sendData(endpoint, formData);
      }
      else      {
        const _this = this;
        const intervalId = setInterval(() => {
          if (_this.fileCount === 0) {
            clearInterval(intervalId);
            formData.groupData = JSON.stringify(_this.formFields);
            self.sendData(endpoint, formData);
          }
        }
          ,10);
      }
    }
    else {
      setTimeout(() => {
        const errorLabels = self._form.querySelectorAll('.form-field--invalid .form-field__label');
        const errors = [];

        Array.prototype.map.call(errorLabels, (n) => {
          const label = n.dataset.label;
          const id = n.getAttribute('for');

          errors.push(`<label for="${id}">${label}</label>`);
        });

        self.setState({
          errors
        },() => {
          if(errors.length > 0) {
            self._errors.scrollIntoView();
          }
        });
      }, 1);
    }

    return false;
  }

  render () {
    const {submit, heading, success, failure, errorTitle, endpoint, testSuccess, testFailure, editButton, intro, isOpvForm}     = this.props;
    const baseClass             = 'form';

    const isSuccess = this.state.isSuccess;
    const isFailure = this.state.isFailure;
    const hasMessage = isSuccess || isFailure;
    const hasOpvForm = isOpvForm;
    const isOpv = this.state.isOpv;
    const otacErr = {
      content: ''
    };
    otacErr.content = this.state.otacErr;
    const errors = this.state.errors.map((error,i) => {
      return (<li key={`item-${i}`}><Editable html={error} /></li>);
    });

    const hasErrors = errors.length > 0;

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true} ref={(m) => { this._wrapper = m; }}>
        <form action={endpoint} method="post" data-validateall ref={(m) => { this._form = m; }} onSubmit={this._onSubmit}>
          {testSuccess && (
            <input type="hidden" id="testSuccess" value="true"/>
          )}
          {testFailure && (
            <input type="hidden" id="testFailure" value="true"/>
          )}
          {(heading || intro) && (
            <div className={`${baseClass}__main-heading`}>
              {heading && (
                <h2><Editable html={heading} /></h2>
              )}
              <div ref={(m) => { this._feedback = m; }}>
                {isOpv && (
                  <Feedback  {...otacErr} callback={this.showForm.bind(this)} />
                )}
              </div>
              {intro && (
                <p><Editable html={intro} /></p>
              )}
            </div>
          )}
          {editButton && (
            <div><EditButton html={editButton} align="right" /></div>
          )}
          <div ref={(m) => { this._feedback = m; }}>
            {isSuccess && (
              <Feedback {...success} />
            )}
            {isFailure && (
              <Feedback  {...failure} callback={this.showForm.bind(this)} />
            )}
          </div>
          {hasErrors && (
          <div className="form-errors" ref={(m) => { this._errors = m; }}>
            <h3>
              <Icon icon="warning" />
              <Editable html={errorTitle} />
            </h3>
            <ul>
              {errors}
            </ul>
          </div>
          )}
          {!hasMessage && (
            <div>
              {this.getGroups()}
              <div className={'form-field form-field--button'}>
                <button className="cta cta--sm cta--primary cta--no-icon" name="submit">{hasOpvForm && (<Icon icon="rightarrow" fillColor="#fff" />)}{submit}</button>
              </div>
            </div>
          )}

          {(hasMessage && hasOpvForm) && (
            <div>
              {this.getGroups()}
              <div className={'form-field form-field--button'}>
                <button className="cta cta--sm cta--primary cta--icon" name="submit"><Icon icon="rightarrow" fillColor="#fff" />{submit}</button>
              </div>
            </div>
          )}
        <Loading isActive={this.state.loading} isFullScreen={true}/>
        </form>
      </ModuleWrapper>
    );
  }
}

Form.defaultProps = {
  url                           : '',
  success                       : null,
  failure                       : null,
  endpoint                      : '',
  errorTitle                    : 'You\'ve missed some form fields, please fill in the following fields',
  id                            : '',
  heading                       : '',
  submit                        : 'Submit',
  groups                        : [],
  testSuccess                   : false,
  testFailure                   : false,
  editButton                    : ''

};

Form.propTypes = {
  url                           : PropTypes.string,
  success                       : PropTypes.object,
  failure                       : PropTypes.object,
  endpoint                      : PropTypes.string,
  errorTitle                    : PropTypes.string,
  id                            : PropTypes.string,
  heading                       : PropTypes.string,
  submit                        : PropTypes.string,
  groups                        : PropTypes.array,
  testSuccess                   : PropTypes.any,
  testFailure                   : PropTypes.any,
  editButton                    : PropTypes.string,
  intro                         : PropTypes.string,
  isVertical                    : PropTypes.bool,
  customSuccessMethod           : PropTypes.func,
  isOpvForm                     : PropTypes.bool
};

module.exports = Form;
