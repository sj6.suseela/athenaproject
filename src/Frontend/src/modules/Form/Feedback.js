import React from 'react';
import PropTypes from 'prop-types';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditableImage from '../Editable/EditableImage';
import Cta from '../Cta/Cta';

/**
 * Feedback Module

  <div id="m-Feedback"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Feedback, {
    }), document.getElementById("m-Feedback"));
  </script>
 */
class Feedback extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading, content, image, backText, callback} = this.props;
    const baseClass = 'feedback';

    return (
      <div className={baseClass}>
        <div className="columns@md">
          <div>
            <Editable tag="h2" html={heading} />
            <EditableDiv html={content} />
            <Cta type="primary" isSmall={true} isWide={true} rawCta={backText} onClick={callback} />
          </div>
          {image && (
          <div>
            <EditableImage image={image.html} isNotAsync={true}/>
          </div>
          )}
        </div>
      </div>
    );
  }
}

Feedback.defaultProps = {
  heading : '',
  content : '',
  backText : '',
  image : '',
  callback : null
};

Feedback.propTypes = {
  heading : PropTypes.string,
  content : PropTypes.string,
  backText : PropTypes.string,
  image : PropTypes.string,
  callback : PropTypes.func

};

module.exports = Feedback;
