import React from 'react';
import PropTypes from 'prop-types';

import './Form.scss';

import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditButton from '../CMS/EditButton';
import EditableImage from '../Editable/EditableImage';

class Fieldset extends React.Component {

  constructor (props) {
    super(props);
  }


  render () {
    const {legend, children, subheading, baseClass, editButton, image, isVertical, headingTag, additionalHeadingTxt, isHidden} = this.props;

    if(isHidden) {
      return children;
    }

    return (
      <fieldset>
        <div className={!isVertical ? 'xx' : 'vertical'}>
          <div className={!isVertical ? 'xx__left' : ''}>
            {legend && (
              <div className="legend-wrapper">
                <legend>{legend}</legend>
                <Editable tag={headingTag} html={legend} />
                <Editable className="sub-text--small" html={additionalHeadingTxt} />
              </div>
            )}
            {image && (
              <div>
                <EditableImage image={image.html} imageExtension={image.extension} imageSize={image.size}/>
              </div>
            )}
          </div>
          <div className={!isVertical ? 'xx__right' : ''}>
            {subheading && (
              <EditableDiv className={`${baseClass}__header`} html={subheading} />
            )}
            {editButton && (
              <EditButton html={editButton} align="right" />
            )}
            {children}
          </div>
        </div>
      </fieldset>
    );
  }
}

Fieldset.defaultProps = {
  subheading: '',
  baseClass: '',
  legend: '',
  editButton: '',
  headingTag: 'h2',
  children: [],
  isHidden: false
};

Fieldset.propTypes = {
  subheading:PropTypes.string,
  baseClass:PropTypes.string.isRequired,
  legend:PropTypes.string,
  editButton:PropTypes.string,
  children:PropTypes.array,
  headingTag:PropTypes.string,
  image:PropTypes.string,
  isHidden: PropTypes.bool,
  isVertical: PropTypes.bool,
  additionalHeadingTxt: PropTypes.string
};

module.exports = Fieldset;
