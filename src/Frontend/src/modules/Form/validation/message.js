import React from 'react';
import PropTypes from 'prop-types';

class Message extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      hidden:true
    };
  }

  show () {
    this.setState({hidden:false});
  }

  hide () {
    this.setState({hidden:true});
  }

  _getMessage () {
    return this.state.hidden ? null : <span className="validator__message">{this.props.children}</span>;
  }

  render () {
    return (
            this._getMessage()
    );
  }
}

Message.defaultProps = {
  children : ''
};

Message.propTypes = {
  children : PropTypes.oneOfType([PropTypes.string, PropTypes.array, PropTypes.object])
};

module.exports = Message;
