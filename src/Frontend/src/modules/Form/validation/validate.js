import validator from './validator';
import Message from './message';

function validateItem (target, name, descriptor) {
  if(!SERVER) {
    if(!window.messages) {
      window.messages = [];
    }
  }

  const sf = descriptor.value;

  descriptor.value = function (e) {
    const elem = e.target;
    const isValid = validator.validate.bind(this)(elem);
    return sf.bind(this, isValid)(e);
  };

  return descriptor;
}

function findAncestor (el, cls) {
  while ((el = el.parentElement) && !el.classList.contains(cls)); // eslint-disable-line no-param-reassign
  return el;
}

function validateAll (target, name, descriptor) {
  const sf = descriptor.value;

  descriptor.value = function (e) {
    const parent = validator.findParent(e.target);
    const children = parent.querySelectorAll('[data-validate]');
    const checkboxes = parent.querySelectorAll('[data-checkbox-allrequired]');
    const checkboxesAny = parent.querySelectorAll('[data-checkbox-anyrequired]');
    const radios = parent.querySelectorAll('[data-radio-anyrequired]');

    let allValid = true;
    for(let i = 0, il = children.length; i < il; i++) {
      const cisv = validator.validate.bind(this)(children[i]);

      if(!cisv) {
        allValid = false;

        children[i].blur();
        const el = findAncestor(children[i],'form-field--valid');
        if(el) {
          el.classList.remove('form-field--valid');
          el.classList.add('form-field--invalid');
        }
      }
      else{
        const el = findAncestor(children[i],'form-field--invalid');
        if(el) {
          el.classList.add('form-field--valid');
          el.classList.remove('form-field--invalid');
        }
      }
    }

    for(let i = 0, il = checkboxes.length; i < il; i++) {
      const cisv = validator.validateCheckboxGroup.bind(this)(checkboxes[i],'all');

      if(!cisv) {
        allValid = false;

        const classlist = checkboxes[i].getAttribute('class').replace('form-field--valid', 'form-field--invalid');
        checkboxes[i].setAttribute('class', classlist);
      }
      else{
        const classlist = checkboxes[i].getAttribute('class').replace('form-field--invalid', 'form-field--valid');
        checkboxes[i].setAttribute('class', classlist);
      }
    }

    for(let i = 0, il = checkboxesAny.length; i < il; i++) {
      const cisv = validator.validateCheckboxGroup.bind(this)(checkboxesAny[i],'any');

      if(!cisv) {
        allValid = false;

        const classlist = checkboxesAny[i].getAttribute('class').replace('form-field--valid', 'form-field--invalid');
        checkboxesAny[i].setAttribute('class', classlist);
      }
      else{
        const classlist = checkboxesAny[i].getAttribute('class').replace('form-field--invalid', 'form-field--valid');
        checkboxesAny[i].setAttribute('class', classlist);
      }
    }

    for(let i = 0, il = radios.length; i < il; i++) {
      const cisv = validator.validateRadioGroup.bind(this)(radios[i]);

      if(!cisv) {
        allValid = false;

        const classlist = radios[i].getAttribute('class').replace('form-field--valid', 'form-field--invalid');
        radios[i].setAttribute('class', classlist);
      }
      else{
        const classlist = radios[i].getAttribute('class').replace('form-field--invalid', 'form-field--valid');
        radios[i].setAttribute('class', classlist);
      }
    }

    return sf.bind(this, allValid)(e);
/*
    if(allValid) {
      return sf.bind(this)(e);
    }
    else {
      return false;
    }*/
  };

  return descriptor;
}

module.exports = {validate:validateItem, validateAll, Message};
