import rules from './rules';

const validator = {
  obj:{},
  add (item) {
    const obj = [];
    if(item.dataset.validate) {
      if(item.dataset.required) {
        obj.required = true;
      }
      if(item.dataset.minlength) {
        obj.minlength = item.dataset.minlength;
      }
      if(item.dataset.maxlength) {
        obj.maxlength = item.dataset.maxlength;
      }
      if(item.dataset.exactlength) {
        obj.exactlength = item.dataset.exactlength;
      }
      if(item.dataset.alphanumeric) {
        obj.alphanumeric = true;
      }
      if(item.dataset.alphanumericspace) {
        obj.alphanumericspace = true;
      }
      if(item.dataset.postcode) {
        obj.postcode = true;
      }
      if(item.dataset.policynumber) {
        obj.policynumber = true;
      }
      if(item.dataset.month) {
        obj.month = true;
      }
      if(item.dataset.year) {
        obj.year = true;
      }
      if(item.dataset.size) {
        obj.size = Number(item.dataset.size);
      }
      if(item.dataset.email) {
        obj.email = true;
      }
      if(item.pattern) {
        obj.pattern = item.pattern;
      }
      if(item.dataset.min) {
        obj.min = item.dataset.min;
      }
      if(item.dataset.max) {
        obj.max = item.dataset.max;
      }
      if(item.dataset.mindate) {
        obj.mindate = item.dataset.mindate;
      }
      if(item.dataset.maxdate) {
        obj.maxdate = item.dataset.maxdate;
      }
      if(item.dataset.equals) {
        obj.equals = item.dataset.equals;
      }
      if(item.dataset.equalselem) {
        obj.equalsElem = item.dataset.equalselem;
      }
      if(item.dataset.notequals) {
        obj.notEquals = item.dataset.notequals;
      }
      if(item.dataset.carddetails) {
        obj.carddetails = item.dataset.carddetails;
      }
      if(item.dataset.password) {
        obj.password = item.dataset.password;
      }
      if(item.dataset.changepassword) {
        obj.changepassword = item.dataset.changepassword;
      }
      if(item.dataset.numeric) {
        obj.numeric = item.dataset.numeric;
      }
      if(item.dataset.nospaces) {
        obj.nospaces = item.dataset.nospaces;
      }
    }

    validator.obj[item.name] = obj;
  },
  validate (elem) {
    if(validator.isHidden(elem) && elem.type !== 'checkbox') return validator.isValid;

    if(!validator.obj[elem.name]) {
      validator.add(elem);
    }

    // TODO: get a list of messages and pass them through
    /*
    console.log(validator.obj[elem.name])
    console.log(this.messages)
    console.log(this.messages[elem.name])
    console.dir(elem)
    */

    const itemvalid = validator.isValid.bind(this)(elem);

    if(itemvalid) {
      elem.classList.remove('error');
      elem.classList.add('valid');

      elem.dataset.valid = true;
    }
    else {
      elem.classList.remove('valid');
      elem.classList.add('error');

      elem.dataset.valid = false;
    }

    return itemvalid;
  },
  validateCheckboxGroup (elem,type) {
    if(validator.isHidden(elem)) return validator.isValid;

    if(!validator.obj[elem.id]) {
      validator.add(elem);
    }

    const allCheckBoxes = elem.querySelectorAll('input[type=checkbox]');
    const checked = elem.querySelectorAll('input:checked');
    let valid = false;

    if(type === 'all') {
      valid = checked.length === allCheckBoxes.length;
    }
    else if (type === 'any') {
      let checked = false;
      [].slice.call(allCheckBoxes).every((el) => {
        if(el.checked) {
          checked = true;
          return false;
        }
        return true;
      });
      valid = checked;
    }

    if(!SERVER) {
      if (valid) {
        if(window.messages[`${elem.id}_required`]) {
          window.messages[`${elem.id}_required`].hide();
          document.getElementById(elem.id).classList.remove('hide-label');
        }
      }
      else {
        if(window.messages[`${elem.id}_required`]) {
          window.messages[`${elem.id}_required`].show();
          document.getElementById(elem.id).classList.add('hide-label');
        }
      }
    }

    return valid;
  },
  validateRadioGroup (elem) {
    if(validator.isHidden(elem)) return validator.isValid;

    if(!validator.obj[elem.id]) {
      validator.add(elem);
    }

    const radios = elem.querySelectorAll('input[type=radio]');
    let valid = false;
    [].slice.call(radios).every((el) => {
      if(el.checked) {
        valid = true;
        return false;
      }
      return true;
    });

    if(!SERVER) {
      if (valid) {
        if(window.messages[`${elem.id}_required`]) {
          window.messages[`${elem.id}_required`].hide();
          document.getElementById(elem.id).classList.remove('hide-label');
        }
      }
      else {
        if(window.messages[`${elem.id}_required`]) {
          window.messages[`${elem.id}_required`].show();
          document.getElementById(elem.id).classList.add('hide-label');
        }
      }
    }

    return valid;
  },
  isValid (item) {
    const v = validator.obj[item.name];
    let val = (item.type && item.type === 'checkbox') ? item.checked.toString() : item.value;

    if((item.type === 'select-one' || item.type === 'select-multiple') && item.value === 'default') {
      val = '';
    }

    let isv = true;
    for(const rule in v) {
      const result = rules[rule](val,v[rule], item);

      if(!result) {
        isv = false;

        if(!SERVER) {
          if(window.messages[`${item.name}_${rule}`]) {
            window.messages[`${item.name}_${rule}`].show();
          }
        }
      }
      else {
        if(!SERVER) {
          if(window.messages[`${item.name}_${rule}`]) {
            window.messages[`${item.name}_${rule}`].hide();
          }
        }
      }
    }

    if(!SERVER) {
      if(window.messages[item.name]) {
        if(!isv) {
          window.messages[item.name].show();
        }
        else {
          window.messages[item.name].hide();
        }
      }
    }

    return isv;
  },
  isHidden (el) {return (el.offsetParent === null);},
  findParent (el) {
    let element = el;

    while(!element.dataset.validateall) {
      element = element.parentNode;
    }
    return element;
  }
};

export default validator;
