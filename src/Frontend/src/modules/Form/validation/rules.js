export default {
  required (value) {
    return value.replace(/ /g,'').length > 0;
  },
  email (value) {
    const pattern = /^$|^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i;
    return !!pattern.exec(value.trim());
  },
  alpha (value) {
    const pattern = /^[a-zA-Z]+$/;
    return !!pattern.exec(value);
  },
  size (value, mb, input) {
    if(input.files.length) {
      const bytes = mb * 1048576;
      const fileSize = input.files[0].size;

      return fileSize <= bytes;
    }
    return true;
  },
  numeric (value) {
    const pattern = /^[+0-9]+$/;
    let v = value.replace(/ /g,'');
    if(v.length === 0) v = 0;
    return !!pattern.exec(v);
  },
  carddetails (value) {
    const val = value.replace(/ /g,'').replace(/\//g,'');
    const pattern = /^\d+$/;
    return !!pattern.exec(val);
  },
  alphanumeric (value) {
    const pattern = /^[a-z0-9]+$/i;
    return !!pattern.exec(value);
  },
  alphanumericspace (value) {
    const pattern = /^[a-z0-9 ]+$/i;
    return !!pattern.exec(value);
  },
  month (value) {
    const pattern = /^(0?[1-9]|1[012])$/;
    return !!pattern.exec(value);
  },
  year (value) {
    let cur_dt = new Date().getFullYear();
    let min_dt = cur_dt - 120;
    let arr1 = cur_dt.toString().split("");
    let arr2 = min_dt.toString().split("");
   
    let t = 0;
    if(arr1[2]){
      t = arr1[2] - 1;
    }
    let re = arr2[0]+arr2[1]+"["+arr2[2]+"-9][0-9]|"+arr1[0]+arr1[1]+"[0-"+arr1[2]+"][0-"+arr1[3]+"]|"+arr1[0]+arr1[1]+"[0-"+t+"][0-9]";
   
    const pattern = RegExp(re);
    return !!pattern.exec(value);
  },
  postcode (value) {
    const pattern = /^[A-Za-z0-9]{2,4}\s[A-Za-z0-9]{3}$/;
    return !!pattern.exec(value);
  },
  policynumber (value) {
    // const pattern = /^[a-zA-Z]{2}[0-9]{6}$/;
    const pattern = /^[0-9]+$/i;
    return !!pattern.exec(value);
  },
  url (value) {
    const pattern = /^(http|https):\/\/(([a-zA-Z0-9$\-_.+!*'(),;:&=]|%[0-9a-fA-F]{2})+@)?(((25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])){3})|localhost|([a-zA-Z0-9\-\u00C0-\u017F]+\.)+([a-zA-Z]{2,}))(:[0-9]+)?(\/(([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*(\/([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*)*)?(\?([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?(\#([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?)?$/;
    return !!pattern.exec(value);
  },
  pattern (value, pattern) {
    return !!new RegExp(pattern).exec(value);
  },
  equals (value, match) {
    return value === match;
  },
  notEquals (value, match) {
    return value !== match;
  },
  min (value, length) {
    return value >= parseInt(length);
  },
  max (value, length) {
    return value <= parseInt(length);
  },

  mindate (value, date) {
    const input = new Date(value);
    const test = new Date(date);

    // if the date is valid
    if(input instanceof Date && !isNaN(input)) {
      return input >= test;
    }

    return false;
  },
  maxdate (value, date) {
    const input = new Date(value);
    const test = new Date(date);

    // if the date is valid
    if(input instanceof Date && !isNaN(input)) {
      return input <= test;
    }

    return false;
  },
  minlength (value, length) {
    return value.length >= parseInt(length);
  },
  maxlength (value, length) {
    return value.length <= parseInt(length);
  },
  exactlength (value, length) {
    return value.length == parseInt(length);
  },
  equalsElem (value, match) {
    const m = document.querySelector(`input[name=${match}]`).value;
    return value === m;
  },
  nospaces (value) {
    return value.indexOf(' ') === -1;
  },
  password (value) {
    const hasUpper = (/[A-Z]/.test(value));
    const hasLower = (/[a-z]/.test(value));
    const hasNumber = (/\d/.test(value));

    return hasUpper && hasLower && hasNumber;
  },
  // changepassword (value) {
  //   const atleast3Digits = (/(.*\d){3,}/.test(value));
  //   const atleast1CapitalLetter = (/[A-Z]{1,}/.test(value));
  //   const atleast1SpecialChar = (/[!@#$%^&*()_+/\-=[]{}|;:<>?,.]{1,}/.test(value));
  //   const hasLower = (/[a-z]/.test(value));
  //   return atleast3Digits && atleast1CapitalLetter && atleast1SpecialChar && hasLower;
  // }
  changepassword (value) {
    const pattern = /^(?=.{3,}$)(?=.*[A-Za-z])(?=.*[0-9])(?=.*\W).*$/;
    return !!pattern.exec(value);
  }
};
