import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Contact from '../Contact/Contact';
import IconList from '../_ContentModules/List/IconList';

import './Support.scss';

/** Support
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Support, {
    id : 'id',
    theme : {},
    heading : `Policy support`,
    links : [
      {
        icon : '<img src="dist/images/icon/Icon-pdf.svg" />',
        link : '<a href="faq-answer.html">Renew policy</a>'
      },
      {
        icon : '<img src="dist/images/icon/Icon-pdf.svg" />',
        link : '<a href="faq-answer.html">Make policy changes</a>'
      },
      {
        icon : '<img src="dist/images/icon/Icon-pdf.svg" />',
        link : '<a href="faq-answer.html">Request policy documents</a>'
      }
    ],
    advice : {
      title : `Pre-travel advice`,
      content : `<p>For information about medical facilities, visas, vaccines and health agreements</p>`,
      contact: {
        tel : '01234 621 537',
        opening : 'Lines open <b>24 hours</b>, 7 days a week',
        info : 'Calls may be recorded and/or monitored for training and audit purposes.'
      }
    }
  }), document.getElementById("element"));
*/
class Support extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading, links, advice} = this.props;
    const baseClass = 'support';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass}>
        <div className="columns@md">
          <div>
            <Editable tag="h2" className={`title ${baseClass}__title`} html={heading} />
            <IconList items={links} />
          </div>
          {advice && (
          <div>
            <Editable tag="h2" className={`title ${baseClass}__title`} html={advice.title} />
            <EditableDiv html={advice.content} />
            {advice.contact && (
              <Contact {...advice.contact} />
            )}
          </div>
          )}
        </div>
      </ModuleWrapper>
    );
  }
}

Support.defaultProps = {
  heading : '',
  links : null,
  advice : {
    title : '',
    content : '',
    contact : null
  }
};

Support.propTypes = {
  heading : PropTypes.string,
  links : PropTypes.array,
  advice : PropTypes.object
};

module.exports = Support;
