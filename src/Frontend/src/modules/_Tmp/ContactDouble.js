import React from 'react';
import PropTypes from 'prop-types';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import Contact from '../Contact/Contact';

import './ContactDouble.scss';

/** ContactDouble
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.ContactDouble, {
    id : 'id',
    theme : {
      themeName : 'mint',
      themeBefore : 'white',
      topCurve : { type: 'convex' },
      bottomCurve : { type: 'convex' }
    },
    leftContact : {
      title : `Medical Emergencies`,
      tel : '01234 621 537',
      telAlt : '+44 1243 621 537',
      telAltIntro : 'From abroad:',
      opening : 'Lines open <b>24 hours</b>, 7 days a week',
      info : 'Calls may be recorded and/or monitored for training and audit purposes.'
    },
    rightContact : {
      title : `All your other travel claims`,
      tel : '01234 621 537',
      telAlt : '+44 1243 621 537',
      telAltIntro : 'From abroad:',
      opening : 'Lines open <b>24 hours</b>, 7 days a week',
      info : 'Calls may be recorded and/or monitored for training and audit purposes.'
    }
  }), document.getElementById("element"));
*/
class ContactDouble extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {leftContact, rightContact} = this.props;
    const baseClass = 'contact-double';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass}>
        <div className="columns@md spaced">
          {leftContact && (
          <div>
            <div className="box box--sm">
              <Contact {...leftContact} />
            </div>
          </div>
          )}
          {rightContact && (
          <div>
            <div className="box box--sm">
              <Contact {...rightContact} />
            </div>
          </div>
          )}
        </div>
      </ModuleWrapper>
    );
  }
}

ContactDouble.defaultProps = {
  leftContact : null,
  rightContact : null
};

ContactDouble.propTypes = {
  leftContact : PropTypes.object,
  rightContact : PropTypes.object
};

module.exports = ContactDouble;
