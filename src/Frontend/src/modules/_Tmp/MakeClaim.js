import React from 'react';
import PropTypes from 'prop-types';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import EditableImage from '../Editable/EditableImage';
import Editable from '../Editable/Editable';
import IconList from '../_ContentModules/List/IconList';

import './MakeClaim.scss';

/** MakeClaim
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.MakeClaim, {
    id : 'id',
    theme : {},
    heading : `Like to make a Travel Insurance Claim`,
    image : {
      html:`<img src="dist/images/cms/flourish-insurance.svg" />`
    },
    title : `Policy support`,
    links : [
      {
        icon : '<img src="dist/images/icon/Icon-pdf.svg" />',
        link : '<a href="faq-answer.html">Claims considerations</a>'
      },
      {
        icon : '<img src="dist/images/icon/Icon-pdf.svg" />',
        link : '<a href="faq-answer.html">How the claims process works</a>'
      }
    ]
  }), document.getElementById("element"));
*/
class MakeClaim extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  render () {
    // get the prop settings
    const {heading, title, links, image} = this.props;
    const baseClass = 'make-claim';

    const hasHeader = heading || image;

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <div className="columns@md">
          {hasHeader && (
            <div>
              <Editable tag="h2" className={`title ${baseClass}__title`} html={heading} />
              <EditableImage className={`${baseClass}__image hide@sm`} image={image.html} />
            </div>
          )}
          <div>
            <Editable tag="h3" className={`title ${baseClass}__title`} html={title} />
            <IconList items={links} />
            <EditableImage className={`${baseClass}__image hide@md hide@lg`} image={image.html} />
          </div>
        </div>
      </ModuleWrapper>
    );
  }
}

MakeClaim.defaultProps = {
  heading : '',
  image : {},
  title : '',
  links : null
};

MakeClaim.propTypes = {
  heading : PropTypes.string,
  image : PropTypes.object,
  title : PropTypes.string,
  links : PropTypes.array
};

module.exports = MakeClaim;
