import React from 'react';
import PropTypes from 'prop-types';

import ModuleHeader from '../_Product/ModuleHeader/ModuleHeader';

import Cta from '../Cta/Cta';
import EditableDiv from '../Editable/EditableDiv';

import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';

import './HeaderCta.scss';

/** HeaderCta
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.HeaderCta, {
    id: 'module-reviews',
    heading: "We don't want to brag, but <b>99%</b> of our customers would buy from us again",
    label: 'Good value, great values',
    cta: {type:'primary', label:'Read more customer reviews', href:'#'},
    icon: "<img src='dist/images/flourish/quote-hearts.png'/>",
  }), document.getElementById("element"));
*/
class HeaderCta extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    // get the prop settings
    const {heading, theme, cta, label, icon, copy} = this.props;

    // base css class
    const baseClass = 'headercta';

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <ModuleHeader headline={heading} label={label} themeColour={theme.textColour} icon={icon}  />
        <EditableDiv className={`${baseClass}__copy`} html={copy} />
        {cta && (
          <Cta cssclass={`${baseClass}__cta`} rawCta={cta.link} type={cta.type} hasNoMargin={true} />
        )}
      </ModuleWrapper>
    );
  }
}

HeaderCta.defaultProps = {
  id:'headercta',
  items:[],
  theme: {}
};

HeaderCta.propTypes = {
  theme: PropTypes.object,
  heading: PropTypes.string,
  id:PropTypes.string,
  cta:PropTypes.object,
  label: PropTypes.string,
  icon: PropTypes.string,
  copy: PropTypes.string
};

module.exports = HeaderCta;
