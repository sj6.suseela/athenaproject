import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import './OpvRegTooltip.scss';

class OpvRegTooltip extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      clicked: false,
      clickedLink1: false,
      clickedLink2: false
    };

    this._click = this._click.bind(this);
    this._clickLink1 = this._clickLink1.bind(this);
    this._clickLink2 = this._clickLink2.bind(this);
  }

  _click (e) {
    e.preventDefault();
    this.state.clicked ? this.setState({clicked: false}) : this.setState({clicked: true});
  }

  _clickLink1 (e) {
    e.preventDefault();
    this.state.clickedLink1 ? this.setState({clickedLink1: false}) : this.setState({clickedLink1: true});
  }

  _clickLink2 (e) {
    e.preventDefault();
    this.state.clickedLink2 ? this.setState({clickedLink2: false}) : this.setState({clickedLink2: true});
  }

  render () {
    const { link, informationLink, termsLink, title, informationTitle, termsTitle, description, informationDescription, termsDescription} = this.props;
    const baseClass = 'opv-reg-tooltip';
    const className = (this.state.clicked) ? 'on-rac' : 'off-rac';
    const className1 = (this.state.clickedLink1) ? 'on-rac' : 'off-rac';
    const className2 = (this.state.clickedLink2) ? 'on-rac' : 'off-rac';

    return (

      <div className={`${baseClass}`}>

        <div className={`${baseClass}__info-notelink1`} onClick={this._click}>
          <Editable tag="a" html={link} className={`${baseClass}__info-link`} />
        </div>

        <div className={`${baseClass}__info-notelink2`} onClick={this._clickLink1}>
          <Editable tag="a" html={informationLink} className={`${baseClass}__info-link1`} />
        </div>

        <div className={`${baseClass}__info-notelink3`} onClick={this._clickLink2}>
          <Editable tag="a" html={termsLink} className={`${baseClass}__info-link2`} />
        </div>

        <div id="tooltip-rac" className={`${className} top`}>
          <div className={`${baseClass}__tooltip-inner-rac`}>
              <span className={`${baseClass}__closenote-rac`} onClick={this._click}>X</span>
              <Editable tag="h6" className={`${baseClass}__infoheading`} html={title} />
              <EditableDiv className="infoNoteContent1" html={description} />
          </div>
        </div>

        <div id="tooltip-rac1" className={`${className1} top tooltip-rac1`}>
          <div className={`${baseClass}__tooltip-inner-rac1`}>
              <span className={`${baseClass}__closenote-rac1`} onClick={this._clickLink1}>X</span>
              <Editable tag="h6" className={`${baseClass}__infoheading`} html={informationTitle} />
              <EditableDiv className="infoNoteContent1" html={informationDescription} />
          </div>
        </div>

        <div id="tooltip-rac2" className={`${className2} top tooltip-rac2`}>
          <div className={`${baseClass}__tooltip-inner-rac2`}>
              <span className={`${baseClass}__closenote-rac2`} onClick={this._clickLink2}>X</span>
              <Editable tag="h6" className={`${baseClass}__infoheading`} html={termsTitle} />
              <EditableDiv className="infoNoteContent1" html={termsDescription} />
          </div>
        </div>

      </div>

    );
  }
}



OpvRegTooltip.propTypes = {
  link : PropTypes.string,
  informationLink : PropTypes.string,
  termsLink : PropTypes.string,
  title: PropTypes.string,
  informationTitle: PropTypes.string,
  termsTitle: PropTypes.string,
  description: PropTypes.object,
  informationDescription: PropTypes.object,
  termsDescription: PropTypes.object
};

module.exports = OpvRegTooltip;
