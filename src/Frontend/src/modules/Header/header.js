import React from 'react';
import PropTypes from 'prop-types';

import './header.scss';

// import MediaQuery from 'react-responsive';
import PrimaryNav from '../PrimaryNav/PrimaryNav';
import Cta from '../Cta/Cta';

class Header extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};

    if(props.ctas) {
      this.ctas = props.ctas.map((item) => {
        return <Cta key={item.label} label={item.label} href={item.href} cssclass={item.cssclass} />;
      });
    }
  }

  render () {
    // const hasAdvisorBar = !!this.props.advisorBar;

    let className = 'header';
    if(this.props.isMini) className += ' header--mini';

    return (

      <div className={className}>
        <div className="header__content">
          <div className="container">
            <PrimaryNav {...this.props} />
          </div>
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  ctas: null,
  items: null,
  isMini: false
};

Header.propTypes = {
  ctas: PropTypes.array,
  items: PropTypes.array,
  isMini: PropTypes.bool
};

module.exports = Header;
