import React from 'react';
import PropTypes from 'prop-types';

import Cta from '../Cta/Cta';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import EditableImage from '../Editable/EditableImage';
import EditButton from '../CMS/EditButton';
import Quote from '../_ContentModules/Quote/Quote';
import ReevooSingleQuote from '../_ContentModules/Quote/ReevooSingleQuote';

import './StoryCard.scss';

/** StoryCard
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.StoryCard, {
    id: 'module-story',
  }), document.getElementById("element"));
*/
class StoryCard extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      isIE11: false
    };
    this.baseClass = 'storycards';
  }

  componentDidMount () {
    if(!SERVER) {
      const isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
      this.setState({
        isIE11
      });
    }
  }

  render () {
    // get the prop settings
    const {theme, backgroundImage, backgroundImageSrc, image, video, icon, editButton} = this.props;
    const {cta, label, heading, content} = this.props;
    const {type, isWide, isTall} = this.props;

    // base css class
    const baseClass = 'storycards__card';

    let cardClass = baseClass;

    if (['quote', 'reevoo', 'story', 'image', 'icon', 'video'].indexOf(type) >= 0) {
      cardClass += ` ${this.baseClass}--${type}`;
    }

    if (backgroundImage) {
      cardClass += ` ${this.baseClass}--background`;
    }

    if (cta) {
      cardClass += ` ${this.baseClass}--cta`;
    }

    if (isWide) {
      cardClass += ` ${this.baseClass}--wide`;
    }

    if (isTall) {
      cardClass += ` ${this.baseClass}--tall`;
    }

    let themeClass = '';

    if (theme.themeName) {
      themeClass += ` theme-${theme.themeName}`;
    }

    const isGreen = theme.textColour === 'green';

    if (!isGreen) {
      if (theme.textColour) {
        themeClass += ` theme-text-${theme.textColour}`;
      }
      else {
        themeClass += ' theme-text-contrast';
      }
    }


    let backgroundImageStyle = {};
    if (this.state.isIE11) {
      if (backgroundImage) {
        backgroundImageStyle = {backgroundImage: `url(${backgroundImageSrc})`, backgroundPosition: 'center'};
      }
    }

    return (
      <div className={cardClass}>
        <EditButton html={editButton} isSmall={true} align="right" />
        {type === 'reevoo' &&
          <ReevooSingleQuote {...this.props} noFadeInModule={true} cssClass={`${baseClass}__inner`} theme={theme.themeName} themeTextColour={theme.textColour} reevooParams={this.props.reevooParams} />
        }
        {type === 'quote' &&
          <Quote {...this.props} noFadeInModule={true} cssClass={`${baseClass}__inner`} theme={theme.themeName} themeTextColour={theme.textColour} />
        }
        {(type !== 'quote' && type !== 'reevoo') &&
          <div style={backgroundImageStyle} className={`${themeClass} ${baseClass}__inner`}>
            {!this.state.isIE11 && (
              <EditableImage image={backgroundImage} className={`box-background ${baseClass}__background`} />
            )}
            <div className={`${baseClass}__content-wrapper`}>
              <div className={`${baseClass}__top-wrapper`}>
                <Editable className="label" tag="h3" html={label} />
                <Editable className={`${baseClass}__title${isGreen ? ' theme-text-green' : ''}`} tag="h4" html={heading} />
                <EditableDiv html={content} />
              </div>
              <div className={`${baseClass}__bottom-wrapper`}>
                {type === 'video' && (
                  <div href={video.href} className={`${baseClass}__video`}>
                    {video.hasIcon && (
                      <Cta href={video.href} cssclass='video-play' icon='play' type='secondary' />
                    )}
                    <Editable tag="p" html={video.html} />
                  </div>
                )}
                {type === 'icon' && (
                  <EditableImage className={`${baseClass}__icon`} image={icon} />
                )}
                {type !== 'video' && type !== 'icon' && type !== 'image' && (
                  <Cta type={cta.type}  rawCta={cta.link} />
                )}
              </div>
            </div>
            {type === 'image' &&
              <div>
                <EditableImage className={`${baseClass}__image`} image={image} />
                <Cta type={cta.type}  rawCta={cta.link} />
              </div>
            }
          </div>
        }
      </div>
    );
  }
}

StoryCard.defaultProps = {
  id:'StoryCard',
  theme: {}
};

StoryCard.propTypes = {
  theme: PropTypes.object,
  heading: PropTypes.string,
  id:PropTypes.string,
  cta:PropTypes.object,
  label: PropTypes.string,
  type:PropTypes.string,
  content:PropTypes.string,
  backgroundImage:PropTypes.string,
  backgroundImageSrc:PropTypes.string,
  image:PropTypes.string,
  video:PropTypes.object,
  isTall: PropTypes.bool,
  isWide: PropTypes.bool,
  icon: PropTypes.string,
  editButton:PropTypes.string,
  reevooParams: PropTypes.any
};

module.exports = StoryCard;
