import React from 'react';
import PropTypes from 'prop-types';

import ModuleHeader from '../_Product/ModuleHeader/ModuleHeader';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';

import Cta from '../Cta/Cta';

import StoryCard from './StoryCard';

import './StoryCards.scss';

/** StoryCards
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.StoryCards, {
    id: 'storycards',
    label: 'Our Philosophy',
    heading: 'Great customer service is just the beginning',
    cta: {
      type: 'secondary',
      label: 'More stories',
      href:'#'
    },
    theme: {
      themeName: 'dusty-mint',
      topCurve:{
        type:'convex'
      },
      bottomCurve:{
        type: 'convex'
      },
      themeBefore:'warm-grey',
      themeAfter:'warm-grey'
    },
    cards: [
      {
        type: 'quote',
        theme: {
          themeName: 'white',
          // textColour: 'green'
        },
        copy:'The Green Heart Foundation  lorem ipsum dolor set amet sed do eius mod. Duis aute irure dolor in repre henderit indolore fugiat nulla pariatur set.',
        authorLink: {
          href: "/faq",
          html: "<a id=\"6C71B935B59745DB98FD32AE7E3EF26A-AE48812DEADC450DB3A374EA593DDC5C/faq\" href=\"/faq\">FAQ</a>",
          id: "6C71B935B59745DB98FD32AE7E3EF26A-AE48812DEADC450DB3A374EA593DDC5C/faq",
          label: "FAQ",
          rel: ""
        },
        author:'Barbara Name',
        role:'LV= member',
      },
      {
        type: 'video',
        theme: {
          textColour: 'white',
          themeName: 'warm-grey'
        },
        backgroundImage: '<img src="dist/images/placeholder/background-city.jpg">',
        backgroundImageSrc: 'dist/images/placeholder/background-city.jpg',
        label: 'LV=stories',
        heading: '2 Find out about Sheila’s story and what LV did',
        video: {html:"<p>Watch Sheila's Story</p>", href:'#'}
      },
      {
        type: 'story',
        theme: {
          themeName: 'strawberry'
        },
        label: 'LV=stories',
        heading: '3 Find out about Sheila’s story and what LV did',
        content: 'aiddhgaisdbcak ui awgfuwegf uksf gs',
        cta: {type:'secondary',label:'Read more'}
      },
      {
        type: 'icon',
        theme: {
          themeName: 'clover'
        },
        label: 'Awards',
        heading: '4 Proud to be the consumer champion’s best insurance brand',
        icon: '<a href="http://www.which.com"><img src="dist/images/which.png"></a>',
      },
      {
        type: 'image',
        theme: {
          themeName: 'white',
          textColour: 'green'
        },
        label: 'Awards',
        heading: "We're delighted when you take the time to <b>share your moments</b>",
        image: 'http://placehold.it/370x270',
        cta: {type:'secondary',label:'Read more'}
      },
    ]
  }), document.getElementById("element"));
*/
class StoryCards extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
    this.baseClass = 'storycards';

    this.cards = props.cards.map((card,i) => {
      let isWide = false;
      let isTall = false;
      if (i === 1 || i === 2) {
        isTall = true;
      }
      if  (i === 4) {
        isWide = true;
      }
      return(
        <StoryCard key={props.id + i + card.heading} {...card} isWide={isWide} isTall={isTall} />
      );
    });
  }

  render () {
    // get the prop settings
    const {heading, theme, cta, label} = this.props;

    // base css class
    const baseClass = this.baseClass;

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true}>
        <ModuleHeader headline={heading} label={label} themeColour={theme.textColour}  />
        <div className={`${baseClass}__cards`}>
          <div className={`${baseClass}__cards__top`}>
            {this.cards.slice(0,4)}
          </div>
          <div className={`${baseClass}__cards__bottom`}>
            {this.cards.slice(4,5)}
          </div>
        </div>
        <div className="cta-wrapper"><Cta cssclass='more-stories' {...cta} /></div>
      </ModuleWrapper>
    );
  }
}

StoryCards.defaultProps = {
  id:'StoryCards',
  theme: {},
  cards:[]
};

StoryCards.propTypes = {
  theme: PropTypes.object,
  heading: PropTypes.string,
  id:PropTypes.string,
  cta:PropTypes.object,
  label: PropTypes.string,
  cards: PropTypes.array
};

module.exports = StoryCards;
