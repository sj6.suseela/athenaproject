import React from 'react';
import PropTypes from 'prop-types';

import './Loading.scss';

/** Loading
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.Loading, {
    isActive: true,
    className: 'heartsPreloader',
    icon: "http://localhost:1234/src/Frontend/dist/images/icon/icon-heart.svg",
    icon2: "http://localhost:1234/src/Frontend/dist/images/icon/icon-heart2.svg",
    icon3: "http://localhost:1234/src/Frontend/dist/images/icon/icon-heart3.svg",
    isFullScreen: false,
  }), document.getElementById("element"));
*/

class Loading extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    const {isActive, className, icon, icon2, icon3, isFullScreen} = this.props;
    const loadingIcon = icon ? icon : '/dist/images/icon/icon-heart.svg';
    const loadingIcon2 = icon2 ? icon2 : '/dist/images/icon/icon-heart2.svg';
    const loadingIcon3 = icon3 ? icon3 : '/dist/images/icon/icon-heart3.svg';
    const cssClass = isFullScreen ? 'loading--fullscreen' : '';
    if (!isActive) {
      return null;
    }
    return (
      <div className={`loading ${className} ${cssClass}`}>
        <img src={loadingIcon} className="img__left" />
        <img src={loadingIcon2} className="img__middle" />
        <img src={loadingIcon3} className="img__right" />
      </div>
    );
  }
}

Loading.propTypes = {
  isActive: PropTypes.bool,
  className: PropTypes.string,
  icon: PropTypes.string,
  icon2: PropTypes.string,
  icon3: PropTypes.string,
  isFullScreen: PropTypes.bool
};

module.exports = Loading;
