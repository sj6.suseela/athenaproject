import React from 'react';
import {renderToString} from 'react-dom/server';
import PropTypes from 'prop-types';
import axios from 'axios';

import './PopularArticles.scss';

import Carousel from '../Carousel/Carousel';
import ArticlePreview from '../ArticlePreview/ArticlePreview';
import SectionTitle from '../Section/SectionTitle';
import ModuleWrapper from '../ModuleWrapper/ModuleWrapper';
import LoadMore from '../LoadMore/LoadMore';
import Editable from '../Editable/Editable';

/** PopularArticles
  * @description description
  * @example

  var item = {
    title : "Lorem ipsum lorem ipsum lorem ipsum lorem ...",
    category : 'Category',
    link:{href : "#/article-1/"},
    readTime : "5 min read",
    image : '<img src="http://placehold.it/600x400" alt="" />',
    date : { day:12, month:{ label:"December", number:12, }, year:2017, time:"12:00" },
  };

  ReactDOM.render(React.createElement(Components.PopularArticles, {
    id : 'id',
    theme : {},
    heading : "These are our most loved stories",
    icon : '<img src="dist/images/icon/icon-books.svg">',
    items : [item, item, item, item, item, item]
  }), document.getElementById("element"));
*/
class PopularArticles extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      items:props.items,
      pageCount:2,
      btnVisible:true,
      btnLoading:false,
      loading:false
    };

    this.updateItems = this.updateItems.bind(this);
    this._onChange = this._onChange.bind(this);
    this.loadMore = this.loadMore.bind(this);
  }

  updateItems (newitems,auto) {
    if(auto) {
      const itemList = newitems.items.map((item) => {
        const div = renderToString(<ArticlePreview {...item} />);
        return (div);
      });

      this.carousel.append(itemList);
    }
    else{
      const updatedItems = [...this.state.items, ...newitems.items];
      this.setState({
        items:updatedItems
      });
    }
  }

  loadMore (auto) {
    const _this = this;

    this.carousel.disableDrag();

    this.setState({
      btnLoading:true
    });

    const pagecount = this.state.pageCount;
    const ajaxlink = `${this.props.link.href}`;

    const config = {
      headers: {'Data-Type': 'json'},
      params: {
        page: pagecount
      }
    };

    this.setState({
      loading: true
    });

    axios(ajaxlink,config)
      .then((response) => {
        const jsonResponse = JSON.parse(response.data);
        if(jsonResponse.isLastPage) {
          _this.setState({
            btnVisible:false
          });
        }

        _this.carousel.enableDrag();

        _this.setState({
          btnLoading:false,
          pageCount: pagecount + 1,
          loading: false
        });
        _this.updateItems(jsonResponse,auto);
      })
      .catch(() => {
         // console.log(error);
        this.setState({
          loading: false
        });
      });
  }

  _onChange (current,total) {
    if(current >= total - 2) {
      if(!this.state.btnLoading && this.state.btnVisible) {
        this.loadMore(true);
      }
    }
  }

  render () {
    const baseClass = 'popularArticles';

    const options = {
      cellAlign: 'left',
      prevNextButtons: false,
      pageDots: false,
      groupCells: true,
      wrapAround: false
    };

    const {id,cta,ajax, totalItems,noMobileCarousel} = this.props;

    const {btnVisible,btnLoading} = this.state;

    const itemList = this.state.items.map((item,i) => {
      return (<div key={i}><ArticlePreview {...item} /></div>);
    });

    const ctaClass = ajax ? 'desktopOnly' : '';

    const mobileCarousel = noMobileCarousel ? 'carousel--noMobileCarousel' : '';

    let classes = '';

    if(this.props.theme.themeName) {
      classes += ` ${baseClass}--hasBg`;
    }

    return (
      <ModuleWrapper {...this.props} baseClass={baseClass} isProduct={true} classes={classes}>
        <SectionTitle {...this.props} />
        <Editable className={`${baseClass}__total`} html={totalItems} />
        <div className={`carousel carousel--3up ${mobileCarousel}`}>
          <Carousel isLazyLoad={true} itemsPerRow={3} onChange={this._onChange} items={itemList} options={options} id={id} ref={instance => { this.carousel = instance; }} noMobileCarousel={noMobileCarousel} />
          {cta && cta.link && cta.link.html && this.state.items.length > 3 && (
            <div className={`popularArticles__cta ${ctaClass} `}>
              <LoadMore ajax={ajax} ref={c => this.loadmore = c} btnType={cta.type} btnLink={cta.link.html} loadMoreFunc={this.loadMore} isBtnVisible={btnVisible} isBtnLoading={btnLoading}/>
            </div>
          )}
        </div>
      </ModuleWrapper>
    );
  }
}

PopularArticles.defaultProps = {
  theme : {
    themeName : 'warm-grey'
  },
  totalItems: '',
  items: [],
  heading: '',
  cta:{},
  link: {}
};

PopularArticles.propTypes = {
  items:PropTypes.array,
  totalItems: PropTypes.string,
  heading:PropTypes.string,
  id:PropTypes.string,
  cta:PropTypes.object,
  ajax:PropTypes.any,
  noMobileCarousel:PropTypes.bool,
  theme:PropTypes.object,
  link:PropTypes.object
};

module.exports = PopularArticles;
