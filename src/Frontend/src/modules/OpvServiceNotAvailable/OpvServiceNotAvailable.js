import React from 'react';
import PropTypes from 'prop-types';
import Editable from '../Editable/Editable';
import EditableDiv from '../Editable/EditableDiv';
import Cta from '../Cta/cta';
import './OpvServiceNotAvailable.scss';

class OpvServiceNotAvailable extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
  }

  render () {
    const {submit, title, description} = this.props;
    const baseClass = 'service-not-available';

    return (
      <div>
        <Editable tag="h2" className={`${baseClass}__title`} html={title} />
        <EditableDiv className={`${baseClass}__content`} html={description} />
        <Cta type="primary" isSmall={true} rawCta={submit} />
      </div>
    );
  }
}

OpvServiceNotAvailable.defaultProps = {
  submit: '',
  title: '',
  description: ''
};

OpvServiceNotAvailable.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  submit: PropTypes.string
};

module.exports = OpvServiceNotAvailable;
