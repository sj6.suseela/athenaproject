import React from 'react';
import PropTypes from 'prop-types';

import Theme from '../Theme';

class ModuleCurve extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
    };
  }

  render () {
    const {position,type,bgtheme,className,id} = this.props;

    let pathdata,bggradient;

    let top,bottom;

    let pathStyle = {};

    if(type === 'convex') {
      if(position === 'top') {
        pathdata = ['M0,36S294,0,719.5,0,1439,36,1439,36V0H0Z'];
        bggradient = Theme.getTheme(bgtheme);
        top = 0;
        bottom = 'auto';
        pathStyle = {
          transform:'translateY(-0.3px)'
        };
      }
      else{
        pathdata = ['M1440,0c-1.3,0.1-2.7,0.3-4,0.4c-209.1,22.3-454.1,35.6-716,35.6h720V0z','M720,35.6C456.4,35.6,210,22.6,0,0v35.6H720z'];
        bggradient = Theme.getTheme(bgtheme);
        pathStyle = {
        };
        top = 'auto';
        bottom = 0;
      }
    }
    else if(type === 'concave') {
      if(position === 'top') {
        pathdata = ['M0,0S127,36,719.5,36,1439,0,1439,0Z'];
        bggradient = Theme.getTheme(bgtheme);
        pathStyle = {
        };

        top = 0;
        bottom = 'auto';
      }
      else{
        pathdata = ['M0,36S300.6,0,716.6,0V36Z','M1440,36S1136.55,0,716.6,0V36Z'];
        bggradient = Theme.getTheme(bgtheme);
        top = 'auto';
        bottom = 0;
      }
    }

    const divstyle = {
      position:'absolute',
      width:'100%',
      top,
      bottom,
      left:'0',
      pointerEvents:'none'
    };

    const svgstyle = {
      display:'block',
      lineHeight:0,
      width:'101%',
      zIndex:3,
      top,
      bottom,
      left:'0',
      marginBottom: '-1px',
      marginTop: '-1px',
      marginLeft: '-0.5%',
      height:'auto'
    };

    if(!pathdata) {
      return null;
    }
    const paths = pathdata.map((item) => {
      return <path key={item} d={item} fill="#ffffff" style={pathStyle}/>;
    });

    let aspectRatio;
    if(position === 'top') {
      aspectRatio = 'xMinYMin';
    }
    else{
      aspectRatio = 'xMinYMax';
    }

    return (
      <div style={divstyle} className={className}>
        <svg version="1.1" x="0px" y="0px" viewBox="0 0 1439 35.6" preserveAspectRatio={aspectRatio} style={svgstyle}  xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" xmlSpace="preserve">
          <defs>
            <linearGradient id={`${id}-gradbg`}>
              <stop offset="0%" stopColor={bggradient[0]} />
              <stop offset="100%" stopColor={bggradient[1]} />
            </linearGradient>
            <mask id={`${id}-mask`}>
              {paths}
            </mask>
          </defs>
          <rect mask={`url(#${id}-mask)`} fill={`url(#${id}-gradbg)`}  style={pathStyle} width="1440" height="35.6"/>
        </svg>
      </div>
    );
  }
}

ModuleCurve.propTypes = {
  position:PropTypes.string,
  type:PropTypes.string,
  foretheme:PropTypes.string,
  bgtheme:PropTypes.string,
  className:PropTypes.string,
  id:PropTypes.string
};

module.exports = ModuleCurve;
