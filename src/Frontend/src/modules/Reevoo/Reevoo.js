import React from 'react';
import PropTypes from 'prop-types';

import './Reevoo.scss';

import EditableImage from '../Editable/EditableImage';

class Reevoo extends React.Component {
  constructor (props) {
    super(props);

    this.state = { };
  }

  componentDidMount () {
    if(!SERVER) {
      const reevooScriptId = 'reevoomark-loader';
      const reevooScript = document.getElementById(reevooScriptId);
      if (!reevooScript) {
        const reevooInjector = document.createElement('script');
        reevooInjector.type = 'text/javascript';
        reevooInjector.id = reevooScriptId;
        reevooInjector.src = `${this.props.scriptBase}reevoo-script.js`;
        document.body.appendChild(reevooInjector);
      }
    }
  }

  render () {
    const {icon, reevooParams} = this.props;
    const hasProps = !!reevooParams && !!reevooParams.trkref && !!reevooParams.sku;

    if (hasProps && !reevooParams.variant) {
      reevooParams.variant = 'newBrand';
    }

    return (
      <div className="reevoo">
        <div className="reevoo__container">
          {hasProps && (
            <div className="reevoo__info">
                <reevoo-reviewable-badge {...reevooParams}>
                </reevoo-reviewable-badge>
            </div>
          )}
          <EditableImage className="reevoo__image" image={icon} />
        </div>
        <div className='reevoo__curve'>
          <svg viewBox="0 0 286 24">
            <path d="M284.8 2.269s-135-7-284 20" stroke="#46616E" strokeWidth="1.6" fill="none" strokeLinecap="round" strokeLinejoin="round"/>
          </svg>
        </div>
      </div>
    );
  }
}

Reevoo.defaultProps = {
  scriptBase:'/dist/'
};

Reevoo.propTypes = {
  icon: PropTypes.string,
  reevooParams: PropTypes.object,
  scriptBase:PropTypes.string
};

module.exports = Reevoo;
