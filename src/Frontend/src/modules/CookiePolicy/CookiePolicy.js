import React from 'react';
import PropTypes from 'prop-types';

import './CookiePolicy.scss';

import ContentExpander from '../ContentExpander/ContentExpander';
import Icon from '../Icon/Icon';
import EditableDiv from '../Editable/EditableDiv';
import Editable from '../Editable/Editable';

import SitecoreSettings from '../SitecoreSettings';

/** CookiePolicy
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.CookiePolicy, {
    id : 'id',
    theme : {},
    text : '<p>We use cookies to give you the best possible experience online. By continuing to use our website, you agree to receiving our cookies on your web browser. Visit our <a href="#">cookie policy page</a> to find out more and how to change your cookie settings.</p>',
    closeText : "Close"
  }), document.getElementById("element"));
*/
class CookiePolicy extends React.Component {
  constructor (props) {
    super(props);

    this._close = this._close.bind(this);
    const cookieAcceptedTicks = parseInt(this.getCookie('cookiePolicyAccepted'));
    const cookieAccepted = (cookieAcceptedTicks >= props.lastUpdated || cookieAcceptedTicks === 2);

    this.state = {
      open:false,
      accepted:cookieAccepted,
      isEditing:false
    };
  }

  _close (e) {
    e.preventDefault();
    const _this = this;

    if(!this.state.isEditing) {
      this.setCookie('cookiePolicyAccepted',this.props.lastUpdated, 365 * 50);
    }

    this.setState({
      open:false
    });

    setTimeout(() => {
      _this.setState({
        accepted:true
      });
    },500);
  }

  setCookie (cname, cvalue, exdays) {
    if(!SERVER) {
      const d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
      const expires = `expires=${ d.toUTCString()}`;
      document.cookie = `${cname  }=${  cvalue  };${  expires  };path=/`;
    }
  }

  getCookie (cname) {
    if(!SERVER) {
      const name = `${cname  }=`;
      const decodedCookie = decodeURIComponent(document.cookie);
      const ca = decodedCookie.split(';');
      for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
          return c.substring(name.length, c.length);
        }
      }
      return '';
    }
  }

  componentDidMount () {
    this.setState({
      open:true
    });

    const sc = new SitecoreSettings();

    if(this.state.isEditing !== sc.isExperienceEditor()) {
      this.setState({
        isEditing:sc.isExperienceEditor()
      });
    }
  }

  render () {
    const {open,accepted} = this.state;

    if(!accepted) {
      return (

          <div className="cookie-msg">
              <ContentExpander isOpen={open} className="" fillScreen={false} hasClose={false} onClose={this._close}>
                <div className="cookie-msg__body">
                  <Icon icon="info"/>
                  <EditableDiv html={this.props.text} />
                  <a href="#" className="cta cta--primary cta--sm" onClick={this._close}><Editable html={this.props.closeText} /></a>
                </div>
              </ContentExpander>
          </div>
      );
    }
    else{
      return null;
    }
  }
}

CookiePolicy.propTypes = {
  text: PropTypes.string,
  closeText: PropTypes.string,
  lastUpdated:PropTypes.number
};

module.exports = CookiePolicy;
