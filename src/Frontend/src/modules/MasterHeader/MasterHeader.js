import React from 'react';
import PropTypes from 'prop-types';

import './MasterHeader.scss';
import ContentExpander from '../ContentExpander/ContentExpander';
import MediaQuery from 'react-responsive';
import Icon from '../Icon/Icon';
import Editable from '../Editable/Editable';

/** MasterHeader
  * @description description
  * @example
  ReactDOM.render(React.createElement(Components.MasterHeader, {
    mobileLabel: 'Menu',
    isSiteHeader: true,
    items:[
      {
        label:'Insurance',
        link:{
          html:'<a href="homepage-insurance.html">Insurance</a>',
          href:'homepage-insurance.html',
        },
      },
      {
        label:'Life Insurance, Investments & Pensions',
        isSelected: true,
        link:{
          html:'<a href="homepage-life.html">Life Insurance, Investments & Pensions</a>',
          href:'homepage-life.html'
        },
      },
    ]
  }), document.getElementById("element"));
*/
class MasterHeader extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      navOpen:false
    };

    this.items = this.props.items.map((item,i) => {
      const {label,isSelected,link} = item;

      const cssClass = isSelected ? 'selected' : '';

      return (
          <li title={label} key={item + i} className={cssClass}>
            <a id={link.id} href={link.href} rel={link.rel}>
              <span className="masterHeader__arrowIcon"><Icon icon="rightarrow"/></span>
              <Editable html={label} />
              <span className='masterHeader__navChev'>
                <Icon icon="navArrow" fillColor='#fff'/>
              </span>
            </a>
          </li>
      );
    });

    this.mobileItems = this.props.items.map((item,i) => {
      const {isSelected} = item;
      const cssClass = isSelected ? 'selected' : '';
      const label = (item.mobileLabel && item.mobileLabel.length > 0) ? item.mobileLabel : item.label;
      const link = item.link ? item.link : {};
      return (
        <li className={cssClass} key={item + i}><a id={link.id} href={link.href} rel={link.rel}>
          <Editable html={label} /></a>
        </li>
      );
    });

    this._toggleNav = this._toggleNav.bind(this);
    this._closeNav = this._closeNav.bind(this);
  }

  _toggleNav (e) {
    e.preventDefault();
    this.setState({
      navOpen:!this.state.navOpen
    });
  }

  _closeNav () {
    this.setState({
      navOpen:false
    });
  }

  render () {
    const {mobileLabel, hasNoneSelected} = this.props;

    const baseClass = 'masterHeader';

    let cssClass = baseClass;
    if (hasNoneSelected) {
      cssClass += ` ${baseClass}--none-selected`;
    }

    let triggerClass = 'masterHeader__trigger';

    if(this.state.navOpen) triggerClass += ' active';

    return (
      <div className={cssClass}>
        <div className="container">
          <div className={triggerClass} ref={(c) => {this.trigger = c; }}>
            <a href="#" onClick={this._toggleNav} >
              <Icon icon="downarrow" fillColor="#04589b"/>
              <Editable html={mobileLabel} />
            </a>
          </div>
          <nav className={`${baseClass}__nav ${baseClass}__nav--desktop`} onMouseEnter={this._onMouseOverNav} onMouseLeave={this._onMouseOutNav}>
            <ul className={`${baseClass}__ul`}>
              {this.items}
            </ul>
          </nav>
        </div>
        <MediaQuery query="(max-width: 768px)">
          <ContentExpander isOpen={this.state.navOpen} isFillScreen={false} hasClose={false} onClose={this._closeNav} trigger={this.trigger}>
              <div className="container">
                <nav className={`${baseClass}__nav`}>
                  <ul className={`${baseClass}__ul`}>
                    {this.mobileItems}
                  </ul>
                </nav>
              </div>
          </ContentExpander>
        </MediaQuery>
      </div>
    );
  }
}

MasterHeader.defaultProps = {
  items:[],
  isFillScreen:false,
  hasClose:false
};

MasterHeader.propTypes = {
  items:PropTypes.array,
  mobileLabel:PropTypes.string,
  hasNoneSelected:PropTypes.bool
};

module.exports = MasterHeader;
