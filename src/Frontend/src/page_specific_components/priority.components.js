import PrimaryNav from "../modules/PrimaryNav/PrimaryNav";
import MicrositeNav from "../modules/MicrositeNav/MicrositeNav";
import SecondaryNav from "../modules/SecondaryNav/SecondaryNav";
import HeaderBanner from "../modules/HeaderBanner/HeaderBanner";
import MasterHeader from "../modules/MasterHeader/MasterHeader";
import OpvHeaderBanner from "../modules/OpvHeaderBanner/OpvHeaderBanner";
import "../sass/variables.mab.scss";

import "../sass/mixins.scss";
import "../sass/fonts.scss";
import "../sass/main.scss";
import "../sass/opvcustom.scss";
import "../sass/typography.scss";
import "../sass/buttons.scss";
import "../sass/layout.scss";
import "../sass/theme.scss";
import "../sass/component.hide.scss";

import "../modules/Date/Date.scss";
import "../modules/Icon/Icon.scss";
import "../modules/Cta/Cta.scss";
import "../modules/SearchBox/SearchBox.scss";

const PriorityComponents = {
  PrimaryNav,
  MicrositeNav,
  SecondaryNav,
  HeaderBanner,
  MasterHeader,
  OpvHeaderBanner
};

module.exports = PriorityComponents;
