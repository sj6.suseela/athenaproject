import HeaderBanner from "../modules/HeaderBanner/HeaderBanner";
import BodyCopy from "../modules/_ContentModules/BodyCopy/BodyCopy";
import FaqHead from "../modules/Faq/FaqHead";

const Help = {
  HeaderBanner,
  BodyCopy,
  FaqHead
};

module.exports = Help;
