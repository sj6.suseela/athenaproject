import Accordion from '../modules/Accordion/Accordion';
import Advice from '../modules/_Product/Advice/Advice';
import AnchorHeading from '../modules/AnchorHeading/AnchorHeading';
import AnchorNav from '../modules/AnchorNav/AnchorNav';
import Archive from '../modules/Archive/Archive';
import Author from '../modules/Author/Author';
import Awards from '../modules/_Product/Awards/Awards';
import BlockLinks from '../modules/_Product/BlockLinks/BlockLinks';
import BodyCopy from '../modules/_ContentModules/BodyCopy/BodyCopy';
import BodyIntro from '../modules/_ContentModules/BodyIntro/BodyIntro';
import Breadcrumb from '../modules/Breadcrumb/Breadcrumb';
import MasterHeader from '../modules/MasterHeader/MasterHeader';
import CarouselImage from '../modules/Carousel/CarouselImage';
import CaseStudyCarousel from '../modules/CaseStudy/CaseStudyCarousel';
import Claims from '../modules/Claims/Claims';
import ComparisonTable from '../modules/_ContentModules/ComparisonTable/ComparisonTable';
import ContactCard from '../modules/ContactCard/ContactCard';
import ContactCards from '../modules/ContactCard/ContactCards';
import ContactNumber from '../modules/ContactNumber/ContactNumber';
import ContactUs from '../modules/ContactUs/ContactUs';
import CookiePolicy from '../modules/CookiePolicy/CookiePolicy';
import Cover from '../modules/_Product/Cover/Cover';
import CtaModule from '../modules/_ContentModules/CtaModule/CtaModule';
import Documents from '../modules/_Product/Documents/Documents';
import Example from '../modules/Example/Example';
import Extras from '../modules/_Product/Extras/Extras';
import Faq from '../modules/Faq/Faq';
import FaqHead from '../modules/Faq/FaqHead';
import FaqSection from '../modules/Faq/FaqSection';
import FeaturedArticles from '../modules/FeaturedArticles/FeaturedArticles';
import Features from '../modules/_Product/Features/Features';
import FloatingActionButton from '../modules/FloatingActionButton/FloatingActionButton';
import Footer from '../modules/Footer/Footer';
import Form from '../modules/Form/Form';
import Glossary from '../modules/Glossary/Glossary';
import HeaderBanner from '../modules/HeaderBanner/HeaderBanner';
import HeaderCopy from '../modules/HeaderCopy/HeaderCopy';
import HeaderCta from '../modules/HeaderCta/HeaderCta';
import IFrame from '../modules/IFrame/IFrame';
import GoogleMap from '../modules/GoogleMap/GoogleMap';
import Image from '../modules/_ContentModules/Image/Image';
import ImageBlocks from '../modules/ImageBlocks/ImageBlocks';
import ImportantInformation from '../modules/_Product/ImportantInformation/ImportantInformation';
import InPageNav from '../modules/InPageNav/InPageNav';
import Item from '../modules/_Product/Products/Item';
import MakeClaim from '../modules/_Tmp/MakeClaim';
import Media from '../modules/Media/Media';
import Notification from '../modules/Notification/Notification';
import Pagination from '../modules/Pagination/Pagination';
import PopularArticles from '../modules/PopularArticles/PopularArticles';
import PrimaryNav from '../modules/PrimaryNav/PrimaryNav';
import ProductList from '../modules/_Product/Products/ProductList';
import ProductTiles from '../modules/ProductTiles/ProductTiles';
import Promo from '../modules/Promo/Promo';
import Quicklinks from '../modules/_Product/Quicklinks/Quicklinks';
import Quote from '../modules/_ContentModules/Quote/Quote';
import ReevooQuote from '../modules/_Product/ReevooQuote/ReevooQuote';
import ReevooSingleQuote from '../modules/_ContentModules/Quote/ReevooSingleQuote';
import Related from '../modules/Related/Related';
import SearchResults from '../modules/SearchResults/SearchResults';
import SecondaryNav from '../modules/SecondaryNav/SecondaryNav';
import Share from '../modules/SocialIcons/Share';
import StartQuote from '../modules/_Product/StartQuote/StartQuote';
import StickyNav from '../modules/_Product/StickyNav/StickyNav';
import StoryCards from '../modules/StoryCards/StoryCards';
import SubArticles from '../modules/SubArticles/SubArticles';
import Support from '../modules/_Tmp/Support';
import Tags from '../modules/Tags/Tags';
import Timeline from '../modules/_Product/Timeline/Timeline';
import UnitPrice from '../modules/_Product/UnitPrice/UnitPrice';
import Video from '../modules/_ContentModules/Video/Video';
import VideoVimeo from '../modules/_ContentModules/Video/VideoVimeo';
import NotificationDetail from '../modules/Notification/NotificationDetail';
import IconList from '../modules/_ContentModules/List/IconList';
import Contact from '../modules/Contact/Contact';
import ProductAccordion from '../modules/_Product/ProductAccordion/ProductAccordion';

const KitchensinkComponents = {
  Accordion,
  Advice,
  AnchorHeading,
  AnchorNav,
  Archive,
  Author,
  Awards,
  BlockLinks,
  BodyCopy,
  BodyIntro,
  Breadcrumb,
  MasterHeader,
  CarouselImage,
  CaseStudyCarousel,
  Claims,
  ComparisonTable,
  ContactCard,
  ContactCards,
  ContactNumber,
  ContactUs,
  CookiePolicy,
  Cover,
  CtaModule,
  Documents,
  Example,
  Extras,
  Faq,
  FaqHead,
  FaqSection,
  FeaturedArticles,
  Features,
  FloatingActionButton,
  Footer,
  Form,
  Glossary,
  HeaderBanner,
  HeaderCopy,
  HeaderCta,
  IFrame,
  GoogleMap,
  Image,
  ImageBlocks,
  ImportantInformation,
  InPageNav,
  Item,
  MakeClaim,
  Media,
  Notification,
  Pagination,
  PopularArticles,
  PrimaryNav,
  ProductList,
  ProductTiles,
  Promo,
  Quicklinks,
  Quote,
  ReevooQuote,
  ReevooSingleQuote,
  Related,
  SearchResults,
  SecondaryNav,
  Share,
  StartQuote,
  StickyNav,
  StoryCards,
  SubArticles,
  Support,
  Tags,
  Timeline,
  UnitPrice,
  Video,
  VideoVimeo,
  NotificationDetail,
  IconList,
  Contact,
  ProductAccordion
};

module.exports = KitchensinkComponents;
