import Example from '../modules/Example/Example';
import PrimaryNav from '../modules/PrimaryNav/PrimaryNav';
import SecondaryNav from '../modules/SecondaryNav/SecondaryNav';
import Footer from '../modules/Footer/Footer';
import CookiePolicy from '../modules/CookiePolicy/CookiePolicy';
import FloatingActionButton from '../modules/FloatingActionButton/FloatingActionButton';
import MasterHeader from '../modules/MasterHeader/MasterHeader';
import Breadcrumb from '../modules/Breadcrumb/Breadcrumb';
import HeaderBanner from '../modules/HeaderBanner/HeaderBanner';
import HeaderCopy from '../modules/HeaderCopy/HeaderCopy';
import BodyCopy from '../modules/_ContentModules/BodyCopy/BodyCopy';
import BodyIntro from '../modules/_ContentModules/BodyIntro/BodyIntro';
import Image from '../modules/_ContentModules/Image/Image';
import Video from '../modules/_ContentModules/Video/Video';
import VideoVimeo from '../modules/_ContentModules/Video/VideoVimeo';
import Quote from '../modules/_ContentModules/Quote/Quote';
import Promo from '../modules/Promo/Promo';
import Share from '../modules/SocialIcons/Share';
import CtaModule from '../modules/_ContentModules/CtaModule/CtaModule';
import CarouselImage from '../modules/Carousel/CarouselImage';
import Tags from '../modules/Tags/Tags';
import InPageNav from '../modules/InPageNav/InPageNav';
import Accordion from '../modules/Accordion/Accordion';
import Form from '../modules/Form/Form';
import IFrame from '../modules/IFrame/IFrame';
import GoogleMap from '../modules/GoogleMap/GoogleMap';
import ContactUs from '../modules/ContactUs/ContactUs';
import ContactNumber from '../modules/ContactNumber/ContactNumber';
import Claims from '../modules/Claims/Claims';
import Faq from '../modules/Faq/Faq';
import FaqSection from '../modules/Faq/FaqSection';
import FaqHead from '../modules/Faq/FaqHead';
import Notification from '../modules/Notification/Notification';
import SearchResults from '../modules/SearchResults/SearchResults';
import Pagination from '../modules/Pagination/Pagination';
import Support from '../modules/_Tmp/Support';
import MakeClaim from '../modules/_Tmp/MakeClaim';
import Media from '../modules/Media/Media';
import ContactCard from '../modules/ContactCard/ContactCard';
import ContactCards from '../modules/ContactCard/ContactCards';
import Archive from '../modules/Archive/Archive';
import Loading from '../modules/Loading/Loading';
import SearchRefinerNav from '../modules/SearchRefinerNav/SearchRefinerNav';

import "../sass/mixins.scss";
import '../sass/"fonts.scss';
import "../sass/main.scss";
import "../sass/typography.scss";
import "../sass/buttons.scss";
import "../sass/layout.scss";
import "../sass/theme.scss";
import "../sass/component.hide.scss";

const CoreComponents = {
  Archive,
  PrimaryNav,
  SecondaryNav,
  Footer,
  CookiePolicy,
  FloatingActionButton,
  MasterHeader,
  Breadcrumb,
  HeaderBanner,
  HeaderCopy,
  BodyCopy,
  Image,
  Video,
  VideoVimeo,
  Quote,
  Promo,
  Share,
  CtaModule,
  CarouselImage,
  Tags,
  InPageNav,
  Accordion,
  BodyIntro,
  Example,
  Form,
  IFrame,
  GoogleMap,
  ContactUs,
  Faq,
  FaqSection,
  FaqHead,
  Notification,
  ContactNumber,
  Claims,
  Support,
  MakeClaim,
  SearchResults,
  Media,
  ContactCard,
  Pagination,
  ContactCards,
  Loading,
  SearchRefinerNav
};

module.exports = CoreComponents;
