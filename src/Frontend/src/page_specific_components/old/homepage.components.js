import Awards from '../modules/_Product/Awards/Awards';
import HeaderCta from '../modules/HeaderCta/HeaderCta';
import PopularArticles from '../modules/PopularArticles/PopularArticles';
import ProductTiles from '../modules/ProductTiles/ProductTiles';
import StoryCards from '../modules/StoryCards/StoryCards';
import Quicklinks from '../modules/_Product/Quicklinks/Quicklinks';



const HomepageComponents = {
  Awards,
  HeaderCta,
  ProductTiles,
  PopularArticles,
  StoryCards,
  Quicklinks
};

module.exports = HomepageComponents;
