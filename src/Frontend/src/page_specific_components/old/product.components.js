import Glossary from '../modules/Glossary/Glossary';
import Advice from '../modules/_Product/Advice/Advice';
import Awards from '../modules/_Product/Awards/Awards';
import BuyapowaModule from '../modules/Buyapowa/Buyapowa';
import Cover from '../modules/_Product/Cover/Cover';
import Documents from '../modules/_Product/Documents/Documents';
import Quicklinks from '../modules/_Product/Quicklinks/Quicklinks';
import Extras from '../modules/_Product/Extras/Extras';
import Faq from '../modules/Faq/Faq';
import Features from '../modules/_Product/Features/Features';
import ImportantInformation from '../modules/_Product/ImportantInformation/ImportantInformation';
import StartQuote from '../modules/_Product/StartQuote/StartQuote';
import Timeline from '../modules/_Product/Timeline/Timeline';
import Related from '../modules/Related/Related';
import CaseStudyCarousel from '../modules/CaseStudy/CaseStudyCarousel';
import ReevooQuote from '../modules/_Product/ReevooQuote/ReevooQuote';
import ReevooMoments from '../modules/ReevooMoments/ReevooMoments';
import ReevooReviews from '../modules/ReevooReviews/ReevooReviews';
import BlockLinks from '../modules/_Product/BlockLinks/BlockLinks';
import Item from '../modules/_Product/Products/Item';
import ProductList from '../modules/_Product/Products/ProductList';
import UnitPrice from '../modules/_Product/UnitPrice/UnitPrice';
import StickyNav from '../modules/_Product/StickyNav/StickyNav';
import ComparisonTable from '../modules/_ContentModules/ComparisonTable/ComparisonTable';
import ProductAccordion from '../modules/_Product/ProductAccordion/ProductAccordion';

const Product = {
  Advice,
  Awards,
  BuyapowaModule,
  Cover,
  Documents,
  Quicklinks,
  Extras,
  Faq,
  Features,
  ImportantInformation,
  StartQuote,
  Related,
  Glossary,
  Timeline,
  ReevooQuote,
  ReevooMoments,
  ReevooReviews,
  BlockLinks,
  ProductList,
  Item,
  StickyNav,
  CaseStudyCarousel,
  UnitPrice,
  ComparisonTable,
  ProductAccordion
};

module.exports = Product;
