import ImageBlocks from '../modules/ImageBlocks/ImageBlocks';

import '../sass/pages/partners.scss';

const PartnerComponents = {
  ImageBlocks
};

module.exports = PartnerComponents;
