import Related from '../modules/Related/Related';
import Author from '../modules/Author/Author';
import AnchorNav from '../modules/AnchorNav/AnchorNav';
import FeaturedArticles from '../modules/FeaturedArticles/FeaturedArticles';
import PopularArticles from '../modules/PopularArticles/PopularArticles';
import SubArticles from '../modules/SubArticles/SubArticles';
import AnchorHeading from '../modules/AnchorHeading/AnchorHeading';

import '../sass/pages/article.scss';

const ArticleComponents = {
  Related,
  Author,
  AnchorNav,
  AnchorHeading,
  FeaturedArticles,
  PopularArticles,
  SubArticles
};

module.exports = ArticleComponents;
