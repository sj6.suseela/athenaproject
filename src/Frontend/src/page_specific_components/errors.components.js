import ImportantInformation from "../modules/_Product/ImportantInformation/ImportantInformation";
import BlockLinks from "../modules/_Product/BlockLinks/BlockLinks";
import PrimaryNav from "../modules/PrimaryNav/PrimaryNav";
import Footer from "../modules/Footer/Footer";
import BodyCopy from "../modules/_ContentModules/BodyCopy/BodyCopy";
import HeaderCopy from "../modules/HeaderCopy/HeaderCopy";

const ErrorComponents = {
  BlockLinks,
  BodyCopy,
  Footer,
  HeaderCopy,
  ImportantInformation,
  PrimaryNav
};

module.exports = ErrorComponents;
