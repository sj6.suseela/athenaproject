
import ScrollToY from '../modules/scrolltoy';

(function () {
  if (!SERVER)  {
    const links = [].slice.call(document.querySelectorAll('a[href^="#"]'));

    links.forEach(link => {
      link.addEventListener('click', (e) => {
        e.preventDefault();
        const href = link.getAttribute('href').substring(1);
        const el = document.getElementById(href);
        if(el) {
          const screenWidth = document.body.clientWidth;
          const offset = (screenWidth <= 768) ? el.getAttribute('data-offset-mobile') : el.getAttribute('data-offset-desktop');
          const pos = el.getBoundingClientRect().top + window.pageYOffset - offset;
          new ScrollToY(pos, 500, 'easeInOutQuint');
        }
      });
    });
  }
})();