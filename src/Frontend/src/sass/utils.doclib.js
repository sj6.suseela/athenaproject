// utils.js
const path = require('path');
const resources = [
  '../../../../node_modules/breakpoint-sass/stylesheets/_breakpoint.scss',
  'breakpoints.scss',
  'variables.doclib.scss',
  'mixins.scss'
];
module.exports = resources.map(file => path.resolve(__dirname, file));
