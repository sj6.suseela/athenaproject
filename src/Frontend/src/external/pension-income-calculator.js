window.onload = function () {
  window.addEventListener('message', function(e) {
    const eventName = e.data[0];
    const data = e.data[1];
    const iframe = document.getElementById('lvplannerframe');

    switch(eventName) {
    case 'setLVPlannerHeight':
      iframe.style.height = data+"px";
      break;
    case 'setLVPlannerScroll':
      if (!iframe.classList.contains('isEditing')) {
        if (iframe.classList.contains('isPreview')) {
          const elPosition = iframe.offsetTop;
          const topSpacer = document.getElementById('scCrossPiece');
          let topOffset = 0;
          if (topSpacer) {
            topOffset = topSpacer.offsetHeight;
          }
          else {
            topOffset = 186;
          }
          window.scrollTo({
            left: 0,
            top: elPosition - topOffset,
            behavior: 'smooth'
          });
          break;
        }
        else {
          iframe.scrollIntoView(true);
        }
      }
    }
  }, false);
};
